<?php

$lang['error_text'] = 'Error!';
$lang['error_title_text'] = 'An error occurred';
$lang['error_description_text'] = 'An error occurred while dealing with the payment method, please contact Prosperis Support';
$lang['unknown_text'] = 'Unknown';
$lang['payment_method_text'] = 'Payment Method';
$lang['error_type_text'] = 'Error Description';
$lang['contact_link_text'] = 'Contact Link';
$lang['contact_support_text'] = 'Contact Support';
$lang['thank_you_text'] = 'Thank You';