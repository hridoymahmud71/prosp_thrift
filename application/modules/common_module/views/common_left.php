
<?php
$this->lang->load('common_left');
?>
<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul>
                <li class="text-muted menu-title">Navigation</li>
                <li class="has_sub">
                    <a href="" class="waves-effect"><span class="label label-pill label-primary float-right">1</span><i class="zmdi zmdi-view-dashboard"></i><span><?php echo lang('cl_menu_dashboard_text') ?></span></a>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-envelope-o"></i> <span><?php echo lang('cl_menu_message_text')?></span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="message_module/send_message"><?php echo lang('cl_menu_message_sub_menu_send_message_text') ?></a></li>
                        <li><a href="message_module/outbox"><?php echo lang('cl_menu_message_sub_menu_outbox_text') ?></a></li>
                        <li><a href="message_module/inbox"><?php echo lang('cl_menu_message_sub_menu_inbox_text') ?></a></li>
                    </ul>
                </li>
                <?php if($this->ion_auth->is_admin()){?>
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog"></i> <span><?php echo lang('cl_menu_settings_text')?></span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="settings_module/general_settings"><?php echo lang('cl_menu_settings_sub_menu_general_settings_text') ?></a></li>
                            <li><a href="settings_module/contact_settings"><?php echo lang('cl_menu_settings_sub_menu_contact_settings_text') ?></a></li>
                            <li><a href="settings_module/currency_settings"><?php echo lang('cl_menu_settings_sub_menu_currency_settings_text') ?></a></li>
                            <li><a href="settings_module/datetime_settings"><?php echo lang('cl_menu_settings_sub_menu_datetime_settings_text') ?></a></li>
                            <li><a href="settings_module/prosperisgold_settings"><?php echo lang('cl_menu_settings_sub_menu_system_settings_text') ?></a></li>

                            <li><a href="bank_module/all_bank_info"><?php echo lang('cl_menu_settings_sub_menu_all_bank_text') ?></a></li>
                            <li><a href="bank_module/add_bank_info"><?php echo lang('cl_menu_settings_sub_menu_add_bank_text') ?></a></li>
                            <li><a href="bank_module/all_state_info"><?php echo lang('cl_menu_settings_sub_menu_all_state_text') ?></a></li>
                        </ul>
                    </li>
                <?php }?>
                <?php if($this->ion_auth->is_admin()){?>
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-users"></i> <span><?php echo lang('cl_menu_users_text') ?></span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="users/auth"><?php echo lang('cl_menu_users_sub_menu_users_text')?></a></li>
                            <li><a href="users/auth/create_user"><?php echo lang('cl_menu_users_sub_menu_add_user_text')?></a></li>
                            <!-- <li><a href="users/auth/show_user_groups"><?php echo lang('cl_menu_users_sub_menu_groups_text')?></a></li> -->
                        </ul>
                    </li>
                <?php }?>
                <?php if(!$this->ion_auth->in_group('employee') && !$this->ion_auth->in_group('trustee')){?>
                    <!-- // admin -->
                    <li class="has_sub">
                        <?php if($this->ion_auth->is_admin() ){?>
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o"></i> <span><?php echo lang('cl_menu_employer_dashboard_text') ?></span> <span class="menu-arrow"></span></a>
                        <?php }?>
                        <?php if($this->ion_auth->in_group('employer')){?>
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o"></i> <span><?php echo lang('cl_menu_employer_member_dashboard_text') ?></span> <span class="menu-arrow"></span></a>
                        <?php }?>
                        <ul class="list-unstyled">
                            <?php if($this->ion_auth->is_admin() ){?>
                                <li><a href="employer_module/all_employee_info"><?php echo lang('cl_menu_all_employee_text') ?></a></li>
                            <?php }?>
                            <?php if($this->ion_auth->in_group('employer')){?>
                                <li><a href="employer_module/my_employee_info"><?php echo lang('cl_menu_my_employee_member_text') ?></a></li>
                            <?php }?>
                            <?php if($this->ion_auth->is_admin()){?>
                                <li><a href="employer_module/add_employee_info"><?php echo lang('cl_menu_add_employee_text') ?></a></li>
                            <?php }?>
                            <?php if($this->ion_auth->in_group('employer')){?>
                                <li><a href="employer_module/add_employee_info"><?php echo lang('cl_menu_add_employee_member_text') ?></a></li>
                            <?php }?>

                            <?php if($this->ion_auth->is_admin()){?>
                                <li><a href="employer_module/add_employer_info"><?php echo lang('cl_menu_add_employer_text') ?></a></li>
                                <li><a href="employer_module/all_employer_info"><?php echo lang('cl_menu_all_employer_view_text') ?></a></li>
                            <?php }?>

                        </ul>
                    </li>
                <?php }?>

                <?php if($this->ion_auth->is_admin()){?>
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-product-hunt"></i> <span><?php echo lang('cl_menu_product_text') ?></span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="product_module/all_product_info"><?php echo lang('cl_menu_all_product_text') ?></a></li>
                            <li><a href="product_module/add_product_info"><?php echo lang('cl_menu_add_product_text') ?></a></li>
                        </ul>
                    </li>
                <?php }?>


                <?php if($this->ion_auth->is_admin()){?>
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-superpowers"></i> <span><?php echo lang('cl_menu_thrift_text') ?></span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="thrift_module/show_thrifts/all"><?php echo lang('cl_menu_all_thrift_text') ?></a></li>
                        </ul>
                    </li>
                <?php }?>

                <?php if($this->ion_auth->in_group('employee')){?>
                    <li id="tour-thrift-menu" class="has_sub">ff
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-superpowers"></i> <span><?php echo lang('cl_menu_thrift_text') ?></span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="thrift_module/show_thrifts/my"><?php echo lang('cl_menu_my_thrift_text') ?></a></li>
                        </ul>
                    </li>
                <?php }?>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-list-alt"></i> <span><?php echo lang('cl_menu_report_text') ?></span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <?php if( $this->ion_auth->is_admin() || $this->ion_auth->in_group('trustee') ){?>
                            <li><a href="report_module/all_employer"><?php echo lang('cl_menu_all_employer_text') ?></a></li>
                            <li><a href="report_module/all_employee"><?php echo lang('cl_menu_all_employee_text') ?></a></li>

                            <li><a href="report_module/show_report/all_report/payment_report/all"><?php echo lang('cl_menu_all_payment_report_text') ?></a></li>
                            <li><a href="report_module/show_report/all_report/payment_recieve_report/all"><?php echo lang('cl_menu_payment_recieve_report_text') ?></a></li>

                            <li><a href="report_module/all_report/payment_report/years/all"><?php echo lang('cl_menu_payment_report_by_year_text') ?></a></li>
                            <li><a href="report_module/all_report/payment_report/months/all_year"><?php echo lang('cl_menu_payment_report_by_month_text') ?></a></li>

                            <li><a href="report_module/all_report/payment_recieve_report/years/all"><?php echo lang('cl_menu_payment_recieve_report_by_year_text') ?></a></li>
                            <li><a href="report_module/all_report/payment_recieve_report/months/all_year"><?php echo lang('cl_menu_payment_recieve_report_by_month_text') ?></a></li>

                        <?php } ?>

                        <?php if( $this->ion_auth->in_group('employer') ){?>
                            <li><a href="report_module/show_report/employer_report_as_employer/payment_report/all"><?php echo lang('cl_menu_my_payment_report_text') ?></a></li>
                            <li><a href="report_module/show_report/employer_report_as_employer/payment_recieve_report/all"><?php echo lang('cl_menu_my_payment_recieve_report_text') ?></a></li>

                            <li><a href="report_module/employer_report_as_employer/payment_report/years/all"><?php echo lang('cl_menu_payment_report_by_year_text') ?></a></li>
                            <li><a href="report_module/employer_report_as_employer/payment_report/months/all_year"><?php echo lang('cl_menu_payment_report_by_month_text') ?></a></li>

                            <li><a href="report_module/employer_report_as_employer/payment_recieve_report/years/all"><?php echo lang('cl_menu_payment_recieve_report_by_year_text') ?></a></li>
                            <li><a href="report_module/employer_report_as_employer/payment_recieve_report/months/all_year"><?php echo lang('cl_menu_payment_recieve_report_by_month_text') ?></a></li>
                        <?php } ?>

                        <?php if( $this->ion_auth->in_group('employee') ){?>
                            <li><a href="report_module/show_report/employee_report_as_employee/payment_report/all"><?php echo lang('cl_menu_my_payment_report_text') ?></a></li>
                            <li><a href="report_module/show_report/employee_report_as_employee/payment_recieve_report/all"><?php echo lang('cl_menu_my_payment_recieve_report_text') ?></a></li>

                            <li><a href="report_module/employee_report_as_employee/payment_report/years/all"><?php echo lang('cl_menu_payment_report_by_year_text') ?></a></li>
                            <li><a href="report_module/employee_report_as_employee/payment_report/months/all_year"><?php echo lang('cl_menu_payment_report_by_month_text') ?></a></li>

                            <li><a href="report_module/employee_report_as_employee/payment_recieve_report/years/all"><?php echo lang('cl_menu_payment_recieve_report_by_year_text') ?></a></li>
                            <li><a href="report_module/employee_report_as_employee/payment_recieve_report/months/all_year"><?php echo lang('cl_menu_payment_recieve_report_by_month_text') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>


            </ul>
            <div class="clearfix"></div>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>

</div>
<!-- Left Sidebar End -->