<!--<div class="content-page">-->

<?php if ($this->session->flashdata('thrift_error')) { ?>
    <section class="content mt-0">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="alert-heading"><?php echo lang('unsuccessful_text') ?></h4>

                    <?php if ($this->session->flashdata('thrift_error_exceed_limit_text')) { ?>
                        <p>
                            <?php echo lang('thrift_error_exceed_limit_text') ?>
                        </p>
                    <?php } ?>

                    <?php if ($this->session->flashdata('thrift_error_only_employee_allowed_text')) { ?>
                        <p>
                            <?php echo lang('thrift_error_only_employee_allowed_text') ?>
                        </p>
                    <?php } ?>

                </div>
            </div>
        </div>
    </section>
<?php } ?>

<!-- Start content -->
<!--    <div class="content">-->
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php if ($is_admin == 'admin') echo lang('page_subtitle_admin_dashboard_text') ?>
                    <?php if ($is_trustee == 'trustee') echo lang('page_subtitle_trustee_dashboard_text') ?>
                    <?php if ($is_employer == 'employer') echo lang('page_subtitle_employer_dashboard_text') ?>
                    <?php if ($is_employee == 'employee') echo lang('page_subtitle_employee_dashboard_text') ?>
                </h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href=""><?php echo lang('breadcrum_home_text') ?></a></li>
                    <li class="breadcrumb-item active"><?php echo lang('breadcrum_page_text') ?></li>
                </ol>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- end row -->


    <style>
        .equal-top-cards {
            min-height: 92% !important;
        }
    </style>


    <?php if ($is_employee == 'employee') { ?>
        <hr>
        <div class="row equal">
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card-box tilebox-one equal-top-cards">
                    <i class="fa fa-diamond float-right text-muted"></i>
                    <h6 class="text-muted text-uppercase m-b-20"><?php echo lang('total_disbursment_text'); ?></h6>
                    <h2 class="m-b-20"><?= $currency ?><?= number_format($total_disbursment[0]['total_disburs_amount']); ?></h2>
                    <a class="label label-warning"
                       href="thrift_module/show_thrifts/my"><?php echo lang('see_my_thrifts_text'); ?></a>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card-box tilebox-one equal-top-cards">
                    <i class="fa fa-credit-card float-right text-muted"></i>
                    <h6 class="text-muted text-uppercase m-b-20"><?php echo lang('concluded_thrifts_text'); ?></h6>
                    <h2 class="m-b-20"><?= $concluded_thrift ? $concluded_thrift : 0; ?></h2>

                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card-box tilebox-one equal-top-cards">
                    <i class="fa fa-paper-plane float-right text-muted "></i>
                    <h6 class="text-muted text-uppercase m-b-20"><?php echo lang('active_thrifts_text'); ?></h6>
                    <h2 class="m-b-20"><?= $active_thrift ? $active_thrift : 0; ?></h2>
                    <a class="label label-warning"
                       href="thrift_module/show_thrifts/my"><?php echo lang('see_my_thrifts_text'); ?></a>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card-box tilebox-one equal-top-cards">
                    <i class="fa fa-star float-right text-muted"></i>
                    <h6 class="text-muted text-uppercase m-b-20"><?php echo lang('my_ratings_text'); ?></h6>
                    <h2 class="m-b-20"><?= $user_rating[0]['user_rating']; ?></h2>

                </div>
            </div>
        </div>
        <style>
            .restyle_price {
                font-size: 24px !important;
            }

            .custom-bg-gold {
                color: whitesmoke;
                background-color: goldenrod;
            }

            .custom-col-ht-eql {
                min-height: 96%;
            }
            .t_name{
                padding: 10px 5px 30px 5px !important;
            }
        </style>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div id="tour-join-thrift" class="card-box custom-col-ht-eql">
                    <div class="text-center">
                        <h4 class="header-title m-t-0 m-b-30"><?= lang('join_thrift_group_text') ?></h4>
                        <div class="row">

                            <div class="col-sm-12 col-xs-12 col-md-4 col-lg-4">
                                <div class="price_card text-center ">
                                    <div class="pricing-header custom-bg-gold">
                                        <span class="price restyle_price"><?= lang('custom_product_name_text') ?></span>
                                        <span class="name t_name"><?= lang('custom_product_start_text') ?></span>
                                    </div>
                                    <ul class="price-features">
                                    </ul>
                                    <div class="row m-b-10">
                                        <div class="col-md-12 w-100">
                                            <?php if ($this->ion_auth->in_group('employee')) { ?>
                                                <a class="w-100 btn btn-primary waves-effect waves-light btn-sm"
                                                   href="thrift_module/add_custom_product/"><?= lang('start_thrift_text') ?></a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-xs-12 col-md-4 col-lg-4">
                                <div class="price_card text-center">
                                    <div class="pricing-header custom-bg-gold">
                                        <span class="price restyle_price"><?= lang('individual_product_name_text') ?></span>
                                        <span class="name t_name"><?= lang('individual_product_start_text') ?></span>
                                    </div>
                                    <ul class="price-features">
                                    </ul>
                                    <div class="row m-b-10">
                                        <div class="col-md-12 w-100">
                                            <?php if ($this->ion_auth->in_group('employee')) { ?>
                                                <a class="w-100 btn btn-primary waves-effect waves-light btn-sm"
                                                   href="thrift_module/add_individual_product/"><?= lang('start_thrift_text') ?></a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12 col-md-4  col-lg-4">
                                <div class="price_card text-center">
                                    <div class="pricing-header custom-bg-gold">
                                        <span class="price restyle_price"><?= lang('loan_product_name_text') ?></span>
                                        <span class="name t_name"><?= lang('loan_product_start_text') ?></span>
                                    </div>
                                    <ul class="price-features">
                                    </ul>
                                    <div class="row m-b-10">
                                        <div class="col-md-12 w-100">
                                            <?php if ($this->ion_auth->in_group('employee')) { ?>
                                                <a class="w-100 btn btn-primary waves-effect waves-light btn-sm"
                                                   href="thrift_module/add_loan_product/"><?= lang('start_thrift_text') ?></a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="row equal">
                    <div class="col-xs-12 col-md-6 ">
                        <div id="tour-payments" class="card-box custom-col-ht-eql">
                            <div>
                                <h4 class="header-title m-t-0 m-b-10"><?= lang('next_payment_text') ?></h4>
                                <div>
                                    <?php if ($next_payment_details) { ?>
                                        <div><?= $currency ? $currency : '' ?><?= number_format($next_payment_details[0]->thrift_group_payment_amount, 2, '.', ','); ?></div>
                                        <div><?= $this->custom_datetime_library->convert_and_return_TimestampToDate($next_payment_details[0]->thrift_group_payment_date); ?></div>
                                        <div><?= $next_payment_details[0]->thrift_group_number ?></div>
                                    <?php } else { ?>
                                        <div><?= lang('not_joined_thrift_yet_text') ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <hr style="margin-top: .6rem;margin-bottom: .6rem">
                            <div>
                                <h4 class="header-title m-t-20 m-b-10"><?= lang('next_disbursement_text') ?> </h4>
                                <div>
                                    <?php if ($next_disbursement_details) { ?>
                                        <div><?= $currency ? $currency : '' ?><?= number_format($next_disbursement_details[0]->thrift_group_payment_recieve_amount, 2, '.', ','); ?></div>
                                        <div><?= $this->custom_datetime_library->convert_and_return_TimestampToDate($next_disbursement_details[0]->thrift_group_payment_date); ?></div>
                                        <div> <?= $next_disbursement_details[0]->thrift_group_number ?></div>
                                    <?php } else { ?>
                                        <div><?= lang('not_joined_thrift_yet_text') ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php if ($invitations) { ?>
                                <hr style="margin-top: .6rem;margin-bottom: .6rem">
                                <div>
                                    <h4 class="header-title m-t-0 m-b-0">New Thrift Invitation</h4>
                                    <div class="inbox-widget nicescroll"
                                         style="height: 80px;max-height: 100px;outline:none" tabindex="5000">
                                        <?php foreach ($invitations as $inv) { ?>
                                            <a href="thrift_module/custom_product_thrift/view/<?= $inv->cpi_id ?>">
                                                <div class="inbox-item">
                                                    <!--<div class="inbox-item-img"><img src="assets/images/users/avatar-1.jpg"
                                                                                     class="rounded-circle" alt=""></div>-->
                                                    <p class="inbox-item-author"><?= $inv->inviter_name ?></p>
                                                    <p class="inbox-item-text"><?= $inv->cpi_number ?></p>
                                                    <p class="inbox-item-date" style="top: 20px!important;"><?= $inv->cpi_created_at_datestring ?></p>
                                                </div>
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    </div><!-- end col-->
                    <div class="col-xs-12 col-md-6 ">
                        <div class="card-box custom-col-ht-eql">
                            <h4 class="header-title m-t-0 m-b-30 text-center"><?= lang('recent_thrift_text') ?></h4>
                            <div class="text-center nicescroll" style="height: 250px;" tabindex="5000">
                                <?php if ($thrift_percentage) { ?>
                                    <?php foreach ($thrift_percentage as $row) { ?>
                                        <a href="thrift_module/view_thrift/<?= $row['thrift_grp_id'] ?>">
                                            <div class="progress m-b-20">
                                                <div class="progress-bar <?php if ($row['thrift_percent'] != '100%') {
                                                    echo "progress-bar-striped progress-bar-animated";
                                                } else {
                                                    echo "bg-success";
                                                } ?> text-center" role="progressbar"
                                                     style="width: <?= $row['thrift_percent']; ?>" aria-valuenow="25"
                                                     aria-valuemin="0"
                                                     aria-valuemax="100"><?= $row['thrift_percent'] ?></div>
                                            </div>
                                        </a>
                                    <?php } ?>
                                <?php } ?>

                            </div>
                            <div class="text-center">
                                <?php if (count($thrift_percentage) == 0) {
                                    echo lang('not_joined_thrift_yet_text');
                                } ?>
                                <?php if (count($thrift_percentage) > 0) { ?>
                                    <a class="btn w-100 btn-sm btn-primary"
                                       href="thrift_module/show_thrifts/my"><?= lang('view_all_thrifts_text') ?></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <?php if ($all_product) { ?>
            <div class="col-xs-12 col-lg-12 col-xl-12">
                <div class="card-box">
                    <div class="text-center"><h4
                                class="header-title m-t-0 m-b-30"><?= lang('join_thrift_group_text') ?></h4>
                        <div class="row">

                            <?php foreach ($all_product as $row) { ?>
                                <div class="col-sm-12 col-xs-12 col-md-3 col-lg-3">
                                    <div class="price_card text-center">
                                        <div class="pricing-header custom-bg-gold">
                                            <!--<span class="price"><? /*= $currency . number_format($row['product_price']) */ ?></span>-->
                                            <span class="price"><?= $currency . number_format($row['product_price']) ?></span>
                                            <span class="name"><?= $row['product_name'] ?></span>
                                            <span class="name"><?= $row['product_term_duration'] ?>
                                                - Month Duration</span>
                                        </div>
                                        <ul class="price-features">
                                        </ul>
                                        <div class="row m-b-10">
                                            <div class="col-md-12 w-100">
                                                <?php if ($this->ion_auth->in_group('employee')) { ?>
                                                    <a class="w-100 btn btn-primary waves-effect waves-light btn-sm"
                                                       href="thrift_module/start_thrift/<?= $row['product_id']; ?>"><?= lang('start_thrift_text') ?></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <div class="card-box custom-col-ht-eql">
                    <h4 class="header-title m-t-0 m-b-20"><?= lang('inbox_text') ?>
                        (<?= (int)$count_messages + (int)$count_comments ?>)</h4>
                    <a href="message_module/inbox" type="button"
                       class="btn btn-info btn-rounded waves-effect waves-light"><?= lang('view_all_text') ?></a>

                    <?php if ($messages_and_comments) { ?>
                        <div class="inbox-widget nicescroll"
                             style="height: 250px;
                                 tabindex=" 5000>

                            <?php foreach ($messages_and_comments as $messages_and_comment) { ?>
                                <a href="<?= $messages_and_comment->url ?>">
                                    <div class="inbox-item">
                                        <!--<div class="inbox-item-img"><img src="assets/images/users/avatar-1.jpg"
                                                                         class="rounded-circle" alt=""></div>-->
                                        <p class="inbox-item-author"><?= $messages_and_comment->sender ?></p>
                                        <p class="inbox-item-text"><?= $messages_and_comment->text ?></p>
                                        <p class="inbox-item-date"><?= $messages_and_comment->datestring ?></p>
                                    </div>
                                </a>
                            <?php } ?>
                        </div>
                    <?php } ?>

                    <?php if (!$messages_and_comments || empty($messages_and_comments)) { ?>
                        <p><?= lang('no_unread_messages_text') ?></p>
                    <?php } ?>

                </div>

            </div><!-- end col-->

            <div class="col-xs-12 col-md-3">
                <div class="card-box custom-col-ht-eql">
                    <h4 class="header-title m-t-0 m-b-20"><?= lang('quick_menu_text') ?></h4>
                    <nav class="nav flex-column">
                        <a style="text-decoration: underline;color: #2b2b2b" class="nav-link"
                           href="thrift_module/show_thrifts/my">&#8226 <?= lang('review_my_thrift_text') ?></a>
                        <a style="text-decoration: underline;color: #2b2b2b" class="nav-link"
                           href="thrift_module/add_custom_product">&#8226 <?= lang('invite_colleagues_text') ?></a>
                        <a style="text-decoration: underline;color: #2b2b2b" class="nav-link"
                           href="user_profile_module/edit_user_info/<?= $this->session->userdata('user_id') ?>">&#8226 <?= lang('update_my_profile_text') ?></a>
                        <a style="text-decoration: underline;color: #2b2b2b" class="nav-link"
                           href="report_module/show_report/employee_report_as_employee/payment_recieve_report/all">&#8226 <?= lang('view_my_statements_text') ?></a>
                    </nav>
                </div>
            </div><!-- end col-->
        </div>


    <?php } ?>


</div>


<!-- Chart JS -->
<script src="assets/backend_assets/plugins/chart.js/chart.min.js"></script>

<!--Morris Chart-->
<script src="assets/backend_assets/plugins/morris/morris.min.js"></script>
<script src="assets/backend_assets/plugins/raphael/raphael-min.js"></script>

<script>
    $(document).ready(function () {
        var placementRight = 'right';
        var placementLeft = 'left';

        // Define the tour!
        var tour = {
            id: "my-intro",
            steps: [
                {
                    target: "tour-profile",
                    title: "Profile",
                    content: "Update your banking information and other personal data in your user profile",
                    placement: placementLeft,
                    yOffset: 10
                },
                {
                    target: "tour-join-thrift",
                    title: "Join Thrift",
                    content: "Start a group thrift or a personal thrift here. Set contribution amounts, payment dates. Invite friends to thrift with you.",
                    placement: 'top',
                    arrowOffset: 'center',
                    zindex: 999
                },
                {
                    target: "tour-payments",
                    title: "Payments",
                    content: "Your payment reminders and upcoming deposits will show here. Make sure your banking/payment information is accurate",
                    placement: 'top',
                    arrowOffset: 'center',
                    zindex: 999
                },
                {
                    target: "tour-thrift-menu",
                    title: "Thrift menu",
                    content: "Access thrifts, invitations and other information about your thrifts and payments here.",
                    placement: 'bottom',
                    yOffset: 10
                },
                {
                    target: "noti",
                    title: "Notification",
                    content: "New notifications will appear here. Watch it closely so you do not miss any communications or invitations.",
                    placement: placementLeft,
                    yOffset: 10
                },
            ],
            onEnd: function () {
                tourComplete();
            },
            onClose: function () {
                tourComplete();
            },
            showPrevButton: true
        };

        $("#tour-btn").on("click",function (e) {
            e.preventDefault();
            hopscotch.startTour(tour);
        });

        function tourComplete() {
            $.post( "common_module/tour_completed" );
        }

        $.ajax({
            url: '<?php echo base_url() . 'common_module/check_tour_completed'?>',
            type: 'POST',
            success: function (tour_completed) {

                if(tour_completed == 0){
                    // Start the tour!
                    hopscotch.startTour(tour);
                }
            }
        });
    });
</script>



