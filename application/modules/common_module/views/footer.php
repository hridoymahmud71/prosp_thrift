
<style>
    .footer {
        border-top: 1px solid rgba(67, 89, 102, 0.1);
        bottom: 0px;
        color: #58666e;
        text-align: center!important;
        padding: 20px 0px;
        position: absolute;
        right: 0px;
        left: 0px;
    }
</style>

<footer class="footer">
    ©<?=date('Y');?>. Prosperis & Associates, LLC, USA. All rights reserved. Powered by <a target="_blank" href="https://www.obsvirtual.com/">OBS</a>
</footer>


</div>
<!-- END wrapper -->


<script>
    var resizefunc = [];
</script>

<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
</script>


<script src="assets/backend_assets/js/popper.min.js"></script><!-- Tether for Bootstrap -->
<script src="assets/backend_assets/js/bootstrap.min.js"></script>
<script src="assets/backend_assets/js/detect.js"></script>
<script src="assets/backend_assets/js/fastclick.js"></script>
<script src="assets/backend_assets/js/jquery.blockUI.js"></script>
<script src="assets/backend_assets/js/waves.js"></script>
<script src="assets/backend_assets/js/jquery.nicescroll.js"></script>
<script src="assets/backend_assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/backend_assets/js/jquery.slimscroll.js"></script>
<script src="assets/backend_assets/plugins/switchery/switchery.min.js"></script>

<!-- Tour page js -->
<script src="assets/backend_assets/plugins/hopscotch/js/hopscotch.min.js"></script>

<!--dropify-->
<script src="assets/backend_assets/plugins/fileuploads/js/dropify.min.js"></script>

<!-- Counter Up  -->
<script src="assets/backend_assets/plugins/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="assets/backend_assets/plugins/counterup/jquery.counterup.min.js"></script>

<!-- Modal-Effect -->
<script src="assets/backend_assets/plugins/custombox/js/custombox.min.js"></script>
<script src="assets/backend_assets/plugins/custombox/js/legacy.min.js"></script>

<!-- Required datatable js -->
<script src="assets/backend_assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/backend_assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="assets/backend_assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/backend_assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/backend_assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/backend_assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/backend_assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/backend_assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/backend_assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/backend_assets/plugins/datatables/buttons.colVis.min.js"></script>
<!-- Responsive examples -->
<script src="assets/backend_assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/backend_assets/plugins/datatables/responsive.bootstrap4.min.js"></script>

<!--swal-->
<script src="assets/backend_assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>

<!--jquery-ui-->
<!--<script src="assets/backend_assets/plugins/jquery-ui/jquery-ui.min.js"></script>-->

<!--datepicker-->
<script src="assets/backend_assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

<!--daterangepicker and moment js-->
<script src="assets/backend_assets/plugins/moment/moment.js"></script>
<script src="assets/backend_assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<!--timepicker js-->
<script src="assets/backend_assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>

<!--colorpicker-->
<script src="assets/backend_assets/plugins/mjolnic-bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>

<!-- App js -->
<script src="assets/backend_assets/js/jquery.core.js"></script>
<script src="assets/backend_assets/js/jquery.app.js"></script>


<!-- Validation js (Parsleyjs) -->
<script type="text/javascript" src="assets/backend_assets/plugins/parsleyjs/parsley.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('form').parsley();
    });
</script>


</body>
</html>