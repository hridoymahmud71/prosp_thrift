<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['employer_module/generate_banks_with_ids_pdf'] = 'EmployerController/generateBanksWithIdsPdf';

$route['employer_module/my_employee_info'] = 'EmployerController/my_employee_info';
$route['employer_module/all_employee_info'] = 'EmployerController/all_employee_info';

$route['employer_module/my_unapproved_employee_info'] = 'EmployerController/my_unapproved_employee_info';
$route['employer_module/all_unapproved_employee_info'] = 'EmployerController/all_unapproved_employee_info';

/*$route['employer_module/employers_employee_info/(:any)'] = 'EmployerController/employers_employee_info';*/
$route['employer_module/get_employee_by_ajax'] = 'EmployerController/getEmployeeByAjax';

$route['employer_module/get_unapproved_employee_by_ajax'] = 'EmployerController/getUnapprovedEmployeeByAjax';

$route['employer_module/view_employee_detail/(:any)'] = 'EmployerController/view_employee_detail';

$route['employer_module/add_employee_info'] = 'EmployerController/add_employee_info';
$route['employer_module/insert_employee_info'] = 'EmployerController/insert_employee_info';
$route['employer_module/insert_single_employee'] = 'EmployerController/insert_single_employee';

$route['employer_module/edit_employee_info/(:any)'] = 'EmployerController/edit_employee_info';
$route['employer_module/update_employee_info'] = 'EmployerController/update_employee_info';

$route['employer_module/get_employer_list'] = 'EmployerController/get_employer_list';

$route['employer_module/delete_employee/(:any)'] = 'EmployerController/delete_employee';

$route['employer_module/all_employer_info'] = 'EmployerController/all_employer_info';
$route['employer_module/get_employer_by_ajax'] = 'EmployerController/getEmployerByAjax';
$route['employer_module/add_employer_info'] = 'EmployerController/add_employer_info';
$route['employer_module/insert_employer_info'] = 'EmployerController/insert_employer_info';
$route['employer_module/edit_employer_info/(:any)'] = 'EmployerController/edit_employer_info';
$route['employer_module/update_employer_info'] = 'EmployerController/update_employer_info';


//first any = user id , second any is where to redirect
$route['employer_module/send_password_via_mail/(:any)/(:any)'] = 'EmployerController/sendPasswordViaMail';
$route['employer_module/send_password_via_mail/(:any)'] = 'EmployerController/sendPasswordViaMail';

$route['employer_module/get_countries_by_select2'] = 'EmployerController/getCountriesBySelect2';
$route['employer_module/get_states_by_select2'] = 'EmployerController/getStatesBySelect2';
$route['employer_module/get_cities_by_select2'] = 'EmployerController/getCitiesBySelect2';

$route['employer_module/approve_user/(:any)'] = 'EmployerController/approveUser';
$route['employer_module/disapprove_user/(:any)'] = 'EmployerController/disapproveUser';

$route['employer_module/activate_thrifter/(:any)'] = 'EmployerController/activateThrifter';
$route['employer_module/deactivate_thrifter/(:any)'] = 'EmployerController/deactivateThrifter';


/*-----------contact person-----------------------------*/
$route['employer_module/add_organization_contact_person'] = 'EmployerController/addOrganizationContactPerson';
$route['employer_module/create_organization_contact_person'] = 'EmployerController/createOrganizationContactPerson';

$route['employer_module/edit_organization_contact_person/(:any)'] = 'EmployerController/editOrganizationContactPerson';
$route['employer_module/update_organization_contact_person'] = 'EmployerController/updateOrganizationContactPerson';

$route['employer_module/activate_organization_contact_person/(:any)'] = 'EmployerController/activateOrganizationContactPerson';
$route['employer_module/deactivate_organization_contact_person/(:any)'] = 'EmployerController/deactivateOrganizationContactPerson';

$route['employer_module/contact_person_list/my'] = 'EmployerController/getContactList';
$route['employer_module/get_contact_list_by_ajax'] = 'EmployerController/getContactListByAjax';

$route['employer_module/activate_contact/(:any)'] = 'EmployerController/activateContact';
$route['employer_module/deactivate_contact/(:any)'] = 'EmployerController/deactivateContact';
$route['employer_module/delete_contact/(:any)'] = 'EmployerController/deleteContact';