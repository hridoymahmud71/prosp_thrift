<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmployerController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('');
        }

        $this->load->library('email');

        //$this->load->model('Contact_model');
        $this->load->model('user/Ion_auth_model');
        $this->load->model('Employer_model');

        // application/settings_module/library/custom_settings_library*
        $this->load->library('settings_module/custom_settings_library');
        $this->load->library('custom_datetime_library');
        $this->load->library('custom_log_library');
        $this->load->library('MPDF/mpdf');

        //this is for writing
        $this->load->library('excel');

        //this is for reading
        require_once(FCPATH . 'external_libraries/excel_reader.php');
        $this->load->model('employer_module/Employer_model');

        /*$eee = $this->custom_datetime_library
            ->convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat('1921-01-02','Y-m-d');
        echo $eee;die();*/
    }

    public function get_employee_info()
    {
        $user_id = false;
        $which_list = $this->uri->segment(3);
        if ($this->uri->segment(4)) {
            $user_id = $this->uri->segment(4);
        }
        $this->employee_info();
    }


    public function my_employee_info()
    {
        if (!$this->ion_auth->in_group('employer')) {
            redirect('users/auth/does_not_exist');
        }

        $this->lang->load('employee_list');

        // $data['employers_id'] = $this->session->userdata('user_id');
        $data['which_list'] = 'my_employee_info';
        $data['which_employees'] = 'my';

        $data['all_info'] = $this->Employer_model->get_employer_info();

        $this->load->view("common_module/header");

        $this->load->view("employee_list", $data);
        $this->load->view("common_module/footer");
    }

    public function all_employee_info()
    {
        if (!($this->ion_auth->is_admin() || $this->ion_auth->in_group('employer'))) {
            redirect('users/auth/need_permission');
        }

        $this->lang->load('employee_list');

        $data['all_info'] = $this->Employer_model->get_employer_info();
        $data['which_list'] = 'all_employee_info';
        $data['which_employees'] = 'all';

        $this->load->view("common_module/header");

        $this->load->view("employee_list", $data);
        $this->load->view("common_module/footer");
    }

    /*public function employers_employee_info()
    {
        if (!($this->ion_auth->is_admin() || $this->ion_auth->in_group('employer'))) {
            redirect('users/auth/need_permission');
        }

        $this->lang->load('employee_list');

        $data['employers_id'] = $this->uri->segment(3);
        $data['which_list'] = 'employers_employee_info';

        $data['all_info'] = $this->Employer_model->get_employer_info();

        $this->load->view("common_module/header");

        $this->load->view("employee_list", $data);
        $this->load->view("common_module/footer");
    }*/


    public function getEmployeeByAjax()
    {
        $this->lang->load('employee_list');

        $users = array();

        $which_employees = $_REQUEST['which_employees'];
        $which_list = $_REQUEST['which_list'];

        $employer_id = false;

        if ($which_list == 'my_employee_info' && $which_employees == 'my') {
            $employer_id = $this->session->userdata('user_id');
        }

        $requestData = $_REQUEST;
        //print_r($requestData);

        $columns[0] = 'created_on';
        $columns[1] = 'mem_id_num';
        $columns[2] = 'first_name';
        $columns[3] = 'last_name';
        $columns[4] = 'email';
        $columns[5] = 'user_employer_id';
        $columns[6] = 'active';
        $columns[7] = 'actions';

        $common_filter_value = false;
        $order_column = false;

        $specific_filters = array();
        $specific_filters = false;

        if (!empty($requestData['columns'][1]['search']['value'])) {
            $specific_filters['mem_id_num'] = $requestData['columns'][1]['search']['value'];
        }

        if (!empty($requestData['columns'][2]['search']['value'])) {
            $specific_filters['first_name'] = $requestData['columns'][2]['search']['value'];
        }

        if (!empty($requestData['columns'][3]['search']['value'])) {
            $specific_filters['last_name'] = $requestData['columns'][3]['search']['value'];
        }

        if (!empty($requestData['columns'][4]['search']['value'])) {
            $specific_filters['email'] = $requestData['columns'][4]['search']['value'];
        }

        if (!empty($requestData['columns'][5]['search']['value'])) {
            $specific_filters['user_employer_id'] = $requestData['columns'][5]['search']['value'];
        }

        if (!empty($requestData['columns'][6]['search']['value'])) {
            $specific_filters['active'] = $requestData['columns'][6]['search']['value'];
        }


        if (!empty($requestData['search']['value'])) {
            $common_filter_value = $requestData['search']['value'];
        }

        if ($specific_filters == true || !empty($specific_filters)) {
            $common_filter_value = false;       //either search with specific filters or with common filter
        }

        $order['column'] = $columns[$requestData['order'][0]['column']];
        $order['by'] = $requestData['order'][0]['dir'];


        $limit['start'] = $requestData['start'];
        $limit['length'] = $requestData['length'];

        $totalData = $this->Employer_model->countEmployees(false, false, $employer_id);

        if ($common_filter_value == true || $specific_filters == true) {

            $totalFiltered = $this->Employer_model->countEmployees($common_filter_value, $specific_filters, $employer_id);
        } else {
            $totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
        }


        $users = $this->Employer_model->getEmployees($common_filter_value, $specific_filters, $order, $limit, $employer_id);

        if ($users == false || empty($users) || $users == null) {
            $users = false;
        }

        $last_query = $this->db->last_query();

        $this->load->library('custom_datetime_library');


        if ($users) {
            $i = 0;
            foreach ($users as $a_user) {

                /*date time starts*/
                $users[$i]->cr_on = new stdClass();

                $users[$i]->cr_on->timestamp = $a_user->created_on;

                if ($a_user->created_on == 0 || $a_user->created_on == false || $a_user->created_on == null) {
                    $users[$i]->cr_on->display = $this->lang->line('unavailable_text');
                } else {
                    $users[$i]->cr_on->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDateAndTime($a_user->created_on);
                }
                /*date time ends*/


                /*org starts*/
                $users[$i]->org = "<span style='font-style: italic; !important'>Non-organizational thrifter</span>";

                if ($a_user->user_employer_id != null && $a_user->user_employer_id > 0) {
                    $employer = $this->Employer_model->getUser($a_user->user_employer_id);

                    $users[$i]->org = $employer->company;
                }
                /*org ends*/

                /*active - inactive starts*/
                $users[$i]->act = new stdClass();
                $users[$i]->act->int = $a_user->active;

                if ($a_user->active == 1) {

                    $status_span = '<span class = "label label-primary">' . $this->lang->line('status_active_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_deactivate_text');
                    $status_url = base_url() . 'employer_module/deactivate_thrifter/' . $a_user->id;
                    $status_anchor_span = '<span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';

                } else {
                    $status_span = '<span class = "label label-default">' . $this->lang->line('status_inactive_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_activate_text');
                    $status_url = base_url() . 'employer_module/activate_thrifter/' . $a_user->id;
                    $status_anchor_span = '<span class="label label-success"><i class="fa fa-check" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';
                }

                $users[$i]->act->html = $status_span . '&nbsp;' . $status_anchor;


                /*active - inactive ends*/

                /*action starts*/
                $view_tooltip = $this->lang->line('tooltip_view_text');
                $view_url = base_url() . 'user_profile_module/user_profile_overview/' . $a_user->id;
                $view_anchor =
                    '<a ' . ' title="' . $view_tooltip . '" ' . ' href="' . $view_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-eye fa-lg" aria-hidden="true"></i>'
                    . '</a>';


                $edit_tooltip = $this->lang->line('tooltip_edit_text');
                $edit_url = base_url() . 'employer_module/edit_employee_info/' . $a_user->id;
                $edit_anchor = '<a ' . ' title="' . $edit_tooltip . '" ' . ' href="' . $edit_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'
                    . '</a>';


                $password_tooltip = $this->lang->line('tooltip_send_password_text');
                $password_url = base_url() . 'employer_module/send_password_via_mail/' . $a_user->id . '/' . $which_list;
                $password_anchor = '<a ' . ' title="' . $password_tooltip . '" ' . ' href="' . $password_url . '" ' . ' class="password_change_confirmation" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-key fa-lg" aria-hidden="true">'
                    . '</a>';

                if (!$this->ion_auth->is_admin()) {
                    $password_anchor = '';
                }

                $users[$i]->action = $view_anchor . '&nbsp;&nbsp;' . $edit_anchor . '&nbsp;&nbsp;' . '&nbsp;&nbsp;' . $password_anchor;
                /*action ends*/


                $i++;

            }
        }


        $json_data['draw'] = intval($requestData['draw']);
        /* $totalData: for every request/draw by clientside ,
         they send a number as a parameter, when they recieve a response/data they first check the draw number,
         so we are sending same number in draw.*/
        $json_data['recordsTotal'] = intval($totalData); // total number of records after searching, if there is no searching then totalFiltered = totalData
        $json_data['recordsFiltered'] = intval($totalFiltered);

        //$users = $this->removeKeys($users); // converting to numeric indices.
        $json_data['data'] = $users;

        // checking requests in console.log() for testing starts;
        $json_data['last_query'] = $last_query;
        $json_data['common_filter_value'] = $common_filter_value;
        $json_data['specific_filters'] = $specific_filters;
        $json_data['order_column'] = $order['column'];
        $json_data['order_by'] = $order['by'];
        $json_data['limit_length'] = $limit['length'];
        $json_data['limit_start'] = $limit['start'];
        $json_data['which_list'] = $which_list;
        // checking requests in console.log() for testing ends;

        echo(json_encode($json_data));

    }


    public function my_unapproved_employee_info()
    {
        if (!$this->ion_auth->in_group('employer')) {
            redirect('users/auth/does_not_exist');
        }

        $this->lang->load('unapproved_employee_list');

        // $data['employers_id'] = $this->session->userdata('user_id');
        $data['which_list'] = 'my_employee_info';
        $data['employers_id'] = false;

        $data['all_info'] = $this->Employer_model->get_employer_info();

        $this->load->view("common_module/header");

        $this->load->view("unapproved_employee_list", $data);
        $this->load->view("common_module/footer");
    }

    public function all_unapproved_employee_info()
    {
        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        }

        $this->lang->load('unapproved_employee_list');

        $data['all_info'] = $this->Employer_model->get_employer_info();

        $data['employers_id'] = false;
        $data['which_list'] = 'all_employee_info';

        $this->load->view("common_module/header");

        $this->load->view("unapproved_employee_list", $data);
        $this->load->view("common_module/footer");
    }

    public function getUnapprovedEmployeeByAjax()
    {
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $email = $this->input->post('email');
        if ($this->ion_auth->in_group('employer')) {
            $employer = $this->session->userdata('user_id');
        } elseif ($this->ion_auth->is_admin() || $this->ion_auth->in_group('trustee')) {
            $employer = $this->input->post('employer');
        }
        $status_chk = $this->input->post('status_chk');
        $get_search_data = $this->Employer_model->get_unapproved_selected_search_data($fname, $lname, $email, $employer, $status_chk);
        foreach ($get_search_data as $key => $row) {
            $emp_name = $this->Employer_model->get_employer_name($row['user_employer_id']);
            $get_search_data[$key]['employer_name'] = $emp_name[0]['first_name'];
            $get_search_data[$key]['company'] = $emp_name[0]['company'];
            if ($get_search_data[$key]['company'] == '' || $get_search_data[$key]['company'] == null) {
                $get_search_data[$key]['company'] = "<span style='font-style: italic; !important'>Non-organizational thrifter</span>";
            }
            $get_search_data[$key]['employer_name'] = $emp_name[0]['first_name'] . ' ' . $emp_name[0]['last_name'];

        }
        print_r(json_encode($get_search_data));
    }

    public function add_employee_info()
    {
        $this->lang->load('employee_add_excel_form');

        $data['all_info'] = $this->Employer_model->get_employer_info();

        $data['banks'] = $this->Employer_model->get_all_bank_list($only_undeleted = true);

        $this->load->view("common_module/header");

        $this->load->view('add_employee_excel_form_page', $data);
        $this->load->view("common_module/footer");
    }

    public function generateBanksWithIdsPdf()
    {
        $this->lang->load('generate_banks_with_ids_pdf');

        $data['banks'] = $this->Employer_model->get_all_bank_list($only_undeleted = true);

        $data['site_name'] = 'Prosperis';

        $mpdf = new mPDF('win-1252', 'A4', '', '', 5, 5, 16, 60, 5, 5, 'C');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('BanksWithIds');
        $mpdf->SetAuthor($data['site_name']);
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->debug = true;


        /*do not load header,common left footer as this view contains the whole page*/
        $html = $this->load->view('employer_module/generate_banks_with_ids_pdf_page', $data, TRUE);

        //echo $html;die();

        $name = 'BanksWithIds' . '.pdf';

        //echo  $link;die();
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'D');

        exit;
    }

    function un_num($fname, $lname, $user_type)
    {
        if ($fname == '') {
            $fname = chr(97 + mt_rand(0, 25));
        }
        if ($lname == '') {
            $lname = chr(97 + mt_rand(0, 25));
        }

        if ($user_type == 'thrifter') {
            $un_num_is = 'T' . rand(10000, 99999) . ucfirst($fname) . ucfirst($lname);
            $get_thrift_id = $this->Employer_model->chk_thrift_unique_id($un_num_is);
        } elseif ($user_type == 'organizer') {
            $un_num_is = 'O' . rand(10000, 99999) . ucfirst($fname) . ucfirst($lname);
            $get_thrift_id = $this->Employer_model->chk_thrift_unique_id($un_num_is);
        } elseif ($user_type == 'organization_contact') {
            $un_num_is = 'OC' . rand(10000, 99999) . ucfirst($fname) . ucfirst($lname);
            $get_thrift_id = $this->Employer_model->chk_thrift_unique_id($un_num_is);
        }

        if ($get_thrift_id != 0) {
            $x = $this->un_num($fname, $lname, $user_type);
        } else {
            return $un_num_is;
        }
        return $x;
    }

    public function insert_employee_info()
    {
        if ($this->ion_auth->in_group('employer')) {
            $user_employer_id = $this->session->userdata('user_id');
        } else {
            $user_employer_id = $this->input->post('user_employer_id');
            $this->form_validation->set_rules('user_employer_id', 'Employer name', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('employer_id_err', 'employer Name required');
                redirect('employer_module/add_employee_info');
            }
        }

        $err_row = 0;
        $uploaded_row = 0;
        $err_row_numbers = array();
        if (isset($_FILES['excel_file'])) {
            $excel = new PhpExcelReader;
            $excel->read($_FILES['excel_file']['tmp_name']);


            $all_data = $excel->sheets[0]['cells'];

            // Test to see the excel data stored in $sheets property
            /*echo '<pre>';
            print_r($all_data);
            echo '</pre>';die();*/


            for ($i = 2; $i <= count($all_data); $i++) {
                if (!isset($all_data[$i][1])) {
                    $all_data[$i][1] = '';
                }
                if (!isset($all_data[$i][2])) {
                    $all_data[$i][2] = '';
                }
                if (!isset($all_data[$i][3])) {
                    $all_data[$i][3] = '';
                }
                if (!isset($all_data[$i][4])) {
                    $all_data[$i][4] = '';
                }
                if (!isset($all_data[$i][5])) {
                    $all_data[$i][5] = '';
                }
                if (!isset($all_data[$i][6])) {
                    $all_data[$i][6] = '';
                }
                if (!isset($all_data[$i][7])) {
                    $all_data[$i][7] = '';
                }
                if (!isset($all_data[$i][8])) {
                    $all_data[$i][8] = '';
                }
                if (!isset($all_data[$i][9])) {
                    $all_data[$i][9] = '';
                }
                if (!isset($all_data[$i][10])) {
                    $all_data[$i][10] = '';
                }

                $data2['user_ofc_id'] = trim($all_data[$i][1]);

                $data1['first_name'] = trim($all_data[$i][2]);
                $data1['last_name'] = trim($all_data[$i][3]);

                $data1['first_name'] = ucwords( $data1['first_name']," \t\r\n\f\v-");
                $data1['last_name']= ucwords( $data1['last_name']," \t\r\n\f\v-");

                //$data2['user_home_address'] = $all_data[$i][4]; //we do not have a user home address

                $data1['phone'] = trim($all_data[$i][4]);
                $data1['email'] = trim($all_data[$i][5]);

                $pw = $this->alphaNum(true, 8, false, false);
                $data1['password'] = $this->Ion_auth_model->hash_password($pw);
                $data1['username'] = $data1['email'];
                $data1['active'] = 1;
                $company_name = $this->Employer_model->get_employer_org_name($user_employer_id);
                $data1['company'] = trim($company_name[0]['company']);
                $data1['created_on'] = $this->custom_datetime_library->getCurrentTimestamp();

                $user_type = 'thrifter';
                $data1['mem_id_num'] = $this->un_num($data1['first_name'][0], $data1['last_name'][0], $user_type);

                $data2['user_gender'] = trim($all_data[$i][6]);


                if ($this->date_preg_match(trim($all_data[$i][7]))) {
                    $data2['user_dob'] =
                        $this->custom_datetime_library
                            ->convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat(trim($all_data[$i][7]), 'Y-m-d');
                } else {
                    $data2['user_dob'] = 0;
                }

                if ($this->date_preg_match(trim($all_data[$i][8]))) {
                    $data2['user_hire_date'] =
                        $this->custom_datetime_library
                            ->convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat(trim($all_data[$i][8]), 'Y-m-d');
                } else {
                    $data2['user_hire_date'] = 0;
                }


                if (is_numeric(trim($all_data[$i][9]))) {
                    $data2['user_salary'] = trim($all_data[$i][9]);
                } else {
                    $data2['user_salary'] = 0.00;
                }

                $data2['user_bvn'] = trim($all_data[$i][10]);
                $data2['user_employer_id'] = $user_employer_id;


                if ($data1['email'] != '') {
                    $check_mail_num = $this->Employer_model->check_email_id($data1['email']);
                    if ($check_mail_num == 0) {
                        if ($this->email_preg_match($data1['email'])) {

                            /*echo '<pre>';
                            print_r($data1);
                            print_r($data2);
                            echo $this->custom_datetime_library
                                ->convert_and_return_TimestampToDateTimeGivenFormat($data2['user_dob'], 'Y-m-d');
                            echo '</pre>';
                            die();*/

                            $uploaded_row = $uploaded_row + 1;

                            $data2['user_id'] = $this->Employer_model->insert_employee_credential($data1);

                            $this->Employer_model->insert_employee_optional_credential($data2);

                            $grp_mem['user_id'] = $data2['user_id'];
                            $grp_mem['group_id'] = '2';
                            $this->Employer_model->insert_as_group_member($grp_mem);

                            $grp_employee['user_id'] = $data2['user_id'];
                            $grp_employee['group_id'] = '7';
                            $this->Employer_model->insert_as_employee($grp_employee);

                            /*creating log starts*/

                            $activity_by = '';

                            if ($this->ion_auth->is_admin()) {
                                $activity_by = 'admin';
                            } else {
                                $activity_by = 'employer';
                            }

                            $this->custom_log_library->createALog
                            (
                                $this->session->userdata('user_id'),                                    //1.    $created_by
                                '',                                                                     //2.    $created_for
                                'employee',                                                             //3.    $type
                                $data2['user_id'],                                                      //4.    $type_id
                                'created',                                                              //5.    $activity
                                $activity_by,                                                           //6.    $activity_by
                                '',                                                                     //7.    $activity_for
                                '',                                                                     //8.    $sub_type
                                '',                                                                     //9.    $sub_type_id
                                'employer',                                                             //10.   $super_type
                                $data2['user_employer_id'],                                             //11.   $super_type_id
                                '',                                                                     //12.   $other_information
                                ''                                                                      //13.   $change_list
                            );
                            /*creating log ends*/

                            //$this->snd_pw_v_mail($data2['user_id'], $pw);
                            $this->snd_pw_link_v_mail($data2['user_id']);
                        } else {
                            $err_row = $err_row + 1;
                            $err_row_numbers[] = array('row' => $i - 1, 'error' => 'Invalid Email Address');
                        }

                    } else {
                        $err_row = $err_row + 1;
                        $err_row_numbers[] = array('row' => $i - 1, 'error' => 'Duplicate E-mail');
                    }
                } else {
                    $err_row = $err_row + 1;
                    $err_row_numbers[] = array('row' => $i - 1, 'error' => 'E-mail Address Missing');
                }

            }
        }
        if ($uploaded_row > 0) {
            $this->session->set_flashdata('upload_message', $uploaded_row);
        }
        if ($err_row > 0) {
            $this->session->set_flashdata('employee_insert_with_err', $err_row);
            $this->session->set_flashdata('employee_row_err_no', $err_row_numbers);
        }

        redirect('employer_module/add_employee_info');
    }

    private function date_preg_match($datestring)
    {
        $ret = false;

        if (preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}\z/', $datestring)) {
            $ret = true;
        }

        return $ret;

    }

    private function email_preg_match($email)
    {
        $ret = false;

        if (preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i', $email)) {
            $ret = true;
        }

        return $ret;
    }

    public function insert_single_employee()
    {

        if ($this->ion_auth->in_group('employer')) {
            $user_employer_id = $this->session->userdata('user_id');
        } else {
            $user_employer_id = $this->input->post('user_employer_id');
            $this->form_validation->set_rules('user_employer_id', 'Employer name', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('employer_detail_err', 'employer Name required');
                redirect('employer_module/add_employee_info');
            }

        }

        if (!$this->input->post('user_bank')) {
            $this->session->set_flashdata('bank_detail_err', 'Bank required');
            redirect('employer_module/add_employee_info');
        }

        $this->form_validation->set_rules('first_name', 'First name', 'required');
        $this->form_validation->set_rules('phone', 'Phone number', 'required');
        $this->form_validation->set_rules('email', 'Email ID', 'required');
        $this->form_validation->set_rules('user_ofc_id', 'Office ID', 'required');
        $this->form_validation->set_rules('user_salary', 'Salary', 'required');
        $this->form_validation->set_rules('user_gender', 'Gender', 'required');
        $this->form_validation->set_rules('user_dob', 'Age', 'required');
        $this->form_validation->set_rules('user_hire_date', 'Appointed', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('employer_detail_err', validation_errors());
            redirect('employer_module/add_employee_info');
        }

        $user_dob = $this->input->post('user_dob');
        $dob_date = DateTime::createFromFormat('Y-m-d', $user_dob);

        $user_hire_date = $this->input->post('user_hire_date');
        $hire_date = DateTime::createFromFormat('Y-m-d', $user_hire_date);
        $data['company'] = $this->Employer_model->get_employer_org_name($user_employer_id);

        $data1['first_name'] = trim($this->input->post('first_name'));
        $data1['last_name'] = trim($this->input->post('last_name'));

        $data1['first_name'] = ucwords( $data1['first_name']," \t\r\n\f\v-");
        $data1['last_name']= ucwords( $data1['last_name']," \t\r\n\f\v-");

        $data1['phone'] = $this->input->post('phone');
        $data1['email'] = $this->input->post('email');
        $data1['username'] = $this->input->post('email');

        $pw = $this->alphaNum(true, 8, false, false);
        $data1['password'] = $this->Ion_auth_model->hash_password($pw);
        $data1['active'] = 1;
        $company_name = $this->Employer_model->get_employer_org_name($user_employer_id);


        $data1['company'] = $company_name[0]['company'];
        $data1['created_on'] = $this->custom_datetime_library->getCurrentTimestamp();

        $user_type = 'thrifter';
        $data1['mem_id_num'] = $this->un_num($data1['first_name'][0], $data1['last_name'][0], $user_type);

        $data2['user_ofc_id'] = $this->input->post('user_ofc_id');
        $data2['user_salary'] = $this->input->post('user_salary');

        $data2['user_gender'] = $this->input->post('user_gender');
        $data2['user_dob'] = $dob_date->getTimestamp();
        $data2['user_hire_date'] = $hire_date->getTimestamp();
        $data2['user_bvn'] = $this->input->post('user_bvn');
        $data2['user_bank'] = $this->input->post('user_bank');
        $data2['user_bank_account_no'] = $this->input->post('user_bank_account_no');
        $data2['user_employer_id'] = $user_employer_id;

        $data2['user_street_1'] = trim($this->input->post('user_street_1'));
        $data2['user_street_2'] = trim($this->input->post('user_street_2'));

        $data2['user_country'] = $this->input->post('user_country');
        $data2['user_state'] = $this->input->post('user_state');
        $data2['user_city'] = $this->input->post('user_city');


        $check_mail_num = $this->Employer_model->check_email_id($data1['email']);
        if ($check_mail_num == 0) {
            $data2['user_id'] = $this->Employer_model->insert_employee_credential($data1);
            $this->Employer_model->insert_employee_optional_credential($data2);

            $grp_mem['user_id'] = $data2['user_id'];
            $grp_mem['group_id'] = '2';
            $this->Employer_model->insert_as_group_member($grp_mem);

            $grp_employee['user_id'] = $data2['user_id'];
            $grp_employee['group_id'] = '7';
            $this->Employer_model->insert_as_employee($grp_employee);

            /*creating log starts*/

            $activity_by = '';

            if ($this->ion_auth->is_admin()) {
                $activity_by = 'admin';
            } else {
                $activity_by = 'employer';
            }

            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                '',                                                                     //2.    $created_for
                'employee',                                                             //3.    $type
                $data2['user_id'],                                                      //4.    $type_id
                'created',                                                              //5.    $activity
                $activity_by,                                                           //6.    $activity_by
                '',                                                                     //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                'employer',                                                             //10.   $super_type
                $data2['user_employer_id'],                                             //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );
            /*creating log ends*/

            //$this->snd_pw_v_mail($data2['user_id'], $pw);
            $this->snd_pw_link_v_mail($data2['user_id']);

            $this->session->set_flashdata('employee_insert', 'Employee successfully uploaded');
        } else {
            $this->session->set_flashdata('employee_insert_err_text', 'Email id exists');
        }

        if ($this->ion_auth->in_group('employer')) {
            redirect('employer_module/my_employee_info');
        } else if ($this->ion_auth->is_admin()) {
            redirect('employer_module/all_employee_info');
        } else {
            redirect('/');
        }

    }


    public function edit_employee_info()
    {
        $this->lang->load('employee_edit_input_form');
        $user_id = $this->uri->segment('3');
        $data['user_info'] = $this->Employer_model->get_employee_info($user_id);

        $data['banks'] = $this->Employer_model->get_all_bank_list();

        $data['user_info'][0]->user_dob = date('Y-m-d', $data['user_info'][0]->user_dob);
        $data['user_info'][0]->user_hire_date = date('Y-m-d', $data['user_info'][0]->user_hire_date);


        $data['user_info'][0]->user_country_name = '';
        $data['user_info'][0]->user_state_name = '';
        $data['user_info'][0]->user_city_name = '';

        if ($data['user_info'][0]->user_country > 0) {
            $country = $this->Employer_model->getCountry($data['user_info'][0]->user_country);
            if ($country) {
                $data['user_info'][0]->user_country_name = $country->country_name;
            }
        } else {
            $data['user_info'][0]->user_country = false;
        }

        if ($data['user_info'][0]->user_state > 0) {
            $state = $this->Employer_model->getState($data['user_info'][0]->user_state);
            if ($state) {
                $data['user_info'][0]->user_state_name = $state->state_name;
            }
        } else {
            $data['user_info'][0]->user_state = false;
        }

        if ($data['user_info'][0]->user_city > 0) {
            $city = $this->Employer_model->getCity($data['user_info'][0]->user_city);
            if ($city) {
                $data['user_info'][0]->user_city_name = $city->city_name;
            }
        } else {
            $data['user_info'][0]->user_city = false;
        }

        $this->load->view("common_module/header");

        $this->load->view('edit_employee_view', $data);
        $this->load->view("common_module/footer");
    }


    public function update_employee_info()
    {
        $user_dob = $this->input->post('user_dob');
        $user_hire_date = $this->input->post('user_hire_date');

        if (!$this->input->post('user_bank')) {
            $this->session->set_flashdata('bank_detail_err', 'Bank required');
            redirect('employer_module/edit_employee_info');
        }

        // $date = DateTime::createFromFormat('Y-m-d', '9 Jul 20017');

        $user_id = $this->input->post('user_id');

        $data1['first_name'] = $this->input->post('first_name');
        $data1['last_name'] = $this->input->post('last_name');

        $data1['first_name'] = ucwords( $data1['first_name']," \t\r\n\f\v-");
        $data1['last_name']= ucwords( $data1['last_name']," \t\r\n\f\v-");

        $data1['phone'] = $this->input->post('phone');
        $this->Employer_model->update_employee_credential($data1, $user_id);

        $data2['user_ofc_id'] = $this->input->post('user_ofc_id');
        $data2['user_salary'] = $this->input->post('user_salary');
        $data2['user_gender'] = $this->input->post('user_gender');
        $data2['user_dob'] = $this->custom_datetime_library->convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($user_dob, 'Y-m-d');
        $data2['user_hire_date'] = $this->custom_datetime_library->convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($user_hire_date, 'Y-m-d');
        $data2['user_bvn'] = $this->input->post('user_bvn');
        $data2['user_bank'] = $this->input->post('user_bank');
        $data2['user_bank_account_no'] = $this->input->post('user_bank_account_no');

        $data2['user_street_1'] = trim($this->input->post('user_street_1'));
        $data2['user_street_2'] = trim($this->input->post('user_street_2'));

        $data2['user_country'] = $this->input->post('user_country');
        $data2['user_state'] = $this->input->post('user_state');
        $data2['user_city'] = $this->input->post('user_city');

        $this->Employer_model->update_employee_optional_credential($data2, $user_id);

        $activity_by = '';
        if ($this->ion_auth->is_admin()) {
            $activity_by = 'admin';
        } else if ($this->ion_auth->in_group('employer')) {
            $activity_by = 'employer';
        }

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            '',                                                                     //2.    $created_for
            'employee',                                                             //3.    $type
            $user_id,                                                               //4.    $type_id
            'updated',                                                              //5.    $activity
            $activity_by,                                                           //6.    $activity_by
            '',                                                                     //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            '',                                                                     //10.   $super_type
            '',                                                                     //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/


        if ($this->ion_auth->in_group('employer')) {
            redirect('employer_module/my_employee_info');
        } else if ($this->ion_auth->is_admin()) {
            redirect('employer_module/all_employee_info');
        } else {
            redirect('/');
        }
    }

    public function delete_employee()
    {
        $user_id = $this->uri->segment('3');
        $this->Employer_model->delete_employer_id($user_id);
        $this->session->set_flashdata('delete_success', 'Data Successfully deleted');
        redirect('employer_module/my_employee_info');
    }

    public function all_employer_info()
    {
        $this->lang->load('employer_list');

        $data['which_list'] = 'all_employer_info';
        $this->load->view("common_module/header");

        $this->load->view('employer_list', $data);
        $this->load->view("common_module/footer");
    }


    public function getEmployerByAjax()
    {
        $this->lang->load('employer_list');

        $users = array();

        $which_list = $_REQUEST['which_list'];

        $requestData = $_REQUEST;
        //print_r($requestData);

        $columns[0] = 'created_on';
        $columns[1] = 'company';
        $columns[2] = 'email';
        $columns[3] = 'phone';
        $columns[4] = 'user_website';
        $columns[5] = 'active';
        $columns[6] = 'actions';

        $common_filter_value = false;
        $order_column = false;

        $specific_filters = array();
        $specific_filters = false;

        if (!empty($requestData['columns'][0]['search']['value'])) {
            $specific_filters['company'] = $requestData['columns'][0]['search']['value'];
        }

        if (!empty($requestData['columns'][1]['search']['value'])) {
            $specific_filters['company'] = $requestData['columns'][1]['search']['value'];
        }

        if (!empty($requestData['columns'][2]['search']['value'])) {
            $specific_filters['email'] = $requestData['columns'][2]['search']['value'];
        }

        if (!empty($requestData['columns'][3]['search']['value'])) {
            $specific_filters['phone'] = $requestData['columns'][3]['search']['value'];
        }
        if (!empty($requestData['columns'][4]['search']['value'])) {
            $specific_filters['user_website'] = $requestData['columns'][4]['search']['value'];
        }
        if (!empty($requestData['columns'][5]['search']['value'])) {
            $specific_filters['active'] = $requestData['columns'][5]['search']['value'];
        }


        if (!empty($requestData['search']['value'])) {
            $common_filter_value = $requestData['search']['value'];
        }

        if ($specific_filters == true || !empty($specific_filters)) {
            $common_filter_value = false;       //either search with specific filters or with common filter
        }

        $order['column'] = $columns[$requestData['order'][0]['column']];
        $order['by'] = $requestData['order'][0]['dir'];


        $limit['start'] = $requestData['start'];
        $limit['length'] = $requestData['length'];

        $totalData = $this->Employer_model->countEmployers(false, false);

        if ($common_filter_value == true || $specific_filters == true) {

            $totalFiltered = $this->Employer_model->countEmployers($common_filter_value, $specific_filters);
        } else {
            $totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
        }


        $users = $this->Employer_model->getEmployers($common_filter_value, $specific_filters, $order, $limit);

        if ($users == false || empty($users) || $users == null) {
            $users = false;
        }

        $last_query = $this->db->last_query();

        $this->load->library('custom_datetime_library');


        if ($users) {
            $i = 0;
            foreach ($users as $a_user) {

                /*date time starts*/
                $users[$i]->cr_on = new stdClass();

                $users[$i]->cr_on->timestamp = $a_user->created_on;

                if ($a_user->created_on == 0 || $a_user->created_on == false || $a_user->created_on == null) {
                    $users[$i]->cr_on->display = $this->lang->line('unavailable_text');
                } else {
                    $users[$i]->cr_on->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDateAndTime($a_user->created_on);
                }
                /*date time ends*/

                /*active - inactive starts*/
                $users[$i]->act = new stdClass();
                $users[$i]->act->int = $a_user->active;

                if ($a_user->active == 1) {

                    $status_span = '<span class = "label label-primary">' . $this->lang->line('status_active_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_deactivate_text');
                    $status_url = base_url() . 'users/auth/deactivateUser/' . $a_user->id;
                    $status_anchor_span = '<span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';

                } else {
                    $status_span = '<span class = "label label-default">' . $this->lang->line('status_inactive_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_activate_text');
                    $status_url = base_url() . 'users/auth/activateUser/' . $a_user->id;
                    $status_anchor_span = '<span class="label label-success"><i class="fa fa-check" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';
                }

                $users[$i]->act->html = $status_span . '&nbsp;' . $status_anchor;


                /*active - inactive ends*/

                /*action starts*/
                $view_tooltip = $this->lang->line('tooltip_view_text');
                $view_url = base_url() . 'user_profile_module/user_profile_overview/' . $a_user->id;
                $view_anchor =
                    '<a ' . ' title="' . $view_tooltip . '" ' . ' href="' . $view_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-eye fa-lg" aria-hidden="true"></i>'
                    . '</a>';


                $edit_tooltip = $this->lang->line('tooltip_edit_text');
                $edit_url = base_url() . 'employer_module/edit_employer_info/' . $a_user->id;
                $edit_anchor = '<a ' . ' title="' . $edit_tooltip . '" ' . ' href="' . $edit_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'
                    . '</a>';


                $password_tooltip = $this->lang->line('tooltip_send_password_text');
                $password_url = base_url() . 'employer_module/send_password_via_mail/' . $a_user->id . '/' . $which_list;
                $password_anchor = '<a ' . ' title="' . $password_tooltip . '" ' . ' href="' . $password_url . '" ' . ' class="password_change_confirmation" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-key fa-lg" aria-hidden="true">'
                    . '</a>';

                if (!$this->ion_auth->is_admin()) {
                    $password_anchor = '';
                }

                $users[$i]->action = $view_anchor . '&nbsp;&nbsp;' . $edit_anchor . '&nbsp;&nbsp;' . '&nbsp;&nbsp;' . $password_anchor;
                /*action ends*/

                $i++;

            }
        }


        $json_data['draw'] = intval($requestData['draw']);
        /* $totalData: for every request/draw by clientside ,
         they send a number as a parameter, when they recieve a response/data they first check the draw number,
         so we are sending same number in draw.*/
        $json_data['recordsTotal'] = intval($totalData); // total number of records after searching, if there is no searching then totalFiltered = totalData
        $json_data['recordsFiltered'] = intval($totalFiltered);

        //$users = $this->removeKeys($users); // converting to numeric indices.
        $json_data['data'] = $users;

        // checking requests in console.log() for testing starts;
        /*$json_data['last_query'] = $last_query;
        $json_data['common_filter_value'] = $common_filter_value;
        $json_data['specific_filters'] = $specific_filters;
        $json_data['order_column'] = $order['column'];
        $json_data['order_by'] = $order['by'];
        $json_data['limit_length'] = $limit['length'];
        $json_data['limit_start'] = $limit['start'];*/
        // checking requests in console.log() for testing ends;

        echo(json_encode($json_data));

    }

    public function add_employer_info()
    {
        $this->lang->load('employer_add_form');
        $this->load->view("common_module/header");
        $this->load->view('add_employer_form_page');
        $this->load->view("common_module/footer");
    }

    public function insert_employer_info()
    {
        $data1['first_name'] = '';
        $data1['last_name'] = '';
        $data1['phone'] = trim($this->input->post('phone'));
        $data1['email'] = trim($this->input->post('email'));
        $data1['username'] = trim($this->input->post('email'));
        $data1['company'] = trim($this->input->post('company'));
        $data1['active'] = 1;

        $pw = $this->alphaNum(true, 8, false, false);
        $data1['password'] = $this->Ion_auth_model->hash_password($pw);
        $user_type = 'organizer';
        $data1['mem_id_num'] = $this->un_num('X', $data1['company'][0], $user_type);
        $data1['created_on'] = $this->custom_datetime_library->getCurrentTimestamp();
        $data2['user_website'] = $this->input->post('user_website');

        $this->form_validation->set_rules('phone', 'Phone number', 'required');
        $this->form_validation->set_rules('email', 'Email ID', 'required');
        $this->form_validation->set_rules('company', 'Company', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('employer_insert_error', 'All (*) marked Field is required');
            redirect('employer_module/add_employer_info');
        }

        $chk_email = $this->Employer_model->check_employer_email($data1['email']);
        if ($chk_email == 0) {
            $data2['user_id'] = $this->Employer_model->insert_employer_credential($data1);
            $data2['subdomain'] = $this->createSubdomain($data1['company']);
            $this->Employer_model->insert_employer_optional_credential($data2);

            $grp_mem['user_id'] = $data2['user_id'];
            $grp_mem['group_id'] = '2';
            $this->Employer_model->insert_as_group_member($grp_mem);

            $grp_employer['user_id'] = $data2['user_id'];
            $grp_employer['group_id'] = '6';
            $this->Employer_model->insert_as_employer($grp_employer);

            /*creating log starts*/
            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                '',                                                                     //2.    $created_for
                'employer',                                                             //3.    $type
                $data2['user_id'],                                                      //4.    $type_id
                'created',                                                              //5.    $activity
                'admin',                                                                //6.    $activity_by
                '',                                                                     //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                '',                                                                     //10.   $super_type
                '',                                                                     //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );
            /*creating log ends*/

            //$this->snd_pw_v_mail($data2['user_id'], $pw);
            $this->snd_pw_link_v_mail($data2['user_id']);

            $this->session->set_flashdata('employer_insert', 'Organization Profile Created Successfully');
            redirect('employer_module/add_employer_info');
        } else {
            $this->session->set_flashdata('employer_exist_error', 'This Organization\'s Email Already Exists !');
            redirect('employer_module/add_employer_info');
        }

    }

    public function createSubdomain($company)
    {
        $subdomain = strtolower(trim($company));
        $exp_sd = explode(" ", $subdomain);
        $subdomain = implode("-", $exp_sd);

        while ($this->Employer_model->subdomainExist($subdomain)) {
            $subdomain .= "-".rand(1, 99);
        }

        return $subdomain;
    }

    public function prepareSubdomain($subdomain)
    {
        $subdomain = strtolower(trim($subdomain));
        $exp_sd = explode(" ", $subdomain);
        $subdomain = implode("-", $exp_sd);

        return $subdomain;
    }

    public function generateThriftGroupPaymentNumber()
    {
        $num = 'P';
        $num .= $this->alphaNum(10, false, true);

        $exists = $this->Thrift_model->doesThriftGroupPaymentNumberExist($num);

        if ($exists) {
            $x = $this->generateThriftGroupPaymentNumber();
        } else {
            return $num;
        }
        return $x;

    }

    public function edit_employer_info()
    {
        $this->lang->load('employer_edit_form');

        $user_id = $this->uri->segment(3);
        $data['emp_info'] = $this->Employer_model->get_single_employer_info($user_id);

        $this->load->view("common_module/header");

        $this->load->view('edit_employer_form_page', $data);
        $this->load->view("common_module/footer");


    }

    public function update_employer_info()
    {
        $id = $this->input->post('id');
        $data1['first_name'] = '';
        $data1['last_name'] = '';
        $data1['phone'] = $this->input->post('phone');
        $data1['company'] = $this->input->post('company');
        $data2['user_website'] = $this->input->post('user_website');

        $data2['subdomain'] = $this->prepareSubdomain($this->input->post('subdomain')) ;

        $subdomain_exists = false;
        if(!empty($data2['subdomain'])){
            $subdomain_exists =  $this->Employer_model->subdomainExistForOther($data2['subdomain'],$id);
        }

        if($subdomain_exists){
            $this->session->set_flashdata('subdomain_error', 'subdomain_error');
            redirect('employer_module/edit_employer_info/' . $id);
        }

        $this->form_validation->set_rules('phone', 'Phone number', 'required');
        $this->form_validation->set_rules('company', 'Company', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('employer_update_error', 'employer_update_error');
            redirect('employer_module/edit_employer_info/' . $id);
        }
        $this->Employer_model->update_employer_credential($data1, $id);
        $this->Employer_model->update_employer_optional_credential($data2, $id);

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            '',                                                                     //2.    $created_for
            'employer',                                                             //3.    $type
            $id,                                                                    //4.    $type_id
            'updated',                                                              //5.    $activity
            'admin',                                                                //6.    $activity_by
            '',                                                                     //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            '',                                                                     //10.   $super_type
            '',                                                                     //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        $this->session->set_flashdata('employer_update', 'Organization Details is Successfully Updated');
        redirect('employer_module/edit_employer_info/' . $id);

    }

    public function sendPasswordViaMail()
    {
        $user_id = $this->uri->segment(3);

        $redirect_where = $this->uri->segment(4);

        //currently segment 5 is un available
        $employer_id = false;
        if ($this->uri->segment(5)) {
            $employer_id = $this->uri->segment(5);
        }

        /*generate and update this user's password and send it*/

        /*$password = $this->alphaNum(false, 8, false, false);
        $hashed_password = $this->Ion_auth_model->hash_password($password);
        $this->Employer_model->updateNewPassword($hashed_password,$user_id);
        $this->snd_pw_v_mail($user_id, $password);*/

        /*by ion auth forgot password <starts>*/
        $link_send_success = false;
        $user = $this->Employer_model->getUser($user_id);

        if ($user) {
            if ($user->email != null && $user->email != '') {

                $link_send_success = $this->ion_auth->forgotten_password($user->email);

            }
        }
        /*by ion auth forgot password <ends>*/

        if ($link_send_success) {
            $this->session->set_flashdata('success', 'success');
            $this->session->set_flashdata('password_send_success', 'password_send_success');
            $this->session->set_flashdata('flash_user_id', $user->id);
        } else {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('password_send_error', 'password_send_error');
            $this->session->set_flashdata('flash_user_id', $user->id);
        }


        if ($redirect_where == 'my_employee_info') {

            redirect('employer_module/my_employee_info');

        } else if ($redirect_where == 'all_employee_info') {

            redirect('employer_module/all_employee_info');

        } else if ($redirect_where == 'employers_employee_info' && $employer_id) {

            redirect('employer_module/employers_employee_info/' . $employer_id);

        } else if ($redirect_where == 'all_employer_info') {

            redirect('employer_module/all_employer_info');

        } else {

            redirect('/');

        }


    }

    private function snd_pw_link_v_mail($user_id)
    {
        /*by ion auth forgot password <starts>*/
        $link_send_success = false;
        $user = $this->Employer_model->getUser($user_id);

        if ($user) {
            if ($user->email != null && $user->email != '') {
                $link_send_success = $this->ion_auth->forgotten_password($user->email);
            }
        }
        /*by ion auth forgot password <ends>*/
    }

    private function snd_pw_v_mail($user_id, $password)
    {
        $this->lang->load('password_email');

        $user = $this->Employer_model->getUser($user_id);


        if ($user) {

            $to = $user->email;
            //$to = 'mahmud@sahajjo.com';

            $subject = $this->lang->line('password_change_subject_text');
            $message = sprintf($this->lang->line('password_change_message_text'), $password);

            $mail_data['to'] = $user->email;
            $mail_data['subject'] = $subject;
            $mail_data['message'] = $message;

            $this->sendEmail($mail_data);

        }
    }


    private function alphaNum($cap = false, $length = false, $only_alphabets = false, $only_integers = false)
    {
        if (!$length) {
            $length = 8;
        }


        if ($cap == true) {
            $alphabets = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        } else {
            $alphabets = 'abcdefghijklmnopqrstuvwxyx';
            $characters = '0123456789abcdefghijklmnopqrstuvwxyx';
        }


        $integers = '0123456789';


        if ($only_alphabets) {
            $characters = $alphabets;
        }

        if ($only_integers) {
            $characters = $integers;
        }

        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randomString;
    }

    public function getCountriesBySelect2()
    {
        $total_count = 0;
        $more_pages = false;
        $last_query = 'No query attempted';


        if (isset($_REQUEST['keyword'])) {
            $keyword = $_REQUEST['keyword'];
        } else {
            $keyword = '';
        }

        if (isset($_REQUEST['page'])) {
            $page = $_REQUEST['page'];
        } else {
            $page = 1;
        }


        $limit = 10;

        $total_count = $this->Employer_model->countTotalCountries($keyword);
        $offset = ($page - 1) * $limit;

        $end_count = $offset + $limit;
        $more_pages = $total_count > $end_count;

        $countries = $this->Employer_model->getTotalCountries($keyword, $limit, $offset);

        $last_query = $this->db->last_query();

        $json_data = array();
        $items = array();


        if ($countries) {

            foreach ($countries as $country) {
                $p = array();
                $p['id'] = $country->country_id;
                $p['text'] = $country->country_name;

                $items = $p;
                $json_data['items'][] = $items;
            }
        } else {
            $p = array();
            $p['id'] = '';
            $p['text'] = 'No Country Found';

            $items = $p;
            $json_data['items'][] = $items;
        }


        //echo '<pre>';print_r($json_data);die();


        $json_data['total_count'] = $total_count;
        $json_data['more_pages'] = $more_pages;
        $json_data['last_query'] = $last_query;

        echo json_encode($json_data);

    }

    public function getStatesBySelect2()
    {
        $total_count = 0;
        $more_pages = false;
        $last_query = 'No query attempted';

        $country_id = false;

        if (isset($_REQUEST['user_country'])) {
            $country_id = $_REQUEST['user_country'];
        }

        if (isset($_REQUEST['keyword'])) {
            $keyword = $_REQUEST['keyword'];
        } else {
            $keyword = '';
        }

        if (isset($_REQUEST['page'])) {
            $page = $_REQUEST['page'];
        } else {
            $page = 1;
        }


        $limit = 10;

        $total_count = $this->Employer_model->countTotalStates($keyword, $country_id);
        $offset = ($page - 1) * $limit;

        $end_count = $offset + $limit;
        $more_pages = $total_count > $end_count;

        $states = $this->Employer_model->getTotalStates($country_id, $keyword, $limit, $offset);

        $last_query = $this->db->last_query();

        $json_data = array();
        $items = array();


        if ($states) {

            foreach ($states as $state) {
                $p = array();
                $p['id'] = $state->state_id;
                $p['text'] = $state->state_name;

                $items = $p;
                $json_data['items'][] = $items;
            }
        } else {
            $p = array();
            $p['id'] = '';
            $p['text'] = 'No State Found';

            $items = $p;
            $json_data['items'][] = $items;
        }


        //echo '<pre>';print_r($json_data);die();


        $json_data['total_count'] = $total_count;
        $json_data['more_pages'] = $more_pages;
        $json_data['last_query'] = $last_query;

        echo json_encode($json_data);

    }

    public function getCitiesBySelect2()
    {
        $total_count = 0;
        $more_pages = false;
        $last_query = 'No query attempted';

        $state_id = false;

        if (isset($_REQUEST['user_state'])) {
            $state_id = $_REQUEST['user_state'];
        }

        if (isset($_REQUEST['keyword'])) {
            $keyword = $_REQUEST['keyword'];
        } else {
            $keyword = '';
        }

        if (isset($_REQUEST['page'])) {
            $page = $_REQUEST['page'];
        } else {
            $page = 1;
        }


        $limit = 10;

        $total_count = $this->Employer_model->countTotalCities($keyword, $state_id);
        $offset = ($page - 1) * $limit;

        $end_count = $offset + $limit;
        $more_pages = $total_count > $end_count;

        $cities = $this->Employer_model->getTotalCities($state_id, $keyword, $limit, $offset);

        $last_query = $this->db->last_query();

        $json_data = array();
        $items = array();


        if ($cities) {

            foreach ($cities as $city) {
                $p = array();
                $p['id'] = $city->city_id;
                $p['text'] = $city->city_name;

                $items = $p;
                $json_data['items'][] = $items;
            }
        } else {
            $p = array();
            $p['id'] = '';
            $p['text'] = 'No City Found';

            $items = $p;
            $json_data['items'][] = $items;
        }


        //echo '<pre>';print_r($json_data);die();


        $json_data['total_count'] = $total_count;
        $json_data['more_pages'] = $more_pages;
        $json_data['last_query'] = $last_query;

        echo json_encode($json_data);

    }

    public function approveUser()
    {
        $user_id = $this->uri->segment(3);


        $data['approve'] = 1;
        $data['active'] = 1;
        $data['deletion_status'] = 0;

        $this->Employer_model->approveUser($user_id, $data);

        $activity_by = '';
        if ($this->ion_auth->is_admin()) {
            $activity_by = 'admin';
        } else if ($this->ion_auth->in_group('employer')) {
            $activity_by = 'employer';
        }

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            '',                                                                     //2.    $created_for
            'user',                                                                 //3.    $type
            $user_id,                                                               //4.    $type_id
            'approved',                                                             //5.    $activity
            $activity_by,                                                           //6.    $activity_by
            '',                                                                     //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            '',                                                                     //10.   $super_type
            '',                                                                     //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        $this->approveEmail($user_id);

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('user_approve_success_text', 'user_approve_success_text');
        $this->session->set_flashdata('flash_user_id', $user_id);

        if ($this->ion_auth->is_admin()) {
            redirect('employer_module/all_unapproved_employee_info');
        } else if ($this->ion_auth->in_group('employer')) {
            redirect('employer_module/my_unapproved_employee_info');
        } else {
            redirect('/');
        }

    }


    //be careful! user will be permenantly removed
    public function disapproveUser()
    {
        $user_id = $this->uri->segment(3);

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('user_disapprove_success_text', 'user_disapprove_success_text');

        $this->disapproveEmail($user_id);

        $this->Employer_model->removeUserFromGroups($user_id);
        $this->Employer_model->removeUserFromDetailsTable($user_id);
        $this->Employer_model->removeUserFromUserTable($user_id);

        if ($this->ion_auth->is_admin()) {
            redirect('employer_module/all_unapproved_employee_info');
        } else if ($this->ion_auth->in_group('employer')) {
            redirect('employer_module/my_unapproved_employee_info');
        } else {
            redirect('/');
        }
    }

    private function approveEmail($id)
    {
        $this->lang->load('approval_email_text');

        $user = $this->Employer_model->getUser($id);

        if ($user) {
            $mail_data['to'] = $user->email;

            /*-------*/
            $base_url = $this->getBaseUrl($id);

            $username = $user->first_name . ' ' . $user->last_name;

            $mail_data['to'] = $user->email;

            $template = $this->Employer_model->getEmailTempltateByType('new_thrifter_approval_confirmation');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;


                $actual_link = $base_url . 'users/auth/login';

                $find = array("{{username}}", "{{actual_link}}");
                $replace = array($username, $actual_link);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);
            }
        }

    }

    private function disapproveEmail($id)
    {
        $this->lang->load('approval_email_text');

        $user = $this->Employer_model->getUser($id);

        if ($user) {
            $mail_data['to'] = $user->email;

            $mail_data['subject'] = $this->lang->line('disapprove_subject_text');

            /*-------*/
            $base_url = $this->getBaseUrl($id);

            $url = $base_url;

            if ($this->ion_auth->in_group('admin', $id)) {
                $mail_data['message'] = '<p>' . $this->lang->line('disapprove_by_system_message_text') . '</p>';
            } else if ($this->ion_auth->in_group('employer', $id)) {
                $mail_data['message'] = '<p>' . $this->lang->line('disapprove_by_organization_message_text') . '</p>';
            } else {
                $mail_data['message'] = '<p>' . $this->lang->line('disapprove_message_text') . '</p>';
            }

            $this->sendEmail($mail_data);
        }

    }

    private function sendEmail($mail_data)
    {

        $site_name = $this->custom_settings_library->getASettingsValue('general_settings', 'site_name');
        $site_email = $this->custom_settings_library->getASettingsValue('general_settings', 'site_email');

        if (!$site_name) {
            $site_name = 'Prosperis';
        }

        if (!$site_email) {
            $site_email = 'prosperis@info.com';
        }

        try {

            //$mail_data['to'] = 'mahmud@sahajjo.com';
            $this->email->from($site_email, $site_name);
            $this->email->to($mail_data['to']);

            $this->email->subject($mail_data['subject']);
            $this->email->message(PROSPERIS_MAIL_TOP . $mail_data['message'] . PROSPERIS_MAIL_BOTTOM);
            $this->email->set_mailtype("html");

            /* echo '<hr>'.'<br>';
             echo $mail_data['subject'].'<br>';
             echo $mail_data['message'],'<br>';
             echo '<hr>'.'<br>';*/
            @$this->email->send();

            /*$headers = 'From: '.$site_email.'\r\n';
            mail(mail_data['to'],$mail_data['subject'],$mail_data['message'],$headers);*/

        } catch (Exception $e) {
            // echo $e->getMessage();
        }

    }


    public
    function activateThrifter()
    {

        $id = $this->uri->segment(3);

        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }

        // do we have the right userlevel?
        if (!($this->ion_auth->is_admin() || $this->ion_auth->in_group('employer'))) {
            redirect('users/auth/need_permission');
        } else {
            $id = (int)$id;
            $this->ion_auth->activate($id);

            $activity_by = '';
            if ($this->ion_auth->is_admin()) {
                $activity_by = 'admin';
            } else if ($this->ion_auth->in_group('employer')) {
                $activity_by = 'employer';
            }

            /*creating log starts*/
            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                '',                                                                     //2.    $created_for
                'user',                                                                 //3.    $type
                $id,                                                                    //4.    $type_id
                'activated',                                                            //5.    $activity
                $activity_by,                                                           //6.    $activity_by
                '',                                                                     //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                '',                                                                     //10.   $super_type
                '',                                                                     //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );
            /*creating log ends*/

            $this->activationEmail($id);

        }

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('flash_user_id', $id);
        $this->session->set_flashdata('activate_success', 'activate_success');

        if ($this->ion_auth->is_admin()) {
            redirect('employer_module/all_employee_info');
        } else if ($this->ion_auth->in_group('employer')) {
            redirect('employer_module/my_employee_info');
        } else {
            redirect('/');
        }
    }

    private function activationEmail($id)
    {
        $this->lang->load('email');

        $user = $this->Employer_model->getUser($id);

        if ($user) {
            $username = $user->first_name . ' ' . $user->last_name;
            if ($this->ion_auth->in_group('employer', $id)) {
                $username = $user->company;
            }

            $mail_data['to'] = $user->email;

            $template = $this->Employer_model->getEmailTempltateByType('user_activation');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;

                /*-------*/
                $base_url = $this->getBaseUrl($id);

                $actual_link = $base_url . 'users/auth/login';

                $find = array("{{username}}", "{{actual_link}}");
                $replace = array($username, $actual_link);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);
            }
        } else {
            return false;
        }

    }

    public
    function deactivateThrifter()
    {
        $this->lang->load('email');

        $id = $this->uri->segment(3);

        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }

        if (!($this->ion_auth->is_admin() || $this->ion_auth->in_group('employer'))) {
            redirect('users/auth/need_permission');
        }


        $id = (int)$id;
        $this->ion_auth->deactivate($id);

        $activity_by = '';
        if ($this->ion_auth->is_admin()) {
            $activity_by = 'admin';
        } else if ($this->ion_auth->in_group('employer')) {
            $activity_by = 'employer';
        }

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            '',                                                                     //2.    $created_for
            'user',                                                                 //3.    $type
            $id,                                                                    //4.    $type_id
            'deactivated',                                                          //5.    $activity
            $activity_by,                                                           //6.    $activity_by
            '',                                                                     //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            '',                                                                     //10.   $super_type
            '',                                                                     //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        $this->deactivationEmail($id);


        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('flash_user_id', $id);
        $this->session->set_flashdata('deactivate_success', 'deactivate_success');

        if ($this->ion_auth->is_admin()) {
            redirect('employer_module/all_employee_info');
        } else if ($this->ion_auth->in_group('employer')) {
            redirect('employer_module/my_employee_info');
        } else {
            redirect('/');
        }
    }

    private function deactivationEmail($id)
    {
        $this->lang->load('email');

        $user = $this->Employer_model->getUser($id);

        if ($user) {
            $username = $user->first_name . ' ' . $user->last_name;
            if ($this->ion_auth->in_group('employer', $id)) {
                $username = $user->company;
            }

            $mail_data['to'] = $user->email;

            $template = $this->Custom_auth_model->getEmailTempltateByType('user_deactivation');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;

                /*-------*/
                $base_url = $this->getBaseUrl($id);

                $actual_link = $base_url . 'users/auth/login';

                $find = array("{{username}}", "{{actual_link}}");
                $replace = array($username, $actual_link);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);
            }
        }

    }

    private function deletionEmail($id)
    {
        $this->lang->load('email');

        $user = $this->Employer_model->getUser($id);

        if ($user) {
            $mail_data['to'] = $user->email;

            $mail_data['subject'] = $this->lang->line('deletion_subject_text');

            /*-------*/
            $base_url = $this->getBaseUrl($id);

            $url = $base_url;
            $mail_data['message'] = '<p>' . $this->lang->line('deletion_message_text') . '</p>';

            $this->sendEmail($mail_data);
        }

    }

    public function addOrganizationContactPerson()
    {
        if (!$this->ion_auth->in_group('employer')) {
            redirect('users/auth/need_permission');
        }

        $this->lang->load('employer_contact_form');

        $data['contact_person'] = false;
        $data['form_url'] = 'employer_module/create_organization_contact_person';
        $data['which_form'] = 'add';
        $data['id'] = false;

        $this->load->view("common_module/header");
        $this->load->view('employer_contact_form_page', $data);
        $this->load->view("common_module/footer");
    }

    public function createOrganizationContactPerson()
    {
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('phone', 'Phone Number', 'required');
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('employer_module/add_organization_contact_person');
        }

        $data1['first_name'] = trim($this->input->post('first_name'));
        $data1['last_name'] = trim($this->input->post('last_name'));
        $data1['phone'] = trim($this->input->post('phone'));
        $data1['email'] = trim($this->input->post('email'));
        $data1['username'] = trim($this->input->post('email'));
        $data1['active'] = 1;

        $pw = $this->alphaNum(true, 8, false, false);
        $data1['password'] = $this->Ion_auth_model->hash_password($pw);
        $user_type = 'organization_contact';
        $data1['mem_id_num'] = $this->un_num($data1['first_name'][0], $data1['last_name'][0], $user_type);
        $data1['created_on'] = $this->custom_datetime_library->getCurrentTimestamp();

        $data2['user_id'] = $this->Employer_model->insert_employer_credential($data1);
        $data2['user_employer_id'] = $this->session->userdata('user_id');
        $data2['user_website'] = $this->input->post('user_website');
        $this->Employer_model->insert_employer_optional_credential($data2);

        $grp_mem['user_id'] = $data2['user_id'];
        $grp_mem['group_id'] = '2';
        $this->Employer_model->insert_as_group_member($grp_mem);

        $grp_employer['user_id'] = $data2['user_id'];
        $grp_employer['group_id'] = '9';
        $this->Employer_model->insert_as_group_member($grp_employer); //9 is organization_contact

        //$this->snd_pw_v_mail($data2['user_id'], $pw);
        $this->snd_pw_link_v_mail($data2['user_id']);

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('contact_create_success_text', 'contact_create_success_text');
        $this->session->set_flashdata('flash_user_id', $data2['user_id']);
        redirect('employer_module/add_organization_contact_person');

    }

    public function editOrganizationContactPerson()
    {
        if (!$this->ion_auth->in_group('employer')) {
            redirect('users/auth/need_permission');
        }

        $id = $this->uri->segment(3);

        $this->lang->load('employer_contact_form');

        $data['contact_person'] = $this->Employer_model->getUser($id);
        $data['form_url'] = 'employer_module/update_organization_contact_person';
        $data['which_form'] = 'edit';
        $data['id'] = $id;

        $this->load->view("common_module/header");
        $this->load->view('employer_contact_form_page', $data);
        $this->load->view("common_module/footer");
    }

    public function updateOrganizationContactPerson()
    {

        $id = $this->input->post('id');

        $this->form_validation->set_rules('phone', 'Phone Number', 'required');
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');


        if ($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('employer_module/edit_organization_contact_person/' . $id);
        }

        $data1['first_name'] = trim($this->input->post('first_name'));
        $data1['last_name'] = trim($this->input->post('last_name'));
        $data1['phone'] = trim($this->input->post('phone'));
        $data2['user_website'] = trim($this->input->post('user_website'));

        $this->Employer_model->update_employer_credential($data1, $id);
        $this->Employer_model->update_employer_optional_credential($data2, $id);

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('contact_update_success_text', 'contact_update_success_text');
        $this->session->set_flashdata('flash_user_id', $id);
        redirect('employer_module/edit_organization_contact_person/' . $id);
    }

    public function getContactList()
    {
        $which_list = $this->uri->segment(3);

        if ($which_list == 'all' && !$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        }
        if ($which_list == 'my' && !$this->ion_auth->in_group('employer')) {
            redirect('users/auth/need_permission');
        }

        if ($this->session->userdata('org_contact_user_id')) {
            redirect('users/auth/need_permission');
        }


        $this->lang->load('employer_contact_list');

        $data['employers_id'] = $this->uri->segment(3);
        $data['which_list'] = 'employers_employee_info';

        $data['all_info'] = $this->Employer_model->get_employer_info();

        $this->load->view("common_module/header");

        $this->load->view("employer_contact_list_page", $data);
        $this->load->view("common_module/footer");
    }

    public function getContactListByAjax()
    {
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $email = $this->input->post('email');
        if ($this->ion_auth->in_group('employer')) {
            $employer = $this->session->userdata('user_id');
        } elseif ($this->ion_auth->is_admin() || $this->ion_auth->in_group('trustee')) {
            $employer = $this->input->post('employer');
        }
        $status_chk = $this->input->post('status_chk');
        $get_search_data = $this->Employer_model->getContactList($fname, $lname, $email, $employer, $status_chk);
        foreach ($get_search_data as $key => $row) {
            $emp_name = $this->Employer_model->get_employer_name($row['user_employer_id']);
            $get_search_data[$key]['employer_name'] = $emp_name[0]['first_name'];
            $get_search_data[$key]['company'] = $emp_name[0]['company'];
            if ($get_search_data[$key]['company'] == '' || $get_search_data[$key]['company'] == null) {
                $get_search_data[$key]['company'] = 'Unavailable';
            }
            $get_search_data[$key]['employer_name'] = $emp_name[0]['first_name'] . ' ' . $emp_name[0]['last_name'];

        }
        print_r(json_encode($get_search_data));
    }

    public
    function activateContact()
    {
        $id = $this->uri->segment(3);

        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }

        // do we have the right userlevel?
        if (!($this->ion_auth->is_admin() || $this->ion_auth->in_group('employer'))) {
            redirect('users/auth/need_permission');
        } else {
            $id = (int)$id;
            $this->ion_auth->activate($id);

            $activity_by = '';
            if ($this->ion_auth->is_admin()) {
                $activity_by = 'admin';
            } else if ($this->ion_auth->in_group('employer')) {
                $activity_by = 'employer';
            }

            /*creating log starts*/
            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                '',                                                                     //2.    $created_for
                'user',                                                                 //3.    $type
                $id,                                                                    //4.    $type_id
                'activated',                                                            //5.    $activity
                'admin',                                                                //6.    $activity_by
                $activity_by,                                                           //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                '',                                                                     //10.   $super_type
                '',                                                                     //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );
            /*creating log ends*/

            $this->activationEmail($id);

        }

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('flash_user_id', $id);
        $this->session->set_flashdata('activate_success', 'activate_success');

        if ($this->ion_auth->is_admin()) {
            redirect('employer_module/contact_person_list/all');
        } else if ($this->ion_auth->in_group('employer')) {
            redirect('employer_module/contact_person_list/my');
        } else {
            redirect('/');
        }
    }


    public
    function deactivateContact()
    {
        $id = $this->uri->segment(3);

        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }

        if (!($this->ion_auth->is_admin() || $this->ion_auth->in_group('employer'))) {
            redirect('users/auth/need_permission');
        }


        $id = (int)$id;
        $this->ion_auth->deactivate($id);

        $activity_by = '';
        if ($this->ion_auth->is_admin()) {
            $activity_by = 'admin';
        } else if ($this->ion_auth->in_group('employer')) {
            $activity_by = 'employer';
        }

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            '',                                                                     //2.    $created_for
            'user',                                                                 //3.    $type
            $id,                                                                    //4.    $type_id
            'deactivated',                                                          //5.    $activity
            $activity_by,                                                           //6.    $activity_by
            '',                                                                     //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            '',                                                                     //10.   $super_type
            '',                                                                     //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        $this->deactivationEmail($id);

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('flash_user_id', $id);
        $this->session->set_flashdata('deactivate_success', 'deactivate_success');

        if ($this->ion_auth->is_admin()) {
            redirect('employer_module/contact_person_list/all');
        } else if ($this->ion_auth->in_group('employer')) {
            redirect('employer_module/contact_person_list/my');
        } else {
            redirect('/');
        }
    }

    public
    function deleteContact()
    {
        $id = $this->uri->segment(3);

        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }

        if (!($this->ion_auth->is_admin() || $this->ion_auth->in_group('employer'))) {
            redirect('users/auth/need_permission');
        }


        $id = (int)$id;
        $this->ion_auth->deactivate($id);

        $data['deletion_status'] = 1;

        $this->Employer_model->deleteUser($id, $data);

        $activity_by = '';
        if ($this->ion_auth->is_admin()) {
            $activity_by = 'admin';
        } else if ($this->ion_auth->in_group('employer')) {
            $activity_by = 'employer';
        }

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            '',                                                                     //2.    $created_for
            'user',                                                                 //3.    $type
            $id,                                                                    //4.    $type_id
            'deleted',                                                              //5.    $activity
            $activity_by,                                                           //6.    $activity_by
            '',                                                                     //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            '',                                                                     //10.   $super_type
            '',                                                                     //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        $this->deletionEmail($id);

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('deletion_success', 'deletion_success');

        if ($this->ion_auth->is_admin()) {
            redirect('employer_module/contact_person_list/all');
        } else if ($this->ion_auth->in_group('employer')) {
            redirect('employer_module/contact_person_list/my');
        } else {
            redirect('/');
        }
    }

    private function getBaseUrl($id)
    {
        $base_url = '';
        if ($this->ion_auth->in_group('admin', $id)) {
            $base_url = $this->config->item('office_base_url');
        } else if ($this->ion_auth->in_group('employer', $id) || $this->ion_auth->in_group('organization_contact', $id)) {
            $base_url = $this->config->item('partner_base_url');
        } else if ($this->ion_auth->in_group('employee', $id)) {
            $base_url = $this->Utility_model->makeThriftUrlWithEmployerSubdomain($this->config->item('thrift_base_url'), $id);
        } else if ($this->ion_auth->in_group('trustee', $id)) {
            $base_url = $this->config->item('trustee_base_url');
        }

        return $base_url;
    }


}