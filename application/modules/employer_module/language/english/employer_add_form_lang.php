<?php

$lang['page_title_text'] = 'Add Organization';

$lang['breadcrum_home_text'] = 'Organization Management';
$lang['breadcrumb_page_add_text'] = 'New Organization';

$lang['employee_add_form_header_text'] = "Add New Organization Information";

/*page texts*/
$lang['employer_first_name_text'] = 'First Name';

$lang['employer_last_name_text'] = 'Last Name';

$lang['employer_home_address_text'] = 'Address';

$lang['employer_phone_text'] = 'Phone';

$lang['employer_contact_person_text'] = 'Contact Person';

$lang['employer_email_text'] = 'Email';

$lang['employer_website_text'] = 'Website';

$lang['employer_organization_text'] = 'Organization Name';

$lang['file_submit_text'] = 'SUBMIT';
$lang['modal_cancel_text'] = 'RESET';

?>