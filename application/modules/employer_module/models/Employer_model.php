<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Employer_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function check_email_id($email)
    {
        $this->db->select('email');
        $this->db->from('users');
        $this->db->where('email', $email);

        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function insert_employee_credential($data1)
    {
        $this->db->insert('users', $data1);
        return $this->db->insert_id();
    }

    public function insert_employee_optional_credential($data2)
    {
        $this->db->insert('rspm_tbl_user_details', $data2);
    }

    public function update_employee_credential($data1, $user_id)
    {
        $this->db->where('id', $user_id);
        $this->db->update('users', $data1);
    }

    public function update_employee_optional_credential($data2, $user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->update('rspm_tbl_user_details', $data2);
    }

    public function insert_as_group_member($grp_mem)
    {
        $this->db->insert('users_groups', $grp_mem);
    }

    public function insert_as_employee($grp_employee)
    {
        $this->db->insert('users_groups', $grp_employee);
    }

    public function get_employee_info($user_id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('rspm_tbl_user_details', 'users.id=rspm_tbl_user_details.user_id');
        $this->db->where('users.id', $user_id);

        $query = $this->db->get();
        $queryresult = $query->result();
        return $queryresult;
    }

    public function activate_product($product_id)
    {
        $this->db->set('product_is_active', 1);
        $this->db->where('product_id', $product_id);
        $this->db->update('pg_product');
    }

    public function deactivate_product($product_id)
    {
        $this->db->set('product_is_active', 0);
        $this->db->where('product_id', $product_id);
        $this->db->update('pg_product');
    }

    public function get_all_product()
    {
        $this->db->select('*');
        $this->db->from('pg_product');
        $this->db->where('product_deletion_status', 0);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function get_product_data($product_id)
    {
        $this->db->select('*');
        $this->db->from('pg_product');
        $this->db->where('product_id', $product_id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function update_product_data($data, $product_id)
    {
        $this->db->where('product_id', $product_id);
        $this->db->update('pg_product', $data);
    }

    public function delete_product($product_id)
    {
        $this->db->set('product_deletion_status', 1);
        $this->db->where('product_id', $product_id);
        $this->db->update('pg_product');
    }

    public function get_selected_search_data($fname, $lname, $email, $employer, $status_chk)
    {
        $this->db->select('users.id, users.first_name, users.last_name, users.email, users.active, users.created_on, users.created_on, users.mem_id_num, users.company, rspm_tbl_user_details.user_employer_id');
        $this->db->from('users');
        $this->db->where('users.deletion_status!=', 1);
        $this->db->join('rspm_tbl_user_details', 'users.id=rspm_tbl_user_details.user_id');
        $this->db->join('users_groups', 'users.id=users_groups.user_id');
        if ($employer != '') {
            $this->db->where('rspm_tbl_user_details.user_employer_id', $employer);
        }
        $this->db->where('users_groups.group_id', 7);
        if ($fname != '') {
            $this->db->like('users.first_name', $fname);
        }
        if ($lname != '') {
            $this->db->like('users.last_name', $lname);
        }
        if ($email != '') {
            $this->db->like('users.email', $email);
        }
        if ($status_chk != '') {
            $this->db->where('users.active', $status_chk);
        }
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_unapproved_selected_search_data($fname, $lname, $email, $employer, $status_chk)
    {
        $this->db->select('users.id, users.first_name, users.last_name, users.email, users.active,users.approve, users.created_on, users.created_on, users.mem_id_num, users.company, rspm_tbl_user_details.user_employer_id');
        $this->db->from('users');
        $this->db->where('users.approve', 0);
        $this->db->where('users.verification', 1);
        $this->db->join('rspm_tbl_user_details', 'users.id=rspm_tbl_user_details.user_id');
        $this->db->join('users_groups', 'users.id=users_groups.user_id');
        if ($employer != '') {
            $this->db->where('rspm_tbl_user_details.user_employer_id', $employer);
        }
        $this->db->where('users_groups.group_id', 7);
        if ($fname != '') {
            $this->db->like('users.first_name', $fname);
        }
        if ($lname != '') {
            $this->db->like('users.last_name', $lname);
        }
        if ($email != '') {
            $this->db->like('users.email', $email);
        }
        if ($status_chk != '') {
            $this->db->where('users.active', $status_chk);
        }
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function getContactList($fname, $lname, $email, $employer, $status_chk)
    {
        $this->db->select('users.id, users.first_name, users.last_name, users.email, users.active, users.created_on, users.created_on, users.mem_id_num, users.company, rspm_tbl_user_details.user_employer_id');
        $this->db->from('users');
        $this->db->where('users.deletion_status!=', 1);
        $this->db->join('rspm_tbl_user_details', 'users.id=rspm_tbl_user_details.user_id');
        $this->db->join('users_groups', 'users.id=users_groups.user_id');
        if ($employer != '') {
            $this->db->where('rspm_tbl_user_details.user_employer_id', $employer);
        }
        $this->db->where('users_groups.group_id', 9);
        if ($fname != '') {
            $this->db->like('users.first_name', $fname);
        }
        if ($lname != '') {
            $this->db->like('users.last_name', $lname);
        }
        if ($email != '') {
            $this->db->like('users.email', $email);
        }
        if ($status_chk != '') {
            $this->db->where('users.active', $status_chk);
        }
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_employer_name($employer_id)
    {
        $this->db->select('users.first_name,users.last_name,users.company');
        $this->db->from('users');
        $this->db->join('rspm_tbl_user_details', 'rspm_tbl_user_details.user_id=users.id');
        $this->db->where('rspm_tbl_user_details.user_id', $employer_id);

        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_employer_info()
    {
        $this->db->select('users.id, users.first_name, users.company, users.last_name, users.email');
        $this->db->from('users');
        $this->db->join('users_groups', 'users.id=users_groups.user_id');
        $this->db->where('users_groups.group_id', 6);

        $this->db->order_by('users.company');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function get_employer_data($employee_id)
    {
        $query = 'SELECT * FROM rspm_tbl_user_details as ud WHERE ud.user_id=' . $employee_id;

        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.id', $query);


        $query = $this->db->get();
        $queryresult = $query->result();
        return $queryresult;
    }

    public function delete_employer_id($user_id)
    {
        $this->db->set('user_employer_id', '0');
        $this->db->where('user_id', $user_id);
        $this->db->update('rspm_tbl_user_details');
    }

    public function chk_thrift_unique_id($chk_thrift_id)
    {
        $this->db->select('mem_id_num');
        $this->db->from('users');
        $this->db->where('mem_id_num', $chk_thrift_id);

        $query = $this->db->get();
        $result = $query->num_rows();
        return $result;
    }

    public function check_employer_email($email)
    {
        $this->db->select('email');
        $this->db->from('users');
        $this->db->where('email', $email);

        $query = $this->db->get();
        $result = $query->num_rows();
        return $result;
    }

    public function insert_employer_credential($data1)
    {
        $this->db->insert('users', $data1);
        return $this->db->insert_id();
    }

    public function insert_employer_optional_credential($data2)
    {
        $this->db->insert('rspm_tbl_user_details', $data2);
    }

    public function insert_as_employer($grp_employer)
    {
        $this->db->insert('users_groups', $grp_employer);
    }

    public function get_single_employer_info($user_id)
    {
        $this->db->select('users.id, users.first_name, users.last_name, users.phone, users.email, users.company, rtud.user_website,rtud.subdomain');
        $this->db->from('users');
        $this->db->join('rspm_tbl_user_details rtud', 'rtud.user_id=users.id', 'LEFT');
        $this->db->where('users.id', $user_id);
        // $this->db->join('users_groups','users_groups.user_id=users.id');
        // $this->db->where('users_groups.group_id',6);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


    public function get_selected_employer_data($orgname, $fname, $lname, $email, $employer, $status_chk)
    {
        $this->db->select('users.id, users.first_name, users.last_name, users.email, users.active, users.phone, users.company, rspm_tbl_user_details.user_website');
        $this->db->from('users');
        $this->db->where('users.deletion_status!=', 1);
        $this->db->join('rspm_tbl_user_details', 'users.id=rspm_tbl_user_details.user_id');
        $this->db->join('users_groups', 'users.id=users_groups.user_id');
        if ($employer != '') {
            $this->db->where('rspm_tbl_user_details.user_employer_id', $employer);
        }
        $this->db->where('users_groups.group_id', 6);
        // if($fname!='')
        // {
        //     $this->db->group_start();
        //     $this->db->like('users.first_name',$fname);
        //     $this->db->or_like('users.last_name',$fname);
        //     $this->db->group_end();
        // }
        if ($orgname != '') {
            $this->db->like('users.company', $orgname);
        }
        if ($fname != '') {
            $this->db->like('users.first_name', $fname);
        }
        if ($lname != '') {
            $this->db->like('users.last_name', $lname);
        }
        if ($email != '') {
            $this->db->like('users.email', $email);
        }
        if ($status_chk != '') {
            $this->db->where('users.active', $status_chk);
        }
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }


    public function update_employer_credential($data1, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('users', $data1);

    }

    public function update_employer_optional_credential($data2, $id)
    {
        $this->db->where('user_id', $id);
        $this->db->update('rspm_tbl_user_details', $data2);
    }

    public function get_employer_org_name($user_employer_id)
    {
        $this->db->select('users.company');
        $this->db->from('users');
        $this->db->where('users.id', $user_employer_id);

        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }


    public function getUser($user_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.id', $user_id);
        $this->db->join('rspm_tbl_user_details as ud', 'u.id = ud.user_id');

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function updateNewPassword($hashed_password, $user_id)
    {
        $this->db->set('password', $hashed_password);
        $this->db->where('id', $user_id);
        $this->db->update('users');
    }

    /*------------------------------------------------------------*/

    public function get_all_bank_list($only_undeleted = false)
    {
        $this->db->select('*');
        $this->db->from('pg_bank');

        if ($only_undeleted) {
            $this->db->where('bank_deletion_status!=', 1);
        }

        $this->db->order_by('bank_name');

        $query = $this->db->get();
        $num_rows = $query->result();
        return $num_rows;
    }


    /*-------------------------------------------------------------*/
    public function countTotalCountries($keyword)
    {
        $this->db->select('*');
        $this->db->from('countries');

        $this->db->like('country_name', $keyword);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function getTotalCountries($keyword, $limit, $offset)
    {
        $this->db->select('*');
        $this->db->from('countries');

        $this->db->like('country_name', $keyword);

        $this->db->limit($limit, $offset);

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function countTotalStates($keyword, $country_id)
    {
        $this->db->select('*');
        $this->db->from('states');

        if ($country_id) {
            $this->db->where('country_id', $country_id);
        }

        $this->db->like('state_name', $keyword);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function getTotalStates($country_id, $keyword, $limit, $offset)
    {
        $this->db->select('*');
        $this->db->from('states');

        if ($country_id) {
            $this->db->where('country_id', $country_id);
        }

        $this->db->like('state_name', $keyword);

        $this->db->limit($limit, $offset);

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function countTotalCities($keyword, $state_id)
    {
        $this->db->select('*');
        $this->db->from('cities');

        if ($state_id) {
            $this->db->where('state_id', $state_id);
        }

        $this->db->like('city_name', $keyword);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function getTotalCities($state_id, $keyword, $limit, $offset)
    {
        $this->db->select('*');
        $this->db->from('cities');

        if ($state_id) {
            $this->db->where('state_id', $state_id);
        }


        $this->db->like('city_name', $keyword);

        $this->db->limit($limit, $offset);

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function getCountry($country_id)
    {
        $this->db->select('*');
        $this->db->from('countries');
        $this->db->where('country_id', $country_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getState($state_id)
    {
        $this->db->select('*');
        $this->db->from('states');
        $this->db->where('state_id', $state_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getCity($city_id)
    {
        $this->db->select('*');
        $this->db->from('cities');
        $this->db->where('city_id', $city_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    /*----------------------------------------------*/
    public function approveUser($user_id, $data)
    {
        $this->db->where('id', $user_id);
        $this->db->update('users', $data);
    }

    public function removeUserFromGroups($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->delete('users_groups');
    }

    public function removeUserFromDetailsTable($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->delete('rspm_tbl_user_details');
    }

    public function removeUserFromUserTable($user_id)
    {
        $this->db->where('id', $user_id);
        $this->db->delete('users');
    }

    public function deleteUser($user_id, $data)
    {
        $this->db->where('id', $user_id);
        $this->db->update('users', $data);
    }

    public function getEmailTempltateByType($type)
    {
        $this->db->select('*');
        $this->db->from('tbl_email_template');
        $this->db->where('email_template_type', $type);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }


    /*----------------------------------------------------------------------------------------------------------------*/
    public function countEmployees($common_filter_value = false, $specific_filters = false, $employer_id)
    {
        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.mem_id_num as mem_id_num,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
                        ud.user_employer_id as user_employer_id
        ');


        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id', 7); // employee group_id 6

        $this->db->join('rspm_tbl_user_details as ud', 'ug.user_id=ud.user_id');

        if ($employer_id) {
            $this->db->where('ud.user_employer_id', $employer_id);
        }


        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('u.first_name', $common_filter_value);
            $this->db->or_like('u.last_name', $common_filter_value);
            $this->db->or_like('u.email', $common_filter_value);
            $this->db->or_like('u.mem_id_num', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'first_name' || $column_name == 'last_name' || $column_name == 'email' || $column_name == 'mem_id_num') {
                    $this->db->like('u.' . $column_name, $filter_value);
                }

                if ($column_name == 'user_employer_id') {
                    if ($filter_value != '') {
                        $this->db->where('ud.' . $column_name, $filter_value);
                    }

                }

                if ($column_name == 'active') {
                    if ($filter_value == 'yes') {
                        $this->db->where('u.active', 1);
                    } else {
                        $this->db->where('u.active!=', 1);
                    }

                }
            }

        }

        $query = $this->db->get();

        $num_rows = $query->num_rows();


        return $num_rows;
    }

    public function getEmployees($common_filter_value = false, $specific_filters = false, $order, $limit, $employer_id)
    {
        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.mem_id_num as mem_id_num,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
                        ud.user_employer_id as user_employer_id
        ');


        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id', 7); // employee group_id 6

        $this->db->join('rspm_tbl_user_details as ud', 'ug.user_id=ud.user_id');

        if ($employer_id) {
            $this->db->where('ud.user_employer_id', $employer_id);
        }


        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('u.first_name', $common_filter_value);
            $this->db->or_like('u.last_name', $common_filter_value);
            $this->db->or_like('u.email', $common_filter_value);
            $this->db->or_like('u.mem_id_num', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'first_name' || $column_name == 'last_name' || $column_name == 'email' || $column_name == 'mem_id_num') {
                    $this->db->like('u.' . $column_name, $filter_value);
                }

                if ($column_name == 'user_employer_id') {
                    $this->db->like('ud.' . $column_name, $filter_value);
                }

                if ($column_name == 'active') {
                    if ($filter_value == 'yes') {
                        $this->db->where('u.active', 1);
                    } else {
                        $this->db->where('u.active!=', 1);
                    }

                }
            }

        }


        $this->db->order_by('u.' . $order['column'], $order['by']);
        $this->db->limit($limit['length'], $limit['start']);

        $query = $this->db->get();


        $result = $query->result();


        return $result;
    }

    public function countEmployers($common_filter_value = false, $specific_filters = false)
    {
        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
                        ud.user_website
        ');


        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id', 6); // employer group_id 6

        $this->db->join('rspm_tbl_user_details as ud', 'ug.user_id=ud.user_id');


        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->or_like('u.email', $common_filter_value);
            $this->db->or_like('u.company', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'company' || $column_name == 'email') {
                    $this->db->like('u.' . $column_name, $filter_value);
                }

                if ($column_name == 'active') {
                    if ($filter_value == 'yes') {
                        $this->db->where('u.active', 1);
                    } else {
                        $this->db->where('u.active!=', 1);
                    }

                }
            }

        }

        $query = $this->db->get();

        $num_rows = $query->num_rows();


        return $num_rows;
    }

    public function getEmployers($common_filter_value = false, $specific_filters = false, $order, $limit)
    {

        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
                        ud.user_website
        ');


        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id', 6); // employer group_id 6

        $this->db->join('rspm_tbl_user_details as ud', 'ug.user_id=ud.user_id');


        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->or_like('u.email', $common_filter_value);
            $this->db->or_like('u.company', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'company' || $column_name == 'email') {
                    $this->db->like('u.' . $column_name, $filter_value);
                }

                if ($column_name == 'active') {
                    if ($filter_value == 'yes') {
                        $this->db->where('u.active', 1);
                    } else {
                        $this->db->where('u.active!=', 1);
                    }

                }
            }

        }


        if ($order['column'] == 'user_website') {
            $this->db->order_by('ud.' . $order['column'], $order['by']);
        } else {
            $this->db->order_by('u.' . $order['column'], $order['by']);
        }

        $this->db->limit($limit['length'], $limit['start']);

        $query = $this->db->get();


        $result = $query->result();


        return $result;
    }

    public function subdomainExist($subdomain)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_user_details');
        $this->db->where('subdomain', $subdomain);

        $query = $this->db->get();

        $exist = $query->num_rows() > 0 ? true : false;
        return $exist;
    }

    public function subdomainExistForOther($subdomain,$user_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_user_details');
        $this->db->where('subdomain', $subdomain);
        $this->db->where('user_id!=', $user_id);

        $query = $this->db->get();

        $exist = $query->num_rows() > 0 ? true : false;
        return $exist;
    }


}