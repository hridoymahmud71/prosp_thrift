<!-- Content Wrapper. Contains page content -->
<!-- <div class="content-page"> -->
<!-- Start content -->
<!-- <div class="content"> -->
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php echo lang('page_title_text') ?>
                    <!-- <small><?php echo lang('page_subtitle_add_text') ?></small> -->
                </h4>
                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a
                                href="employer_module/add_employee_info"><?php echo lang('breadcrum_home_text') ?></a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_add_text') ?></li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30"><?php echo lang('employee_add_form_header_text') ?></h4>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-6">
                        <form role="form" action="employer_module/update_employee_info" method="post"
                              data-parsley-validate novalidate>
                            <div class="form-group row">
                                <label for="first_name"
                                       class="col-sm-4 form-control-label"><?php echo lang('modal_first_name_text'); ?>
                                    <span class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <input type="text" required parsley-type="first_name" class="form-control"
                                           name="first_name" value="<?= $user_info[0]->first_name; ?>"
                                           id="first_name" placeholder="<?php echo lang('modal_first_name_text'); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="last_name"
                                       class="col-sm-4 form-control-label"><?php echo lang('modal_last_name_text'); ?></label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="last_name"
                                           value="<?= $user_info[0]->last_name; ?>"
                                           id="last_name" placeholder="<?php echo lang('modal_last_name_text'); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="user_ofc_id"
                                       class="col-sm-4 form-control-label"><?php echo lang('modal_user_ofc_id_text'); ?>
                                    <span class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <input type="text" required parsley-type="user_ofc_id" class="form-control"
                                           name="user_ofc_id" value="<?= $user_info[0]->user_ofc_id; ?>"
                                           id="user_ofc_id" placeholder="<?php echo lang('modal_user_ofc_id_text'); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="user_salary"
                                       class="col-sm-4 form-control-label"><?php echo lang('modal_user_salary_text'); ?>
                                    <span class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <input type="text" required parsley-type="user_salary" class="form-control"
                                           name="user_salary" value="<?= $user_info[0]->user_salary; ?>"
                                           id="user_salary" placeholder="<?php echo lang('modal_user_salary_text'); ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="phone"
                                       class="col-sm-4 form-control-label"><?php echo lang('modal_phone_text'); ?><span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <input type="text" required parsley-type="phone" class="form-control" name="phone"
                                           value="<?= $user_info[0]->phone; ?>"
                                           id="phone" placeholder="<?php echo lang('modal_phone_text'); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone"
                                       class="col-sm-4 form-control-label"><?php echo lang('modal_user_gender_text'); ?>
                                    <span class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <div>
                                            <div class="radio">
                                                <input type="radio" name="user_gender" id="radio1"
                                                       value="male" <?php if ($user_info[0]->user_gender == 'male') {
                                                    echo 'checked';
                                                } ?> required>
                                                <label for="radio1"><?php echo lang('modal_user_gender_male_text'); ?></label>
                                            </div>
                                            <div class="radio">
                                                <input type="radio" name="user_gender" id="radio2"
                                                       value="female" <?php if ($user_info[0]->user_gender == 'female') {
                                                    echo 'checked';
                                                } ?>>
                                                <label for="radio2"><?php echo lang('modal_user_gender_female_text'); ?></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="user_dob"
                                       class="col-sm-4 form-control-label"><?php echo lang('modal_age_text'); ?><span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <input readonly type="text" required parsley-type="user_dob"
                                           class="form-control date_chk" name="user_dob"
                                           value="<?= $user_info[0]->user_dob; ?>"
                                           id="user_dob" placeholder="<?php echo lang('modal_age_text'); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="user_hire_date"
                                       class="col-sm-4 form-control-label"><?php echo lang('modal_user_hire_date_text'); ?>
                                    <span class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <input type="text" readonly class="form-control date_chk" id="user_hire_date"
                                           value="<?= $user_info[0]->user_hire_date; ?>" name="user_hire_date">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="user_bvn"
                                       class="col-sm-4 form-control-label"><?php echo lang('modal_user_bvn_text'); ?>
                                    <span class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <input type="text" required parsley-type="user_bvn" class="form-control"
                                           name="user_bvn" value="<?= $user_info[0]->user_bvn; ?>"
                                           id="user_bvn" placeholder="<?php echo lang('modal_user_bvn_text'); ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="user_employer_id"
                                       class="col-sm-4 form-control-label"><?php echo lang('bank_select_text'); ?><span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <select name="user_bank" required style="width: 100%;"
                                            class="form-control select2 myselect user_bank" id="">
                                        <option value=""><?= lang('bank_select_text') ?></option>
                                        <?php foreach ($banks as $bank) { ?>
                                            <option value="<?= $bank->bank_id ?>" <?php if($user_info[0]->user_bank == $bank->bank_id){ echo ' selected="selected" '; }?> ><?= $bank->bank_name ?></option>
                                        <?php } ?>
                                    </select>
                                    <?php if ($this->session->flashdata('bank_detail_err')) { ?>
                                        <label class="text-danger"
                                               for="confirm2"><?php echo lang('field_mandatory_text'); ?></label>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="user_bank_account_no"
                                       class="col-sm-4 form-control-label"><?php echo lang('modal_user_bank_account_no_text'); ?></label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="user_bank_account_no"
                                           value="<?= $user_info[0]->user_bank_account_no; ?>"
                                           id="user_bank_account_no"
                                           placeholder="<?php echo lang('modal_user_bank_account_no_text'); ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="user_street_1" class="col-sm-4 form-control-label"><?php echo lang('label_user_street_1_text');?></label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="user_street_1"
                                           value="<?= $user_info[0]->user_street_1?>"
                                           id="user_street_1"
                                           placeholder="<?php echo lang('placeholder_user_street_1_text'); ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="user_street_2" class="col-sm-4 form-control-label"><?php echo lang('label_user_street_2_text');?></label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="user_street_2"
                                           value="<?= $user_info[0]->user_street_2?>"
                                           id="user_street_1"
                                           placeholder="<?php echo lang('placeholder_user_street_2_text'); ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="user_country" class="col-sm-4 form-control-label"><?php echo lang('label_user_country_text');?><span class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <select name="user_country" required  style="width: 100%;" class="form-control select2  user_country" id="user_country">
                                        <?php if ($user_info[0]->user_country) { ?>
                                            <option value="<?= $user_info[0]->user_country?>" selected="selected"><?= $user_info[0]->user_country_name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="user_state" class="col-sm-4 form-control-label"><?php echo lang('label_user_state_text');?><span class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <select name="user_state" required  style="width: 100%;" class="form-control select2 user_state  user_state" id="user_state">
                                        <?php if ($user_info[0]->user_state) { ?>
                                            <option value="<?= $user_info[0]->user_state?>" selected="selected"><?= $user_info[0]->user_state_name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="user_city" class="col-sm-4 form-control-label"><?php echo lang('label_user_city_text');?><span class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <select name="user_city" required  style="width: 100%;" class="form-control select2 select2  user_city" id="user_city">
                                        <?php if ($user_info[0]->user_city) { ?>
                                            <option value="<?= $user_info[0]->user_city?>" selected="selected"><?= $user_info[0]->user_city_name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>



                            <input type="hidden" name="user_id" value="<?= $user_info[0]->id; ?>">

                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('file_submit_text'); ?>
                                </button>
                                <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                    <?php echo lang('modal_cancel_text'); ?>
                                </button>
                            </div>
                        </form>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>
</div>
<!-- </div> -->
<!-- </div> -->


<script type="text/javascript">
    $(function () {
        $('#user_dob').datepicker({
            format: 'yyyy-mm-dd',
            endDate: '-18y',
            autoclose: true
        });

        $('#user_hire_date').datepicker({
            format: 'yyyy-mm-dd',
            endDate: '-1d',
            autoclose: true
        });

        $('.user_bank').select2();

        $('.user_country').select2({

            allowClear: true,
            placeholder:'<?= lang('placeholder_user_country_text')?>',

            "language": {
                "noResults": function () {
                    var btn_html = "<?= lang('country_not_found_text')?>";
                    var div = document.createElement("div");
                    div.innerHTML = btn_html;
                    return div;
                }
            },

            minimumInputLength: 1,
            ajax: {
                url: '<?= base_url() ?>employer_module/get_countries_by_select2',
                dataType: 'json',
                cache: true,
                delay: 1000,
                allowClear: true,


                data: function (params) {

                    return {
                        keyword: params.term, // search term
                        page: params.page,
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    //console.log(data.items);

                    return {
                        results: data.items,
                        pagination: {
                            more: data.more_pages
                        }
                    };
                },

                /*templateResult: formatRepo,
                 templateSelection: formatRepoSelection,*/

                escapeMarkup: function (markup) {
                    return markup;
                } // let our custom formatter work


            }

        });

        $('.user_state').select2({

            allowClear: true,
            placeholder:'<?= lang('placeholder_user_state_text')?>',

            "language": {
                "noResults": function () {
                    var btn_html = "<?= lang('state_not_found_text')?>";
                    var div = document.createElement("div");
                    div.innerHTML = btn_html;
                    return div;
                }
            },

            minimumInputLength: 1,
            ajax: {
                url: '<?= base_url() ?>employer_module/get_states_by_select2',
                dataType: 'json',
                cache: true,
                delay: 1000,
                allowClear: true,


                data: function (params) {
                    return {
                        user_country:$('#user_country').val(),
                        keyword: params.term, // search term
                        page: params.page,
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    //console.log(data.items);

                    return {
                        results: data.items,
                        pagination: {
                            more: data.more_pages
                        }
                    };
                },

                /*templateResult: formatRepo,
                 templateSelection: formatRepoSelection,*/

                escapeMarkup: function (markup) {
                    return markup;
                } // let our custom formatter work


            }

        });

        $('.user_city').select2({

            allowClear: true,
            placeholder:'<?= lang('placeholder_user_city_text')?>',

            "language": {
                "noResults": function () {
                    var btn_html = "<?= lang('city_not_found_text')?>";
                    var div = document.createElement("div");
                    div.innerHTML = btn_html;
                    return div;
                }
            },

            minimumInputLength: 1,
            ajax: {
                url: '<?= base_url() ?>employer_module/get_cities_by_select2',
                dataType: 'json',
                cache: true,
                delay: 1000,
                allowClear: true,


                data: function (params) {

                    return {
                        user_state:$('#user_state').val(),
                        keyword: params.term, // search term
                        page: params.page,
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    //console.log(data.items);

                    return {
                        results: data.items,
                        pagination: {
                            more: data.more_pages
                        }
                    };
                },

                /*templateResult: formatRepo,
                 templateSelection: formatRepoSelection,*/

                escapeMarkup: function (markup) {
                    return markup;
                } // let our custom formatter work


            }

        });

    })
</script>


