<?php


$lang['page_title_text'] = 'Message';
$lang['breadcrum_home_text'] = 'Home';
$lang['breadcrum_page_text'] = 'View Message';


$lang['posted_on_text'] = 'Posted on';
$lang['message_box_title_text'] = 'Message';
$lang['comment_box_title_text'] = 'Comments';
$lang['write_comment_box_title_text'] = 'Write Comment';



$lang['write_comment_text'] = 'Write Your Comment';
$lang['submit_btn_text'] = 'Send';
$lang['commented_on_text'] = 'Commented on';
$lang['no_comments_yet_text'] = 'No Comments Yet';


$lang['reply_text'] = 'Reply';

//validation

$lang['successful_text'] = 'Successful!';
$lang['unsuccessful_text'] = 'Unsuccessful!';
$lang['comment_text_required_text'] = 'Comment cannot be empty!';
$lang['comment_sent_success_text'] = 'Message Posted. Your comment has been posted';






