<div class="container">

    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left"><?= lang('page_title_text') ?></h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="/"><?= lang('breadcrum_home_text') ?></a></li>
                    <li class="breadcrumb-item active"><?= lang('breadcrum_page_text') ?></li>
                </ol>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- end row -->

    <?php if ($this->session->flashdata('comment_sent_success')) { ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong><?= lang('successful_text') ?></strong>
            <?php

            if ($this->session->flashdata('comment_sent_success_text')) {
                echo $this->session->flashdata('comment_sent_success_text');
            }
            ?>
        </div>
    <?php } ?>

    <?php if ($this->session->flashdata('comment_sent_error')) { ?>
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong><?= lang('unsuccessful_text') ?></strong>
            <?php

            if ($this->session->flashdata('validation_errors')) {
                echo $this->session->flashdata('validation_errors');
            }
            ?>

            <?php
            if ($this->session->flashdata('comment_text_required_text')) {
                echo $this->session->flashdata('comment_text_required_text');
            }
            ?>

        </div>
    <?php } ?>

    <div class="row">
        <div class="col-12">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-30"><?= lang('message_box_title_text') ?></h4>

                <div class="row">

                    <div class="col-lg-12">

                        <!-- Author -->
                        <p class="lead">
                            by
                            <a href="#"><?php if ($message_sender) { ?>
                                    <a href="<?php echo base_url() . 'user_profile_module/user_profile_overview/' . $message_sender->user_id ?>">
                                        <?php
                                        if ($this->ion_auth->in_group('employer',$message_sender->user_id)) {
                                            echo $message_sender->company;
                                        } else {
                                            echo $message_sender->first_name . ' ' . $message_sender->last_name;
                                        }
                                        ?>
                                    </a>
                                <?php } ?>
                            </a>
                        </p>

                        <hr>
                        <?php if ($message) { ?>
                            <!-- Date/Time -->
                            <p><?= lang('posted_on_text') ?> <?= $message->created_at_datetimestring ?></p>

                            <hr>
                            <?= $message->message ?>
                            <hr>
                        <?php } ?>
                        <div class="pull-right">
                            <button class="reply_btn" class="btn btn-default btn-sm"><i class="fa fa-reply"></i>
                                &nbsp;
                                <?php echo lang('reply_text') ?>
                            </button>
                        </div>
                    </div>


                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>

    <?php $message_types_dont_allow_comment = ['custom_product_invitation', 'thrift_group_removed', 'custom_product_started'] ?>

    <div class="row" id="comment-form-main-row"
        <?php if ($message) {
            if (in_array($message->message_type, $message_types_dont_allow_comment)) { ?><?php }
        } ?> > <!-- style="display: none" -->
        <div class="col-12">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-30"><?= lang('write_comment_box_title_text') ?></h4>

                <div class="row">
                    <div class="col-md-12">
                        <form id="#comment-form" action="<?= $form_action ?>" method="post"
                              enctype="multipart/form-data">

                            <fieldset class="form-group w-100">
                                <label for=""><?= lang('write_comment_text') ?></label>
                                <textarea id="comment_text" class="form-control" name="comment_text"
                                          rows="3"></textarea>
                            </fieldset>

                            <button type="submit"
                                    class="btn btn-primary"><?= lang('submit_btn_text') ?></button>
                        </form>
                    </div><!-- end col -->


                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-30"><?= lang('comment_box_title_text') ?></h4>

                <div class="row">
                    <div class="col-md-12">

                        <?php if ($comments_with_commenter) {
                            $i = 0; ?>
                            <?php foreach ($comments_with_commenter as $cc) {
                                $i++; ?>

                                <?php if ($i > 1) { ?>
                                    <hr class="hr-style-four">
                                <?php } ?>

                                <!-- Single Comment -->
                                <div class="media mb-4" id="comment-<?= $cc->comment_id ?>">
                                    <a href="#comment-<?= $cc->comment_id ?>"></a>
                                    <!--<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">-->
                                    <div class="media-body">
                                        <h5 class="mt-0">

                                            <a href="<?php echo base_url() . 'user_profile_module/user_profile_overview/' . $cc->id ?>">
                                                <?php echo $cc->first_name . ' ' . $cc->last_name ?>
                                            </a>

                                        </h5>
                                        <hr>
                                        <!-- Date/Time -->
                                        <p><?= lang('commented_on_text') ?> <?= $cc->commented_at_datetimestring ?> </p>
                                        <hr>
                                        <?= $cc->comment ?>
                                        <hr>
                                        <div class="pull-right">
                                            <button class="reply_btn" class="btn btn-default btn-sm"><i
                                                        class="fa fa-reply"></i>
                                                &nbsp;
                                                <?php echo lang('reply_text') ?>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                        <?php } else { ?>
                            <div class="text-center">
                                <?= lang('no_comments_yet_text') ?>
                            </div>
                        <?php } ?>

                    </div><!-- end col -->


                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>


</div> <!-- container -->


<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->

<style>
    .hr-style-four {
        height: 12px;
        border: 0;
        box-shadow: inset 0 12px 12px -12px rgba(0, 0, 0, 0.5);
    }
</style>

<script>
    $(function () {

        $(".reply_btn").on("click", function (e) {
            e.preventDefault();

            $('html, body').animate({
                scrollTop: $("#comment-form-main-row").offset().top
            }, 1000);
        });


        tinymce.init({
            selector: '#comment_text',
            height: 200,
            menubar: false,
            plugins: [
                'advlist autolink lists link charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime table contextmenu paste code help'
            ],
            toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        });


    });
</script>