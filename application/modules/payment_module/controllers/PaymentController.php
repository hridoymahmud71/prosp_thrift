<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PaymentController extends MX_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->library('custom_log_library');
        $this->load->library('settings_module/custom_settings_library');
        $this->load->library('payment_module/custom_payment_library');

        $this->load->model('payment_module/Payment_model');

    }

    public function ggbb()
    {
        $this->custom_payment_library->ggbb();
    }

    public function flutterwaveBinCheck()
    {
        $this->custom_payment_library->flutterwaveBinCheck();
    }

    public function flutterwaveRecurrentPaymentInitiate()
    {
        $this->custom_payment_library->flutterwaveRecurrentPaymentInitiate();
    }

    public function flutterwaveCreateUser()
    {
        $this->custom_payment_library->flutterwaveCreateUser();
    }

    public function flutterwaveEncrypt()
    {
        $x = $this->custom_payment_library->flutterwaveEncrypt('54654132654');
        echo $x;
    }


    public function raveInlinePage()
    {
        $this->lang->load('rave_inline_form');
        $data = array();

        $this->load->view("common_module/header");
        $this->load->view("payment_module/rave_inline_form_page", $data);
        $this->load->view("common_module/footer");
    }

    public function raveCallback()
    {
        if(isset($_GET['resp'])){
            $decoded_resp = json_decode($_GET['resp']);
            echo '<pre>';
            print_r($decoded_resp);
            echo '</pre>';
        }
    }

    public function getSinglePaystackTransaction()
    {
        $transaction_data['id'] = 16881293;
        $transaction = $this->custom_payment_library->getSinglePaystackTransaction($transaction_data);
        echo "<pre>";
        print_r($transaction);
        echo "</pre>";
    }

    public function getPaystackTransactionList()
    {
        $transaction_data['perPage'] = 500;
        $transaction_list = $this->custom_payment_library->getPaystackTransactionList($transaction_data);
        echo "<pre>";
        print_r($transaction_list);
        echo "</pre>";
    }

    public function getPaystackTransferrecipientList()
    {
        $transfer_data['perPage'] = 500;
        $transferrecipient_list = $this->custom_payment_library->getPaystackTransferrecipientList($transfer_data);
        echo "<pre>";
        print_r($transferrecipient_list);
        echo "</pre>";
    }

    //currently do not need this
    /*public function autoCreatePaystackCustomers()
    {
        $non_paystackcustomer_employees = $this->Payment_model->getNonPaystackCustomerEmployees();


        if ($non_paystackcustomer_employees) {


            foreach ($non_paystackcustomer_employees as $employee) {

                $customer = array();

                if ($employee->first_name == null || $employee->first_name == false || $employee->first_name == '') {
                    $customer['first_name'] = 'X';
                } else {
                    $customer['first_name'] = $employee->first_name;
                }

                if ($employee->last_name == null || $employee->last_name == false || $employee->last_name == '') {
                    $customer['last_name'] = 'Y';
                } else {
                    $customer['last_name'] = $employee->last_name;
                }

                if ($employee->email == null || $employee->email == false || $employee->email == '') {
                    // do not create customer
                } else {
                    $customer['email'] = $employee->email;

                    $ret_data = $this->custom_payment_library->createSinglePaystackCustomer($customer, $employee->user_id);

                    if ($ret_data['got_data'] && !$ret_data['error_response_object'] && !$ret_data['error_sk']) {
                        $created_customer = $ret_data['got_data'];

                        if ($created_customer) {
                            if ($created_customer->status == 1 && !empty($created_customer->data) && $created_customer->data != null) {
                                $upd_data['paystack_customer_code'] = $created_customer->data->customer_code;
                                $upd_data['paystack_customer_id'] = $created_customer->data->id;
                                $upd_data['paystack_integration'] = $created_customer->data->integration;

                                $this->Payment_model->updateEmployeeAsPaystackCustomer($upd_data, $employee->user_id);


                            }
                        }
                    }

                    if ($ret_data['error_response_object']) {
                        print_r($ret_data['error_response_object']);
                    }
                    if ($ret_data['error_message']) {
                        echo $ret_data['error_message'];
                    }
                    if ($ret_data['error_sk']) {
                        echo $ret_data['error_sk'];
                    }
                }

            }


        }
    }*/


    //currently do not need this
    /*provided that data  was not saved properly while creating paystack customers */
    /*public function autoUpdateAlreadyCreatedPaystackCustomers()
    {
        $non_paystackcustomer_employees = $this->Payment_model->getNonPaystackCustomerEmployees();

        $npe_email_list = array();

        if ($non_paystackcustomer_employees) {


            foreach ($non_paystackcustomer_employees as $employee) {

                if ($employee) {
                    $npe_email_list[] = $employee->email;
                }

            }


        }

        $ret_data = $this->custom_payment_library->getPaystackCustomerList();

        if ($ret_data['got_data'] && !$ret_data['error_response_object'] && !$ret_data['error_sk']) {
            $paystack_customers = $ret_data['got_data'];

            if ($paystack_customers) {

                if ($paystack_customers->status == 1
                    && !empty($paystack_customers->data) && $paystack_customers->data != null
                    && !empty($npe_email_list) && $npe_email_list != null
                ) {

                    foreach ($paystack_customers->data as $pc) {

                        if (in_array($pc->email, $npe_email_list)) {
                            $upd_data['paystack_customer_code'] = $pc->customer_code;
                            $upd_data['paystack_customer_id'] = $pc->id;
                            $upd_data['paystack_integration'] = $pc->integration;

                            $user = $this->Payment_model->getUserWithEmail($pc->email);

                            if ($user) {
                                $this->Payment_model->updateEmployeeAsPaystackCustomer($upd_data, $user->id);
                            }


                        }

                    }

                }
            }
        }

        if ($ret_data['error_response_object']) {
            print_r($ret_data['error_response_object']);
        }
        if ($ret_data['error_message']) {
            echo $ret_data['error_message'];
        }
        if ($ret_data['error_sk']) {
            echo $ret_data['error_sk'];
        }


    }*/


}