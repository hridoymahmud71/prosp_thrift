<?php


$lang['page_title_add_text'] = 'Individual Thrifting';
$lang['page_title_edit_text'] = 'Individual Thrifting';
$lang['page_title_view_text'] = 'Individual Thrifting';

$lang['breadcrum_home_text'] = 'Home';
$lang['breadcrum_page_text'] = 'Individual Thrifting';


$lang['box_title_add_text'] = 'Enter Thrift Savings Details';
$lang['box_title_edit_text'] = 'Edit Details';
$lang['box_title_view_text'] = 'View Details';

/*invitation*/

$lang['thrift_amount_text'] = 'Thrift Amount';
$lang['per_person_per_month_text'] = 'Per Person/Per Month';
$lang['number_of_payments_text'] = 'Number of payments';

$lang['set_start_date_text'] = 'Set start date (Payments are deducted same day every month)';

//chk
$lang['start_thrift_text'] = 'Start Thrift';
$lang['start_thrift_long_text'] = 'Start a thrift with this individual product';



$lang['submit_btn_add_text'] = 'Add';
$lang['submit_btn_create_thrift_text'] = 'Create Thrift';
$lang['submit_btn_update_text'] = 'Update';
//validation


//flash
$lang['successful_text'] = 'Successful !';
$lang['unsuccessful_text'] = 'Unsuccessful !';

$lang['view_individual_product_thrift_text'] = 'See here';

$lang['individual_product_create_success'] = 'Successfully created individual product';
$lang['individual_product_update_success'] = 'Successfully updated individual product';
$lang['flash_thrift_percentage_error_text'] = 'Your monthly salary must be greater than %s%% of the monthly contribution amount to join this thrift';













