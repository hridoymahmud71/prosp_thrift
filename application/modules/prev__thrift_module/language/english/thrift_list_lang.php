<?php

$lang['page_title_all_thrift_text'] = 'All Thrifts';
$lang['page_title_my_thrift_text'] = 'My Thrifts';
$lang['page_title_my_employees_thrift_text'] = 'My Employees\'s Thrifts';

$lang['table_title_text'] = 'Thrift List';
$lang['no_thrift_found_text'] = 'No Thrift Is Found !';
$lang['no_matching_thrift_found_text'] = 'No matching Thrift Is Found !';

$lang['breadcrumb_home_text'] = 'Thrift';
$lang['breadcrumb_section_text'] = 'Thrift List';
$lang['breadcrumb_page_text'] = 'Thrift List';

$lang['add_button_text'] = 'Add A Thrift';


$lang['toggle_column_text'] = 'Toggle Columns';

$lang['option_all_product_text'] = 'All Products';
$lang['select_product_text'] = 'Select Product';


$lang['option_all_text'] = 'All';
$lang['option_active_text'] = 'Active';
$lang['option_inactive_text'] = 'Inactive';

$lang['option_open_text'] = 'Open';
$lang['option_close_text'] = 'Close';

$lang['option_complete_text'] = 'Complete';
$lang['option_incomplete_text'] = 'Incomplete';

$lang['option_member_full_text'] = 'Full';
$lang['option_member_not_full_text'] = 'Not full';

/*Column names of the table*/
$lang['column_group_number_text'] = 'Group ID';
$lang['column_product_name_text'] = 'Product';
$lang['column_product_price_text'] = 'Product Price';
$lang['column_contribution_amount_text'] = 'Contribution Amount';
$lang['column_completion_text'] = 'Completion';
$lang['column_member_status_text'] = 'Members';
$lang['column_status_text'] = 'Status';
$lang['column_open_staus_text'] = 'Open?';
$lang['column_creation_date_text'] = 'Created';
$lang['column_start_date_text'] = 'Started';
$lang['column_actions_text'] = 'Actions';

$lang['status_active_text'] = 'Active';
$lang['status_inactive_text'] = 'Inactive';

$lang['status_open_text'] = 'Open';
$lang['status_close_text'] = 'Close';

/*swal texts*/
$lang['swal_title_text'] = 'Are You Sure To Delete This Thrift ?';
$lang['swal_confirm_button_text'] = 'yes delete this thrift';
$lang['swal_cancel_button_text'] = 'No, keep this thrift';

$lang['delete_success_text'] = 'Successfully deleted the thrift.';

$lang['successfull_text'] = 'Successful';
$lang['user_add_success_text'] = 'Successfully added thrift';


$lang['activate_success_text'] = 'Thrift Activated';
$lang['dectivate_success_text'] = 'Thrift Deactivated';

$lang['open_success_text'] = 'Thrift Opened';
$lang['close_success_text'] = 'Thrift closed';

$lang['see_thrift_text'] = 'See Thrift';
$lang['edit_thrift_text'] = 'Edit Thrift';

$lang['creation_time_unknown_text'] = 'Unknown';
$lang['not_started_text'] = 'Not started';


/*tooltip text*/
$lang['tooltip_activate_text'] = 'Make Thrift Active';
$lang['tooltip_deactivate_text'] = 'Make Thrift Deactive';

$lang['tooltip_open_text'] = 'Make Thrift Open';
$lang['tooltip_close_text'] = 'Make Thrift Close';

$lang['tooltip_view_text'] = 'View Thrift';
$lang['tooltip_edit_text'] = 'Edit Thrift';
$lang['tooltip_delete_text'] = 'Delete Thrift';



/*loading*/
$lang['loading_text'] = 'Loading Thrifts . . .';



/*flash*/
$lang['thrift_success_text'] = 'Awesome! You have successfully joined a thrift. Way to go!';
$lang['thrift_create_success_text'] = 'Awesome! You have successfully created your thrift. Way to go!';
$lang['see_thrift_group_text'] = 'see thrift group';

$lang['unsuccessful_text'] = 'Unsuccessful';
$lang['thrifting_error_text'] = 'Thrifting Error';
$lang['flash_thrift_percentage_error_text'] = 'Your monthly salary must be greater than %s%% of the monthly contribution amount to join this thrift';


$lang['thrift_error_only_employee_allowed_text'] = 'Only Thrifters are allowed to join thrift';

$lang['thrift_error_exceed_limit_text'] = 'Exeeds thrift limit';
$lang['thrift_error_no_employer_text'] = 'You do not belong to any organization';



