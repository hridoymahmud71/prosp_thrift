<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->


<!--clearing the extra arrow-->
<style>
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: unset;
    }
</style>
<div class="container">

    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">

                <h4 class="page-title float-left"><?= lang('page_title_text') ?></h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="/"><?= lang('breadcrum_home_text') ?></a></li>
                    <li class="breadcrumb-item active"><?= lang('breadcrum_page_text') ?></li>
                </ol>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- end row -->

    <?php if ($this->session->flashdata('unsuccessful')) { ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong><?= lang('unsuccessful_text') ?></strong>
            <?php

            if ($this->session->flashdata('validation_errors')) {
                echo $this->session->flashdata('validation_errors');
            }
            ?>
        </div>
    <?php } ?>


    <div class="row">
        <div class="col-12">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-30">
                    <?= lang('payment_method_description_text') ?>
                </h4>


                <div class="row">
                    <div class="col-md-6">
                        <form id="#custom_produc_-form" action="<?= $form_action ?>" method="post"
                              enctype="multipart/form-data">

                            <fieldset class="form-group">
                                <label><?= lang('choose_payment_method_text') ?></label>
                                <div>
                                    <div class="input-group">
                                        <select name="user_chosen_payment_method" class="form-control" id="">
                                            <option value="paystack"><?= lang('paystack_text')?></option>
                                            <option value="flutterwave" disabled><?= lang('flutterwave_text')?></option>
                                        </select>

                                    </div><!-- input-group -->
                                </div>
                            </fieldset>
                            <fieldset class="form-group">
                                <label style="font-style: italic; font-size: smaller;color: red" ><?= lang('payment_method_one_time_auth_text') ?></label>
                            </fieldset>

                            <button type="submit"
                                    class="btn btn-primary"><?= lang('submit_btn_text') ?></button>

                        </form>
                    </div><!-- end col -->


                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>


</div> <!-- container -->


