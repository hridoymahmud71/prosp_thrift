<?php

/*page texts*/
$lang['page_title_add_text'] = 'Add Product';
$lang['page_title_edit_text'] = 'Edit Product';
$lang['page_subtitle_add_text'] = 'Product add';
$lang['page_subtitle_edit_text'] = 'Product edit';

$lang['breadcrum_home_text'] = 'Product Add';
$lang['breadcrumb_page_add_text'] = 'Product add';
$lang['breadcrumb_page_edit_text'] = 'Product edit';

/*page form*/

$lang['product_add_form_header_text'] = 'Manage Product information';

$lang['product_input_name_text'] = 'Product Name';
$lang['product_input_name_placeholder_text'] = 'Product Name';

$lang['product_input_number_text'] = 'Thrift duration (months)';
$lang['product_input_number_placeholder_text'] = 'Thrift duration (months)';

$lang['product_input_amount_text'] = 'Thrift Amount (Per Person/Per Month)';
$lang['product_input_amount_placeholder_text'] = 'Thrift Amount';

$lang['product_description_text']='Product Description';

$lang['prodct_submit_text'] = 'SUBMIT';
$lang['upload_site_logo_text'] = 'Upload';

?>