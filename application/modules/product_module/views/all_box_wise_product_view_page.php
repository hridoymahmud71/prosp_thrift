<!-- <div class="content-page"> -->
    <?php if ($this->session->flashdata('thrift_error')) { ?>
        <section class="content mt-0">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="alert-heading"><?php echo lang('unsuccessful_text') ?></h4>

                        <?php if ($this->session->flashdata('thrift_error_exceed_limit_text')) { ?>
                            <p>
                                <?php echo lang('thrift_error_exceed_limit_text') ?>
                            </p>
                        <?php } ?>

                        <?php if ($this->session->flashdata('thrift_error_only_employee_allowed_text')) { ?>
                            <p>
                                <?php echo lang('thrift_error_only_employee_allowed_text') ?>
                            </p>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </section>
    <?php } ?>
    <!-- Start content -->
    <!-- <div class="content"> -->
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">
                            <?php echo lang('page_subtitle_text') ?>
                        </h4>

                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="product_module/all_product_box_wise_info"><?php echo lang('breadcrum_home_text') ?></a></li>
                            <li class="breadcrumb-item active"><?php echo lang('breadcrum_page_text') ?></li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row">
                <?php foreach ($all_product as $key => $row): ?>
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                        <div class="card-box tilebox-three">
                            <div class="bg-icon float-left">
                                <i class=" icon-wallet"></i>
                            </div>
                            <div class="text-right">
                                <h6 class="text-success text-uppercase m-b-15 m-t-10"><?= $row->product_name ?></h6>
                                <h5 class="m-b-10"><span data-plugin="counterup"><?= $row->product_price ?></span></h5>
                            </div>
                            <h6 class="mb-0 float-right"><?= $row->product_term_duration; ?> <?= lang('months_text') ?> </h6>
                            <br>
                            <hr>

                            <div class="row m-b-10">
                                <div class="col-xs-12 w-100">
                                    <?php if ($this->ion_auth->in_group('employee')) { ?>
                                        <a class="w-100 btn btn-success waves-effect waves-light btn-sm"
                                           href="thrift_module/start_thrift/<?= $row->product_id; ?>"><?= lang('start_thrift_text') ?></a>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 w-100">
                                    <a href="product_module/view_product_details/<?= $row->product_id; ?>"><button class="w-100 btn btn-success waves-effect waves-light btn-sm"><?= lang('product_details_text') ?></button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>

        </div>
    <!-- </div> -->
<!-- </div> -->