<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReportController extends MX_Controller
{

    function __construct()
    {
        parent::__construct();

        set_time_limit ( 0 );
        ini_set('post_max_size ', 52428800 );
        ini_set('upload_max_filesize   ', 52428800 );
        ini_set('max_execution_time ', 900);

        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->library('MPDF/mpdf');
        $this->load->library('excel');

        $this->load->library('settings_module/custom_settings_library');
        $this->load->library('custom_datetime_library');

        $this->load->model('report_module/Report_model');
    }

    public function getCurrencySign()
    {
        $sign = $this->custom_settings_library->getASettingsValue('çurrency_settings', 'currency_sign');

        if ($sign) {
            return $sign;
        } else {
            return '';
        }
    }


    public function allEmployer()
    {

        if (!($this->ion_auth->is_admin() || $this->ion_auth->in_group('trustee'))) {
            redirect('users/auth/need_permission');
        }

        $this->lang->load('employer_list');

        $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

        $this->load->library('custom_datetime_library');


        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("report_module/employer_list_page", $data);
        $this->load->view("common_module/footer");
    }

    public function getAllEmployerByAjax()
    {
        $this->lang->load('employer_list');

        $users = array();

        $requestData = $_REQUEST;
        //print_r($requestData);

        $columns[0] = 'company';
        $columns[1] = 'email';
        $columns[2] = 'active';
        $columns[3] = 'actions';

        $common_filter_value = false;
        $order_column = false;

        $specific_filters = array();
        $specific_filters = false;

        if (!empty($requestData['columns'][0]['search']['value'])) {
            $specific_filters['company'] = $requestData['columns'][0]['search']['value'];
        }

        if (!empty($requestData['columns'][1]['search']['value'])) {
            $specific_filters['email'] = $requestData['columns'][1]['search']['value'];
        }

        if (!empty($requestData['columns'][2]['search']['value'])) {
            $specific_filters['group'] = $requestData['columns'][2]['search']['value'];
        }

        if (!empty($requestData['columns'][3]['search']['value'])) {
            $specific_filters['active'] = $requestData['columns'][3]['search']['value'];
        }


        if (!empty($requestData['search']['value'])) {
            $common_filter_value = $requestData['search']['value'];
        }

        if ($specific_filters == true || !empty($specific_filters)) {
            $common_filter_value = false;       //either search with specific filters or with common filter
        }

        $order['column'] = $columns[$requestData['order'][0]['column']];
        $order['by'] = $requestData['order'][0]['dir'];


        $limit['start'] = $requestData['start'];
        $limit['length'] = $requestData['length'];

        $totalData = $this->Report_model->countEmployers(false, false);

        if ($common_filter_value == true || $specific_filters == true) {

            $totalFiltered = $this->Report_model->countEmployers($common_filter_value, $specific_filters);
        } else {
            $totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
        }


        $users = $this->Report_model->getEmployers($common_filter_value, $specific_filters, $order, $limit);

        if ($users == false || empty($users) || $users == null) {
            $users = false;
        }

        $last_query = $this->db->last_query();

        $this->load->library('custom_datetime_library');


        if ($users) {
            $i = 0;
            foreach ($users as $a_user) {


                /*active - inactive starts*/
                $users[$i]->act = new stdClass();
                $users[$i]->act->int = $a_user->active;

                if ($a_user->active == 1) {

                    $status_span = '<span class = "label label-primary">' . $this->lang->line('status_active_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_deactivate_text');
                    $status_url = base_url() . 'users/auth/deactivateUser/' . $a_user->id;
                    $status_anchor_span = '<span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';

                } else {
                    $status_span = '<span class = "label label-default">' . $this->lang->line('status_inactive_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_activate_text');
                    $status_url = base_url() . 'users/auth/activateUser/' . $a_user->id;
                    $status_anchor_span = '<span class="label label-success"><i class="fa fa-check" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';
                }

                $users[$i]->act->html = $status_span;


                /*active - inactive ends*/

                /*action starts*/


                $payment_report_tooltip = $this->lang->line('tooltip_payment_report_text');
                $payment_report_url = base_url() . 'report_module/show_report/employer_report/payment_report/all/' . $a_user->id;
                $payment_report_anchor =
                    '<a ' . ' title="' . $payment_report_tooltip . '" ' . ' href="' . $payment_report_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-list-alt fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $payment_recieve_report_tooltip = $this->lang->line('tooltip_payment_recieve_report_text');
                $payment_recieve_report_url = base_url() . 'report_module/show_report/employer_report/payment_recieve_report/all/' . $a_user->id;
                $payment_recieve_report_anchor =
                    '<a ' . ' title="' . $payment_recieve_report_tooltip . '" ' . ' href="' . $payment_recieve_report_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-file-text-o fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $users[$i]->action = $payment_report_anchor . '&nbsp;&nbsp;' . $payment_recieve_report_anchor;

                $i++;

            }
        }


        $json_data['draw'] = intval($requestData['draw']);
        /* $totalData: for every request/draw by clientside ,
         they send a number as a parameter, when they recieve a response/data they first check the draw number,
         so we are sending same number in draw.*/
        $json_data['recordsTotal'] = intval($totalData); // total number of records after searching, if there is no searching then totalFiltered = totalData
        $json_data['recordsFiltered'] = intval($totalFiltered);

        //$users = $this->removeKeys($users); // converting to numeric indices.
        $json_data['data'] = $users;

        // checking requests in console.log() for testing starts;
        /*$json_data['last_query'] = $last_query;
        $json_data['common_filter_value'] = $common_filter_value;
        $json_data['specific_filters'] = $specific_filters;
        $json_data['order_column'] = $order['column'];
        $json_data['order_by'] = $order['by'];
        $json_data['limit_length'] = $limit['length'];
        $json_data['limit_start'] = $limit['start'];*/
        // checking requests in console.log() for testing ends;

        echo(json_encode($json_data));

    }

    public function employee()
    {
        $which_employees = $this->uri->segment(3);

        $rpt_fmt = $this->uri->segment(2);

        $report_format = '';
        if ($rpt_fmt == 'employee') {
            $report_format = 'none';
        } else if ($rpt_fmt == 'employee_report_pdf') {
            $report_format = 'pdf';
        } else if ($rpt_fmt == 'employee_report_excel') {
            $report_format = 'excel';
        }

        if ($which_employees == 'all') {

            if (!($this->ion_auth->is_admin() || $this->ion_auth->in_group('trustee'))) {
                redirect('users/auth/need_permission');
            }

        }

        if ($which_employees == 'my') {

            if (!$this->ion_auth->in_group('employer')) {
                redirect('users/auth/need_permission');
            }

        }

        $this->lang->load('employee_list');


        if ($report_format == 'pdf' || $report_format == 'excel') {
            $this->employeeReportFile($report_format, $which_employees);
            exit;
        }

        $data['which_employees'] = $which_employees;
        $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

        $this->load->library('custom_datetime_library');


        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("report_module/employee_list_page", $data);
        $this->load->view("common_module/footer");
    }

    public function getEmployeeByAjax()
    {
        $this->lang->load('employee_list');

        $users = array();

        $which_employees = $_REQUEST['which_employees'];

        $employer_id = false;

        if ($which_employees == 'my') {
            $employer_id = $this->session->userdata('user_id');
        }

        $requestData = $_REQUEST;
        //print_r($requestData);

        $columns[0] = 'created_on';
        $columns[1] = 'name';
        $columns[2] = 'mem_id_num';
        $columns[3] = 'email';
        $columns[4] = 'employer_id';
        $columns[5] = 'concluded_thrifts';
        $columns[6] = 'active';
        $columns[7] = 'actions';

        $common_filter_value = false;
        $order_column = false;

        $specific_filters = array();
        $specific_filters = false;

        if (!empty($requestData['columns'][0]['search']['value'])) {
            $specific_filters['created_on'] = $requestData['columns'][0]['search']['value'];
        }

        if (!empty($requestData['columns'][1]['search']['value'])) {
            $specific_filters['name'] = $requestData['columns'][1]['search']['value'];
        }

        if (!empty($requestData['columns'][2]['search']['value'])) {
            $specific_filters['mem_id_num'] = $requestData['columns'][2]['search']['value'];
        }

        if (!empty($requestData['columns'][3]['search']['value'])) {
            $specific_filters['email'] = $requestData['columns'][3]['search']['value'];
        }

        if (!empty($requestData['columns'][4]['search']['value'])) {
            $specific_filters['employer_id'] = $requestData['columns'][4]['search']['value'];
        }

        if (!empty($requestData['columns'][5]['search']['value'])) {
            $specific_filters['concluded_thrifts'] = $requestData['columns'][5]['search']['value'];
        }

        if (!empty($requestData['columns'][6]['search']['value'])) {
            $specific_filters['active'] = $requestData['columns'][6]['search']['value'];
        }


        if (!empty($requestData['search']['value'])) {
            $common_filter_value = $requestData['search']['value'];
        }

        if ($specific_filters == true || !empty($specific_filters)) {
            $common_filter_value = false;       //either search with specific filters or with common filter
        }

        $order['column'] = $columns[$requestData['order'][0]['column']];
        $order['by'] = $requestData['order'][0]['dir'];


        $limit['start'] = $requestData['start'];
        $limit['length'] = $requestData['length'];

        $totalData = $this->Report_model->countEmployees(false, false, $employer_id);

        if ($common_filter_value == true || $specific_filters == true) {

            $totalFiltered = $this->Report_model->countEmployees($common_filter_value, $specific_filters, $employer_id);
        } else {
            $totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
        }


        $users = $this->Report_model->getEmployees($common_filter_value, $specific_filters, $order, $limit, $employer_id);

        if ($users == false || empty($users) || $users == null) {
            $users = false;
        }

        $last_query = $this->db->last_query();

        $this->load->library('custom_datetime_library');


        if ($users) {
            $i = 0;
            foreach ($users as $a_user) {

                /*date time starts*/
                $users[$i]->cr_on = new stdClass();

                $users[$i]->cr_on->timestamp = $a_user->created_on;

                if ($a_user->created_on == 0) {
                    $users[$i]->cr_on->display = $this->lang->line('unavailable_text');
                } else {
                    $users[$i]->cr_on->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDateAndTime($a_user->created_on);
                }

                /*date time ends*/

                $users[$i]->concluded_thrifts = $this->Report_model->countConcludedThrifts($a_user->user_id);

                /*name starts*/
                $users[$i]->name = $a_user->first_name . ' ' . $a_user->last_name;
                /*name ends*/

                /*org starts*/
                $users[$i]->org = "<span style='font-style: italic; !important'>Non-organizational thrifter</span>";

                if ($a_user->user_employer_id != null && $a_user->user_employer_id > 0) {
                    $employer = $this->Report_model->getUserInfo($a_user->user_employer_id);

                    $users[$i]->org = $employer->company;
                }
                /*org ends*/

                /*active - inactive starts*/
                $users[$i]->act = new stdClass();
                $users[$i]->act->int = $a_user->active;

                if ($a_user->active == 1) {

                    $status_span = '<span class = "label label-primary">' . $this->lang->line('status_active_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_deactivate_text');
                    $status_url = base_url() . 'users/auth/deactivateUser/' . $a_user->id;
                    $status_anchor_span = '<span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';

                } else {
                    $status_span = '<span class = "label label-default">' . $this->lang->line('status_inactive_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_activate_text');
                    $status_url = base_url() . 'users/auth/activateUser/' . $a_user->id;
                    $status_anchor_span = '<span class="label label-success"><i class="fa fa-check" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';
                }

                $users[$i]->act->html = $status_span;


                /*active - inactive ends*/

                /*action starts*/


                $payment_report_tooltip = $this->lang->line('tooltip_payment_report_text');
                $payment_report_url = base_url() . 'report_module/show_report/employee_report/payment_report/all/' . $a_user->id;
                $payment_report_anchor =
                    '<a ' . ' title="' . $payment_report_tooltip . '" ' . ' href="' . $payment_report_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-list-alt fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $payment_recieve_report_tooltip = $this->lang->line('tooltip_payment_recieve_report_text');
                $payment_recieve_report_url = base_url() . 'report_module/show_report/employee_report/payment_recieve_report/all/' . $a_user->id;
                $payment_recieve_report_anchor =
                    '<a ' . ' title="' . $payment_recieve_report_tooltip . '" ' . ' href="' . $payment_recieve_report_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-file-text-o fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $users[$i]->action = $payment_report_anchor . '&nbsp;&nbsp;' . $payment_recieve_report_anchor;

                $i++;

            }
        }


        $json_data['draw'] = intval($requestData['draw']);
        /* $totalData: for every request/draw by clientside ,
         they send a number as a parameter, when they recieve a response/data they first check the draw number,
         so we are sending same number in draw.*/
        $json_data['recordsTotal'] = intval($totalData); // total number of records after searching, if there is no searching then totalFiltered = totalData
        $json_data['recordsFiltered'] = intval($totalFiltered);

        //$users = $this->removeKeys($users); // converting to numeric indices.
        $json_data['data'] = $users;

        // checking requests in console.log() for testing starts;
        /*$json_data['last_query'] = $last_query;
        $json_data['common_filter_value'] = $common_filter_value;
        $json_data['specific_filters'] = $specific_filters;
        $json_data['order_column'] = $order['column'];
        $json_data['order_by'] = $order['by'];
        $json_data['limit_length'] = $limit['length'];
        $json_data['limit_start'] = $limit['start'];*/
        // checking requests in console.log() for testing ends;

        echo(json_encode($json_data));

    }

    private function employeeReportFile($report_format, $which_employees)
    {

        $this->lang->load('employee_list');

        $users = array();

        $which_employees = $_REQUEST['which_employees'];

        $employer_id = false;

        if ($which_employees == 'my') {
            $employer_id = $this->session->userdata('user_id');
        }

        $order['column'] = 'created_on';
        $order['by'] = 'desc';


        $users = $this->Report_model->getEmployees($common_filter_value = false, $specific_filters = false, $order, $limit = false, $employer_id);

        if ($users == false || empty($users) || $users == null) {
            $users = false;
        }

        $last_query = $this->db->last_query();

        $this->load->library('custom_datetime_library');


        if ($users) {
            $i = 0;
            foreach ($users as $a_user) {

                /*date time starts*/
                $users[$i]->cr_on = new stdClass();

                $users[$i]->cr_on->timestamp = $a_user->created_on;

                if ($a_user->created_on == 0) {
                    $users[$i]->cr_on->display = $this->lang->line('unavailable_text');
                } else {
                    $users[$i]->cr_on->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDateAndTime($a_user->created_on);
                }

                /*date time ends*/

                $users[$i]->concluded_thrifts = $this->Report_model->countConcludedThrifts($a_user->user_id);

                /*name starts*/
                $users[$i]->name = $a_user->first_name . ' ' . $a_user->last_name;
                /*name ends*/

                /*org starts*/
                $users[$i]->org = "Non-organizational thrifter";

                if ($a_user->user_employer_id != null && $a_user->user_employer_id > 0) {
                    $employer = $this->Report_model->getUserInfo($a_user->user_employer_id);

                    $users[$i]->org = $employer->company;
                }
                /*org ends*/

                /*active - inactive starts*/
                $users[$i]->act = new stdClass();
                $users[$i]->act->int = $a_user->active;

                if ($a_user->active == 1) {

                    $status_span = '<span class = "label label-primary">' . $this->lang->line('status_active_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_deactivate_text');
                    $status_url = base_url() . 'users/auth/deactivateUser/' . $a_user->id;
                    $status_anchor_span = '<span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';

                } else {
                    $status_span = '<span class = "label label-default">' . $this->lang->line('status_inactive_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_activate_text');
                    $status_url = base_url() . 'users/auth/activateUser/' . $a_user->id;
                    $status_anchor_span = '<span class="label label-success"><i class="fa fa-check" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';
                }

                $users[$i]->act->html = $status_span;


                /*active - inactive ends*/

                /*action starts*/


                $payment_report_tooltip = $this->lang->line('tooltip_payment_report_text');
                $payment_report_url = base_url() . 'report_module/show_report/employee_report/payment_report/all/' . $a_user->id;
                $payment_report_anchor =
                    '<a ' . ' title="' . $payment_report_tooltip . '" ' . ' href="' . $payment_report_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-list-alt fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $payment_recieve_report_tooltip = $this->lang->line('tooltip_payment_recieve_report_text');
                $payment_recieve_report_url = base_url() . 'report_module/show_report/employee_report/payment_recieve_report/all/' . $a_user->id;
                $payment_recieve_report_anchor =
                    '<a ' . ' title="' . $payment_recieve_report_tooltip . '" ' . ' href="' . $payment_recieve_report_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-file-text-o fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $users[$i]->action = $payment_report_anchor . '&nbsp;&nbsp;' . $payment_recieve_report_anchor;

                $i++;

            }
        }

        $site_logo = $this->custom_settings_library->getASettingsValue('general_settings', 'site_logo');

        $data['author'] = '';
        $site_name = $this->custom_settings_library->getASettingsValue('general_settings', 'site_name');

        if ($site_name) {
            $data['author'] = $site_name;
        }

        $data['users'] = $users;


        $data['site_name'] = $site_name;
        $data['site_logo'] = $site_logo;


        if ($report_format == 'pdf') {
            $this->genEmployeePdf($data);
        }

        if ($report_format == 'excel') {
            $this->genEmployeeExcel($data);
        }


    }

    private function genEmployeePdf($data)
    {
        $mpdf = new mPDF('win-1252', 'A4-L', '', '', 5, 5, 16, 60, 5, 5, 'L');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Member Report');
        $mpdf->SetAuthor($data['author']);
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->debug = true;


        /*do not load header,common left footer as this view contains the whole page*/
        $html = $this->load->view('report_module/employee_report_pdf_page', $data, TRUE);

        //echo $html;die();

        $name = 'members' . rand(10000, 99999) . '.pdf';

        //echo  $link;die();
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'D');

        exit;
    }

    private function genEmployeeExcel($data)
    {

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Member');

        $arr = array();

        $this->excel->getActiveSheet()->setCellValue('A1', $this->lang->line('column_created_on_text'));
        $this->excel->getActiveSheet()->setCellValue('B1', $this->lang->line('column_name_text'));
        $this->excel->getActiveSheet()->setCellValue('C1', $this->lang->line('column_mem_id_num_text'));
        $this->excel->getActiveSheet()->setCellValue('D1', $this->lang->line('column_email_text'));
        $this->excel->getActiveSheet()->setCellValue('E1', $this->lang->line('column_company_text'));
        $this->excel->getActiveSheet()->setCellValue('F1', $this->lang->line('column_concluded_thrifts_text'));

        if ($data['users']) {
            $i = 0;
            foreach ($data['users'] as $u) {
                $arr[$i][0] = $u->cr_on->display;
                $arr[$i][1] = $u->name;
                $arr[$i][2] = $u->mem_id_num;
                $arr[$i][3] = $u->email;

                $arr[$i][4] = "Non-organizational thrifter";

                if ($u->user_employer_id != null && $u->user_employer_id > 0) {
                    $employer = $this->Report_model->getUserInfo($u->user_employer_id);

                    if ($employer) {
                        $arr[$i][4] = $employer->company;
                    }
                }

                $arr[$i][4] = $u->org;
                $arr[$i][5] = $u->concluded_thrifts;


                $i++;
            }

        }


        // read data to active sheet
        $this->excel->getActiveSheet()->fromArray($arr, null, 'A2');


        $filename = $data['which_report'] . rand(10000, 99999) . '.xlsx'; //save our workbook as this file name

        header('Content-Type: application/vnd.ms-excel'); //mime type

        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name

        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as.XLSX Excel 2007 format

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        exit;
    }

    public function showReport()
    {
        $rpt_fmt = $this->uri->segment(2);
        $which_report = $this->uri->segment(3);
        $payment_or_recieve = $this->uri->segment(4);
        $time_range = $this->uri->segment(5);
        $employer_id = false;
        $employee_id = false;
        $static_range_date = false;
        $month_or_year = false;

        $report_format = '';
        if ($rpt_fmt == 'show_report') {
            $report_format = 'none';
        } else if ($rpt_fmt == 'show_report_pdf') {
            $report_format = 'pdf';
        } else if ($rpt_fmt == 'show_report_excel') {
            $report_format = 'excel';
        }


        if ($time_range != 'all') {

            if (strpos($time_range, '-') === false) {
                $month_or_year = 'year';
            } else {
                $month_or_year = 'month';
            }

            if ($month_or_year == 'year') {
                $static_range_date = $time_range . '-01-01-00-00-00' . 'to' . $time_range . '-12-31-23-59-59';
            }

            if ($month_or_year == 'month') {

                $exploded_time_range = explode('-', $time_range);

                $y = (int)$exploded_time_range[0];
                $m = (int)$exploded_time_range[1];

                $arr_31 = [1, 3, 5, 7, 8, 10, 12];
                $arr_30 = [4, 6, 9, 11];

                $d = '30';

                //how many days in a month ?
                if (in_array($m, $arr_31)) {
                    $d = '31';
                } else if (in_array($m, $arr_30)) {
                    $d = '30';
                } else if ($m == 2) {

                    //leap year
                    if ($y % 4 == 0 && ($y % 100 != 0)) {
                        $d = '29';
                    } else {
                        $d = '28';
                    }

                }

                $leading_zero = '';

                if ($m < 10) {
                    $leading_zero = '0';
                }

                $static_range_date = $y . '-' . $leading_zero . $m . '-01-00-00-00' . 'to' . $y . '-' . $leading_zero . $m . '-' . $d . '-23-59-59';
            }

        }

        if ($which_report == 'all_report') {

            if (!($this->ion_auth->is_admin() || $this->ion_auth->in_group('trustee'))) {
                redirect('users/auth/need_permission');
            }

        }

        if ($which_report == 'employer_report' && $this->uri->segment(6)) {

            if (!($this->ion_auth->is_admin() || $this->ion_auth->in_group('trustee'))) {
                redirect('users/auth/need_permission');
            }
            $employer_id = $this->uri->segment(6);
        }

        if ($which_report == 'employee_report' && $this->uri->segment(6)) {

            if (!($this->ion_auth->is_admin() || $this->ion_auth->in_group('trustee'))) {
                redirect('users/auth/need_permission');
            }
            $employee_id = $this->uri->segment(6);
        }


        if ($which_report == 'employer_report_as_employer') {
            if (!$this->ion_auth->in_group('employer')) {
                redirect('users/auth/does_not_exist');
            } else {
                $employer_id = $this->session->userdata('user_id');
            }


        }

        if ($which_report == 'employee_report_as_employee') {
            if (!$this->ion_auth->in_group('employee')) {
                redirect('users/auth/does_not_exist');
            } else {
                $employee_id = $this->session->userdata('user_id');
            }
        }

        if ($payment_or_recieve == 'payment_report') {

            $this->getPaymentReport($report_format, $which_report, $employer_id, $employee_id, $static_range_date);

        }

        if ($payment_or_recieve == 'payment_recieve_report') {

            $this->getPaymentRecieveReport($report_format, $which_report, $employer_id, $employee_id, $static_range_date);

        }

        if (($report_format == 'pdf' || $report_format == 'excel') && $payment_or_recieve == 'payment_with_recieve_report') {

            $this->payment_with_recieve_ReportFile($report_format, $which_report, $employer_id, $employee_id, $static_range_date);
            exit;
        }

        if (($report_format == 'pdf' || $report_format == 'excel') && $payment_or_recieve == 'payment_grouped_report') {

            $this->paymentGroupedReportFile($report_format, $which_report, $employer_id, $employee_id, $static_range_date);
            exit;
        }

        if (($report_format == 'pdf' || $report_format == 'excel') && $payment_or_recieve == 'payment_recieve_grouped_report') {

            $this->paymentRecieveGroupedReportFile($report_format, $which_report, $employer_id, $employee_id, $static_range_date);
            exit;
        }


    }

    private function getPaymentReport($report_format, $which_report, $employer_id, $employee_id, $static_range_date)
    {
        $this->lang->load('payment_report');

        $data['report_format'] = $report_format;
        $data['which_report'] = $which_report;
        $data['employer_id'] = $employer_id;
        $data['employee_id'] = $employee_id;
        $data['static_range_date'] = $static_range_date;

        $employer = false;
        $employee = false;

        if ($employer_id) {
            $employer = $this->Report_model->getUserInfo($employer_id);
        }

        if ($employee_id) {
            $employee = $this->Report_model->getUserInfo($employee_id);
        }

        $data['employer'] = $employer;
        $data['employee'] = $employee;

        $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $link = '/';
        if (strpos($actual_link, 'show_report_pdf') !== false) { // if $link contains show_report_pdf
            $link = str_replace('show_report_pdf', 'show_report', $actual_link);

        } else {
            $link = $actual_link;
        }

        $data['link'] = $link;

        if ($report_format == 'pdf' || $report_format == 'excel') {
            $this->paymentReportFile($link, $report_format, $which_report, $employer_id, $employee_id, $static_range_date);
            exit;
        }


        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("report_module/payment_report_page", $data);
        $this->load->view("common_module/footer");

    }


    public function getPaymentReportByAjax()
    {
        $this->lang->load('payment_report');

        $payments = array();

        $date_range = false;
        $date_from = false;
        $date_to = false;

        $which_report = $_REQUEST['which_report'];
        $employee_id = $_REQUEST['employee_id'];
        $employer_id = $_REQUEST['employer_id'];
        $static_range_date = $_REQUEST['static_range_date'];


        $user_id = $this->session->userdata('user_id');

        $requestData = $_REQUEST;
        //print_r($requestData);

        $columns[0] = 'thrift_group_number'; //tg
        $columns[1] = 'thrift_group_payer_employer_id'; //not sql table col
        $columns[2] = 'thrift_group_payment_amount'; //tp
        $columns[3] = 'thrift_group_payer_member_id'; //tp
        $columns[4] = 'thrift_group_payment_number'; //tp
        $columns[5] = 'thrift_group_payee_member_id';//tp
        $columns[6] = 'thrift_group_payment_date'; //tp
        $columns[7] = 'thrift_group_start_date'; // tg
        $columns[8] = 'thrift_group_end_date'; //tg
        $columns[9] = 'date_range'; //not sql table col


        if (!empty($requestData['columns'][9]['search']['value'])) {
            $date_range = $requestData['columns'][9]['search']['value'];
        }

        //overwrites datepicker
        if ($static_range_date) {
            $date_range = false;
        }

        if ($date_range) {
            $date_range_exploded = explode('to', $date_range);

            $date_from_string = $date_range_exploded[0];
            $date_to_string = $date_range_exploded[1];

            $date_from_string .= '-00-00-00';
            $date_to_string .= '-23-59-59';

            $date_from =
                $this->
                custom_datetime_library->
                convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($date_from_string, 'Y-m-d-H-i-s');

            $date_to =
                $this->
                custom_datetime_library->
                convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($date_to_string, 'Y-m-d-H-i-s');

        }

        if ($static_range_date) {

            $date_range_exploded = explode('to', $static_range_date);

            $date_from_string = $date_range_exploded[0];
            $date_to_string = $date_range_exploded[1];

            $date_from =
                $this->
                custom_datetime_library->
                convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($date_from_string, 'Y-m-d-H-i-s');

            $date_to =
                $this->
                custom_datetime_library->
                convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($date_to_string, 'Y-m-d-H-i-s');

        }


        $common_filter_value = false;
        $order_column = false;

        $specific_filters = array();
        $specific_filters = false;

        if (!empty($requestData['columns'][0]['search']['value'])) {
            $specific_filters['thrift_group_number'] = $requestData['columns'][0]['search']['value'];
        }

        if (!empty($requestData['columns'][1]['search']['value'])) {
            $specific_filters['thrift_group_payer_employer_id'] = $requestData['columns'][1]['search']['value'];
        }

        if (!empty($requestData['columns'][2]['search']['value'])) {
            $specific_filters['thrift_group_payment_amount'] = $requestData['columns'][2]['search']['value'];
        }

        if (!empty($requestData['columns'][3]['search']['value'])) {
            $specific_filters['thrift_group_payer_member_id'] = $requestData['columns'][3]['search']['value'];
        }

        if (!empty($requestData['columns'][4]['search']['value'])) {
            $specific_filters['thrift_group_payment_number'] = $requestData['columns'][4]['search']['value'];
        }

        if (!empty($requestData['columns'][5]['search']['value'])) {
            $specific_filters['thrift_group_payee_member_id'] = $requestData['columns'][5]['search']['value'];
        }

        if (!empty($requestData['columns'][6]['search']['value'])) {
            $specific_filters['thrift_group_payment_date'] = $requestData['columns'][6]['search']['value'];
        }

        if (!empty($requestData['columns'][7]['search']['value'])) {
            $specific_filters['thrift_group_start_date'] = $requestData['columns'][7]['search']['value'];
        }

        if (!empty($requestData['columns'][8]['search']['value'])) {
            $specific_filters['thrift_group_end_date'] = $requestData['columns'][8]['search']['value'];
        }


        if (!empty($requestData['search']['value'])) {
            $common_filter_value = $requestData['search']['value'];
        }

        if ($specific_filters == true || !empty($specific_filters)) {
            $common_filter_value = false;       //either search with specific filters or with common filter
        }

        $order['column'] = $columns[$requestData['order'][0]['column']];
        $order['by'] = $requestData['order'][0]['dir'];


        $limit['start'] = $requestData['start'];
        $limit['length'] = $requestData['length'];

        $totalData =
            $this->
            Report_model->
            countPaymentReport(false, false, $which_report, $employer_id, $employee_id, $date_from, $date_to);

        if ($common_filter_value == true || $specific_filters == true) {

            $totalFiltered =
                $this->
                Report_model->
                countPaymentReport($common_filter_value, $specific_filters, $which_report, $employer_id, $employee_id, $date_from, $date_to);
        } else {
            $totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
        }

        $payments =
            $this->
            Report_model->
            getPaymentReport($common_filter_value, $specific_filters, $order, $limit, $which_report, $employer_id, $employee_id, $date_from, $date_to);

        if ($payments == false || empty($payments) || $payments == null) {
            $payments = false;
        }

        $last_query = $this->db->last_query();


        $this->load->library('custom_datetime_library');


        if ($payments) {
            $i = 0;
            foreach ($payments as $a_payment) {

                $payments[$i]->date_range = 'mock_column';

                /*payer-payee-payer_employer starts*/

                $payments[$i]->thrift_group_payer_employer_name = "<span style='font-style: italic; !important'>Non-organizational thrifter</span>";
                $payments[$i]->thrift_group_payer_name = $this->lang->line('unavailable_text');
                $payments[$i]->thrift_group_payee_name = "<span style='font-style: italic; !important'>Individual Savings</span>";


                if ($payments[$i]->thrift_group_payer_member_id != null && $payments[$i]->thrift_group_payer_member_id != 0) {

                    $payer = $this->Report_model->getUserInfo($payments[$i]->thrift_group_payer_member_id);

                    if ($payer) {
                        if($this->ion_auth->is_admin($payer->id)){
                            $payments[$i]->thrift_group_payer_name = "Prosperis";
                        }else{
                            $payments[$i]->thrift_group_payer_name = $payer->first_name.' '.$payer->last_name;
                        }

                    }
                }

                if ($payments[$i]->thrift_group_payee_member_id != null && $payments[$i]->thrift_group_payee_member_id != 0) {

                    $payee = $this->Report_model->getUserInfo($payments[$i]->thrift_group_payee_member_id);

                    if ($payee) {
                        if($this->ion_auth->is_admin($payee->id)){
                            $payments[$i]->thrift_group_payee_name = "Prosperis";
                        }else{
                            $payments[$i]->thrift_group_payee_name = $payee->first_name.' '.$payee->last_name;
                        }

                    }
                }

                if ($payments[$i]->thrift_group_payer_member_id != null && $payments[$i]->thrift_group_payer_member_id != 0) {

                    $payer = $this->Report_model->getUserInfo($payments[$i]->thrift_group_payer_member_id);

                    $payer_employer = false;
                    if ($payer->user_employer_id != null && $payer->user_employer_id != 0) {
                        $payer_employer = $this->Report_model->getUserInfo($payer->user_employer_id);
                    }

                    if ($payer_employer) {
                        $payments[$i]->thrift_group_payer_employer_name = $payer_employer->company;
                    }
                }
                /*payer-payee-payer_employer ends*/


                /*amount starts*/

                /*amount starts*/


                $payments[$i]->p_amt = new stdClass();

                $payments[$i]->p_amt->display = $this->getCurrencySign() .
                    number_format($payments[$i]->thrift_group_payment_amount, 2, '.', ',');

                $payments[$i]->p_amt->val = $payments[$i]->thrift_group_payment_amount;
                /*amount ends*/


                /*date time starts*/
                $payments[$i]->tg_st_dt = new stdClass();
                $payments[$i]->tg_en_dt = new stdClass();
                $payments[$i]->tp_p_dt = new stdClass();


                $payments[$i]->tg_st_dt->timestamp = $a_payment->thrift_group_start_date;
                $payments[$i]->tg_en_dt->timestamp = $a_payment->thrift_group_end_date;
                $payments[$i]->tp_p_dt->timestamp = $a_payment->thrift_group_payment_date;


                if ($a_payment->thrift_group_start_date == 0) {
                    $payments[$i]->tg_st_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tg_st_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_start_date);
                }

                if ($a_payment->thrift_group_end_date == 0) {
                    $payments[$i]->tg_en_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tg_en_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_end_date);
                }

                if ($a_payment->thrift_group_payment_date == 0) {
                    $payments[$i]->tp_p_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tp_p_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_payment_date);
                }

                /*date time ends*/

                $i++;

            }
        }


        $total_payment_amount =
            $this->
            Report_model->
            getTotalPayment($common_filter_value, $specific_filters, $which_report, $employer_id, $employee_id, $date_from, $date_to);

        if ($total_payment_amount) {
            $json_data['total_payment_amount'] = $total_payment_amount->total_payment_amount;
            $json_data['total_payment_amount_display'] = $this->getCurrencySign() .
                number_format($total_payment_amount->total_payment_amount, 2, '.', ',');
        } else {
            $json_data['total_payment_amount'] = 0.00;
            $json_data['total_payment_amount_display'] = 0.00;
        }


        $json_data['draw'] = intval($requestData['draw']);
        /* $totalData: for every request/draw by clientside ,
         they send a number as a parameter, when they recieve a response/data they first check the draw number,
         so we are sending same number in draw.*/
        $json_data['recordsTotal'] = intval($totalData); // total number of records after searching, if there is no searching then totalFiltered = totalData
        $json_data['recordsFiltered'] = intval($totalFiltered);

        //$payments = $this->removeKeys($payments); // converting to numeric indices.
        $json_data['data'] = $payments;

        // checking requests in console.log() for testing starts;
        /*$json_data['last_query'] = $last_query;
        $json_data['common_filter_value'] = $common_filter_value;
        $json_data['specific_filters'] = $specific_filters;
        $json_data['order_column'] = $order['column'];
        $json_data['order_by'] = $order['by'];
        $json_data['limit_length'] = $limit['length'];
        $json_data['limit_start'] = $limit['start'];*/
        // checking requests in console.log() for testing ends;


        echo(json_encode($json_data));

    }

    private function paymentReportFile($link, $report_format, $which_report, $employer_id, $employee_id, $static_range_date)
    {
        $this->lang->load('payment_report');

        $employer = false;
        $employee = false;

        if ($employer_id) {
            $employer = $this->Report_model->getUserInfo($employer_id);
        }

        if ($employee_id) {
            $employee = $this->Report_model->getUserInfo($employee_id);
        }

        $data = array();
        $data['which_report'] = $which_report;

        $data['employer'] = $employer;
        $data['employee'] = $employee;


        $this->lang->load('payment_report');

        $payments = array();

        $date_range = false;
        $date_from = false;
        $date_to = false;

        $user_id = $this->session->userdata('user_id');


        if ($static_range_date) {

            $date_range_exploded = explode('to', $static_range_date);

            $date_from_string = $date_range_exploded[0];
            $date_to_string = $date_range_exploded[1];

            $date_from =
                $this->
                custom_datetime_library->
                convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($date_from_string, 'Y-m-d-H-i-s');

            $date_to =
                $this->
                custom_datetime_library->
                convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($date_to_string, 'Y-m-d-H-i-s');

        }


        $order['column'] = 'thrift_group_payment_date';
        $order['by'] = 'asc';

        $payments =
            $this->
            Report_model->
            getPaymentReport($common_filter_value = false, $specific_filters = false, $order, $limit = false, $which_report, $employer_id, $employee_id, $date_from, $date_to);

        if ($payments == false || empty($payments) || $payments == null) {
            $payments = false;
        }

        $last_query = $this->db->last_query();


        $this->load->library('custom_datetime_library');


        if ($payments) {
            $i = 0;
            foreach ($payments as $a_payment) {

                $payments[$i]->date_range = 'mock_column';

                /*payer-payee-payer_employer starts*/

                $payments[$i]->thrift_group_payer_employer_name = "Non-organizational thrifter";
                $payments[$i]->thrift_group_payer_name = $this->lang->line('unavailable_text');
                $payments[$i]->thrift_group_payee_name = "Individual Savings";


                if ($payments[$i]->thrift_group_payer_member_id != null && $payments[$i]->thrift_group_payer_member_id != 0) {

                    $payer = $this->Report_model->getUserInfo($payments[$i]->thrift_group_payer_member_id);

                    if ($payer) {
                        if($this->ion_auth->is_admin($payer->id)){
                            $payments[$i]->thrift_group_payer_name = "Prosperis";
                        }else{
                            $payments[$i]->thrift_group_payer_name = $payer->first_name.' '.$payer->last_name;
                        }

                    }
                }

                if ($payments[$i]->thrift_group_payee_member_id != null && $payments[$i]->thrift_group_payee_member_id != 0) {

                    $payee = $this->Report_model->getUserInfo($payments[$i]->thrift_group_payee_member_id);

                    if ($payee) {
                        if($this->ion_auth->is_admin($payee->id)){
                            $payments[$i]->thrift_group_payee_name = "Prosperis";
                        }else{
                            $payments[$i]->thrift_group_payee_name = $payee->first_name.' '.$payee->last_name;
                        }

                    }
                }

                if ($payments[$i]->thrift_group_payer_member_id != null && $payments[$i]->thrift_group_payer_member_id != 0) {

                    $payer = $this->Report_model->getUserInfo($payments[$i]->thrift_group_payer_member_id);

                    $payer_employer = false;
                    if ($payer->user_employer_id != null && $payer->user_employer_id != 0) {
                        $payer_employer = $this->Report_model->getUserInfo($payer->user_employer_id);
                    }

                    if ($payer_employer) {
                        $payments[$i]->thrift_group_payer_employer_name = $payer_employer->company;
                    }
                }
                /*payer-payee-payer_employer ends*/


                /*amount starts*/

                /*amount starts*/


                $payments[$i]->p_amt = new stdClass();

                $payments[$i]->p_amt->display = $this->getCurrencySign() .
                    number_format($payments[$i]->thrift_group_payment_amount, 2, '.', ',');

                $payments[$i]->p_amt->display_without_sign = number_format($payments[$i]->thrift_group_payment_amount, 2, '.', ',');

                $payments[$i]->p_amt->val = $payments[$i]->thrift_group_payment_amount;
                /*amount ends*/


                /*date time starts*/
                $payments[$i]->tg_st_dt = new stdClass();
                $payments[$i]->tg_en_dt = new stdClass();
                $payments[$i]->tp_p_dt = new stdClass();


                $payments[$i]->tg_st_dt->timestamp = $a_payment->thrift_group_start_date;
                $payments[$i]->tg_en_dt->timestamp = $a_payment->thrift_group_end_date;
                $payments[$i]->tp_p_dt->timestamp = $a_payment->thrift_group_payment_date;


                if ($a_payment->thrift_group_start_date == 0) {
                    $payments[$i]->tg_st_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tg_st_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_start_date);
                }

                if ($a_payment->thrift_group_end_date == 0) {
                    $payments[$i]->tg_en_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tg_en_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_end_date);
                }

                if ($a_payment->thrift_group_payment_date == 0) {
                    $payments[$i]->tp_p_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tp_p_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_payment_date);
                }

                /*date time ends*/

                $i++;

            }
        }

        $total_payment_amount = 0.00;

        $total_payment_amount =
            $this->
            Report_model->
            getTotalPayment($common_filter_value = false, $specific_filters = false, $which_report, $employer_id, $employee_id, $date_from, $date_to);


        $site_logo = $this->custom_settings_library->getASettingsValue('general_settings', 'site_logo');

        $data['author'] = '';
        $site_name = $this->custom_settings_library->getASettingsValue('general_settings', 'site_name');

        if ($site_name) {
            $data['author'] = $site_name;
        }

        $data['payments'] = $payments;
        $data['total_payment_amount'] = $total_payment_amount;
        $data['total_payment_amount_display'] = $this->getCurrencySign() .
            number_format($total_payment_amount->total_payment_amount, 2, '.', ',');
        $data['total_payment_amount_display_without_sign'] = number_format($total_payment_amount->total_payment_amount, 2, '.', ',');


        $data['site_name'] = $site_name;
        $data['site_logo'] = $site_logo;
        $data['which_report'] = $which_report;

        if ($report_format == 'pdf') {
            $this->genPaymentPdf($data);
        }


        if ($report_format == 'excel') {
            $this->genPaymentExcel($data);
        }


    }

    private function genPaymentPdf($data)
    {
        $mpdf = new mPDF('win-1252', 'A4-L', '', '', 5, 5, 16, 60, 5, 5, 'L');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Payment Report');
        $mpdf->SetAuthor($data['author']);
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->debug = true;


        /*do not load header,common left footer as this view contains the whole page*/
        $html = $this->load->view('report_module/payment_report_pdf_page', $data, TRUE);

        //echo $html;die();

        $name = $data['which_report'] . rand(10000, 99999) . '.pdf';

        //echo  $link;die();
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'D');

        exit;
    }

    private function genPaymentExcel($data)
    {

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Payment Report');

        $arr = array();


        $this->excel->getActiveSheet()->setCellValue('A1', $this->lang->line('thrift_group_number_text'));
        $this->excel->getActiveSheet()->setCellValue('B1', $this->lang->line('thrift_group_employer_text'));
        $this->excel->getActiveSheet()->setCellValue('C1', $this->lang->line('thrift_group_payment_amount_text'));
        $this->excel->getActiveSheet()->setCellValue('D1', $this->lang->line('thrift_group_payer_member_text'));
        $this->excel->getActiveSheet()->setCellValue('E1', $this->lang->line('thrift_group_payee_member_text'));
        $this->excel->getActiveSheet()->setCellValue('F1', $this->lang->line('thrift_group_payment_date_text'));
        $this->excel->getActiveSheet()->setCellValue('G1', $this->lang->line('thrift_group_payment_amount_text'));
        $this->excel->getActiveSheet()->setCellValue('H1', $this->lang->line('thrift_group_start_date_text'));
        $this->excel->getActiveSheet()->setCellValue('I1', $this->lang->line('thrift_group_end_date_text'));

        if ($data['payments']) {
            $i = 0;
            foreach ($data['payments'] as $p) {

                $arr[$i][0] = $p->thrift_group_number;
                $arr[$i][1] = $p->thrift_group_payer_employer_name;
                $arr[$i][2] = $p->p_amt->display_without_sign;
                $arr[$i][3] = $p->thrift_group_payer_name;
                $arr[$i][4] = $p->thrift_group_payment_number;
                $arr[$i][5] = $p->thrift_group_payee_name;
                $arr[$i][6] = $p->tp_p_dt->display;
                $arr[$i][7] = $p->tg_en_dt->display;
                $arr[$i][8] = $p->tg_st_dt->display;


                $i++;
            }


            $arr[$i + 1][0] = '';
            $arr[$i + 1][1] = '';
            $arr[$i + 1][2] = '';
            $arr[$i + 1][3] = '';
            $arr[$i + 1][4] = '';
            $arr[$i + 1][5] = '';
            $arr[$i + 1][6] = '';
            $arr[$i + 1][7] = '';
            $arr[$i + 1][8] = '';
            $arr[$i + 1][9] = '';

            $arr[$i + 2][0] = '';
            $arr[$i + 2][1] = '';
            $arr[$i + 2][2] = '';
            $arr[$i + 2][3] = '';
            $arr[$i + 2][4] = '';
            $arr[$i + 2][5] = '';
            $arr[$i + 2][6] = '';
            $arr[$i + 2][7] = '';
            $arr[$i + 2][8] = $this->lang->line('total_text');
            $arr[$i + 2][9] = $data['total_payment_amount_display_without_sign'];
        }


        // read data to active sheet
        $this->excel->getActiveSheet()->fromArray($arr, null, 'A2');


        $filename = $data['which_report'] . rand(10000, 99999) . '.xlsx'; //save our workbook as this file name

        header('Content-Type: application/vnd.ms-excel'); //mime type

        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name

        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as.XLSX Excel 2007 format

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        exit;
    }


    private function payment_with_recieve_ReportFile($report_format, $which_report, $employer_id, $employee_id, $static_range_date)
    {

        $this->lang->load('payment_report');

        $employer = false;
        $employee = false;

        if ($employer_id) {
            $employer = $this->Report_model->getUserInfo($employer_id);
        }

        if ($employee_id) {
            $employee = $this->Report_model->getUserInfo($employee_id);
        }

        $data = array();
        $data['which_report'] = $which_report;

        $data['employer'] = $employer;
        $data['employee'] = $employee;


        $this->lang->load('payment_report');

        $payments = array();

        $date_range = false;
        $date_from = false;
        $date_to = false;

        $user_id = $this->session->userdata('user_id');


        if ($static_range_date) {

            $date_range_exploded = explode('to', $static_range_date);

            $date_from_string = $date_range_exploded[0];
            $date_to_string = $date_range_exploded[1];

            $date_from =
                $this->
                custom_datetime_library->
                convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($date_from_string, 'Y-m-d-H-i-s');

            $date_to =
                $this->
                custom_datetime_library->
                convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($date_to_string, 'Y-m-d-H-i-s');

        }


        $order['column'] = 'thrift_group_payment_date';
        $order['by'] = 'asc';

        $payments =
            $this->
            Report_model->
            getPaymentReport($common_filter_value = false, $specific_filters = false, $order, $limit = false, $which_report, $employer_id, $employee_id, $date_from, $date_to);

        if ($payments == false || empty($payments) || $payments == null) {
            $payments = false;
        }


        $this->load->library('custom_datetime_library');


        if ($payments) {
            $i = 0;
            foreach ($payments as $a_payment) {

                $payments[$i]->date_range = 'mock_column';

                /*payer-payee-payer_employer starts*/

                $payments[$i]->thrift_group_payer_employer_name = "Non-organizational thrifter>";
                $payments[$i]->thrift_group_payer_name = $this->lang->line('unavailable_text');
                $payments[$i]->thrift_group_payee_name = "Individual Savings";


                if ($payments[$i]->thrift_group_payer_member_id != null && $payments[$i]->thrift_group_payer_member_id != 0) {

                    $payer = $this->Report_model->getUserInfo($payments[$i]->thrift_group_payer_member_id);

                    if ($payer) {
                        if($this->ion_auth->is_admin($payer->id)){
                            $payments[$i]->thrift_group_payer_name = "Prosperis";
                        }else{
                            $payments[$i]->thrift_group_payer_name = $payer->first_name.' '.$payer->last_name;
                        }

                    }
                }

                if ($payments[$i]->thrift_group_payee_member_id != null && $payments[$i]->thrift_group_payee_member_id != 0) {

                    $payee = $this->Report_model->getUserInfo($payments[$i]->thrift_group_payee_member_id);

                    if ($payee) {
                        if($this->ion_auth->is_admin($payee->id)){
                            $payments[$i]->thrift_group_payee_name = "Prosperis";
                        }else{
                            $payments[$i]->thrift_group_payee_name = $payee->first_name.' '.$payee->last_name;
                        }

                    }
                }

                if ($payments[$i]->thrift_group_payer_member_id != null && $payments[$i]->thrift_group_payer_member_id != 0) {

                    $payer = $this->Report_model->getUserInfo($payments[$i]->thrift_group_payer_member_id);

                    $payer_employer = false;
                    if ($payer->user_employer_id != null && $payer->user_employer_id != 0) {
                        $payer_employer = $this->Report_model->getUserInfo($payer->user_employer_id);
                    }

                    if ($payer_employer) {
                        $payments[$i]->thrift_group_payer_employer_name = $payer_employer->company;
                    }
                }
                /*payer-payee-payer_employer ends*/


                /*amount starts*/

                /*amount starts*/


                $payments[$i]->p_amt = new stdClass();

                $payments[$i]->p_amt->display = $this->getCurrencySign() .
                    number_format($payments[$i]->thrift_group_payment_amount, 2, '.', ',');

                $payments[$i]->p_amt->display_without_sign = number_format($payments[$i]->thrift_group_payment_amount, 2, '.', ',');

                $payments[$i]->p_amt->val = $payments[$i]->thrift_group_payment_amount;
                /*amount ends*/


                /*date time starts*/
                $payments[$i]->tg_st_dt = new stdClass();
                $payments[$i]->tg_en_dt = new stdClass();
                $payments[$i]->tp_p_dt = new stdClass();


                $payments[$i]->tg_st_dt->timestamp = $a_payment->thrift_group_start_date;
                $payments[$i]->tg_en_dt->timestamp = $a_payment->thrift_group_end_date;
                $payments[$i]->tp_p_dt->timestamp = $a_payment->thrift_group_payment_date;


                if ($a_payment->thrift_group_start_date == 0) {
                    $payments[$i]->tg_st_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tg_st_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_start_date);
                }

                if ($a_payment->thrift_group_end_date == 0) {
                    $payments[$i]->tg_en_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tg_en_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_end_date);
                }

                if ($a_payment->thrift_group_payment_date == 0) {
                    $payments[$i]->tp_p_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tp_p_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_payment_date);
                }

                /*date time ends*/

                $i++;

            }
        }


        $recieves =
            $this->
            Report_model->
            getPaymentRecieveReport($common_filter_value = false, $specific_filters = false, $order, $limit = false, $which_report, $employer_id, $employee_id, $date_from, $date_to);

        if ($recieves == false || empty($recieves) || $recieves == null) {
            $recieves = false;
        }

        if ($recieves) {
            $i = 0;
            foreach ($recieves as $a_recieve) {

                $recieves[$i]->date_range = 'mock_column';

                /*payer-payee-payer_employer starts*/

                $recieves[$i]->thrift_group_member_employer_name = "Non-organizational thrifter";
                $recieves[$i]->thrift_group_member_name = $this->lang->line('unavailable_text');


                if ($recieves[$i]->thrift_group_member_id != null && $recieves[$i]->thrift_group_member_id != 0) {

                    $member = $this->Report_model->getUserInfo($recieves[$i]->thrift_group_member_id);

                    if ($member) {
                        if($this->ion_auth->is_admin($member->id)){
                            $recieves[$i]->thrift_group_member_name = "Prosperis";
                        }else{
                            $recieves[$i]->thrift_group_member_name = $member->first_name.' '.$member->first_name;
                        }

                    }
                }


                if ($recieves[$i]->thrift_group_member_id != null && $recieves[$i]->thrift_group_member_id != 0) {

                    $member = $this->Report_model->getUserInfo($recieves[$i]->thrift_group_member_id);

                    $member_employer = false;
                    if ($member->user_employer_id != null && $member->user_employer_id != 0) {
                        $member_employer = $this->Report_model->getUserInfo($member->user_employer_id);
                    }

                    if ($member_employer) {
                        $recieves[$i]->thrift_group_member_employer_name = $member_employer->company;
                    }
                }
                /*payer-payee-payer_employer ends*/


                /*amount starts*/

                /*amount starts*/


                $recieves[$i]->p_amt = new stdClass();
                $recieves[$i]->p_amt->display = $this->getCurrencySign() .
                    number_format($recieves[$i]->thrift_group_payment_recieve_amount, 2, '.', ',');
                $recieves[$i]->p_amt->display_without_sign = number_format($recieves[$i]->thrift_group_payment_recieve_amount, 2, '.', ',');
                $recieves[$i]->p_amt->val = $recieves[$i]->thrift_group_payment_recieve_amount;
                /*amount ends*/


                /*date time starts*/
                $recieves[$i]->tg_st_dt = new stdClass();
                $recieves[$i]->tg_en_dt = new stdClass();
                $recieves[$i]->tp_p_dt = new stdClass();


                $recieves[$i]->tg_st_dt->timestamp = $a_recieve->thrift_group_start_date;
                $recieves[$i]->tg_en_dt->timestamp = $a_recieve->thrift_group_end_date;
                $recieves[$i]->tp_p_dt->timestamp = $a_recieve->thrift_group_payment_date;


                if ($a_recieve->thrift_group_start_date == 0) {
                    $recieves[$i]->tg_st_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $recieves[$i]->tg_st_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_recieve->thrift_group_start_date);
                }

                if ($a_recieve->thrift_group_end_date == 0) {
                    $recieves[$i]->tg_en_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $recieves[$i]->tg_en_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_recieve->thrift_group_end_date);
                }

                if ($a_recieve->thrift_group_payment_date == 0) {
                    $recieves[$i]->tp_p_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $recieves[$i]->tp_p_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_recieve->thrift_group_payment_date);
                }

                /*date time ends*/

                $i++;

            }
        }

        $pay_revs = array();

        if ($payments && $recieves) {


            $i = 0;
            foreach ($recieves as $a_recieve) {

                $recieves[$i]->thrift_group_payer_payee = $a_recieve->thrift_group_member_name;
                $recieves[$i]->thrift_group_employer = $a_recieve->thrift_group_member_employer_name;
                $recieves[$i]->thrift_group_payment_or_recieve_number = $a_recieve->thrift_group_payment_recieve_number;

                $pay_revs[] = $recieves[$i];


                $j = 0;
                foreach ($payments as $a_payment) {

                    if ($recieves[$i]->thrift_group_combine_payment_number == $a_payment->thrift_group_combine_payment_number) {


                        $payments[$j]->thrift_group_payer_payee = $a_payment->thrift_group_payer_name;
                        $payments[$j]->thrift_group_employer = $a_payment->thrift_group_payer_employer_name;
                        $payments[$j]->thrift_group_payment_or_recieve_number = $a_payment->thrift_group_payment_number;

                        $pay_revs[] = $payments[$j];

                    }

                    $j++;
                }


                $i++;
            }


        }

        /*echo '<pre>';

        echo '<div style="background-color: lightpink">';
        print_r($payments);
        echo '</div>';

        echo '<div style="background-color: lightcyan">';
        print_r($recieves);
        echo '</div>';

        echo '<div style="background-color: lightgoldenrodyellow">';
        print_r($pay_revs);
        echo '</div>';

        die();*/


        $site_logo = $this->custom_settings_library->getASettingsValue('general_settings', 'site_logo');

        $data['author'] = '';
        $site_name = $this->custom_settings_library->getASettingsValue('general_settings', 'site_name');

        if ($site_name) {
            $data['author'] = $site_name;
        }

        $data['pay_revs'] = $pay_revs;


        $data['site_name'] = $site_name;
        $data['site_logo'] = $site_logo;
        $data['which_report'] = $which_report;

        if ($report_format == 'pdf') {
            $this->genPaymentWithRecievePdf($data);
        }


        if ($report_format == 'excel') {
            $this->genPaymentWithRecieveExcel($data);
        }


    }

    private function genPaymentWithRecievePdf($data)
    {
        $mpdf = new mPDF('win-1252', 'A4-L', '', '', 5, 5, 16, 60, 5, 5, 'L');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Payment and Disbursement Report');
        $mpdf->SetAuthor($data['author']);
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->debug = true;


        /*do not load header,common left footer as this view contains the whole page*/
        $html = $this->load->view('report_module/payment_with_recieve_report_pdf_page', $data, TRUE);

        //echo $html;die();

        $name = $data['which_report'] . rand(10000, 99999) . '.pdf';

        //echo  $link;die();
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'D');

        exit;
    }

    private function genPaymentWithRecieveExcel($data)
    {
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Payment and Disbursements');

        $arr = array();

        $this->excel->getActiveSheet()->setCellValue('A1', $this->lang->line('thrift_group_payment_date_text'));
        $this->excel->getActiveSheet()->setCellValue('B1', $this->lang->line('thrift_group_number_text'));
        $this->excel->getActiveSheet()->setCellValue('C1', $this->lang->line('thrift_group_employer_text'));
        $this->excel->getActiveSheet()->setCellValue('D1', $this->lang->line('thrift_group_payment_amount_text'));
        $this->excel->getActiveSheet()->setCellValue('E1', $this->lang->line('thrift_group_payer_or_payee_text'));
        $this->excel->getActiveSheet()->setCellValue('F1', $this->lang->line('thrift_group_payment_number_text'));


        if ($data['pay_revs']) {
            $i = 0;
            foreach ($data['pay_revs'] as $pr) {

                $arr[$i][0] = $pr->tp_p_dt->display;
                $arr[$i][1] = $pr->thrift_group_number;
                $arr[$i][2] = $pr->thrift_group_employer;
                $arr[$i][3] = $pr->p_amt->display_without_sign;
                $arr[$i][4] = $pr->thrift_group_payer_payee;
                $arr[$i][5] = $pr->thrift_group_payment_or_recieve_number;

                $i++;
            }


        }


        // read data to active sheet
        $this->excel->getActiveSheet()->fromArray($arr, null, 'A2');


        $filename = $data['which_report'] . rand(10000, 99999) . '.xlsx'; //save our workbook as this file name

        header('Content-Type: application/vnd.ms-excel'); //mime type

        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name

        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as.XLSX Excel 2007 format

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        exit;
    }

    private function paymentGroupedReportFile($report_format, $which_report, $employer_id, $employee_id, $static_range_date)
    {

        $this->lang->load('payment_report');

        $employer = false;
        $employee = false;

        if ($employer_id) {
            $employer = $this->Report_model->getUserInfo($employer_id);
        }

        if ($employee_id) {
            $employee = $this->Report_model->getUserInfo($employee_id);
        }

        $data = array();
        $data['which_report'] = $which_report;

        $data['employer'] = $employer;
        $data['employee'] = $employee;


        $this->lang->load('payment_report');

        $payments = array();

        $date_range = false;
        $date_from = false;
        $date_to = false;

        $user_id = $this->session->userdata('user_id');


        if ($static_range_date) {

            $date_range_exploded = explode('to', $static_range_date);

            $date_from_string = $date_range_exploded[0];
            $date_to_string = $date_range_exploded[1];

            $date_from =
                $this->
                custom_datetime_library->
                convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($date_from_string, 'Y-m-d-H-i-s');

            $date_to =
                $this->
                custom_datetime_library->
                convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($date_to_string, 'Y-m-d-H-i-s');

        }


        $order['column'] = 'thrift_group_payment_date';
        $order['by'] = 'asc';

        $payments =
            $this->
            Report_model->
            getPaymentReport($common_filter_value = false, $specific_filters = false, $order, $limit = false, $which_report, $employer_id, $employee_id, $date_from, $date_to);

        if ($payments == false || empty($payments) || $payments == null) {
            $payments = false;
        }


        $this->load->library('custom_datetime_library');


        $comp = array();
        if ($payments) {
            $i = 0;
            foreach ($payments as $a_payment) {

                $payments[$i]->date_range = 'mock_column';

                /*payer-payee-payer_employer starts*/

                $payments[$i]->thrift_group_payer_employer_name = "Non-organizational thrifter";
                $payments[$i]->thrift_group_payer_name = $this->lang->line('unavailable_text');
                $payments[$i]->thrift_group_payer_id_num = $this->lang->line('unavailable_text');
                $payments[$i]->thrift_group_payee_name = "Individual Savings";


                if ($payments[$i]->thrift_group_payer_member_id != null && $payments[$i]->thrift_group_payer_member_id != 0) {

                    $payer = $this->Report_model->getUserInfo($payments[$i]->thrift_group_payer_member_id);

                    if ($payer) {

                        if ($this->ion_auth->is_admin($payer->id)) {
                            $payments[$i]->thrift_group_payer_name = 'Prosperis';
                        } else {
                            $payments[$i]->thrift_group_payer_name = $payer->first_name . ' ' . $payer->last_name;
                        }


                        $payments[$i]->thrift_group_payer_id_num = $payer->mem_id_num;
                    }
                }

                if ($payments[$i]->thrift_group_payee_member_id != null && $payments[$i]->thrift_group_payee_member_id != 0) {

                    $payee = $this->Report_model->getUserInfo($payments[$i]->thrift_group_payee_member_id);

                    if ($payee) {

                        if ($this->ion_auth->is_admin($payee->id)) {
                            $payments[$i]->thrift_group_payee_name = 'Prosperis';
                        } else {
                            $payments[$i]->thrift_group_payee_name = $payee->first_name . ' ' . $payee->last_name;
                        }

                    }
                }

                if ($payments[$i]->thrift_group_payer_member_id != null && $payments[$i]->thrift_group_payer_member_id != 0) {

                    $payer = $this->Report_model->getUserInfo($payments[$i]->thrift_group_payer_member_id);

                    $payer_employer = false;
                    if ($payer->user_employer_id != null && $payer->user_employer_id != 0) {
                        $payer_employer = $this->Report_model->getUserInfo($payer->user_employer_id);
                    }

                    if ($payer_employer) {
                        $payments[$i]->thrift_group_payer_employer_name = $payer_employer->company;
                    }
                }

                $comp[] = $payments[$i]->thrift_group_payer_employer_name;
                /*payer-payee-payer_employer ends*/


                /*amount starts*/

                /*amount starts*/


                $payments[$i]->p_amt = new stdClass();

                $payments[$i]->p_amt->display = $this->getCurrencySign() .
                    number_format($payments[$i]->thrift_group_payment_amount, 2, '.', ',');

                $payments[$i]->p_amt->display_without_sign = number_format($payments[$i]->thrift_group_payment_amount, 2, '.', ',');

                $payments[$i]->p_amt->val = $payments[$i]->thrift_group_payment_amount;
                /*amount ends*/


                /*date time starts*/
                $payments[$i]->tg_st_dt = new stdClass();
                $payments[$i]->tg_en_dt = new stdClass();
                $payments[$i]->tp_p_dt = new stdClass();


                $payments[$i]->tg_st_dt->timestamp = $a_payment->thrift_group_start_date;
                $payments[$i]->tg_en_dt->timestamp = $a_payment->thrift_group_end_date;
                $payments[$i]->tp_p_dt->timestamp = $a_payment->thrift_group_payment_date;


                if ($a_payment->thrift_group_start_date == 0) {
                    $payments[$i]->tg_st_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tg_st_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_start_date);
                }

                if ($a_payment->thrift_group_end_date == 0) {
                    $payments[$i]->tg_en_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tg_en_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_end_date);
                }

                if ($a_payment->thrift_group_payment_date == 0) {
                    $payments[$i]->tp_p_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tp_p_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_payment_date);
                }

                /*date time ends*/

                $i++;

            }
        }

        $comp = array_unique($comp);
        $grps = false;
        if (!empty($comp) && $payments) {
            for ($i = 0; $i < count($comp); $i++) {

                $grps[$i] = new stdClass();

                $grps[$i]->comp_title = $comp[$i];

                $j = 0;
                $p = false;
                foreach ($payments as $a_payment) {

                    if ($comp[$i] == $a_payment->thrift_group_payer_employer_name) {
                        $p[] = $a_payment;
                    }

                    $grps[$i]->payments = $p;

                    $j++;
                }

            }
        }

        /*echo '<pre>';
        print_r($grps);die();*/

        $total_payment_amount = 0.00;

        $total_payment_amount =
            $this->
            Report_model->
            getTotalPayment($common_filter_value = false, $specific_filters = false, $which_report, $employer_id, $employee_id, $date_from, $date_to);


        $data['grps'] = $grps;
        $data['total_payment_amount'] = $total_payment_amount;
        $data['total_payment_amount_display'] = $this->getCurrencySign() .
            number_format($total_payment_amount->total_payment_amount, 2, '.', ',');
        $data['total_payment_amount_display_without_sign'] = number_format($total_payment_amount->total_payment_amount, 2, '.', ',');


        $site_logo = $this->custom_settings_library->getASettingsValue('general_settings', 'site_logo');

        $data['author'] = '';
        $site_name = $this->custom_settings_library->getASettingsValue('general_settings', 'site_name');

        if ($site_name) {
            $data['author'] = $site_name;
        }


        $data['site_name'] = $site_name;
        $data['site_logo'] = $site_logo;
        $data['which_report'] = $which_report;

        if ($report_format == 'pdf') {
            $this->genPaymentGroupedPdf($data);
        }


        if ($report_format == 'excel') {
            $this->genPaymentGroupedPdfExcel($data);
        }


    }

    private function genPaymentGroupedPdf($data)
    {
        $mpdf = new mPDF('win-1252', 'A4-L', '', '', 5, 5, 16, 60, 5, 5, 'L');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Payment Report Grouped');
        $mpdf->SetAuthor($data['author']);
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->debug = true;


        /*do not load header,common left footer as this view contains the whole page*/
        $html = $this->load->view('report_module/payment_grouped_report_pdf_page', $data, TRUE);

        //echo $html; die();

        $name = $data['which_report'] . rand(10000, 99999) . '.pdf';

        //echo  $link;die();
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'D');

        exit;
    }

    private function genPaymentGroupedPdfExcel($data)
    {
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Payment Report Grouped');

        $arr = array();

        $this->excel->getActiveSheet()->setCellValue('A1', $this->lang->line('thrift_group_payer_member_text'));
        $this->excel->getActiveSheet()->setCellValue('B1', $this->lang->line('thrift_group_payment_amount_text'));
        $this->excel->getActiveSheet()->setCellValue('C1', $this->lang->line('thrift_group_number_text'));
        $this->excel->getActiveSheet()->setCellValue('D1', $this->lang->line('thrift_group_thrifter_id_text'));
        $this->excel->getActiveSheet()->setCellValue('E1', $this->lang->line('thrift_group_payment_number_text'));
        $this->excel->getActiveSheet()->setCellValue('F1', $this->lang->line('thrift_group_payment_date_text'));
        $this->excel->getActiveSheet()->setCellValue('G1', $this->lang->line('thrift_group_start_date_text'));
        $this->excel->getActiveSheet()->setCellValue('H1', $this->lang->line('thrift_group_end_date_text'));

        if ($data['grps']) {
            $i = 0;
            foreach ($data['grps'] as $g) {

                $arr[$i][0] = $g->comp_title;
                $arr[$i][1] = '';
                $arr[$i][2] = '';
                $arr[$i][3] = '';
                $arr[$i][4] = '';
                $arr[$i][5] = '';
                $arr[$i][6] = '';
                $arr[$i][7] = '';


                $i++;
                foreach ($g->payments as $p) {


                    $arr[$i][0] = $p->thrift_group_payer_name;
                    $arr[$i][1] = $p->p_amt->display;
                    $arr[$i][2] = $p->thrift_group_number;
                    $arr[$i][3] = $p->thrift_group_payer_id_num;
                    $arr[$i][4] = $p->thrift_group_payment_number;
                    $arr[$i][5] = $p->tp_p_dt->display;
                    $arr[$i][6] = $p->tg_en_dt->display;
                    $arr[$i][7] = $p->tg_st_dt->display;

                    $i++;
                }

            }

            $arr[$i + 1][0] = '';
            $arr[$i + 1][1] = '';
            $arr[$i + 1][2] = '';
            $arr[$i + 1][3] = '';
            $arr[$i + 1][4] = '';
            $arr[$i + 1][5] = '';
            $arr[$i + 1][6] = '';
            $arr[$i + 1][7] = '';


            $arr[$i + 2][0] = '';
            $arr[$i + 2][1] = '';
            $arr[$i + 2][2] = '';
            $arr[$i + 2][3] = '';
            $arr[$i + 2][4] = '';
            $arr[$i + 2][5] = '';
            $arr[$i + 2][6] = $this->lang->line('total_text');
            $arr[$i + 2][7] = $data['total_payment_amount_display_without_sign'];
        }

        /*echo '<pre>';
        print_r($arr);die();*/


        // read data to active sheet
        $this->excel->getActiveSheet()->fromArray($arr, null, 'A2');


        $filename = $data['which_report'] . rand(10000, 99999) . '.xlsx'; //save our workbook as this file name

        header('Content-Type: application/vnd.ms-excel'); //mime type

        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name

        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as.XLSX Excel 2007 format

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        exit;
    }


    private function getPaymentRecieveReport($report_format, $which_report, $employer_id, $employee_id, $static_range_date)
    {
        $this->lang->load('payment_recieve_report');

        $data['report_format'] = $report_format;
        $data['which_report'] = $which_report;
        $data['employer_id'] = $employer_id;
        $data['employee_id'] = $employee_id;
        $data['static_range_date'] = $static_range_date;

        $employer = false;
        $employee = false;

        if ($employer_id) {
            $employer = $this->Report_model->getUserInfo($employer_id);
        }

        if ($employee_id) {
            $employee = $this->Report_model->getUserInfo($employee_id);
        }

        $data['employer'] = $employer;
        $data['employee'] = $employee;


        $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $link = '/';
        if (strpos($actual_link, 'show_report_pdf') !== false) { // if $link contains show_report_pdf
            $link = str_replace('show_report_pdf', 'show_report', $actual_link);

        } else {
            $link = $actual_link;
        }

        $data['link'] = $link;

        if ($report_format == 'pdf' || $report_format == 'excel') {
            $this->paymentRecieveReportFile($link, $report_format, $which_report, $employer_id, $employee_id, $static_range_date);
            exit;
        }


        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("report_module/payment_recieve_report_page", $data);
        $this->load->view("common_module/footer");

    }

    public function getPaymentRecieveReportByAjax()
    {
        $this->lang->load('payment_recieve_report');

        $payments = array();

        $date_range = false;
        $date_from = false;
        $date_to = false;

        $which_report = $_REQUEST['which_report'];
        $employee_id = $_REQUEST['employee_id'];
        $employer_id = $_REQUEST['employer_id'];
        $static_range_date = $_REQUEST['static_range_date'];


        $user_id = $this->session->userdata('user_id');

        $requestData = $_REQUEST;
        //print_r($requestData);

        $columns[0] = 'thrift_group_number'; //tg
        $columns[1] = 'thrift_group_member_employer_id'; //not sql table col
        $columns[2] = 'thrift_group_payment_recieve_amount'; //tp
        $columns[3] = 'thrift_group_member_id'; //tp
        $columns[4] = 'thrift_group_payment_recieve_number'; //tp
        $columns[5] = 'thrift_group_payment_date'; //tp
        $columns[6] = 'thrift_group_start_date'; // tg
        $columns[7] = 'thrift_group_end_date'; //tg
        $columns[8] = 'date_range'; //not sql table col


        if (!empty($requestData['columns'][8]['search']['value'])) {
            $date_range = $requestData['columns'][8]['search']['value'];
        }

        //overwrites datepicker
        if ($static_range_date) {
            $date_range = false;
        }

        if ($date_range) {
            $date_range_exploded = explode('to', $date_range);

            $date_from_string = $date_range_exploded[0];
            $date_to_string = $date_range_exploded[1];

            $date_from_string .= '-00-00-00';
            $date_to_string .= '-23-59-59';

            $date_from =
                $this->
                custom_datetime_library->
                convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($date_from_string, 'Y-m-d-H-i-s');

            $date_to =
                $this->
                custom_datetime_library->
                convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($date_to_string, 'Y-m-d-H-i-s');

        }

        if ($static_range_date) {

            $date_range_exploded = explode('to', $static_range_date);

            $date_from_string = $date_range_exploded[0];
            $date_to_string = $date_range_exploded[1];

            $date_from =
                $this->
                custom_datetime_library->
                convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($date_from_string, 'Y-m-d-H-i-s');

            $date_to =
                $this->
                custom_datetime_library->
                convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($date_to_string, 'Y-m-d-H-i-s');

        }


        $common_filter_value = false;
        $order_column = false;

        $specific_filters = array();
        $specific_filters = false;

        if (!empty($requestData['columns'][0]['search']['value'])) {
            $specific_filters['thrift_group_number'] = $requestData['columns'][0]['search']['value'];
        }

        if (!empty($requestData['columns'][1]['search']['value'])) {
            $specific_filters['thrift_group_member_employer_id'] = $requestData['columns'][1]['search']['value'];
        }

        if (!empty($requestData['columns'][2]['search']['value'])) {
            $specific_filters['thrift_group_payment_recieve_amount'] = $requestData['columns'][2]['search']['value'];
        }

        if (!empty($requestData['columns'][3]['search']['value'])) {
            $specific_filters['thrift_group_member_id'] = $requestData['columns'][3]['search']['value'];
        }

        if (!empty($requestData['columns'][4]['search']['value'])) {
            $specific_filters['thrift_group_payment_recieve_number'] = $requestData['columns'][4]['search']['value'];
        }

        if (!empty($requestData['columns'][5]['search']['value'])) {
            $specific_filters['thrift_group_payment_date'] = $requestData['columns'][5]['search']['value'];
        }

        if (!empty($requestData['columns'][6]['search']['value'])) {
            $specific_filters['thrift_group_start_date'] = $requestData['columns'][6]['search']['value'];
        }

        if (!empty($requestData['columns'][7]['search']['value'])) {
            $specific_filters['thrift_group_end_date'] = $requestData['columns'][7]['search']['value'];
        }


        if (!empty($requestData['search']['value'])) {
            $common_filter_value = $requestData['search']['value'];
        }

        if ($specific_filters == true || !empty($specific_filters)) {
            $common_filter_value = false;       //either search with specific filters or with common filter
        }

        $order['column'] = $columns[$requestData['order'][0]['column']];
        $order['by'] = $requestData['order'][0]['dir'];


        $limit['start'] = $requestData['start'];
        $limit['length'] = $requestData['length'];

        $totalData =
            $this->
            Report_model->
            countPaymentRecieveReport(false, false, $which_report, $employer_id, $employee_id, $date_from, $date_to);

        if ($common_filter_value == true || $specific_filters == true) {

            $totalFiltered =
                $this->
                Report_model->
                countPaymentRecieveReport($common_filter_value, $specific_filters, $which_report, $employer_id, $employee_id, $date_from, $date_to);
        } else {
            $totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
        }


        $payments =
            $this->
            Report_model->
            getPaymentRecieveReport($common_filter_value, $specific_filters, $order, $limit, $which_report, $employer_id, $employee_id, $date_from, $date_to);

        if ($payments == false || empty($payments) || $payments == null) {
            $payments = false;
        }

        $last_query = $this->db->last_query();


        $this->load->library('custom_datetime_library');


        if ($payments) {
            $i = 0;
            foreach ($payments as $a_payment) {

                $payments[$i]->date_range = 'mock_column';

                /*payer-payee-payer_employer starts*/

                $payments[$i]->thrift_group_member_employer_name = "<span style='font-style: italic; !important'>Non-organizational thrifter</span>";
                $payments[$i]->thrift_group_member_name = $this->lang->line('unavailable_text');


                if ($payments[$i]->thrift_group_member_id != null && $payments[$i]->thrift_group_member_id != 0) {

                    $member = $this->Report_model->getUserInfo($payments[$i]->thrift_group_member_id);

                    if ($member) {
                        if($this->ion_auth->is_admin($member->id)){
                            $payments[$i]->thrift_group_member_name = "Prosperis";
                        }else{
                            $payments[$i]->thrift_group_member_name = $member->first_name .' '.$member->last_name;
                        }

                    }
                }


                if ($payments[$i]->thrift_group_member_id != null && $payments[$i]->thrift_group_member_id != 0) {

                    $member = $this->Report_model->getUserInfo($payments[$i]->thrift_group_member_id);

                    $member_employer = false;
                    if ($member->user_employer_id != null && $member->user_employer_id != 0) {
                        $member_employer = $this->Report_model->getUserInfo($member->user_employer_id);
                    }

                    if ($member_employer) {
                        $payments[$i]->thrift_group_member_employer_name = $member_employer->company;
                    }
                }
                /*payer-payee-payer_employer ends*/


                /*amount starts*/

                /*amount starts*/


                $payments[$i]->p_amt = new stdClass();
                $payments[$i]->p_amt->display = $this->getCurrencySign() .
                    number_format($payments[$i]->thrift_group_payment_recieve_amount, 2, '.', ',');
                $payments[$i]->p_amt->val = $payments[$i]->thrift_group_payment_recieve_amount;
                /*amount ends*/


                /*date time starts*/
                $payments[$i]->tg_st_dt = new stdClass();
                $payments[$i]->tg_en_dt = new stdClass();
                $payments[$i]->tp_p_dt = new stdClass();


                $payments[$i]->tg_st_dt->timestamp = $a_payment->thrift_group_start_date;
                $payments[$i]->tg_en_dt->timestamp = $a_payment->thrift_group_end_date;
                $payments[$i]->tp_p_dt->timestamp = $a_payment->thrift_group_payment_date;


                if ($a_payment->thrift_group_start_date == 0) {
                    $payments[$i]->tg_st_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tg_st_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_start_date);
                }

                if ($a_payment->thrift_group_end_date == 0) {
                    $payments[$i]->tg_en_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tg_en_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_end_date);
                }

                if ($a_payment->thrift_group_payment_date == 0) {
                    $payments[$i]->tp_p_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tp_p_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_payment_date);
                }

                /*date time ends*/

                $i++;

            }
        }


        $total_payment_recieve_amount =
            $this->
            Report_model->
            getTotalPaymentRecieve($common_filter_value, $specific_filters, $which_report, $employer_id, $employee_id, $date_from, $date_to);

        if ($total_payment_recieve_amount) {
            $json_data['total_payment_recieve_amount'] = $total_payment_recieve_amount->total_payment_recieve_amount;
            $json_data['total_payment_recieve_amount_display'] = $this->getCurrencySign() .
                number_format($total_payment_recieve_amount->total_payment_recieve_amount, 2, '.', ',');
        } else {
            $json_data['total_payment_recieve_amount'] = 0.00;
            $json_data['total_payment_recieve_amount_display'] = 0.00;
        }


        $json_data['draw'] = intval($requestData['draw']);
        /* $totalData: for every request/draw by clientside ,
         they send a number as a parameter, when they recieve a response/data they first check the draw number,
         so we are sending same number in draw.*/
        $json_data['recordsTotal'] = intval($totalData); // total number of records after searching, if there is no searching then totalFiltered = totalData
        $json_data['recordsFiltered'] = intval($totalFiltered);

        //$payments = $this->removeKeys($payments); // converting to numeric indices.
        $json_data['data'] = $payments;

        // checking requests in console.log() for testing starts;
        /*$json_data['last_query'] = $last_query;
        $json_data['common_filter_value'] = $common_filter_value;
        $json_data['specific_filters'] = $specific_filters;
        $json_data['order_column'] = $order['column'];
        $json_data['order_by'] = $order['by'];
        $json_data['limit_length'] = $limit['length'];
        $json_data['limit_start'] = $limit['start'];*/
        // checking requests in console.log() for testing ends;

        echo(json_encode($json_data));

    }

    private function paymentRecieveReportFile($link, $report_format, $which_report, $employer_id, $employee_id, $static_range_date)
    {
        $this->lang->load('payment_recieve_report');

        $employer = false;
        $employee = false;

        if ($employer_id) {
            $employer = $this->Report_model->getUserInfo($employer_id);
        }

        if ($employee_id) {
            $employee = $this->Report_model->getUserInfo($employee_id);
        }

        $data = array();
        $data['which_report'] = $which_report;

        $data['employer'] = $employer;
        $data['employee'] = $employee;


        $this->lang->load('payment_report');

        $payments = array();

        $date_range = false;
        $date_from = false;
        $date_to = false;

        $user_id = $this->session->userdata('user_id');


        if ($static_range_date) {

            $date_range_exploded = explode('to', $static_range_date);

            $date_from_string = $date_range_exploded[0];
            $date_to_string = $date_range_exploded[1];

            $date_from =
                $this->
                custom_datetime_library->
                convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($date_from_string, 'Y-m-d-H-i-s');

            $date_to =
                $this->
                custom_datetime_library->
                convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($date_to_string, 'Y-m-d-H-i-s');

        }


        $order['column'] = 'thrift_group_payment_date';
        $order['by'] = 'asc';

        $payments =
            $this->
            Report_model->
            getPaymentRecieveReport($common_filter_value = false, $specific_filters = false, $order, $limit = false, $which_report, $employer_id, $employee_id, $date_from, $date_to);

        if ($payments == false || empty($payments) || $payments == null) {
            $payments = false;
        }

        $last_query = $this->db->last_query();


        $this->load->library('custom_datetime_library');


        if ($payments) {
            $i = 0;
            foreach ($payments as $a_payment) {

                $payments[$i]->date_range = 'mock_column';

                /*payer-payee-payer_employer starts*/

                $payments[$i]->thrift_group_member_employer_name = "Non-organizational thrifter";
                $payments[$i]->thrift_group_member_name = $this->lang->line('unavailable_text');


                if ($payments[$i]->thrift_group_member_id != null && $payments[$i]->thrift_group_member_id != 0) {

                    $member = $this->Report_model->getUserInfo($payments[$i]->thrift_group_member_id);

                    if ($member) {
                        if($this->ion_auth->is_admin($member->id)){
                            $payments[$i]->thrift_group_member_name = "Prosperis";
                        }else{
                            $payments[$i]->thrift_group_member_name = $member->first_name .' '.$member->last_name;
                        }

                    }
                }


                if ($payments[$i]->thrift_group_member_id != null && $payments[$i]->thrift_group_member_id != 0) {

                    $member = $this->Report_model->getUserInfo($payments[$i]->thrift_group_member_id);

                    $member_employer = false;
                    if ($member->user_employer_id != null && $member->user_employer_id != 0) {
                        $member_employer = $this->Report_model->getUserInfo($member->user_employer_id);
                    }

                    if ($member_employer) {
                        $payments[$i]->thrift_group_member_employer_name = $member_employer->company;
                    }
                }
                /*payer-payee-payer_employer ends*/


                /*amount starts*/

                /*amount starts*/


                $payments[$i]->p_amt = new stdClass();
                $payments[$i]->p_amt->display = $this->getCurrencySign() .
                    number_format($payments[$i]->thrift_group_payment_recieve_amount, 2, '.', ',');
                $payments[$i]->p_amt->display_without_sign = number_format($payments[$i]->thrift_group_payment_recieve_amount, 2, '.', ',');
                $payments[$i]->p_amt->val = $payments[$i]->thrift_group_payment_recieve_amount;
                /*amount ends*/


                /*date time starts*/
                $payments[$i]->tg_st_dt = new stdClass();
                $payments[$i]->tg_en_dt = new stdClass();
                $payments[$i]->tp_p_dt = new stdClass();


                $payments[$i]->tg_st_dt->timestamp = $a_payment->thrift_group_start_date;
                $payments[$i]->tg_en_dt->timestamp = $a_payment->thrift_group_end_date;
                $payments[$i]->tp_p_dt->timestamp = $a_payment->thrift_group_payment_date;


                if ($a_payment->thrift_group_start_date == 0) {
                    $payments[$i]->tg_st_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tg_st_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_start_date);
                }

                if ($a_payment->thrift_group_end_date == 0) {
                    $payments[$i]->tg_en_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tg_en_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_end_date);
                }

                if ($a_payment->thrift_group_payment_date == 0) {
                    $payments[$i]->tp_p_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tp_p_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_payment_date);
                }

                /*date time ends*/

                $i++;

            }
        }

        $total_payment_recieve_amount = 0.00;

        $total_payment_recieve_amount =
            $this->
            Report_model->
            getTotalPaymentRecieve($common_filter_value = false, $specific_filters = false, $which_report, $employer_id, $employee_id, $date_from, $date_to);


        $site_logo = $this->custom_settings_library->getASettingsValue('general_settings', 'site_logo');

        $data['author'] = '';
        $site_name = $this->custom_settings_library->getASettingsValue('general_settings', 'site_name');

        if ($site_name) {
            $data['author'] = $site_name;
        }

        $data['payments'] = $payments;
        $data['total_payment_recieve_amount'] = $total_payment_recieve_amount;
        $data['total_payment_recieve_amount_display'] = $this->getCurrencySign() .
            number_format($total_payment_recieve_amount->total_payment_recieve_amount, 2, '.', ',');
        $data['total_payment_recieve_amount_display_without_sign'] = number_format($total_payment_recieve_amount->total_payment_recieve_amount, 2, '.', ',');


        $data['site_name'] = $site_name;
        $data['site_logo'] = $site_logo;
        $data['which_report'] = $which_report;

        if ($report_format == 'pdf') {
            $this->genPaymentRecievePdf($data);
        }


        if ($report_format == 'excel') {
            $this->genPaymentRecieveExcel($data);
        }


    }

    private function genPaymentRecievePdf($data)
    {
        $mpdf = new mPDF('win-1252', 'A4-L', '', '', 5, 5, 16, 60, 5, 5, 'L');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Payment Statement');
        $mpdf->SetAuthor($data['author']);
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->debug = true;


        /*do not load header,common left footer as this view contains the whole page*/
        $html = $this->load->view('report_module/payment_recieve_report_pdf_page', $data, TRUE);

        //echo $html;die();

        $name = $data['which_report'] . rand(10000, 99999) . '.pdf';

        //echo  $link;die();
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'D');

        exit;
    }

    private function genPaymentRecieveExcel($data)
    {

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Payment Recieve');

        $arr = array();

        $this->excel->getActiveSheet()->setCellValue('A1', $this->lang->line('thrift_group_number_text'));
        $this->excel->getActiveSheet()->setCellValue('B1', $this->lang->line('thrift_group_employer_text'));
        $this->excel->getActiveSheet()->setCellValue('C1', $this->lang->line('thrift_group_payment_recieve_amount_text'));
        $this->excel->getActiveSheet()->setCellValue('D1', $this->lang->line('thrift_group_member_text'));
        $this->excel->getActiveSheet()->setCellValue('E1', $this->lang->line('thrift_group_payment_recieve_number_text'));
        $this->excel->getActiveSheet()->setCellValue('F1', $this->lang->line('thrift_group_payment_date_text'));
        $this->excel->getActiveSheet()->setCellValue('G1', $this->lang->line('thrift_group_start_date_text'));
        $this->excel->getActiveSheet()->setCellValue('H1', $this->lang->line('thrift_group_end_date_text'));

        if ($data['payments']) {
            $i = 0;
            foreach ($data['payments'] as $p) {

                $arr[$i][0] = $p->thrift_group_number;
                $arr[$i][1] = $p->thrift_group_member_employer_name;
                $arr[$i][2] = $p->p_amt->display_without_sign;
                $arr[$i][3] = $p->thrift_group_member_name;
                $arr[$i][4] = $p->thrift_group_payment_recieve_number;
                $arr[$i][5] = $p->tp_p_dt->display;
                $arr[$i][6] = $p->tg_en_dt->display;
                $arr[$i][7] = $p->tg_st_dt->display;

                $i++;
            }


            $arr[$i + 1][0] = '';
            $arr[$i + 1][1] = '';
            $arr[$i + 1][2] = '';
            $arr[$i + 1][3] = '';
            $arr[$i + 1][4] = '';
            $arr[$i + 1][5] = '';
            $arr[$i + 1][6] = '';
            $arr[$i + 1][7] = '';

            $arr[$i + 2][0] = '';
            $arr[$i + 2][1] = '';
            $arr[$i + 2][2] = '';
            $arr[$i + 2][3] = '';
            $arr[$i + 2][4] = '';
            $arr[$i + 2][5] = '';
            $arr[$i + 2][6] = $this->lang->line('total_text');
            $arr[$i + 2][7] = $data['total_payment_recieve_amount_display_without_sign'];

        }


        // read data to active sheet
        $this->excel->getActiveSheet()->fromArray($arr, null, 'A2');


        $filename = $data['which_report'] . rand(10000, 99999) . '.xlsx'; //save our workbook as this file name

        header('Content-Type: application/vnd.ms-excel'); //mime type

        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name

        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as.XLSX Excel 2007 format

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        exit;
    }

    private function paymentRecieveGroupedReportFile($report_format, $which_report, $employer_id, $employee_id, $static_range_date)
    {
        $this->lang->load('payment_recieve_report');

        $employer = false;
        $employee = false;

        if ($employer_id) {
            $employer = $this->Report_model->getUserInfo($employer_id);
        }

        if ($employee_id) {
            $employee = $this->Report_model->getUserInfo($employee_id);
        }

        $data = array();
        $data['which_report'] = $which_report;

        $data['employer'] = $employer;
        $data['employee'] = $employee;


        $this->lang->load('payment_report');

        $payments = array();

        $date_range = false;
        $date_from = false;
        $date_to = false;

        $user_id = $this->session->userdata('user_id');


        if ($static_range_date) {

            $date_range_exploded = explode('to', $static_range_date);

            $date_from_string = $date_range_exploded[0];
            $date_to_string = $date_range_exploded[1];

            $date_from =
                $this->
                custom_datetime_library->
                convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($date_from_string, 'Y-m-d-H-i-s');

            $date_to =
                $this->
                custom_datetime_library->
                convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($date_to_string, 'Y-m-d-H-i-s');

        }


        $order['column'] = 'thrift_group_payment_date';
        $order['by'] = 'asc';

        $payments =
            $this->
            Report_model->
            getPaymentRecieveReport($common_filter_value = false, $specific_filters = false, $order, $limit = false, $which_report, $employer_id, $employee_id, $date_from, $date_to);

        if ($payments == false || empty($payments) || $payments == null) {
            $payments = false;
        }

        $last_query = $this->db->last_query();


        $this->load->library('custom_datetime_library');


        $comp = array();
        if ($payments) {
            $i = 0;
            foreach ($payments as $a_payment) {

                $payments[$i]->date_range = 'mock_column';

                /*payer-payee-payer_employer starts*/

                $payments[$i]->thrift_group_member_employer_name = "Non-organizational thrifter";
                $payments[$i]->thrift_group_member_name = $this->lang->line('unavailable_text');


                if ($payments[$i]->thrift_group_member_id != null && $payments[$i]->thrift_group_member_id != 0) {

                    $member = $this->Report_model->getUserInfo($payments[$i]->thrift_group_member_id);

                    if ($member) {

                        if ($this->ion_auth->is_admin($member->id)) {
                            $payments[$i]->thrift_group_member_name = 'Prosperis';
                        } else {
                            $payments[$i]->thrift_group_member_name = $member->first_name . ' ' . $member->last_name;
                        }

                        $payments[$i]->thrift_group_memb_id_num = $member->mem_id_num;
                    }
                }


                if ($payments[$i]->thrift_group_member_id != null && $payments[$i]->thrift_group_member_id != 0) {

                    $member = $this->Report_model->getUserInfo($payments[$i]->thrift_group_member_id);

                    $member_employer = false;
                    if ($member->user_employer_id != null && $member->user_employer_id != 0) {
                        $member_employer = $this->Report_model->getUserInfo($member->user_employer_id);
                    }

                    if ($member_employer) {
                        $payments[$i]->thrift_group_member_employer_name = $member_employer->company;
                    }

                }
                $comp[] = $payments[$i]->thrift_group_member_employer_name;
                /*payer-payee-payer_employer ends*/


                /*amount starts*/

                /*amount starts*/


                $payments[$i]->p_amt = new stdClass();
                $payments[$i]->p_amt->display = $this->getCurrencySign() .
                    number_format($payments[$i]->thrift_group_payment_recieve_amount, 2, '.', ',');
                $payments[$i]->p_amt->display_without_sign = number_format($payments[$i]->thrift_group_payment_recieve_amount, 2, '.', ',');
                $payments[$i]->p_amt->val = $payments[$i]->thrift_group_payment_recieve_amount;
                /*amount ends*/


                /*date time starts*/
                $payments[$i]->tg_st_dt = new stdClass();
                $payments[$i]->tg_en_dt = new stdClass();
                $payments[$i]->tp_p_dt = new stdClass();


                $payments[$i]->tg_st_dt->timestamp = $a_payment->thrift_group_start_date;
                $payments[$i]->tg_en_dt->timestamp = $a_payment->thrift_group_end_date;
                $payments[$i]->tp_p_dt->timestamp = $a_payment->thrift_group_payment_date;


                if ($a_payment->thrift_group_start_date == 0) {
                    $payments[$i]->tg_st_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tg_st_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_start_date);
                }

                if ($a_payment->thrift_group_end_date == 0) {
                    $payments[$i]->tg_en_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tg_en_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_end_date);
                }

                if ($a_payment->thrift_group_payment_date == 0) {
                    $payments[$i]->tp_p_dt->display = $this->lang->line('unavailable_text');
                } else {
                    $payments[$i]->tp_p_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDate($a_payment->thrift_group_payment_date);
                }

                /*date time ends*/

                $i++;

            }
        }


        $comp = array_unique($comp);

        $grps = false;
        if (!empty($comp) && $payments) {
            for ($i = 0; $i < count($comp); $i++) {

                $grps[$i] = new stdClass();

                $grps[$i]->comp_title = $comp[$i];

                $j = 0;
                $p = false;
                foreach ($payments as $a_payment) {

                    if ($comp[$i] == $a_payment->thrift_group_member_employer_name) {
                        $p[] = $a_payment;
                    }

                    $grps[$i]->payments = $p;

                    $j++;
                }

            }
        }

        /*echo '<pre>';
        print_r($grps);die();*/

        $total_payment_recieve_amount = 0.00;

        $total_payment_recieve_amount =
            $this->
            Report_model->
            getTotalPaymentRecieve($common_filter_value = false, $specific_filters = false, $which_report, $employer_id, $employee_id, $date_from, $date_to);


        $site_logo = $this->custom_settings_library->getASettingsValue('general_settings', 'site_logo');

        $data['author'] = '';
        $site_name = $this->custom_settings_library->getASettingsValue('general_settings', 'site_name');

        if ($site_name) {
            $data['author'] = $site_name;
        }

        $data['grps'] = $grps;
        $data['total_payment_recieve_amount'] = $total_payment_recieve_amount;
        $data['total_payment_recieve_amount_display'] = $this->getCurrencySign() .
            number_format($total_payment_recieve_amount->total_payment_recieve_amount, 2, '.', ',');
        $data['total_payment_recieve_amount_display_without_sign'] = number_format($total_payment_recieve_amount->total_payment_recieve_amount, 2, '.', ',');


        $data['site_name'] = $site_name;
        $data['site_logo'] = $site_logo;
        $data['which_report'] = $which_report;

        if ($report_format == 'pdf') {
            $this->genPaymentRecieveGroupedPdf($data);
        }


        if ($report_format == 'excel') {
            $this->genPaymentRecieveGroupedExcel($data);
        }


    }

    private function genPaymentRecieveGroupedPdf($data)
    {
        $mpdf = new mPDF('win-1252', 'A4-L', '', '', 5, 5, 16, 60, 5, 5, 'L');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Payment Statement Grouped');
        $mpdf->SetAuthor($data['author']);
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->debug = true;


        /*do not load header,common left footer as this view contains the whole page*/
        $html = $this->load->view('report_module/payment_recieve_grouped_report_pdf_page', $data, TRUE);

        //echo $html;die();


        $name = $data['which_report'] . rand(10000, 99999) . '.pdf';

        //echo  $link;die();
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'D');

        exit;
    }

    private function genPaymentRecieveGroupedExcel($data)
    {

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Payment Statement Grouped');

        $arr = array();


        $this->excel->getActiveSheet()->setCellValue('A1', $this->lang->line('thrift_group_payee_member_text'));
        $this->excel->getActiveSheet()->setCellValue('B1', $this->lang->line('thrift_group_payment_amount_text'));
        $this->excel->getActiveSheet()->setCellValue('C1', $this->lang->line('thrift_group_number_text'));
        $this->excel->getActiveSheet()->setCellValue('D1', $this->lang->line('thrift_group_thrifter_id_text'));
        $this->excel->getActiveSheet()->setCellValue('E1', $this->lang->line('thrift_group_payment_number_text'));
        $this->excel->getActiveSheet()->setCellValue('F1', $this->lang->line('thrift_group_payment_date_text'));
        $this->excel->getActiveSheet()->setCellValue('G1', $this->lang->line('thrift_group_start_date_text'));
        $this->excel->getActiveSheet()->setCellValue('H1', $this->lang->line('thrift_group_end_date_text'));

        if ($data['grps']) {
            $i = 0;
            foreach ($data['grps'] as $g) {

                $arr[$i][0] = $g->comp_title;
                $arr[$i][1] = '';
                $arr[$i][2] = '';
                $arr[$i][3] = '';
                $arr[$i][4] = '';
                $arr[$i][5] = '';
                $arr[$i][6] = '';
                $arr[$i][7] = '';


                $i++;
                foreach ($g->payments as $p) {


                    $arr[$i][0] = $p->thrift_group_member_name;
                    $arr[$i][1] = $p->p_amt->display_without_sign;
                    $arr[$i][2] = $p->thrift_group_number;
                    $arr[$i][3] = $p->thrift_group_memb_id_num;
                    $arr[$i][4] = $p->thrift_group_payment_recieve_number;
                    $arr[$i][5] = $p->tp_p_dt->display;
                    $arr[$i][6] = $p->tg_en_dt->display;
                    $arr[$i][7] = $p->tg_st_dt->display;

                    $i++;
                }

            }

            $arr[$i + 1][0] = '';
            $arr[$i + 1][1] = '';
            $arr[$i + 1][2] = '';
            $arr[$i + 1][3] = '';
            $arr[$i + 1][4] = '';
            $arr[$i + 1][5] = '';
            $arr[$i + 1][6] = '';
            $arr[$i + 1][7] = '';


            $arr[$i + 2][0] = '';
            $arr[$i + 2][1] = '';
            $arr[$i + 2][2] = '';
            $arr[$i + 2][3] = '';
            $arr[$i + 2][4] = '';
            $arr[$i + 2][5] = '';
            $arr[$i + 2][6] = $this->lang->line('total_text');
            $arr[$i + 2][7] = $data['total_payment_recieve_amount_display_without_sign'];

        }

        /*echo '<pre>';
        print_r($arr);die();*/


        // read data to active sheet
        $this->excel->getActiveSheet()->fromArray($arr, null, 'A2');


        $filename = $data['which_report'] . rand(10000, 99999) . '.xlsx'; //save our workbook as this file name

        header('Content-Type: application/vnd.ms-excel'); //mime type

        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name

        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as.XLSX Excel 2007 format

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        exit;
    }


    /*----------------------------------------------------------------------------------------------------------------*/

    public function showReportYearMonths()
    {

        $this->lang->load('report_year_month');

        $which_report = $this->uri->segment(2);
        $payment_or_recieve = $this->uri->segment(3);
        $years_or_months = $this->uri->segment(4);
        $time_range = $this->uri->segment(5);
        $employer_id = false;
        $employee_id = false;

        $years_list = false;
        $months_list = false;

        $max_min_ts = false;

        $min_year = false;
        $max_year = false;

        $min_month = false;
        $max_month = false;


        $min_ts_y_val = false;
        $max_ts_y_val = false;

        $min_ts_ym_key = false;
        $max_ts_ym_key = false;

        $year_list = false;
        $month_list = false;


        $curr_year_init_ts = false;
        $curr_year_final_ts = false;


        if ($which_report == 'all_report') {

            if (!($this->ion_auth->is_admin() || $this->ion_auth->in_group('trustee'))) {
                redirect('users/auth/need_permission');
            }

        }

        if ($which_report == 'employer_report' && $this->uri->segment(6)) {

            if (!($this->ion_auth->is_admin() || $this->ion_auth->in_group('trustee'))) {
                redirect('users/auth/need_permission');
            }
            $employer_id = $this->uri->segment(6);
        }

        if ($which_report == 'employee_report' && $this->uri->segment(6)) {

            if (!($this->ion_auth->is_admin() || $this->ion_auth->in_group('trustee'))) {
                redirect('users/auth/need_permission');
            }
            $employee_id = $this->uri->segment(6);
        }


        if ($which_report == 'employer_report_as_employer') {
            if (!$this->ion_auth->in_group('employer')) {
                redirect('users/auth/does_not_exist');
            } else {
                $employer_id = $this->session->userdata('user_id');
            }


        }

        if ($which_report == 'employee_report_as_employee') {
            if (!$this->ion_auth->in_group('employee')) {
                redirect('users/auth/does_not_exist');
            } else {
                $employee_id = $this->session->userdata('user_id');
            }
        }


        if ($time_range == 'current_year') {
            $curr_ts = $this->custom_datetime_library->getCurrentTimestamp();
            $curr_year = $this->custom_datetime_library->convert_and_return_TimestampToYearGivenFormat($curr_ts, 'Y');


            $curr_year_init_string = $curr_year . '-01-01-00-00-00';
            $curr_year_final_string = $curr_year . '-12-31-23-59-59';

            $curr_year_init_ts =
                $this
                    ->custom_datetime_library
                    ->convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($curr_year_init_string, 'Y-m-d-H-i-s');
            $curr_year_final_ts =
                $this
                    ->custom_datetime_library
                    ->convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($curr_year_final_string, 'Y-m-d-H-i-s');


        }

        if ($payment_or_recieve == 'payment_report') {

            $max_min_ts =
                $this
                    ->Report_model
                    ->getPaymentMaxMinTimestamps($which_report, $employer_id, $employee_id, $curr_year_init_ts, $curr_year_final_ts);

        } else if ($payment_or_recieve == 'payment_recieve_report') {
            $max_min_ts =
                $this
                    ->Report_model
                    ->getPaymentRecieveMaxMinTimestamps($which_report, $employer_id, $employee_id, $curr_year_init_ts, $curr_year_final_ts);
        }

        if ($max_min_ts) {
            if ($max_min_ts->max_pdate_ts != null && $max_min_ts->min_pdate_ts != null
                &&
                $max_min_ts->min_pdate_ts >= 0 && $max_min_ts->min_pdate_ts >= 0
            ) {

                if ($years_or_months == 'years') {

                    $min_ts_y_val =
                        $this
                            ->custom_datetime_library
                            ->convert_and_return_TimestampToYearGivenFormat($max_min_ts->min_pdate_ts, 'Y');

                    $max_ts_y_val =
                        $this
                            ->custom_datetime_library
                            ->convert_and_return_TimestampToYearGivenFormat($max_min_ts->max_pdate_ts, 'Y');
                }

                if ($years_or_months == 'months') {

                    if ($time_range == 'all_year' || $time_range == 'current_year') {
                        $min_ts_ym_key =
                            $this
                                ->custom_datetime_library
                                ->convert_and_return_TimestampToYearMonthGivenFormat($max_min_ts->min_pdate_ts, 'Y-n');

                        $max_ts_ym_key =
                            $this
                                ->custom_datetime_library
                                ->convert_and_return_TimestampToYearMonthGivenFormat($max_min_ts->max_pdate_ts, 'Y-n');


                    }


                }

            }
        }

        if ($min_ts_y_val && $max_ts_y_val) {

            $min_ts_y_val = (int)$min_ts_y_val;
            $max_ts_y_val = (int)$max_ts_y_val;

            for ($i = $min_ts_y_val; $i <= $max_ts_y_val; $i++) {

                $arr['year'] = $i;
                $arr['payment_report_anchor'] = 'report_module/show_report/' . $which_report . '/' . $payment_or_recieve . '/' . $i;
                $arr['payment_report_pdf_anchor'] = 'report_module/show_report_pdf/' . $which_report . '/' . $payment_or_recieve . '/' . $i;
                $arr['payment_report_excel_anchor'] = 'report_module/show_report_excel/' . $which_report . '/' . $payment_or_recieve . '/' . $i;


                $year_list[] = $arr;

            }
        }

        if ($min_ts_ym_key && $max_ts_ym_key) {

            $min_ts_ym_key_exploded = explode('-', $min_ts_ym_key);
            $max_ts_ym_key_exploded = explode('-', $max_ts_ym_key);

            $miny = (int)$min_ts_ym_key_exploded[0];
            $miny_minm = (int)$min_ts_ym_key_exploded[1];

            $maxy = (int)$max_ts_ym_key_exploded[0];
            $maxy_maxm = (int)$max_ts_ym_key_exploded[1];


            for ($i = $miny; $i <= $maxy; $i++) {

                if ($i == $miny) {
                    $lim_init = $miny_minm;
                } else {
                    $lim_init = 1;
                }

                if ($i == $maxy) {
                    $lim_final = $maxy_maxm;
                } else {
                    $lim_final = 12;
                }

                for ($j = $lim_init; $j <= $lim_final; $j++) {

                    $leading_zero = '';

                    if ($j < 10) {
                        $leading_zero = '0';
                    }

                    $month_arr[1] = 'January';
                    $month_arr[2] = 'February';
                    $month_arr[3] = 'March';
                    $month_arr[4] = 'April';
                    $month_arr[5] = 'May';
                    $month_arr[6] = 'June';
                    $month_arr[7] = 'July';
                    $month_arr[8] = 'August';
                    $month_arr[9] = 'September';
                    $month_arr[10] = 'October';
                    $month_arr[11] = 'November';
                    $month_arr[12] = 'December';

                    $arr_m['month'] = $month_arr[$j] . ' ' . $i;
                    $arr_m['payment_report_anchor'] = 'report_module/show_report/' . $which_report . '/' . $payment_or_recieve . '/' . $i . '-' . $leading_zero . $j;
                    $arr_m['payment_report_pdf_anchor'] = 'report_module/show_report_pdf/' . $which_report . '/' . $payment_or_recieve . '/' . $i . '-' . $leading_zero . $j;
                    $arr_m['payment_report_excel_anchor'] = 'report_module/show_report_excel/' . $which_report . '/' . $payment_or_recieve . '/' . $i . '-' . $leading_zero . $j;

                    $arr_m['month_sort'] = $i . $leading_zero . $j;
                    $month_list[] = $arr_m;

                }


            }

        }

        /*echo ' < pre>';
        echo '$month_list';
        echo ' < br>';
        print_r($month_list) ;

        echo '$year_list';
        echo ' < br>';
        print_r($year_list) ;
        die();*/


        $data = array();

        $data['payment_or_recieve'] = $payment_or_recieve;
        $data['years_or_months'] = $years_or_months;
        $data['time_range'] = $time_range;
        $data['year_list'] = $year_list;
        $data['month_list'] = $month_list;
        $data['time_range'] = $time_range;

        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("report_module/report_year_month_page", $data);
        $this->load->view("common_module/footer");


    }


}