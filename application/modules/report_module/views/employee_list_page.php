
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">
                            <?php echo lang('page_title_text') ?>
                        </h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a
                                        href="/"><?php echo lang('breadcrumb_home_text') ?></a>
                            </li>
                            <li class="breadcrumb-item active"><?php echo lang('breadcrumb_section_text') ?></li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <h4 class="header-title m-t-0 m-b-30"></h4>
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">
                                <small><?php echo lang('page_subtitle_text') ?></small>
                            </h4>

                            <!-- Main content -->
                            <section class="">
                                <div class="">
                                    <div class="col-xs-12">
                                        <div class="box box-primary">
                                            <div class="box-header">
                                                <h3 class="m-t-0 header-title"><?php echo lang('table_title_text') ?></h3>

                                                <div>
                                                    <table style="width: 67%; margin: 0 auto 2em auto;" cellspacing="1"
                                                           cellpadding="3"
                                                           border="0">
                                                        <tbody>

                                                        <tr id="filter_col3" data-column="3">
                                                            <td align="center"><label
                                                                        for=""><?php echo lang('column_email_text') ?></label>
                                                            </td>
                                                            <td align="center">
                                                                <input class="column_filter form-control"
                                                                       id="col3_filter" type="text">
                                                            </td>
                                                        </tr>
                                                        
                                                        <tr id="filter_col6" data-column="6">
                                                            <td align="center"><label
                                                                        for=""><?php echo lang('column_status_text') ?></label>
                                                            </td>
                                                            <td align="center">
                                                                <input class="column_filter form-control"
                                                                       id="col6_filter" type="hidden">
                                                                <select id="custom_status_filter" class="form-control">
                                                                    <option value="all"><?php echo lang('option_all_text') ?></option>
                                                                    <option value="yes"><?php echo lang('option_active_text') ?></option>
                                                                    <option value="no"><?php echo lang('option_inactive_text') ?></option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        </tbody>

                                                    </table>
                                                </div>
                                            </div>

                                            <?php $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>

                                            <div class="text-center">
                                                <div class="btn-group m-b-20">
                                                    <?php if (strpos($actual_link, 'employee_report_pdf') !== false) { ?><!-- if $actual_link contains employee_report_pdf-->
                                                    <a href="<?= $actual_link ?>" class="btn btn-secondary waves-effect">PDF</a>
                                                    <?php } else { ?>
                                                        <a href="<?= str_replace('employee', 'employee_report_pdf', $actual_link) ?>"
                                                           class="btn btn-secondary waves-effect">PDF</a>
                                                    <?php } ?>

                                                    <?php if (strpos($actual_link, 'employee_report_excel') !== false) { ?><!-- if $actual_link contains employee_report_excel-->
                                                    <a href="<?= $actual_link ?>" class="btn btn-secondary waves-effect">EXCEL</a>
                                                <?php } else { ?>
                                                    <a href="<?= str_replace('employee', 'employee_report_excel', $actual_link) ?>"
                                                       class="btn btn-secondary waves-effect">EXCEL</a>
                                                <?php } ?>
                                                </div>
                                            </div>

                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <table id="employee-table"
                                                       class="table table-bordered table-hover table-responsive ">
                                                    <thead>
                                                    <tr>
                                                        <th><?php echo lang('column_created_on_text') ?></th>
                                                        <th><?php echo lang('column_name_text') ?></th>
                                                        <th><?php echo lang('column_mem_id_num_text') ?></th>
                                                        <th><?php echo lang('column_email_text') ?></th>
                                                        <th><?php echo lang('column_company_text') ?></th>
                                                        <th><?php echo lang('column_concluded_thrifts_text') ?></th>
                                                        <th><?php echo lang('column_status_text') ?></th>
                                                        <th><?php echo lang('column_actions_text') ?></th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </section>
                            <!-- /.content -->
                            <div class="clearfix"></div>
                        </div>
                    </div><!-- end col -->
                </div><!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->



<!-- <script>
    $(function () {
        $(document).tooltip();
    })
</script> -->

<!--clearing the extra arrow-->
<style>
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: unset;
    }
</style>

<!--this css style is holding datatable inside the box-->
<style>

    #employee-table {
        table-layout: fixed;
        width: 100% !important;
    }

    #employee-table td,
    #employee-table th {
        width: auto !important;
        white-space: normal;
        text-overflow: ellipsis;
        overflow: hidden;
    }
</style>

<script>
    $(document).ready(function () {

        var loading_image_src = '<?php echo base_url() ?>' + 'base_demo_images/loading.gif';
        var loading_image = '<img src="' + loading_image_src + ' ">';
        var loading_span = '<span><i class="fa fa-refresh fa-spin fa-4x" aria-hidden="true"></i></span> ';
        var loading_text = "<div style='font-size:larger' ><?php echo lang('loading_text')?></div>";


        $('#employee-table').DataTable({

            processing: true,
            serverSide: true,
            paging: true,
            pagingType: "full_numbers",
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            searchDelay: 3000,
            infoEmpty: '<?php echo lang("no_user_found_text")?>',
            zeroRecords: '<?php echo lang("no_matching_user_found_text")?>',
            language: {
                processing: loading_image + '<br>' + loading_text
            },

            lengthMenu: [[10, 25, 50, 18446744073709551615], [10, 25, 50, "All"]],
            dom: 'lfrtip',
            /*buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],*/
            buttons: [
                {
                    extend: 'pdf',
                    footer: true,
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'excel',
                    footer: true,
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'csv',
                    footer: true,
                    exportOptions: {
                        columns: ':visible'
                    }

                },
                {
                    extend: 'copy',
                    footer: true,
                    exportOptions: {
                        columns: ':visible'
                    }

                },
                {
                    extend: 'print',
                    footer: true,
                    exportOptions: {
                        columns: ':visible'
                    }

                }
            ] ,



            columns: [
                {                           //0
                    data: {
                        _: "cr_on.display",
                        sort: "cr_on.timestamp"
                    }
                },
                {data: "name"}, //1
                {data: "mem_id_num"}, //2
                {data: "email"},    //3
                {data: "org"},    //4
                {data: "concluded_thrifts"},    //5
                {                   //6
                    data: {
                        _: "act.html",
                        sort: "act.int"
                    }
                },
                {data: "action"} //7

            ],

            columnDefs: [

                {
                    'targets': 0,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },
                {
                    'targets': 1,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },

                {
                    'targets': 2,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },
                {
                    'targets': 3,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },
                {
                    'targets': 4,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },
                {
                    'targets': 5,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },
                {
                    'targets': 6,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },

                {orderable: false, targets: [1,4,5,7]}, <?php if($which_employees=='my') { ?>  { visible: false, targets: [3,4,6,7] } <?php } ?>

            ],

            aaSorting: [[0, 'asc']],

            ajax: {
                url: "<?php echo base_url() . 'report_module/get_employee_by_ajax' ?>",                   // json datasource
                type: "post",
                data:{
                    which_employees :'<?= $which_employees ?>'
                },
                complete: function (res) {
                    getConfirm();
                }

                //open succes only for test purpuses . remember when success is uncommented datble doesn't diplay data
                /*success: function (res) {

                 console.log(res.last_query);
                 console.log(res.common_filter_value);
                 console.log(res.specific_filters);
                 console.log(res.order_column);
                 console.log(res.order_by);
                 console.log(res.limit_start);
                 console.log(res.limit_length);
                 }*/
            }

        });
    });
</script>


<script>
    /*column toggle*/
    $(function () {

        var table = $('#employee-table').DataTable();

        $('a.toggle-vis').on('click', function (e) {
            e.preventDefault();

            // Get the column API object
            var column = table.column($(this).attr('data-column'));

            // Toggle the visibility
            column.visible(!column.visible());
        });

    });
</script>

<script>
    /*input searches*/
    $(document).ready(function () {
        //customized delay_func starts
        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();
        //customized delay_func ends

        $('input.column_filter').on('keyup', function () {
            var var_this = $(this);
            delay(function () {
                filterColumn($(var_this).parents('tr').attr('data-column'));
            }, 3000);
        });
    });
</script>

<script>
    function filterColumn(i) {

        $('#employee-table').DataTable().column(i).search(
            $('#col' + i + '_filter').val(),
            $('#col' + i + '_regex').prop('checked'),
            $('#col' + i + '_smart').prop('checked')
        ).draw();
    }
</script>

<script>
    /*cutom select searches through input searches*/
    $(function () {

        /*-----------------------------*/
        $('#custom_status_filter').on('change', function () {

            if ($('#custom_status_filter').val() == 'all') {
                $('#col6_filter').val('');
                filterColumn(6);
            } else {
                $('#col6_filter').val($('#custom_status_filter').val());
                filterColumn(6);
            }

        });
        /*-----------------------------*/
    })
</script>

<script>
    function getConfirm() {
        $('.confirmation').click(function (e) {

            e.preventDefault();

            var href = $(this).attr('href');

            swal({
                    title: "<?= lang('swal_title_text')?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= lang('swal_confirm_button_text')?>",
                    cancelButtonText: "<?= lang('swal_cancel_button_text')?>",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = href;
                    }
                });

            return false;
        });
    }

</script>


