<div class="container">

    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php
                    if ($which_report == 'all_report') {
                        echo lang('page_title_all_report_details_page_text');
                    }

                    if ($which_report == 'employer_report') {
                        echo lang('page_title_employer_report_details_page_text');
                    }

                    if ($which_report == 'employee_report') {
                        echo lang('page_title_employee_report_details_page_text');
                    }
                    ?>

                    &nbsp;<?php if ($employer) {
                        echo '(' . $employer->company . ')';
                    } ?>
                    <?php if ($employee) {
                        echo '(' . $employee->first_name . ' ' . $employee->last_name . ')';
                    } ?>
                </h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="#"><?= lang('breadcrum_report_text') ?></a></li>
                    <li class="breadcrumb-item active"><?= lang('breadcrum_report_details_text') ?></li>
                </ol>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-12">
            <div class="card-box">


                <div class="row m-t-0">

                    <div class="col-12 m-t-20">
                        <h4 class="header-title m-t-0"><?= lang('payment_recieve_details_text') ?></h4>
                        <p class="text-muted font-13 m-b-10">
                        </p>

                        <div class="box-header">


                            <div>
                                <table style="width: 67%; margin: 0 auto 2em auto;" cellspacing="1"
                                       cellpadding="3"
                                       border="0">
                                    <tbody>

                                    <tr id="filter_col9" data-column="9"
                                        <?php if ($static_range_date) { ?> style="display: none" <?php } ?>
                                    >

                                        <input class="column_filter form-control"
                                               id="col9_filter" type="hidden">

                                        <td align="center"><label
                                                    for=""><?php echo lang('choose_range_text') ?></label>
                                        </td>
                                        <td align="center">
                                            <div id="reportrange" class="pull-right form-control">
                                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                                <span></span>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr id="filter_col0" data-column="0">
                                        <td align="center">
                                            <label><?php echo lang('thrift_group_number_text') ?></label>
                                        </td>
                                        <td align="center">
                                            <input class="column_filter form-control"
                                                   id="col0_filter" type="text">
                                        </td>
                                    </tr>

                                    <tr id="filter_col4" data-column="4">
                                        <td align="center">
                                            <label><?php echo lang('thrift_group_payment_recieve_number_text') ?></label>
                                        </td>
                                        <td align="center">
                                            <input class="column_filter form-control"
                                                   id="col4_filter" type="text">
                                        </td>
                                    </tr>

                                    </tbody>

                                </table>
                            </div>
                        </div>

                        <?php $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
                        <div class="text-center">

                            <div class="btn-group m-b-20">
                                <a href="<?= str_replace('show_report', 'show_report_pdf', $actual_link) ?>"
                                   class="btn btn-secondary waves-effect"><?= lang('payment_recieve_pdf_text') ?></a>

                                <!---------------------------------------------------------------->

                                <a href="<?= str_replace('show_report', 'show_report_excel', $actual_link) ?>"
                                   class="btn btn-secondary waves-effect"><?= lang('payment_recieve_excel_text') ?></a>
                                <?php if ($this->ion_auth->is_admin() || $this->ion_auth->in_group('trustee')) { ?>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-secondary dropdown-toggle  waves-effect"
                                                data-toggle="dropdown"
                                                aria-expanded="false"><?= lang('other_reports_text') ?><span
                                                    class="caret"></span></button>
                                        <div class="dropdown-menu" x-placement="bottom-start"
                                             style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                                            <!-- if $actual_link contains payment_recieve_report-->
                                            <?php
                                            if (strpos($actual_link, 'payment_recieve_report') !== false) {
                                                $revised_link = str_replace('payment_recieve_report', 'payment_with_recieve_report', $actual_link);
                                                ?>

                                                <a href="<?= str_replace('show_report', 'show_report_pdf', $revised_link) ?>"
                                                   class="dropdown-item"><?= lang('payment_with_disbursement_pdf_text') ?></a>
                                            <?php } ?>

                                            <!-- if $actual_link contains payment_recieve_report-->
                                            <?php
                                            if (strpos($actual_link, 'payment_recieve_report') !== false) {
                                                $revised_link = str_replace('payment_recieve_report', 'payment_with_recieve_report', $actual_link);
                                                ?>

                                                <a href="<?= str_replace('show_report', 'show_report_excel', $revised_link) ?>"
                                                   class="dropdown-item"><?= lang('payment_with_disbursement_excel_text') ?></a>
                                            <?php } ?>


                                            <!-- if $actual_link contains payment_report-->
                                            <?php
                                            if (strpos($actual_link, 'payment_recieve_report') !== false) {
                                                $revised_link = str_replace('payment_recieve_report', 'payment_recieve_grouped_report', $actual_link);
                                                ?>

                                                <a href="<?= str_replace('show_report', 'show_report_pdf', $revised_link) ?>"
                                                   class="dropdown-item"><?= lang('payment_recieve_grouped_pdf_text') ?></a>
                                            <?php } ?>

                                            <!-- if $actual_link contains payment_report-->
                                            <?php
                                            if (strpos($actual_link, 'payment_recieve_report') !== false) {
                                                $revised_link = str_replace('payment_recieve_report', 'payment_recieve_grouped_report', $actual_link);
                                                ?>

                                                <a href="<?= str_replace('show_report', 'show_report_excel', $revised_link) ?>"
                                                   class="dropdown-item"><?= lang('payment_recieve_grouped_pdf_text') ?></a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>


                        </div>

                        <div class="p-20">
                            <div class="">
                                <table class="table table-bordered table-responsive" id="payment-recieve-table">
                                    <thead>
                                    <tr>
                                        <th><?= lang('thrift_group_number_text') ?></th>
                                        <th><?= lang('thrift_group_employer_text') ?></th>
                                        <th><?= lang('thrift_group_payment_recieve_amount_text') ?></th>
                                        <th><?= lang('thrift_group_member_text') ?></th>
                                        <th><?= lang('thrift_group_payment_recieve_number_text') ?></th>
                                        <th><?= lang('thrift_group_payment_date_text') ?></th>
                                        <th><?= lang('thrift_group_start_date_text') ?></th>
                                        <th><?= lang('thrift_group_end_date_text') ?></th>
                                        <th>Date range</th>
                                    </tr>
                                    </thead>

                                    <tfoot>
                                    <th></th>
                                    </tfoot>

                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- end row -->

            </div>
        </div><!-- end col-->

    </div>

    <!-- end row -->


</div> <!-- container -->


<style>
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: unset;
    }
</style>

<!--this css style is holding datatable inside the box-->
<style>

    #payment-recieve-table {
        table-layout: fixed;
        width: 100% !important;
    }

    #payment-recieve-table td,
    #payment-recieve-table th {
        width: auto !important;
        white-space: normal;
        text-overflow: ellipsis; /*ellipsis*/
        overflow: hidden;
    }
</style>

<script>
    $(document).ready(function () {

        var loading_image_src = '<?php echo base_url() ?>' + 'base_demo_images/loading.gif';
        var loading_image = '<img src="' + loading_image_src + ' ">';
        var loading_span = '<span><i class="fa fa-refresh fa-spin fa-4x" aria-hidden="true"></i></span> ';
        var loading_text = "<div style='font-size:larger' ><?php echo lang('loading_text')?></div>";


        $('#payment-recieve-table').DataTable({
            processing: true,
            serverSide: true,
            paging: true,
            pagingType: "full_numbers",
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            searchDelay: 3000,
            infoEmpty: '<?php echo lang("not_found_text")?>',
            zeroRecords: '<?php echo lang("no_matching_found_text")?>',
            language: {
                processing: loading_image + '<br>' + loading_text
            },

            lengthMenu: [[10, 25, 50, 18446744073709551615], [10, 25, 50, "All"]],
            dom: 'lfrtip',
            /*buttons: [
             'copy', 'csv', 'excel', 'pdf', 'print'
             ],*/
            buttons: [
                {
                    extend: 'pdf',
                    footer: true,
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'excel',
                    footer: true,
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'csv',
                    footer: true,
                    exportOptions: {
                        columns: ':visible'
                    }

                },
                {
                    extend: 'copy',
                    footer: true,
                    exportOptions: {
                        columns: ':visible'
                    }

                },
                {
                    extend: 'print',
                    footer: true,
                    exportOptions: {
                        columns: ':visible'
                    }

                }
            ],

            columns: [
                {data: "thrift_group_number"},  //0
                {data: "thrift_group_member_employer_name"}, //1
                {
                    data: {
                        _: "p_amt.display",
                        sort: "p_amt.val"
                    }
                },                                  //2
                {data: "thrift_group_member_name"},  //3
                {data: "thrift_group_payment_recieve_number"},  //4
                {
                    data: {
                        _: "tp_p_dt.display",
                        sort: "tp_p_dt.timestamp"
                    }
                },                                   //5
                {
                    data: {
                        _: "tg_st_dt.display",
                        sort: "tg_st_dt.timestamp"
                    }
                },                                  //7
                {
                    data: {
                        _: "tg_en_dt.display",
                        sort: "tg_en_dt.timestamp"
                    }
                },                                  //7

                {data: "date_range"}  //8

            ],

            columnDefs: [

                {
                    'targets': 0,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },
                {
                    'targets': 1,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },

                {
                    'targets': 2,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },
                {
                    'targets': 3,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },
                {
                    'targets': 4,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },

                {
                    'targets': 4,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },
                {
                    'targets': 5,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },
                {
                    'targets': 6,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },
                {
                    'targets': 7,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },
                {
                    'targets': 8,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },

                {orderable: false, targets: [1, 3]}, {visible: false, targets: [8]}
            ],

            aaSorting: [[6, 'desc']],

            <?php if($which_report == 'all_report' && 0) { ?>

            drawCallback: function (settings) {
                var api = this.api();
                var rows = api.rows({page: 'current'}).nodes();
                var last = null;

                api.column(1, {page: 'current'}).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                            '<tr class="group"><td colspan="10">' + group + '</td></tr>'
                        );

                        last = group;
                    }
                });
            },

            <?php } ?>

            footerCallback: function (tfoot, data, start, end, display) {
                var response = this.api().ajax.json();

                //console.log(response);
                if (response) {
                    var $th = $(tfoot).find('th');

                    //console.log($th);

                    $th.html('Total:<br>' + response['total_payment_recieve_amount_display']); //total_payment_recieve_amount
                    $th.attr('title', response['total_payment_recieve_amount_display']);
                }
            },


            ajax: {
                url: "<?php echo base_url() . 'report_module/get_payment_recieve_report_by_ajax' ?>",                   // json datasource
                type: "post",
                data: {
                    employee_id: '<?= $employee_id ? $employee_id : '' ?>',
                    employer_id: '<?= $employer_id ? $employer_id : '' ?>',
                    which_report: '<?= $which_report ?>',
                    static_range_date: '<?= $static_range_date ? $static_range_date : '' ?>'
                },
                complete: function (res) {
                    //do somthing on complete

                },


                //open succes only for test purpuses . remember when success is uncommented datble doesn't diplay data
                /*success: function (res) {

                 console.log(res.last_query);
                 console.log(res.common_filter_value);
                 console.log(res.specific_filters);
                 console.log(res.order_column);
                 console.log(res.order_by);
                 console.log(res.limit_start);
                 console.log(res.limit_length);
                 }*/
            }

        });
    });
</script>


<script>
    /*column toggle*/
    $(function () {

        var table = $('#payment-recieve-table').DataTable();

        $('a.toggle-vis').on('click', function (e) {
            e.preventDefault();

            // Get the column API object
            var column = table.column($(this).attr('data-column'));

            // Toggle the visibility
            column.visible(!column.visible());
        });

    });
</script>

<script>
    /*input searches*/
    $(document).ready(function () {
        //customized delay_func starts
        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();
        //customized delay_func ends

        $('input.column_filter').on('keyup', function () {
            var var_this = $(this);
            delay(function () {
                filterColumn($(var_this).parents('tr').attr('data-column'));
            }, 3000);
        });
    });
</script>

<script>
    function filterColumn(i) {

        $('#payment-recieve-table').DataTable().column(i).search(
            $('#col' + i + '_filter').val(),
            $('#col' + i + '_regex').prop('checked'),
            $('#col' + i + '_smart').prop('checked')
        ).draw();
    }
</script>


<script>
    $(function () {

        //$('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

        $('#reportrange').daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            minDate: '01/01/2015',
            maxDate: '12/31/2025',
            dateLimit: {
                days: 60
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-custom',
            cancelClass: 'btn-secondary',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Cancel',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        }, function (start, end, label) {
            //console.log(start.toISOString(), end.toISOString(), label);
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

            $('#col9_filter').val(start.format('YYYY-MM-DD') + 'to' + end.format('YYYY-MM-DD'));
            filterColumn(9);
        });

    });
</script>







