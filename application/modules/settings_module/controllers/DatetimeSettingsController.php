<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DatetimeSettingsController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->model('timezone_module/Timezone_model');


        $this->load->library('form_validation');
        $this->load->library('session');

        //customized lib from modules/settings_module/libraries
        $this->load->library('settings_module/custom_settings_library');

        $this->load->library('custom_log_library');

        //customized lib from libraries
        $this->load->library('custom_datetime_library');

        $this->lang->load('datetime_settings');

    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        if (!$this->ion_auth->in_group('superadmin')) {
            redirect('users/auth/need_permission');
        } else {
            $data['all_timezones'] = $this->Timezone_model->getTimezones();

            $a_settings_code = 'datetime_settings';
            $data['all_datetime_settings'] = $this->custom_settings_library->getSettings($a_settings_code);

            $data['date_format_settings'] = $this->custom_settings_library->getASettings($a_settings_code,'date_format');
            $data['time_format_settings'] = $this->custom_settings_library->getASettings($a_settings_code,'time_format');

            $this->load->view("common_module/header");
            // $this->load->view("common_module/common_left");
            $this->load->view("settings_module/datetime_settings_page", $data);
            $this->load->view("common_module/footer");
        }
    }

    public function updateDatetimeSettings()
    {
        if (!$this->ion_auth->in_group('superadmin')) {
            redirect('users/auth/need_permission');
        }

        $data['time_zone'] = $this->input->post('time_zone');
        $data['date_format'] = $this->input->post('date_format');
        $data['time_format'] = $this->input->post('time_format');


        $a_settings_code = 'datetime_settings';
        foreach ($data as $a_settings_key => $a_settings_value) {

            if (($this->custom_settings_library->ifSettingsExist($a_settings_code, $a_settings_key)) == true) {

                $this->custom_settings_library->updateSettings($a_settings_code, $a_settings_key, $a_settings_value);

            } else {

                $this->custom_settings_library->addSettings($a_settings_code, $a_settings_key, $a_settings_value);

            }

        }

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            '',                                                                     //2.    $created_for
            'datetime_settings',                                                    //3.    $type
            '',                                                                     //4.    $type_id
            'updated',                                                              //5.    $activity
            'admin',                                                                //6.    $activity_by
            '',                                                                     //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            '',                                                                     //10.   $super_type
            '',                                                                     //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        $this->session->set_flashdata('update_success_text', $this->lang->line('update_success_text'));
        redirect('settings_module/datetime_settings');
    }
}