<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IndividualSettingsController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('encryption');
        $this->load->library('image_lib');

        //customized lib from application/libraries
        $this->load->library('custom_image_library');
        $this->load->library('custom_file_library');


        //customized lib from modules/settings_module/libraries
        $this->load->library('settings_module/custom_settings_library');

        $this->load->library('custom_log_library');

        $this->load->helper(array('form', 'url'));

        $this->lang->load('individual_settings');


    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }
        $user_id = $this->session->userdata('user_id');
        //may be not needed
        if (!$this->ion_auth->in_group('employer')) {
            redirect('users/auth/need_permission');
        } else {

            $a_settings_code = 'individual_settings';
            $data['all_individual_settings'] = $this->custom_settings_library->getUserSettings($a_settings_code,$user_id);

            $this->load->view("common_module/header");
            // $this->load->view("common_module/common_left");

            if ($data) {
                $this->load->view("settings_module/individual_settings_page", $data);
            } else {
                $this->load->view("settings_module/individual_settings_page");
            }

            $this->load->view("common_module/footer");
        }
    }


    public function updateIndividualSettings()
    {

        $user_id = $this->session->userdata('user_id');

        //may be not needed
        if (!$this->ion_auth->in_group('employer')) {
            redirect('users/auth/need_permission');
        }

        if ($this->input->post('individual_chosen_color')) {
            $data['individual_chosen_color'] = trim($this->input->post('individual_chosen_color'));
        }

        if ($this->input->post('individual_chosen_color_deeper')) {
            $data['individual_chosen_color_deeper'] = trim($this->input->post('individual_chosen_color_deeper'));
        }


        /*if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('settings_module/individual_settings');
        } */

            /*uploading image files starts*/
            if (($_FILES['individual_logo']['name']) != '') {

                $field_name = 'individual_logo';
                $file_details = $_FILES['individual_logo'];

                /*
                 * Function: uploadImage()
                 * @params: $file_details  - array
                 *          $field_name    - string
                 *
                 * @return: if true
                 *              $image_details - array
                 *          else
                 *              false   -   bool
                 *  */

                $individual_logo_image_details = $this->custom_image_library->uploadImage($file_details, $field_name);
                if ($individual_logo_image_details == false) {
                    redirect('settings_module/individual_settings');
                }
                $data['individual_logo'] = $individual_logo_image_details['file_name'];
            }


            /*uploading image files ends*/

            /*
             * if settings already exists, update
             * if not, add.
            */
            $a_settings_code = 'individual_settings';


            foreach ($data as $a_settings_key => $a_settings_value) {

                if (($this->custom_settings_library->ifUserSettingsExist($a_settings_code, $a_settings_key, $user_id)) == true) {

                    $this->custom_settings_library->updateUserSettings($a_settings_code, $a_settings_key, $a_settings_value, $user_id);

                } else {

                    $this->custom_settings_library->addUserSettings($a_settings_code, $a_settings_key, $a_settings_value, $user_id);

                }

            }

            $activity_by = '';
            if ($this->ion_auth->in_group('employer') == 'employer') {
                $activity_by = 'employer';
            } else if ($this->ion_auth->in_group('organization_contact')) {
                $activity_by = 'user';
            }

            /*creating log starts*/
            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                '',                                                                     //2.    $created_for
                'individual_settings',                                                  //3.    $type
                '',                                                                     //4.    $type_id
                'updated',                                                              //5.    $activity
                $activity_by,                                                           //6.    $activity_by
                '',                                                                     //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                '',                                                                     //10.   $super_type
                '',                                                                     //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );
            /*creating log ends*/

            $this->session->set_flashdata('update_success_text', $this->lang->line('update_success_text'));
            redirect('settings_module/individual_settings');


    }


}