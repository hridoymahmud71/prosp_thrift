<?php

/*page texts*/
$lang['page_title_text'] = 'General Settings';
$lang['page_subtitle_text'] = 'Edit and update general settings here';
$lang['box_title_text'] = 'General Settings Form';


$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Settings';
$lang['breadcrumb_page_text'] = 'General Settings';



/*General settings form texts*/
$lang['label_site_name_text'] = 'Site\'s Name';
$lang['label_site_short_name_text'] = 'Site\'s Short Name';
$lang['label_site_email_text'] = 'Site\'s Email';
$lang['label_site_logo_text'] = 'Site\'s Logo';
$lang['label_office_site_logo_text'] = 'Office Site\'s Logo';
$lang['label_partner_site_logo_text'] = 'Partner Site\'s Logo';
$lang['label_thrift_site_logo_text'] = 'Thrift Site\'s Logo';
$lang['label_trustee_site_logo_text'] = 'Trustee Site\'s Logo';
$lang['label_site_banner_text'] = 'Site\'s Banner';
$lang['label_logo_or_site_name_text'] = 'Show Logo or Site\'s Name';

$lang['label_thrifter_registration_terms_html_text'] = 'Thrifter\'s registration terms';
$lang['label_site_maintenance_html_text'] = 'Site Maintenance html';

$lang['option_site_name_text'] = 'Show Site\'s Name';
$lang['option_logo_text'] = 'Show Logo';



$lang['placeholder_site_name_text'] = 'Enter Site\'s Name';
$lang['placeholder_site_short_name_text'] = 'Enter Site\'s Short Name';
$lang['placeholder_site_email_text'] = 'Enter Site\'s Short Name';

$lang['help_site_logo_text'] = 'Upload Site\'s Logo';
$lang['help_office_site_logo_text'] = 'Upload Office Site\'s Logo';
$lang['help_partner_site_logo_text'] = 'Upload Partner Site\'s Logo';
$lang['help_thrift_site_logo_text'] = 'Upload Thrift Site\'s Logo';
$lang['help_trustee_site_logo_text'] = 'Upload Trustee Site\'s Logo';

$lang['help_site_banner_text'] = 'Upload Site\'s Banner';

$lang['upload_site_logo_text'] = 'Upload Logo';
$lang['upload_office_site_logo_text'] = 'Upload Office Logo';
$lang['upload_partner_site_logo_text'] = 'Upload Partner Logo';
$lang['upload_thrift_site_logo_text'] = 'Upload Thrift Logo';
$lang['upload_trustee_site_logo_text'] = 'Upload Trustee Logo';
$lang['upload_site_banner_text'] = 'Upload Banner';

$lang['label_partner_site_under_maintenance_text'] = 'Is Partner Site Under Maintenance ?';
$lang['partner_site_under_maintenance_no_text'] = 'No';
$lang['partner_site_under_maintenance_yes_text'] = 'Yes';

$lang['label_thrift_site_under_maintenance_text'] = 'Is Thrift Site Under Maintenance ?';
$lang['thrift_site_under_maintenance_no_text'] = 'No';
$lang['thrift_site_under_maintenance_yes_text'] = 'Yes';

$lang['label_trustee_site_under_maintenance_text'] = 'Is Trustee Site Under Maintenance ?';
$lang['trustee_site_under_maintenance_no_text'] = 'No';
$lang['trustee_site_under_maintenance_yes_text'] = 'Yes';

$lang['button_submit_text'] = 'Update General Settings';

/*validation error texts*/
$lang['site_name_required'] = 'Site\'s Name is Required';
$lang['site_short_name_required'] = 'Site\'s Short Name is Required';
$lang['site_short_name_maxlength'] = 'Site\'s Short Name cannot be longer than %s characters';

$lang['site_email_required'] = 'Site\'s Email is Required';
$lang['site_email_not_valid'] = 'Site\'s Email is not Valid';

/*success messages*/
$lang['update_success_text'] = 'Successfully Updated General Settings';

