<style>
    @import url(http://fonts.googleapis.com/css?family=Open+Sans:400,700,300);

    .form-control, .thumbnail {
        border-radius: 2px;
    }

    .btn-danger {
        background-color: #B73333;
    }

    /* File Upload */
    .fake-shadow {
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
    }

    .fileUpload {
        position: relative;
        overflow: hidden;
    }

    .fileUpload #logo-id {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 33px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }



    .img-preview {
        max-width: 50%;
    }
</style>

<!-- <div class="content-page"> -->
<!-- Start content -->
<!-- <div class="content"> -->
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php echo lang('page_title_text') ?>
                </h4>
                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="#"><?php echo lang('breadcrumb_home_text') ?></a></li>
                    <li class="breadcrumb-item"><a href="#"><?php echo lang('breadcrumb_section_text') ?></a></li>
                    <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_text') ?></li>
                </ol>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-12">
            <h4 class="header-title m-t-0 m-b-30"></h4>
            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">
                <div class="page-title-box">
                    <?php if ($this->session->flashdata('group_add_success')) { ?>

                        <div class="col-md-6">
                            <div class="panel panel-success copyright-wrap" id="add-success-panel">
                                <div class="panel-heading"><?php echo lang('successfull_text') ?>
                                    <button type="button" class="close" data-target="#add-success-panel"
                                            data-dismiss="alert"><span
                                                aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                                    </button>
                                </div>
                                <div class="panel-body"><?php echo lang('add_successfull_text') ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('group_update_success')) { ?>
                        <div class="col-md-6">
                            <div class="panel panel-success copyright-wrap" id="update-success-panel">
                                <div class="panel-heading"><?php echo lang('successfull_text') ?>
                                    <button type="button" class="close" data-target="#update-success-panel"
                                            data-dismiss="alert"><span
                                                aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                                    </button>
                                </div>
                                <div class="panel-body"><?php echo lang('update_successfull_text') ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                                <!-- individual form elements -->
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h5 class="box-title"><?php echo lang('page_subtitle_text') ?></h5>

                                        <!-- <div class="col-md-offset-2 col-md-8" style="color: maroon;font-size: larger"> -->
                                        <?php if ($this->session->flashdata('validation_errors')) { ?>
                                            <div class="col-md-6">
                                                <div class="text-center alert alert-danger alert-dismissible fade show"
                                                     role="alert">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                    <strong><?php echo $this->session->flashdata('validation_errors') ?></strong>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <!-- </div> -->

                                        <?php if ($this->session->flashdata('image_upload_errors')) { ?>
                                            <div class="col-md-6">
                                                <div class="text-center alert alert-danger alert-dismissible fade show"
                                                     role="alert">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                    <strong><?= $this->session->flashdata('image_upload_errors'); ?></strong>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if ($this->session->flashdata('image_resize_errors')) { ?>
                                            <div class="col-md-6">
                                                <div class="text-center alert alert-danger alert-dismissible fade show"
                                                     role="alert">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                    <strong><?= $this->session->flashdata('image_resize_errors'); ?></strong>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <!--demo-->
                                        <?php if ($this->session->flashdata('file_upload_errors')) { ?>
                                            <div class="col-md-6">
                                                <div class="text-center alert alert-danger alert-dismissible fade show"
                                                     role="alert">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                    <strong><?= $this->session->flashdata('file_upload_errors'); ?></strong>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <!--demo-->

                                    </div>
                                    <div class="col-md-2"></div>

                                    <div class=" col-md-offset-2 col-md-12" style="color: darkgreen;font-size: larger">
                                        <!--demo-->
                                        <?php if ($this->session->flashdata('file_upload_success')) { ?>
                                            <div class="col-md-6">
                                                <div class="text-center alert alert-danger alert-dismissible fade show"
                                                     role="alert">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                    <strong><?= $this->session->flashdata('file_upload_success'); ?></strong>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <!--demo-->
                                        <br>
                                        <?php if ($this->session->flashdata('update_success_text')) { ?>
                                            <div class="text-center alert alert-success alert-dismissible fade show"
                                                 role="alert">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                                <strong><?php echo lang('update_success_text') ?></strong>
                                            </div>
                                        <?php } ?>

                                        <?php if ($this->session->flashdata('image_upload_success')) { ?>
                                            <div class="col-md-6">
                                                <div class="text-center alert alert-success alert-dismissible fade show"
                                                     role="alert">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                    <strong><?= $this->session->flashdata('image_upload_success'); ?></strong>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <br>
                                        <?php if ($this->session->flashdata('image_resize_success')) { ?>
                                            <div class="col-md-6">
                                                <div class="text-center alert alert-success alert-dismissible fade show"
                                                     role="alert">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                    <strong><?= $this->session->flashdata('image_resize_success'); ?></strong>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                <!-- form start -->
                                <form action="<?php echo base_url() . 'settings_module/update_individual_settings' ?>"
                                      role="form"
                                      id="" method="post" enctype="multipart/form-data">
                                    <div class="box-body">

                                        <div class="form-group">
                                            <label><?php echo lang('label_individual_chosen_color_text') ?></label>
                                            <input type="text"
                                                   class="colorpicker-default form-control colorpicker-element"
                                                   name="individual_chosen_color" value="<?php
                                            $col = '#ffffff';
                                            if ($all_individual_settings) {
                                                foreach ($all_individual_settings as $a_individual_settings) {
                                                    if ($a_individual_settings->settings_key == 'individual_chosen_color'){
                                                        echo $a_individual_settings->settings_value;
                                                        $col = $a_individual_settings->settings_value;
                                                    }

                                                }
                                            }
                                            ?>">

                                        </div>
                                        <div class="form-group">
                                            <label><?php echo lang('label_individual_chosen_color_deeper_text') ?></label>
                                            <input type="text"
                                                   class="colorpicker-default form-control colorpicker-element"
                                                   name="individual_chosen_color_deeper" value="<?php
                                            $col = '#ffffff';
                                            if ($all_individual_settings) {
                                                foreach ($all_individual_settings as $a_individual_settings) {
                                                    if ($a_individual_settings->settings_key == 'individual_chosen_color_deeper'){
                                                        echo $a_individual_settings->settings_value;
                                                        $col = $a_individual_settings->settings_value;
                                                    }
                                                }
                                            }
                                            ?>">

                                        </div>


                                        <!--image upload snippet starts-->
                                        <!--if true image from database, else static image //currently not working -->
                                        <?php $image_found = false; ?>

                                        <div class="form-group">
                                            <label for="individual_logo"><?php echo lang('label_individual_logo_text') ?></label>
                                            <div class="main-img-preview">

                                                <?php if ($all_individual_settings) {
                                                    foreach ($all_individual_settings as $a_individual_settings) {
                                                        if (($a_individual_settings->settings_key == 'individual_logo')
                                                            && ($a_individual_settings->settings_value != '')
                                                        ) {
                                                            ?>
                                                            <div><?php $image_found = true ?></div>
                                                            <img class="thumbnail img-preview img-preview-logo"
                                                                 src="<?php echo $this->config->item('pg_upload_source_path') . 'image/' . $a_individual_settings->settings_value ?>"
                                                                 title="Preview Logo" alt="">
                                                        <?php }
                                                    }
                                                } ?>

                                                <?php if ($image_found == false) { ?>

                                                    <img class="thumbnail img-preview img-preview-logo"
                                                         src="<?php echo base_url()
                                                             . 'base_demo_images/placeholder_image_demo.jpg' ?>"
                                                         title="Preview Logo">

                                                <?php } ?>

                                            </div>

                                            <div class="input-group">
                                                <input id="fakeUploadLogo" class="form-control fake-shadow"
                                                       placeholder="Choose File" disabled="disabled">
                                                <div class="input-group-btn">
                                                    <div class="fileUpload btn btn-primary fake-shadow">
                                                        <span><i class="glyphicon glyphicon-upload"></i> <?php echo lang('upload_individual_logo_text') ?></span>
                                                        <input id="logo-id" name="individual_logo" type="file"
                                                               value=""
                                                               class="attachment_upload">
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="help-block"><?php echo lang('help_individual_logo_text') ?></p>
                                        </div>
                                        <!--image upload snippet ends-->

                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer">


                                        <button type="submit" id="btnsubmit"
                                                class="btn btn-primary"><?php echo lang('button_submit_text') ?></button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box -->
                        </div>
                </div>
                <!-- /.row -->
                </section>
                <!-- /.content -->
                <div class="clearfix"></div>
            </div>
        </div><!-- end col -->
    </div><!-- end col -->
</div>
<!-- end row -->
</div>
<!-- </div> -->
<!-- </div> -->


<script>
    $(document).ready(function () {
        var brand = document.getElementById('logo-id');
        brand.className = 'attachment_upload';
        brand.onchange = function () {
            document.getElementById('fakeUploadLogo').value = this.value.substring(12);
        };

        // Source: http://stackoverflow.com/a/4459419/6396981
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.img-preview-logo').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#logo-id").change(function () {
            readURL(this);
        });

    });



</script>


<script>
    $(function () {


        $('.colorpicker-default').colorpicker({
            format: 'hex'
        });

    });

</script>

