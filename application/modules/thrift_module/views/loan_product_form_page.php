<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->


<!--clearing the extra arrow-->
<style>
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: unset;
    }
</style>
<style>
    #ext_mems_fieldset {
        display: none;
    }

    .super_edit_wrap {
        display: none;
    }
</style>
<div class="container">

    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">

                <?php if ($which_form == 'add') { ?>
                    <h4 class="page-title float-left"><?= lang('page_title_add_text') ?></h4>
                <?php } ?>

                <?php if ($which_form == 'edit') { ?>
                    <h4 class="page-title float-left"><?= lang('page_title_edit_text') ?></h4>
                <?php } ?>

                <?php if ($which_form == 'view') { ?>
                    <h4 class="page-title float-left"><?= lang('page_title_view_text') ?></h4>
                <?php } ?>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="/"><?= lang('breadcrum_home_text') ?></a></li>
                    <li class="breadcrumb-item active"><?= lang('breadcrum_page_text') ?></li>
                </ol>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- end row -->

    <!--<pre>
        <?php /*print_r($lp_inv_mems) */ ?>
    </pre>
    <pre>
        <?php /*print_r($lp_acc_mems) */ ?>
    </pre>
    <pre>
        <?php /*print_r($lp_dec_mems) */ ?>
    </pre>-->

    <?php if ($which_form == 'view' && $lp_inv_mems && !empty($lp_inv_mems)) { ?>
        <?php if (in_array($this->session->userdata('user_id'), $lp_inv_mems) && $this->session->userdata('user_id') != $loan_product_invitation->lpi_created_by) { ?>
            <div class="row">
                <div class="col-md-12">

                    <div class="card m-b-20">
                        <div class="card-body">
                            <h4 class="card-title"><?= lang('invitation_card_title_text') ?></h4>
                            <h6 class="card-subtitle text-muted">
                                <?= lang('invitation_card_invited_by_text') ?>:
                                <?php if ($loan_product_invitation) { ?>
                                    <a href="<?= base_url() . 'user_profile_module/user_profile_overview/' . $loan_product_invitation->lpi_created_by ?>">
                                        <?= $invitor_name ?>
                                    </a>
                                <?php } ?>
                            </h6>
                            <hr>
                            <h6 class="card-subtitle text-muted">
                                <?= lang('invitation_card_status_text') ?> :
                                <?php
                                $p = false;
                                $a = false;
                                $d = false;
                                if (!(in_array($this->session->userdata('user_id'), $lp_acc_mems)
                                    || in_array($this->session->userdata('user_id'), $lp_dec_mems))
                                ) {
                                    echo lang('pending_text');
                                    $p = true;
                                } else if (in_array($this->session->userdata('user_id'), $lp_acc_mems)
                                    && !in_array($this->session->userdata('user_id'), $lp_dec_mems)
                                ) {
                                    echo lang('accepted_text');
                                    $a = true;
                                } else if (!in_array($this->session->userdata('user_id'), $lp_acc_mems)
                                    && in_array($this->session->userdata('user_id'), $lp_dec_mems)
                                ) {
                                    echo lang('declined_text');
                                    $d = true;
                                }

                                ?>

                            </h6>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="card-body">
                                    <p class="card-text"><?= lang('invitation_card_description_text') ?></p>
                                    <?php if ($lpi_id) { ?>

                                        <?php if ($p) { ?>
                                            <a style="background-color:green !important" class="btn btn-success"
                                               href="thrift_module/accept_loan_product/<?= $lpi_id ?>"
                                               class="card-link"><?= lang('invitation_card_accept_text') ?></a>
                                            <a style="background-color:red !important" class="btn btn-danger"
                                               href="thrift_module/decline_loan_product/<?= $lpi_id ?>"
                                               class="card-link"><?= lang('invitation_card_decline_text') ?></a>
                                        <?php } else if ($a) { ?>
                                            <a style="background-color:green !important" class="btn btn-success"
                                               href="thrift_module/decline_loan_product/<?= $lpi_id ?>"
                                               class="card-link"><?= lang('invitation_card_decline_text') ?></a>
                                        <?php } else if ($d) { ?>
                                            <a style="background-color:red !important" class="btn btn-danger"
                                               href="thrift_module/accept_loan_product/<?= $lpi_id ?>"
                                               class="card-link"><?= lang('invitation_card_accept_text') ?></a>
                                        <?php } ?>


                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <?php if ($this->session->flashdata('loan_set_unsuccessful')) { ?>
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <strong><?= lang('unsuccessful_text') ?></strong>
                                        <?php

                                        if ($this->session->flashdata('loan_validation_errors')) {
                                            echo $this->session->flashdata('loan_validation_errors');
                                        }
                                        ?>
                                    </div>
                                <?php } ?>
                                <?php if ($this->session->flashdata('loan_promise_success')) { ?>
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <strong><?= lang('successful_text') ?></strong>
                                        <?php

                                        if ($this->session->flashdata('loan_promise_add_success')) {
                                            echo lang('loan_promise_add_success_text');
                                        }

                                        if ($this->session->flashdata('loan_promise_update_success')) {
                                            echo lang('loan_promise_update_success_text');
                                        }
                                        ?>

                                        <?php if ($this->session->flashdata('flash_lpi_id')) { ?>
                                            <a href="<?php echo base_url() . 'thrift_module/loan_product_thrift/view/' . $this->session->flashdata('flash_lpi_id') ?>">
                                                <?php echo lang('view_loan_product_thrift_text'); ?>
                                            </a>
                                        <?php } ?>

                                    </div>
                                <?php } ?>
                                <?php if ($a) { ?>
                                    <div class="card-body">
                                        <form class="form-inline" action="thrift_module/set_loan_promise" method="post">
                                            <label for="promised_amount" style="margin-right: 5px"
                                                   class=""><?= lang("promise_amount_label_text") ?></label>
                                            <input type="hidden" name="lpi_id" value="<?= $lpi_id ?>">
                                            <input type="hidden" name="pg_loan_promise_promised_by"
                                                   value="<?= $this->session->userdata('user_id') ?>">
                                            <div class="input-group">
                                                <span class="input-group-addon"><?= $currency_sign ?> </span>
                                                <input class="form-control"
                                                       name="pg_loan_promise_promised_amount"
                                                       type="number"
                                                       min="0.01" step="0.01"
                                                       placeholder="<?= lang("promise_amount_placeholder_text") ?>"
                                                       value="<?= $pg_loan_promise_promised_amount ?>"
                                                >
                                                <button type="submit"
                                                        class="btn btn-primary"><?= lang("promise_amount_submit_btn_text") ?></button>
                                            </div>
                                        </form>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>


                    </div>

                </div><!-- end col -->
            </div>
        <?php } ?>
    <?php } ?>

    <?php if ($which_form != 'add' && $loan_product_invited_members) { ?>
        <div class="row">
            <div class="col-12">
                <div class="card-box table-responsive">
                    <h4 class="m-t-0 header-title"><b><?= lang('member_box_title_text') ?></b></h4>
                    <table id="member-table" class="table table-striped table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th><?= lang('members_text') ?></th>
                            <th><?= lang('status_text') ?></th>
                            <th><?= lang('order_text') ?></th>
                            <th><?= lang('promised_amount_text') ?></th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php foreach ($loan_product_invited_members as $lpim) { ?>
                            <tr>
                                <td><?= $lpim->name ?>
                                    <?php if ($viewer_is_invitor && count($loan_product_invited_members) > 1) { ?>
                                        <span class="pull-right">
                                    <a title="<?= lang("title_remove_text") ?>" style="background-color:red !important;"
                                       class="btn btn-sm"
                                       href="thrift_module/loan_thrift_member_remove/<?= $lpim->lpi_inv_mem_serial ?>">
                                        <i class="fa fa-times"></i>
                                    </a>
                                    </span>
                                    <?php } ?>
                                </td>
                                <td>
                                    <?= $lpim->status; ?>
                                </td>
                                <td data-order="<?= $lpim->lpi_inv_order ?>">
                                    <?= $lpim->lpi_inv_order ?>
                                </td>
                                <td>
                                    <?= $lpim->promised_amount ? $currency_sign . number_format($lpim->promised_amount, 2, '.', ',') : lang("unavailable_text") ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>

                    <br>
                    <?php if ($viewer_is_invitor) { ?>
                        <div class="row justify-content-center">
                            <button id="super_edit_add_member_btn" class="btn btn-primary waves-effect waves-light">Add
                                Members
                            </button>
                        </div>

                        <br>
                        <?php if ($this->session->flashdata('super_warning')) { ?>
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>

                                <?php
                                if ($this->session->flashdata('flash_non_addable_members')) {
                                    echo sprintf($this->lang->line('could_not_be_added_text'), $this->session->flashdata('flash_non_addable_members'));
                                }
                                ?>
                            </div>
                        <?php } ?>
                        <div class="row super_edit_wrap justify-content-center">

                            <form id="" action="thrift_module/super_edit_loan_thrift" method="post"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="lpi_id" value="<?= $lpi_id ?>">
                                <fieldset class="form-group w-50">
                                    <label for="select_employer"><?php echo lang('select_colleagues_text') ?></label>
                                    <select multiple="multiple" class="form-control select_colleagues select2"
                                            style="width: 100%" name="select_colleagues[]"
                                            id="">
                                    </select>
                                </fieldset>

                                <fieldset class="form-group w-50">
                                    <label for="external_members"><?php echo lang('select_external_members_text') ?></label>
                                    <select multiple="multiple" id="external_members"
                                            class="form-control external_members select2"
                                            style="width: 100%" name="external_members[]"
                                            id="">

                                    </select>
                                </fieldset>

                                <button type="submit"
                                        class="btn btn-primary"><?= lang('submit_btn_update_text') ?>
                                </button>

                            </form>
                        </div>
                    <?php } ?>

                </div>
            </div>
        </div> <!-- end row -->


    <?php } ?>

    <?php if ($this->session->flashdata('successful')) { ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong><?= lang('successful_text') ?></strong>
            <?php

            if ($this->session->flashdata('loan_product_create_success')) {
                echo lang('loan_product_create_success');
            }

            if ($this->session->flashdata('loan_product_update_success')) {
                echo lang('loan_product_update_success');
            }
            ?>

            <?php if ($this->session->flashdata('flash_lpi_id')) { ?>
                <a href="<?php echo base_url() . 'thrift_module/loan_product_thrift/view/' . $this->session->flashdata('flash_lpi_id') ?>">
                    <?php echo lang('view_loan_product_thrift_text'); ?>
                </a>
            <?php } ?>

        </div>
    <?php } ?>

    <?php if ($this->session->flashdata('unsuccessful')) { ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong><?= lang('unsuccessful_text') ?></strong>
            <?php

            if ($this->session->flashdata('validation_errors')) {
                echo $this->session->flashdata('validation_errors');
            }

            if ($this->session->flashdata('atleast_need_one_colleague')) {
                echo lang('atleast_need_one_colleague_text');
            }
            ?>

            <?php if ($this->session->flashdata('thrift_error_no_employer_text')) { ?>

                <?php echo lang('thrift_error_no_employer_text') ?>

            <?php } ?>

            <?php if ($this->session->flashdata('flash_thrift_percentage_error')) { ?>
                <?php echo sprintf($this->lang->line('flash_thrift_percentage_error_text'), $this->session->flashdata('flash_thrift_percentage_error')); ?>
            <?php } ?>

        </div>
    <?php } ?>

    <?php if ($this->session->flashdata('warning')) { ?>
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>

            <?php

            if ($this->session->flashdata('flash_external_members')) {
                echo sprintf($this->lang->line('could_not_be_added_text'), $this->session->flashdata('flash_external_members'));
            }
            ?>

        </div>
    <?php } ?>


    <div class="row">
        <div class="col-12">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-30">
                    <?php if ($which_form == 'add') { ?>
                        <?= lang('box_title_add_text') ?>
                    <?php } ?>

                    <?php if ($which_form == 'edit') { ?>
                        <?= lang('box_title_edit_text') ?>
                    <?php } ?>

                    <?php if ($which_form == 'view') { ?>
                        <?= lang('box_title_view_text') ?>
                    <?php } ?>
                </h4>


                <div class="row">
                    <fieldset class="col-md-12">
                        <form id="loan_product_form" action="<?= $form_action ?>" method="post"
                              enctype="multipart/form-data">
                            <?php if ($which_form != 'view') { ?>
                                <fieldset class="form-group w-50">
                                    <label for="select_employer"><?php echo lang('select_colleagues_text') ?></label>
                                    <select multiple="multiple" class="form-control select_colleagues select2"
                                            style="width: 100%" name="select_colleagues[]"
                                            id="">
                                        <?php if ($loan_product_accepted_members) { ?>

                                            <?php foreach ($loan_product_accepted_members as $lpam) { ?>

                                                <option value="<?= $lpam->lpi_inv_to ?>"
                                                        selected="selected"><?= $lpam->name ?></option>
                                            <?php } ?>

                                        <?php } ?>

                                        <?php $selected_colleagues = false ?>
                                        <?php if ($this->session->flashdata('flash_selected_ids')) {
                                            $sc = $this->session->flashdata('flash_selected_ids');

                                            if ($sc && !empty($sc)) {
                                                $selected_colleagues = $this->Thrift_model->getUsers($sc);
                                            }
                                        } ?>

                                        <?php if ($selected_colleagues) { ?>

                                            <?php foreach ($selected_colleagues as $a_sc) { ?>

                                                <option value="<?= $a_sc->id ?>"
                                                        selected="selected"><?= $a_sc->first_name ? $a_sc->first_name : '' ?> <?= $a_sc->last_name ? $a_sc->last_name : '' ?>
                                                    (<?= $a_sc->email ? $a_sc->email : '' ?>)
                                                </option>
                                            <?php } ?>

                                        <?php } ?>
                                    </select>
                                </fieldset>
                            <?php } ?>

                            <?php if ($which_form != 'view') { ?>
                                <fieldset class="form-group w-50">
                                    <label class="col-sm-6"><?= lang('include_external_members_radio_label_text') ?></label>
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <input name="incl_ext_mem_radio" id="incl_ext_mem_yes" value="1"
                                                   type="radio">
                                            <label for="incl_ext_mem_yes">
                                                <?= lang('include_external_members_radio_yes_text') ?>
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <input name="incl_ext_mem_radio" id="incl_ext_mem_no" value="0"
                                                   type="radio">
                                            <label for="incl_ext_mem_no">
                                                <?= lang('include_external_members_radio_no_text') ?>
                                            </label>
                                        </div>

                                    </div>
                                </fieldset>
                            <?php } ?>

                            <?php if ($which_form != 'view') { ?>
                                <fieldset class="form-group w-50" id="ext_mems_fieldset">
                                    <label for="external_members"><?php echo lang('select_external_members_text') ?></label>
                                    <select multiple="multiple" id="external_members"
                                            class="form-control external_members select2"
                                            style="width: 100%" name="external_members[]"
                                            id="">

                                        <?php if ($this->session->flashdata('flash_external_members_arr')) { ?>
                                            <?php if (!empty($this->session->flashdata('flash_external_members_arr'))) { ?>
                                                <?php foreach ($this->session->flashdata('flash_external_members_arr') as $ex_mem) { ?>

                                                    <option value="<?= $ex_mem ?>"><?= $ex_mem ?></option>

                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>

                                    </select>
                                </fieldset>
                            <?php } ?>

                            <fieldset class="form-group">
                                <input type="hidden" name="posted_lpi_id" value="<?= $lpi_id ?>">
                            </fieldset>

                            <fieldset class="form-group">
                                <label><?= lang('thrift_amount_text') ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><?= $currency_sign ?> </span>
                                    <input <?php if ($which_form == 'view' && !$viewer_is_invitor) { ?> readonly="readonly" <?php } ?>
                                            name="product_price" class="form-control" style="line-height:1.5"
                                            placeholder="<?= lang('thrift_amount_text') ?>"
                                            id="product_price"
                                            type="text"
                                            value="<?= $loan_product_invitation ? number_format($loan_product_invitation->lpi_product_price, 2, '.', ',') : 0.00 ?><?= $this->session->flashdata('flash_product_price') && is_numeric($this->session->flashdata('flash_product_price')) ? number_format($this->session->flashdata('flash_product_price'), 2, '.', ',') : 0.00 ?>">
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <label><?= lang('interest_rate_text') ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon">%</span>
                                    <input <?php if ($which_form == 'view' && !$viewer_is_invitor) { ?> readonly="readonly" <?php } ?>
                                            name="interest_rate" class="form-control" style="line-height:1.5"
                                            placeholder="<?= lang('placeholder_interest_rate_text') ?>"
                                            type="number"
                                            min="1" step="0.01"
                                            max="99"
                                            value="<?= $loan_product_invitation ? $loan_product_invitation->lpi_interest_rate : '' ?><?= $this->session->flashdata('flash_interest_rate') ? $this->session->flashdata('flash_interest_rate') : '' ?>">
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <label><?= lang('repayment_terms_text') ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon">#</span>
                                    <input <?php if ($which_form == 'view' && !$viewer_is_invitor) { ?> readonly="readonly" <?php } ?>
                                            name="repayment_terms" class="form-control" style="line-height:1.5"
                                            placeholder="<?= lang('placeholder_repayments_terms_text') ?>"
                                            type="number"
                                            min="1" step="1"
                                            max="24"
                                            value="<?= $loan_product_invitation ? $loan_product_invitation->lpi_repayment_terms : '' ?><?= $this->session->flashdata('flash_repayment_terms') ? $this->session->flashdata('flash_repayment_terms') : '' ?>">
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <label><?= lang('set_start_date_text') ?></label>
                                <div>
                                    <div class="input-group">
                                        <span class="input-group-addon bg-loan b-0"><i
                                                    class="icon-calender"></i></span>
                                        <input readonly="readonly" type="text" name="start_date" class="form-control"
                                               placeholder="<?= lang('set_start_date_text') ?>" id="start_date"
                                               required
                                               value="<?php
                                               if ($loan_product_invitation) {
                                                   if ($loan_product_invitation->lpi_start_date != 0) {
                                                       echo $loan_product_invitation->lpi_start_datestring;
                                                   }
                                               }
                                               ?><?= $this->session->flashdata('flash_start_date') ? $this->session->flashdata('flash_start_date') : '' ?>"
                                        >

                                    </div><!-- input-group -->
                                </div>
                            </fieldset>

                            <?php if ($which_form == 'edit') { ?>
                                <br><br>
                                <hr>
                                <fieldset class="form-group row">

                                    <div style="padding-left: 1%">
                                        <div>
                                            <input id="start_thrift" type="checkbox" name="start_thrift">
                                            <label for="">
                                                <?= lang('start_thrift_long_text') ?>
                                            </label>
                                        </div>
                                    </div>
                                </fieldset>
                                <hr>
                            <?php } ?>

                            <?php if ($which_form == 'add') { ?>
                                <button type="submit"
                                        class="btn btn-primary"><?= lang('submit_btn_create_thrift_text') ?></button>
                            <?php } ?>

                            <?php if ($which_form == 'view' && $viewer_is_invitor) { ?>
                                <button type="submit"
                                        class="btn btn-primary"><?= lang('submit_btn_update_text') ?></button>
                            <?php } ?>

                        </form>
                </div><!-- end col -->


            </div><!-- end row -->
        </div>
    </div><!-- end col -->
</div>


</div> <!-- container -->


<!-- placeholder: "<?= lang('select_colleagues_text') ?>", -->

<!-- End content-page -->


<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->

<script>
    $(function () {

        $("#product_price").on("focusout", function () {
            console.log($(this).val());

            var new_val = numberWithCommas($(this).val());
            console.log(new_val);
            $("#product_price").val(new_val);
        });

        $("#loan_product_form").on("submit", function (e) {

            $("#product_price").val(replace_commas($("#product_price").val()));
            console.log($("#product_price").val());
        });


        function replace_commas(x) {
            return x.toString().replace(",", "");
        }

        function numberWithCommas(x) {
            var parts = x.toString().split(".");
            return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
        }


        $('.select_colleagues').select2({


            allowClear: true,

            "language": {
                "noResults": function () {
                    var btn_html = "<?= lang('no_colleaguse_found_text')?>";
                    var div = document.createElement("div");
                    div.innerHTML = btn_html;
                    return div;
                }
            },

            minimumInputLength: 1,
            ajax: {
                url: '<?= base_url() ?>thrift_module/get_colleagues_by_select2',
                dataType: 'json',
                cache: true,
                delay: 1000,
                allowClear: true,


                data: function (params) {

                    return {
                        choose_from_colleagues: '<?= $choose_from_colleagues ? 'yes' : 'no'?>',
                        keyword: params.term, // search term
                        page: params.page,
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using loan formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    //console.log(data.items);

                    return {
                        results: data.items,
                        pagination: {
                            more: data.more_pages
                        }
                    };
                },

                /*templateResult: formatRepo,
                 templateSelection: formatRepoSelection,*/

                escapeMarkup: function (markup) {
                    return markup;
                } // let our loan formatter work


            }

        });

        $('#select_colleagues').val('selected').trigger('change');

        $("#incl_ext_mem_yes").on('click', function () {
            $("#ext_mems_fieldset").show();
        });

        $("#incl_ext_mem_no").on('click', function () {
            $("#ext_mems_fieldset").hide();

            //$('.external_members').select2('data', null);

            $('.external_members').find('option').remove()
        });

        $(".external_members").select2({
            tags: true,
            language: {
                noResults: function (params) {
                    return 'Please type an email address'
                }
            }

        });


        <?php if($which_form == 'add' || $viewer_is_invitor) { ?>

        $('#start_date').datepicker({
            autoclose: true,
            disableEntry: true,
            format: "yyyy-mm-dd",
            todayHighlight: true,
            startDate: "<?= $loan_thrift_start_delay?>",
            endDate: "<?= $loan_thrift_end_delay?>"
        });

        <?php if(empty($_POST) && $which_form != "view") { ?>
        swal("<?=$warning_title?>", "<?=$warning_message?>");
        <?php } ?>

        <?php } ?>

        $('#member-table').DataTable({
            order: [[2, 'asc']],
            "columnDefs": [
                {"visible": false, "targets": 2}
            ]
        });

        $("#super_edit_add_member_btn").on("click", function () {
            $(".super_edit_wrap").show();
        });

    });
</script>
