<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['user_profile_module/user_profile_overview/(:any)'] = 'UserProfileController/showUserProfile';
$route['user_profile_module/edit_user_info/(:any)'] = 'UserProfileController/editUserInfo';
$route['user_profile_module/update_user_info/(:any)'] = 'UserProfileController/updateUserInfo';
$route['user_profile_module/user_timeline/(:any)'] = 'UserProfileController/showUserTimeline';

$route['user_profile_module/change_org_email_domain_check/(:any)'] = 'UserProfileController/changeOrgEmailDomainCheck';

$route['user_profile_module/get_countries_by_select2'] = 'UserProfileController/getCountriesBySelect2';
$route['user_profile_module/get_states_by_select2'] = 'UserProfileController/getStatesBySelect2';
$route['user_profile_module/get_cities_by_select2'] = 'UserProfileController/getCitiesBySelect2';


//paystack
//(:any) is $user_id
$route['user_profile_module/create_a_paystack_transfer_recipient/(:any)'] = 'UserProfileController/createAPaystackTransferRecipient/$1';
$route['user_profile_module/create_paystack_customer/(:any)'] = 'UserProfileController/createPaystackCustomer/$1';
$route['user_profile_module/authorize_paystack_customer/(:any)'] = 'UserProfileController/authorizePaystackCustomer/$1';

//google TF auth
$route['user_profile_module/google_tf_auth/(:any)'] = 'UserProfileController/googleTfAuth';
$route['user_profile_module/google_tf_qr_validate'] = 'UserProfileController/google_tf_qr_validate';

