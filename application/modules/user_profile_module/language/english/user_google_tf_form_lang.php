<?php

/*page texts*/
$lang['page_title_text'] = 'Edit Information';
$lang['page_subtitle_text'] = 'Edit information here';
$lang['box_title_text'] = 'Google two factor authentication';

$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'User Profile';
$lang['breadcrumb_page_text'] = 'Edit info';

$lang['address_separator_lang'] = 'Address';
$lang['bank_info_separator_lang'] = 'Bank Information';

/*General settings form texts*/
$lang['label_google_tf_auth_status'] = 'Google two factor authentication status';


/*--------------------------------------------------------*/


$lang['button_submit_text'] = 'Update';
$lang['button_validate_text'] = 'Validate';

/*validation error texts*/
$lang['status_required_text'] = 'Status is required';

/*success messages*/
$lang['update_success_text'] = 'Successfully Updated Information';

$lang['not_found_text'] = 'Not Found';





