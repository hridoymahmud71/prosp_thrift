<?php

/*page texts*/
$lang['page_title_my_profile_text'] = 'My Profile';
$lang['page_title_user_profile_text'] = 'User\'s Profile';
$lang['page_subtitle_text'] = 'Overview';
$lang['box_title_text'] = 'About Me';

$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'User Profile';
$lang['breadcrumb_page_text'] = 'Personal Information overview';

$lang['about_me_box_title_text'] = 'Personal Information';

/*about section*/
$lang['about_section_basic_info_text'] = 'Basic Information';
$lang['about_email_domain_check_text'] = 'Email Domain Check';
$lang['about_section_name_text'] = 'Name';
$lang['about_section_position_text'] = 'Position';
$lang['about_section_company_text'] = 'Company';
$lang['about_section_age_text'] = 'Age';
$lang['about_section_dob_text'] = 'Date of Birth';
$lang['about_section_email_text'] = 'Email';
$lang['about_section_phone_text'] = 'Phone';

$lang['about_section_address_text'] = 'Address';
$lang['about_section_street1_text'] = 'Street 1';
$lang['about_section_street2_text'] = 'Street 2';
$lang['about_section_country_text'] = 'Country';
$lang['about_section_state_text'] = 'State';
$lang['about_section_city_text'] = 'City';

$lang['about_section_financial_info_text'] = 'Financial Information';
$lang['about_employee_id_number_text'] = 'Employee Id Number';
$lang['about_section_bvn_text'] = 'BVN';
$lang['about_section_bank_text'] = 'Bank';
$lang['about_section_account_number_text'] = 'Account Number';

$lang['about_section_payment_info_text'] = 'Payment Information';
$lang['about_section_user_chosen_payment_method_text'] = 'Chosen Payment Method';
$lang['about_section_paystack_authorization_code_text'] = 'Paystack Authorization Code';
$lang['about_section_paystack_customer_code_text'] = 'Paystack Customer Code';
$lang['about_section_paystack_recipient_code_text'] = 'Paystack Recipient Code';

$lang['default_phone_text'] = 'Default Phone:';
$lang['additional_phone_text'] = 'Additional Phone:';

$lang['default_email_text'] = 'Default Email:';
$lang['additional_email_text'] = 'Additional Email:';
$lang['subdomain_text'] = 'Subdomain:';

/*not found text */
$lang['not_found_no_position_text'] = 'No Position';
$lang['not_found_no_company_text'] = 'No Company';
$lang['not_found_no_age_text'] = 'Age Unknown';
$lang['not_found_no_email_text'] = 'No Email';
$lang['not_found_no_phone_number_text'] = 'No Phone Number';
$lang['not_found_no_address_text'] = 'Address Unknown';
$lang['not_found_no_subdomain_text'] = 'Not found subdomain';

$lang['unavailable_text'] = 'Unavailable';
$lang['deleted_text'] = 'Deleted';


$lang['paystack_section_title_text'] = 'Paystack';
$lang['paystack_section_description_text'] = 'Manage User For Paystack';
$lang['paystack_create_recipient_button_text'] = 'Create Paystack Recipient';
$lang['paystack_create_paystack_customer_button_text'] = 'Create Paystack Customer';
$lang['paystack_authorize_paystack_customer_button_text'] = 'Authorize Paystack Customer';
?>