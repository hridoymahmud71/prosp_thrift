<!-- Content Wrapper. Contains page content -->

<style>
    @import url(http://fonts.googleapis.com/css?family=Open+Sans:400,700,300);

    .form-control, .thumbnail {
        border-radius: 2px;
    }

    .btn-danger {
        background-color: #B73333;
    }

    /* File Upload */
    .fake-shadow {
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
    }

    .fileUpload {
        position: relative;
        overflow: hidden;
    }

    .fileUpload #user_profile_image-id {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 33px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .fileUpload #user_cover_image-id {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 33px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .user_profile_image-preview {
        max-width: 25%;
    }

    .user_cover_image-preview {
        max-width: 40%;
    }
</style>


<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php echo lang('page_title_text') ?>
                </h4>
                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href=""><?php echo lang('breadcrumb_home_text') ?></a></li>
                    <li class="breadcrumb-item"><a
                                href="user_profile_module/user_profile_overview/<?= $user_id; ?>"><?php echo lang('breadcrumb_section_text') ?></a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_text') ?></li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <?php if ($this->session->flashdata('employer_insert')) { ?>
                    <div class="text-center alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong><?php echo $this->session->flashdata('employer_insert') ?></strong>
                    </div>
                <?php } ?>

                <?php if ($this->session->flashdata('employer_insert_error')) { ?>
                    <div class="text-center alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong><?php echo $this->session->flashdata('employer_insert_error') ?></strong>
                    </div>
                <?php } ?>

                <?php if ($this->session->flashdata('employer_exist_error')) { ?>
                    <div class="text-center alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong><?php echo $this->session->flashdata('employer_exist_error') ?></strong>
                    </div>
                <?php } ?>

                <?php if ($this->session->flashdata('thrift_start_cond_error')) { ?>
                    <div class="text-center alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong><?php echo $this->session->flashdata('thrift_start_cond_error') ?></strong>
                    </div>
                <?php } ?>

                <h4 class="header-title m-t-0 m-b-30"><?php echo lang('employee_add_form_header_text') ?></h4>
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="box box-primary">
                            <div class="box-body box-profile box-widget widget-user-2">
                                <div class="widget-user-header  custom-user-background">
                                    <img class="profile-user-img img-responsive img-circle" style="max-width:100%;"
                                        <?php if ($user_info->user_profile_image == '' || $user_info->user_profile_image == null) { ?>
                                            src="<?php echo base_url() . 'base_demo_images/user_profile_image_demo.png' ?>"
                                        <?php } else { ?>
                                            src="<?php echo $this->config->item('pg_upload_source_path') ?>image/<?= $user_info->user_profile_image ?>"
                                        <?php } ?>
                                         alt="User profile picture">
                                    <?php if ($is_own_profile == 'others_profile') { ?>
                                        <!--<a href="#" class="btn btn-warning btn-block">
                                            <i class="fa fa-envelope fa-pull-right" aria-expanded="false"></i>
                                            <b>Message</b>
                                        </a>-->
                                    <?php } ?>
                                </div>
                                <hr>
                                <ul class="">
                                    <h5 class="text-muted text-center">
                                        <?php if ($this->ion_auth->in_group('employer') && !$this->session->userdata('org_contact_user_id')) { ?>
                                            <strong>
                                                <?php echo $user_info->company; ?>
                                            </strong>
                                        <?php } else { ?>
                                            <strong>
                                                <?php echo $user_info->first_name . ' ' . $user_info->last_name ?>
                                            </strong>
                                        <?php } ?>
                                    </h5>
                                </ul>
                                <hr>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-6">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?php echo lang('box_title_text') ?></h3>
                                <br>
                                <div class=" col-md-offset-2 col-md-8" style="color: darkred;font-size: larger">
                                    <?php if ($validation_errors) {
                                        echo $validation_errors;
                                        echo '<br>';
                                    }
                                    ?>
                                    <?php if ($this->session->flashdata('profile_image_upload_error')) {
                                        echo $this->session->flashdata('profile_image_upload_error');
                                        echo '<br>';
                                    }
                                    ?>
                                    <?php if ($this->session->flashdata('profile_image_resize_error')) {
                                        echo $this->session->flashdata('profile_image_resize_error');
                                        echo '<br>';
                                    }
                                    ?>
                                    <?php if ($this->session->flashdata('cover_image_upload_error')) {
                                        echo $this->session->flashdata('cover_image_upload_error');
                                        echo '<br>';
                                    }
                                    ?>
                                    <?php if ($this->session->flashdata('cover_image_resize_error')) {
                                        echo $this->session->flashdata('cover_image_resize_error');
                                        echo '<br>';
                                    }
                                    ?>
                                    <!--demo-->
                                    <?php if ($this->session->flashdata('file_upload_errors'))
                                        echo $this->session->flashdata('file_upload_errors');
                                    ?>

                                    <?php if ($this->session->flashdata('subdomain_error'))
                                        echo $this->session->flashdata('subdomain_error');
                                    ?>
                                    <!--demo-->
                                </div>
                                <div class="col-md-2"></div>
                                <div class=" col-md-offset-2 col-md-8" style="color: darkgreen;font-size: larger">
                                    <!--demo-->
                                    <?php if ($this->session->flashdata('file_upload_success')) {
                                        echo $this->session->flashdata('file_upload_success');
                                        echo '<br>';
                                    }
                                    ?>
                                    <!--demo-->
                                    <?php if ($this->session->flashdata('update_success')) {
                                        echo $this->session->flashdata('update_success');
                                        echo '<br>';
                                    }
                                    ?>
                                    <?php if ($this->session->flashdata('profile_image_upload_success')) {
                                        echo $this->session->flashdata('profile_image_upload_success');
                                        echo '<br>';
                                    }
                                    ?>
                                    <?php if ($this->session->flashdata('profile_image_resize_success')) {
                                        echo $this->session->flashdata('profile_image_resize_success');
                                        echo '<br>';
                                    }
                                    ?>
                                    <?php if ($this->session->flashdata('cover_image_upload_success')) {
                                        echo $this->session->flashdata('cover_image_upload_success');
                                        echo '<br>';
                                    }
                                    ?>
                                    <?php if ($this->session->flashdata('cover_image_resize_success')) {
                                        echo $this->session->flashdata('cover_image_resize_success');
                                        echo '<br>';
                                    }
                                    ?>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <!-- /.box-header -->

                            <!-- form start -->
                            <form action="<?php echo base_url() . 'user_profile_module/edit_user_info/' . $user_id ?>"
                                  role="form"
                                  id="" method="post" enctype="multipart/form-data">
                                <div class="box-body">
                                    <input type="hidden" name="user_id" value="<?php echo $user_id ?>">
                                    <div class="form-group" <?php if ($this->ion_auth->in_group('employer') && !$this->session->userdata('org_contact_user_id')) { ?> style="display: none" <?php } ?>>
                                        <label for="first_name"><?php echo lang('label_first_name_text') ?></label>

                                        <input type="text" name="first_name" class="form-control" id="first_name"
                                               value="<?php echo $user_info->first_name ?>"
                                               placeholder="<?php echo lang('placeholder_first_name_text') ?>">
                                    </div>
                                    <div class="form-group" <?php if ($this->ion_auth->in_group('employer') && !$this->session->userdata('org_contact_user_id')) { ?> style="display: none" <?php } ?>>
                                        <label for="last_name"><?php echo lang('label_last_name_text') ?></label>

                                        <input type="text" name="last_name" class="form-control" id="last_name"
                                               value="<?php echo $user_info->last_name ?>"
                                               placeholder="<?php echo lang('placeholder_last_name_text') ?>">
                                    </div>
                                    <div class="form-group" <?php if (!$this->ion_auth->in_group('employer') || ($this->ion_auth->in_group('employer') && $this->session->userdata('org_contact_user_id'))) { ?> style="display: none" <?php } ?> >
                                        <label for="last_name"><?php echo lang('label_company_text') ?></label>

                                        <input type="text" name="company" class="form-control" id="company"
                                               value="<?php echo $user_info->company ?>"
                                               placeholder="<?php echo lang('placeholder_company_text') ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="email"><?php echo lang('label_email_text') ?></label>

                                        <input type="text" name="email" class="form-control" id="email"
                                               value="<?php echo $user_info->email ?>"
                                               placeholder="<?php echo lang('placeholder_email_text') ?>" <?php if(!$this->ion_auth->is_admin()) { echo " readonly "; } ?> >
                                    </div>
                                    <div class="form-group">
                                        <label for="user_additional_email"><?php echo lang('label_user_additional_email_text') ?></label>

                                        <input type="text" name="user_additional_email" class="form-control"
                                               id="user_additional_email"
                                               value="<?php echo $user_info->user_additional_email ?>"
                                               placeholder="<?php echo lang('placeholder_user_additional_email_text') ?>">
                                    </div>


                                    <?php if($this->ion_auth->ion_auth->in_group('employer',$user_id)) { ?>
                                    <div class="form-group">
                                        <label for="subdomain"><?php echo lang('label_subdomain_text') ?></label>

                                        <input type="text" name="subdomain" class="form-control" readonly
                                               id="subdomain"
                                               value="<?php echo $user_info->subdomain ?>"
                                               placeholder="<?php echo lang('placeholder_subdomain_text') ?>">
                                    </div>
                                    <?php } ?>


                                    <div class="form-group">
                                        <label for="change_password"><?php echo lang('label_change_password_text') ?></label>

                                        <div>
                                            <input class="checkbox_chk" name="change_password" id="checkbox1"
                                                   value="1" type="radio"
                                                <?php if ($this->input->post('change_password') == 1) {
                                                    echo ' checked ';
                                                } ?>
                                            >
                                            <label for="checkbox1"><?php echo lang('option_change_password_yes_text') ?></label>
                                        </div>
                                        <div>
                                            <input class="checkbox_chk" name="change_password" id="checkbox2"
                                                   value="0" type="radio" checked>
                                            <label for="checkbox2"><?php echo lang('option_change_password_no_text') ?></label>
                                        </div>
                                    </div>
                                    <div id="password_wrapper">
                                        <div class="form-group">
                                            <label for="password"><?php echo lang('label_password_text') ?>
                                                <span id="see_password" title="see" style="color:#2b2b2b"><i
                                                            class="fa fa-eye fa-lg" aria-hidden="true"></i></span>
                                            </label>

                                            <input type="password" name="password" class="form-control"
                                                   onclick="return show_hide_password_wrapper()"
                                                   id="password"
                                                   value=""
                                                   placeholder="<?php echo lang('placeholder_password_text') ?>">
                                        </div>

                                        <div class="form-group">
                                            <label for="confirm_password"><?php echo lang('label_confirm_password_text') ?>
                                                <span id="see_confirm_password" title="see" style="color:#2b2b2b"><i
                                                            class="fa fa-eye fa-lg" aria-hidden="true"></i></span>
                                            </label>

                                            <input type="password" name="confirm_password" class="form-control"
                                                   id="confirm_password"
                                                   value=""
                                                   placeholder="<?php echo lang('placeholder_confirm_password_text') ?>">
                                        </div>
                                    </div>
                                    <!--if true image from database, else static image //currently not working -->
                                    <?php $image_found = false; ?>
                                    <!--profile image upload snippet starts-->
                                    <div class="form-group">
                                        <label for="user_profile_image"><?php echo lang('label_user_profile_image_text') ?></label>

                                        <div class="main-img-preview">
                                            <?php $image_found = false ?>
                                            <?php if ($user_info->user_profile_image) { ?>
                                                <div><?php $image_found = true ?></div>
                                                <img class="thumbnail user_profile_image-preview"
                                                     src="<?php echo $this->config->item('pg_upload_source_path') ?>image/<?= $user_info->user_profile_image ?>">
                                            <?php } ?>
                                            <?php if ($image_found == false) { ?>
                                                <img class="thumbnail user_profile_image-preview"
                                                     src="<?php echo base_url() . 'base_demo_images/user_profile_image_demo.png' ?>">
                                            <?php } ?>
                                        </div>
                                        <div class="input-group">
                                            <input id="fake_upload_user_profile_image-id"
                                                   class="form-control fake-shadow"
                                                   placeholder="Choose File" disabled="disabled">
                                            <div class="input-group-btn">
                                                <div class="fileUpload btn btn-primary fake-shadow">
                                                    <span><i class="glyphicon glyphicon-upload"></i> <?php echo lang('upload_user_profile_image_text') ?></span>
                                                    <input id="user_profile_image-id" name="user_profile_image"
                                                           type="file"
                                                           value=""
                                                           class="attachment_upload">
                                                </div>
                                            </div>
                                        </div>
                                        <p class="help-block"><?php echo lang('help_user_profile_image_text') ?></p>
                                    </div>
                                    <!--profile image upload snippet ends-->
                                    <div class="form-group">
                                        <label for="phone"><?php echo lang('label_phone_text') ?></label>

                                        <input type="text" name="phone" class="form-control" id="phone"
                                               value="<?php echo $user_info->phone ?>"
                                               placeholder="<?php echo lang('placeholder_user_additional_phone_text') ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="user_additional_phone"><?php echo lang('label_user_additional_phone_text') ?></label>

                                        <input type="text" name="user_additional_phone" class="form-control"
                                               id="user_additional_phone"
                                               value="<?php echo $user_info->user_additional_phone ?>"
                                               placeholder="<?php echo lang('placeholder_user_additional_phone_text') ?>">
                                    </div>
                                    <!--<div id="user_age_wrapper">
                                        <div class="form-group">
                                            <label for="user_age"><?php /*echo lang('label_user_age_text') */ ?></label>

                                            <input type="number" name="user_age" class="form-control" id="user_age"
                                                   value="<?php /*echo $user_info->user_age */ ?>"
                                                   placeholder="<?php /*echo lang('placeholder_user_age_text') */ ?>">
                                        </div>
                                    </div>-->
                                    <div class="form-group">
                                        <label for="user_dob"><?php echo lang('label_user_dob_text') ?></label>

                                        <input type="text" readonly name="user_dob" class="form-control user_dob" id=""
                                               value="<?php echo $user_info->user_dob_datestring ?>"
                                               placeholder="<?php echo lang('placeholder_user_dob_text') ?>">
                                    </div>
                                    <br>
                                    <hr>

                                        <div class="form-group" <?php if(0) { ?> style="display: none"  <?php } ?>>
                                            <label for="user_salary"><?php echo lang('label_user_salary_text') ?></label>

                                            <input type="text" name="user_salary" class="form-control"
                                                   id="user_salary"
                                                   value="<?php  echo  $user_info->user_salary? number_format( (double)$user_info->user_salary, 2, '.', ''):0.00 ?>"
                                                   placeholder="<?php echo lang('placeholder_user_salary_text') ?>">
                                        </div>

                                    <?= lang('address_separator_lang')?>
                                    <hr>
                                    <div class="form-group">
                                        <label for="user_street_1"><?php echo lang('label_user_street_1_text') ?></label>

                                        <input type="text" name="user_street_1" class="form-control" id="user_street_1"
                                               value="<?php echo $user_info->user_street_1 ?>"
                                               placeholder="<?php echo lang('placeholder_user_street_1_text') ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="user_street_2"><?php echo lang('label_user_street_2_text') ?></label>

                                        <input type="text" name="user_street_2" class="form-control" id="user_street_2"
                                               value="<?php echo $user_info->user_street_2 ?>"
                                               placeholder="<?php echo lang('placeholder_user_street_2_text') ?>">
                                    </div>


                                    <div class="form-group">
                                        <label for="user_country"><?php echo lang('label_user_country_text') ?></label>
                                        <select style="width:100%" name="user_country" id="user_country"
                                                class="select2 user_country">
                                            <?php if ($user_info->user_country) { ?>
                                                <option value="<?= $user_info->user_country ?>"
                                                        selected="selected"><?= $user_info->user_country_name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="user_state"><?php echo lang('label_user_state_text') ?></label>
                                        <select style="width:100%" name="user_state" id="user_state"
                                                class="select2 user_state">
                                            <?php if ($user_info->user_state) { ?>
                                                <option selected="selected"
                                                        value="<?= $user_info->user_state ?>"><?= $user_info->user_state_name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="user_city"><?php echo lang('label_user_city_text') ?></label>
                                        <select style="width:100%" name="user_city" id="user_city"
                                                class="select2 user_city">
                                            <?php if ($user_info->user_city) { ?>
                                                <option selected="selected"
                                                        value="<?= $user_info->user_city ?>"><?= $user_info->user_city_name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <?php if($this->ion_auth->is_admin() || $this->ion_auth->in_group('employee')) { ?>
                                    <br>
                                    <hr>
                                    <?= lang('bank_info_separator_lang')?>
                                    <hr>

                                    <div class="form-group row">
                                        <label for="" class="col-sm-12 text-muted m-t-5"><?= lang('bank_select_text') ?></label>
                                        <div class="col-12">
                                            <select style="width: 100%" class="form-control user_bank" name="user_bank" id="" >
                                                <option value=""><?= lang('bank_select_text') ?></option>
                                                <?php if ($banks) { ?>
                                                    <?php foreach ($banks as $bank) { ?>
                                                        <option value="<?= $bank->bank_id ?>" <?php if($bank->bank_id == $user_info->user_bank){echo " selected ";} ?>  ><?= $bank->bank_name ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label for=""
                                               class="col-sm-12 text-muted m-t-5"><?= lang('label_user_bank_account_no_text') ?></label>
                                        <div class="col-12">
                                            <input class="form-control" type="text" name="user_bank_account_no"
                                                   placeholder="<?= lang('placeholder_user_bank_account_no_text') ?>"
                                                   value="<?= $user_info->user_bank_account_no ?>">
                                        </div>
                                    </div>

                                    <?php } ?>


                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" id="btnsubmit" class="btn btn-primary">
                                        <?php echo lang('button_submit_text') ?>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>
</div>
<script>
    $(document).ready(function () {
        var brand = document.getElementById('user_profile_image-id');
        brand.className = 'attachment_upload';
        brand.onchange = function () {
            document.getElementById('fake_upload_user_profile_image-id').value = this.value.substring(12);
        };

        // Source: http://stackoverflow.com/a/4459419/6396981
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.user_profile_image-preview').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#user_profile_image-id").change(function () {
            readURL(this);
        });
    });
</script>

<script>
    $(function () {

        $('.user_dob').datepicker({
            format: 'yyyy-mm-dd',
            endDate: '-18y',
            autoclose: true
        });

        $('#password_wrapper').hide();
        $('input[type="radio"]').click(function () {
            var if_show_pass = $(this).val();
            //alert(if_show_pass)
            if (if_show_pass == 1) {
                //alert('show');
                $('#password_wrapper').show();
            } else {
                $('#password_wrapper').hide();
            }
        });

        /*----------------------------------------------*/

        $('.user_country').select2({

            allowClear: true,
            placeholder: '<?= lang('placeholder_user_country_text')?>',

            "language": {
                "noResults": function () {
                    var btn_html = "<?= lang('country_not_found_text')?>";
                    var div = document.createElement("div");
                    div.innerHTML = btn_html;
                    return div;
                }
            },

            minimumInputLength: 1,
            ajax: {
                url: '<?= base_url() ?>user_profile_module/get_countries_by_select2',
                dataType: 'json',
                cache: true,
                delay: 1000,
                allowClear: true,


                data: function (params) {

                    return {
                        keyword: params.term, // search term
                        page: params.page,
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    //console.log(data.items);

                    return {
                        results: data.items,
                        pagination: {
                            more: data.more_pages
                        }
                    };
                },

                /*templateResult: formatRepo,
                 templateSelection: formatRepoSelection,*/

                escapeMarkup: function (markup) {
                    return markup;
                } // let our custom formatter work


            }

        });

        $('.user_state').select2({

            allowClear: true,
            placeholder: '<?= lang('placeholder_user_state_text')?>',

            "language": {
                "noResults": function () {
                    var btn_html = "<?= lang('state_not_found_text')?>";
                    var div = document.createElement("div");
                    div.innerHTML = btn_html;
                    return div;
                }
            },

            minimumInputLength: 1,
            ajax: {
                url: '<?= base_url() ?>user_profile_module/get_states_by_select2',
                dataType: 'json',
                cache: true,
                delay: 1000,
                allowClear: true,


                data: function (params) {
                    return {
                        user_country: $('#user_country').val(),
                        keyword: params.term, // search term
                        page: params.page,
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    //console.log(data.items);

                    return {
                        results: data.items,
                        pagination: {
                            more: data.more_pages
                        }
                    };
                },

                /*templateResult: formatRepo,
                 templateSelection: formatRepoSelection,*/

                escapeMarkup: function (markup) {
                    return markup;
                } // let our custom formatter work


            }

        });

        $('.user_city').select2({

            allowClear: true,
            placeholder: '<?= lang('placeholder_user_city_text')?>',

            "language": {
                "noResults": function () {
                    var btn_html = "<?= lang('city_not_found_text')?>";
                    var div = document.createElement("div");
                    div.innerHTML = btn_html;
                    return div;
                }
            },

            minimumInputLength: 1,
            ajax: {
                url: '<?= base_url() ?>user_profile_module/get_cities_by_select2',
                dataType: 'json',
                cache: true,
                delay: 1000,
                allowClear: true,


                data: function (params) {

                    return {
                        user_state: $('#user_state').val(),
                        keyword: params.term, // search term
                        page: params.page,
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    //console.log(data.items);

                    return {
                        results: data.items,
                        pagination: {
                            more: data.more_pages
                        }
                    };
                },

                /*templateResult: formatRepo,
                 templateSelection: formatRepoSelection,*/

                escapeMarkup: function (markup) {
                    return markup;
                } // let our custom formatter work


            }

        });

        $('.user_bank').select2();

        /*$('#user_country').val('selected').trigger('change');
        $('#user_state').val('selected').trigger('change');
        $('#user_city').val('selected').trigger('change');*/

    });
</script>

<script>
    $(function () {

        $('#see_password').on('click', function () {
            $('#password').attr('type', 'text');
        });

        $('#see_confirm_password').on('click', function () {
            $('#confirm_password').attr('type', 'text');
        });


    });
</script>
