<?php if ($user_info->user_cover_image != '') { ?>
    <style>
        /*.custom-user-background{
             background : url('


        <?php echo base_url() . $image_directory . '/' . $user_info->user_cover_image; ?>


                        ') no-repeat center center;
                                 opacity: .2;
                             }*/
    </style>
<?php } ?>

<!--userprofile_menu_section starts-->
<div class="box box-primary">
    <div class="box-body box-profile box-widget widget-user-2">

        <div class="widget-user-header  custom-user-background">

            <img class="profile-user-img img-responsive img-circle"
                <?php if ($user_info->user_profile_image == '' || $user_info->user_profile_image == null) { ?>
                    src="<?php echo base_url() . 'project_base_assets/base_demo_images/user_profile_image_demo.png' ?>"
                <?php } else { ?>
                    src="<?php echo base_url() . $image_directory . '/' . $user_info->user_profile_image ?>"
                <?php } ?>
                 alt="User profile picture">

            <h3 class="profile-username text-center"><?php echo $user_info->first_name . ' ' . $user_info->last_name ?></h3>
            <p class="text-muted text-center">
                <?php
                $i = 0;
                foreach ($groups_of_a_user as $a_group_of_a_user) {
                    $i++;
                    if ($i != 1) {
                        echo ',';
                    }

                    if ($is_admin == 'admin') { ?>
                        <a href="#"><?php echo $a_group_of_a_user->user_group_name; ?></a>
                    <?php } else {
                        echo $a_group_of_a_user->user_group_name;
                    }

                }
                ?>
            </p>

            <p class="text-muted text-center">
                <?php if ($user_info->user_position == '') {
                    echo lang('upms_no_position_text');
                } else {
                    echo $user_info->user_position;
                } ?>
            </p>
            <p class="text-muted text-center">
                <strong>
                    <?php if ($user_info->company == '') {
                        echo lang('upms_no_position_text');
                    } else {
                        echo $user_info->company;
                    } ?>
                </strong>
            </p>
            <?php if ($is_own_profile == 'others_profile') { ?>
                <!--<a href="#" class="btn btn-warning btn-block">
                    <i class="fa fa-envelope fa-pull-right" aria-expanded="false"></i>
                    <b>Message</b>
                </a>-->
            <?php } ?>
        </div>


        <hr>

        <ul class="nav nav-stacked">
            <li>
                <?php if ($is_own_profile == 'others_profile' && ($is_admin == 'admin' || $is_staff == 'staff')) { ?>
                    <a href="<?php echo base_url() . 'project_module/my_projects/seen_by_other/'
                        . $user_info->user_id ?>">
                        <?php echo lang('upms_users_projects_text') ?>
                        <span class="pull-right badge bg-blue">
                            <?php echo lang('upms_running_text') ?>
                            :
                            <?php echo $running_projects_count?>
                        </span>
                    </a>
                <?php } ?>

                <?php if ($is_own_profile == 'own_profile') { ?>
                    <a href="<?php echo base_url() . 'project_module/my_projects/' ?>">
                        <?php echo lang('upms_my_projects_text') ?>
                        <span class="pull-right badge bg-blue">
                            <?php echo lang('upms_running_text') ?>
                            :
                            <?php echo $running_projects_count?>
                        </span>
                    </a>
                <?php } ?>
            </li>
            <li>
                <?php if ($is_own_profile == 'others_profile' && ($is_admin == 'admin' || $is_staff == 'staff')) { ?>
                    <a href="<?php echo base_url() . 'task_module/show_my_tasks_from_my_projects/seen_by_other/'
                        . $user_info->user_id ?>">
                        <?php echo lang('upms_users_tasks_text') ?>
                        <span class="pull-right badge bg-aqua">
                            <?php echo lang('upms_running_text') ?>
                            :
                            <?php echo $running_tasks_count?>
                        </span>
                    </a>
                <?php } ?>

                <?php if ($is_own_profile == 'own_profile') { ?>
                    <a href="<?php echo base_url() . 'task_module/show_my_tasks_from_my_projects' ?>">
                        <?php echo lang('upms_my_tasks_text') ?>
                        <span class="pull-right badge bg-aqua">
                           <?php echo lang('upms_running_text') ?>
                            :
                            <?php echo $running_tasks_count?>
                        </span>
                    </a>
                <?php } ?>

            </li>
        </ul>
        <hr>
        <ul class="nav nav-stacked">
            <li>
                <a href="<?php echo base_url() . 'user_profile_module/user_profile_overview/' . $user_info->id ?>">
                    <?php echo lang('upms_info_text') ?>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url() . 'user_profile_module/user_timeline/' . $user_info->id ?>">
                    <?php echo lang('upms_timeline_text') ?>
                </a>
            </li>
        </ul>

    </div>

</div>
<!--userprofile_menu_section ends -->