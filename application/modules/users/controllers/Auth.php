<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 *  Modified By,
 *  Mahmudur Rahman
 *  Web Dev, RS Soft
 *
 * */

require APPPATH . 'libraries/mailchimp-api-master/src/Batch.php';
require APPPATH . 'libraries/mailchimp-api-master/src/MailChimp.php';
require APPPATH . 'libraries/mailchimp-api-master/src/Webhook.php';

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        /* extra line <starts> */
        $this->Utility_model->setTargetInACookie();
        $this->Utility_model->checkMaintenanceMode();
        /* extra line <ends> */

        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));

        $this->load->library('custom_log_library');

        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');
        $this->lang->load('users');

        $this->load->model('Custom_auth_model');
        $this->load->model('Ion_auth_model');
        $this->load->library('settings_module/custom_settings_library');
        $this->load->library('custom_datetime_library');


        /*for test  purpose : remove code in between*/
        set_time_limit(0);
        ini_set('MAX_EXECUTION_TIME', 3600);
        /*for test  purpose : remove code in between*/

    }

    // redirect if needed, otherwise display the user list
    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            /*redirect('auth/login', 'refresh');*/

            /*Redirect above is changed for HMVC Conversion
            see:  http://dmitriykravchuk.co.za/blog/2015/10/30/codeigniter-3-hmvc-ion-auth/
            */
            redirect('users/auth/login', 'refresh');
        }

        if (!$this->ion_auth->is_admin()) {      // remove this if if you want to enable this for non-admins

            // redirect them to the home page because they must be an administrator to view this

            //originally
            //return show_error('You must be an administrator to view this page.');

            redirect('users/auth/need_permission');
        } else {
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');


            // application/libraries
            // this segment us added for customized datetime
            $this->load->library('custom_datetime_library');

            //original demo
            //$this->_render_page('users/auth/index', $this->data);

            //print_r($this->data);die();
            $this->data['group_list'] = $this->Custom_auth_model->getGroups();

            $this->load->view("common_module/header");
            //$this->load->view("common_module/common_left");
            $this->load->view("users/auth/custom_folder/users_page", $this->data);
            $this->load->view("common_module/footer");

        }
    }


    // log the user in
    public function login()
    {
        $this->data['title'] = $this->lang->line('login_heading');

        //validate form input
        $this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required',
            array(
                'required' => $this->lang->line('identity_required')
            )
        );

        $this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required',
            array(
                'required' => $this->lang->line('password_required')
            )
        );

        if ($this->form_validation->run() == true) {

            // check to see if the user is logging in
            // check for "remember me"
            $remember = (bool)$this->input->post('remember');

            if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
                //if the login is successful
                //redirect them back to the home page

                //only employee can pass through here
                if (!$this->ion_auth->in_group('employee')) {
                    $this->session->set_flashdata('message', $this->lang->line('only_employee_can_login_text'));
                    redirect('users/auth/login');
                }

                $this->session->set_flashdata('message', $this->ion_auth->messages());

                /*creating log starts*/
                $this->custom_log_library->createALog
                (
                    $this->session->userdata('user_id'),                                    //1.    $created_by
                    '',                                                                     //2.    $created_for
                    'login_logout',                                                         //3.    $type
                    '',                                                                     //4.    $type_id
                    'logged_in',                                                            //5.    $activity
                    'employee',                                                             //6.    $activity_by
                    'admin',                                                                //7.    $activity_for
                    '',                                                                     //8.    $sub_type
                    '',                                                                     //9.    $sub_type_id
                    '',                                                                     //10.   $super_type
                    '',                                                                     //11.   $super_type_id
                    '',                                                                     //12.   $other_information
                    ''                                                                      //13.   $change_list
                );
                /*creating log ends*/

                /*if cookie has a path redirect there <starts>*/
                $cookie_name = 'redirect_path_after_login';
                if (isset($_COOKIE[$cookie_name])) {

                    $rediect_from_cookie = $_COOKIE[$cookie_name];
                    unset($_COOKIE[$cookie_name]);
                    setcookie($cookie_name, '', time() - 36000, '/');

                    redirect($rediect_from_cookie);
                }
                /*if cookie has a path redirect there <ends>*/

                redirect('common_module', 'refresh');
            } else {
                // if the login was un-successful
                // redirect them back to the login page
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('users/auth/login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
            }
        } else {
            // the user is not logging in so display the login page
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            //set email or username in flash so that
            $this->session->set_flashdata('identity', $this->input->post('identity'));

            $this->data['identity'] = array('name' => 'identity',
                'id' => 'identity',
                'type' => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['password'] = array('name' => 'password',
                'id' => 'password',
                'type' => 'password',
            );

            $settings_code = 'general_settings';
            $thrift_site_logo = $this->custom_settings_library->getASettingsValue($settings_code, 'thrift_site_logo');

            if ($thrift_site_logo == '' || $thrift_site_logo == null || $thrift_site_logo == false) {
                $this->data['site_logo'] = $this->custom_settings_library->getASettingsValue($settings_code, 'site_logo');
            } else {
                $this->data['site_logo'] = $thrift_site_logo;
            }

            $subdomain = $this->Utility_model->getSubdomain();
            $individual_logo = $subdomain != "" ? $this->Utility_model->getIndividualLogo($subdomain, false) : false;
            if ($individual_logo) {
                $this->data['site_logo'] = $individual_logo;
            }

            /*$this->_render_page('users/auth/login', $this->data);*/
            $this->_render_page('users/auth/custom_folder/login_page', $this->data);
        }
    }

    // log the user out
    public function logout()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }
        $this->Utility_model->setTargetInACookie();

        $this->data['title'] = "Logout";

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            '',                                                                     //2.    $created_for
            'login_logout',                                                         //3.    $type
            '',                                                                     //4.    $type_id
            'logged_out',                                                           //5.    $activity
            'employee',                                                             //6.    $activity_by
            'admin',                                                                //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            '',                                                                     //10.   $super_type
            '',                                                                     //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        // log the user out
        $logout = $this->ion_auth->logout();

        // redirect them to the login page
        $this->session->set_flashdata('message', $this->ion_auth->messages());
        redirect('users/auth/login', 'refresh');
    }

    // change password
    public function change_password()
    {
        $this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
        $this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
        $this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        $user = $this->ion_auth->user()->row();

        if ($this->form_validation->run() == false) {
            // display the form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
            $this->data['old_password'] = array(
                'name' => 'old',
                'id' => 'old',
                'type' => 'password',
            );
            $this->data['new_password'] = array(
                'name' => 'new',
                'id' => 'new',
                'type' => 'password',
                'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
            );
            $this->data['new_password_confirm'] = array(
                'name' => 'new_confirm',
                'id' => 'new_confirm',
                'type' => 'password',
                'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
            );
            $this->data['user_id'] = array(
                'name' => 'user_id',
                'id' => 'user_id',
                'type' => 'hidden',
                'value' => $user->id,
            );

            // render
            $this->_render_page('users/auth/change_password', $this->data);
        } else {
            $identity = $this->session->userdata('identity');

            $change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

            if ($change) {
                //if the password was successfully changed
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                $this->logout();
            } else {
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('users/auth/change_password', 'refresh');
            }
        }
    }

    // forgot password
    public function forgot_password()
    {

        /*extra --st*/
        $settings_code = 'general_settings';
        $thrift_site_logo = $this->custom_settings_library->getASettingsValue($settings_code, 'thrift_site_logo');

        if ($thrift_site_logo == '' || $thrift_site_logo == null || $thrift_site_logo == false) {
            $this->data['site_logo'] = $this->custom_settings_library->getASettingsValue($settings_code, 'site_logo');
        } else {
            $this->data['site_logo'] = $thrift_site_logo;
        }

        $subdomain = $this->Utility_model->getSubdomain();
        $individual_logo = $subdomain != "" ? $this->Utility_model->getIndividualLogo($subdomain, false) : false;
        if ($individual_logo) {
            $this->data['site_logo'] = $individual_logo;
        }
        /*extra --en*/

        // setting validation rules by checking whether identity is username or email
        if ($this->config->item('identity', 'ion_auth') != 'email') {
            $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
        } else {
            $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
        }


        if ($this->form_validation->run() == false) {
            $this->data['type'] = $this->config->item('identity', 'ion_auth');
            // setup the input
            $this->data['identity'] = array('name' => 'identity',
                'id' => 'identity',
            );

            if ($this->config->item('identity', 'ion_auth') != 'email') {
                $this->data['identity_label'] = $this->lang->line('forgot_password_identity_label');
            } else {
                $this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
            }

            // set any errors and display the form
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            //$this->_render_page('users/auth/forgot_password', $this->data);
            $this->_render_page('users/auth/custom_folder/forgot_password_page', $this->data);
        } else {
            $identity_column = $this->config->item('identity', 'ion_auth');
            $identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();

            if (empty($identity)) {

                if ($this->config->item('identity', 'ion_auth') != 'email') {
                    $this->ion_auth->set_error('forgot_password_identity_not_found');
                } else {
                    $this->ion_auth->set_error('forgot_password_email_not_found');
                }

                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect("users/auth/forgot_password", 'refresh');
            }

            // run the forgotten password method to email an activation code to the user
            $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

            if ($forgotten) {
                // if there were no errors
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect("users/auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
            } else {
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                //redirect("users/auth/forgot_password", 'refresh');
                redirect("users/auth/forgot_password", 'refresh');
            }
        }
    }

    // reset password - final step for forgotten password
    public function reset_password($code = NULL)
    {
        if (!$code) {
            show_404();
        }

        /*extra --st*/
        $settings_code = 'general_settings';
        $thrift_site_logo = $this->custom_settings_library->getASettingsValue($settings_code, 'thrift_site_logo');

        if ($thrift_site_logo == '' || $thrift_site_logo == null || $thrift_site_logo == false) {
            $this->data['site_logo'] = $this->custom_settings_library->getASettingsValue($settings_code, 'site_logo');
        } else {
            $this->data['site_logo'] = $thrift_site_logo;
        }

        $subdomain = $this->Utility_model->getSubdomain();
        $individual_logo = $subdomain != "" ? $this->Utility_model->getIndividualLogo($subdomain, false) : false;
        if ($individual_logo) {
            $this->data['site_logo'] = $individual_logo;
        }
        /*extra --en*/

        $user = $this->ion_auth->forgotten_password_check($code);

        if ($user) {
            // if the code is valid then display the password reset form

            //original rules
            //$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
            //$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');
            //re-written rule
            $this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|callback_validate_password');
            $this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required|matches[new]');


            if ($this->form_validation->run() == false) {
                // display the form

                // set the flash data error message if there is one
                $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

                $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
                $this->data['new_password'] = array(
                    'name' => 'new',
                    'id' => 'new',
                    'type' => 'password',
                    'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
                );
                $this->data['new_password_confirm'] = array(
                    'name' => 'new_confirm',
                    'id' => 'new_confirm',
                    'type' => 'password',
                    'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
                );

                $this->data['user_id'] = array(
                    'name' => 'user_id',
                    'id' => 'user_id',
                    'type' => 'hidden',
                    'value' => $user->id,
                );
                $this->data['csrf'] = $this->_get_csrf_nonce();
                $this->data['code'] = $code;

                // render
                //$this->_render_page('users/auth/reset_password', $this->data);
                $this->_render_page('users/auth/custom_folder/reset_password_page', $this->data);
            } else {
                // do we have a valid request?

                //if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id')) { //orginally
                if ($user->id != $this->input->post('user_id')) {
                    // something fishy might be up
                    $this->ion_auth->clear_forgotten_password_code($code);

                    show_error($this->lang->line('error_csrf'));

                } else {
                    // finally change the password
                    $identity = $user->{$this->config->item('identity', 'ion_auth')};

                    $change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

                    if ($change) {
                        // if the password was successfully changed
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        redirect("users/auth/login", 'refresh');
                    } else {
                        $this->session->set_flashdata('message', $this->ion_auth->errors());
                        //redirect('users/auth/reset_password/' . $code, 'refresh');
                        redirect('users/auth/reset_password/' . $code, 'refresh');

                    }
                }
            }
        } else {
            // if the code is invalid then send them back to the forgot password page
            $this->session->set_flashdata('message', $this->ion_auth->errors());
            redirect("users/auth/forgot_password", 'refresh');
        }
    }

    function validate_password($str)
    {
        if (preg_match("/^(?=.*[A-Z])(?=.*\d.*)(?=.*\W.*)[a-zA-Z0-9\S]{8,32}$/", $str) !== 0) {
            return true;
        } else {
            $this->form_validation->set_message("validate_password", $this->lang->line('strong_password_text'));
            return false;
        }
    }


    private function getMemIdNum($f_name, $l_name)
    {
        if (!$f_name) {
            $f_name = 'X';
        } else {
            $f_name = ucfirst($f_name[0]);
        }

        if (!$l_name) {
            $l_name = 'X';
        } else {
            $l_name = ucfirst($l_name[0]);
        }


        $mem_id_num = 'M' . $this->alphaNum(5, false, true) . $f_name . $l_name;


        $exists = $this->Custom_auth_model->checkIfMemIdNumExists($mem_id_num);

        if ($exists) {
            $x = $this->getMemIdNum($f_name, $l_name);
        } else {
            return $mem_id_num;
        }

        return $x;


    }

    private function getThrifterIdNum($f_name, $l_name)
    {
        if (!$f_name) {
            $f_name = 'X';
        } else {
            $f_name = ucfirst($f_name[0]);
        }

        if (!$l_name) {
            $l_name = 'X';
        } else {
            $l_name = ucfirst($l_name[0]);
        }


        $mem_id_num = 'T' . $this->alphaNum(5, false, true) . $f_name . $l_name;


        $exists = $this->Custom_auth_model->checkIfMemIdNumExists($mem_id_num);

        if ($exists) {
            $x = $this->getThrifterIdNum($f_name, $l_name);
        } else {
            return $mem_id_num;
        }
        return $x;


    }

    private function alphaNum($length = false, $only_alphabets = false, $only_integers = false)
    {
        if (!$length) {
            $length = 8;
        }

        $alphabets = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $integers = '0123456789';
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        if ($only_alphabets) {
            $characters = $alphabets;
        }

        if ($only_integers) {
            $characters = $integers;
        }

        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randomString;
    }

    public
    function getEmployersBySelect2()
    {

        if (isset($_REQUEST['keyword'])) {
            $keyword = $_REQUEST['keyword'];
        } else {
            $keyword = '';
        }

        if (isset($_REQUEST['page'])) {
            $page = $_REQUEST['page'];
        } else {
            $page = 1;
        }


        $limit = 10;

        $total_count = $this->Custom_auth_model->countTotalEmployerBySelect2($keyword);
        $offset = ($page - 1) * $limit;

        $end_count = $offset + $limit;
        $more_pages = $total_count > $end_count;

        $employers = $this->Custom_auth_model->getTotalEmployerBySelect2($keyword, $limit, $offset);

        $last_query = $this->db->last_query();

        $json_data = array();
        $items = array();


        if ($employers) {

            foreach ($employers as $an_employer) {
                $p = array();
                $p['id'] = $an_employer->id;
                $p['text'] =
                    $an_employer->company;

                $items = $p;
                $json_data['items'][] = $items;
            }
        } else {
            $p = array();
            $p['id'] = '';
            $p['text'] = 'No Employer Found';

            $items = $p;
            $json_data['items'][] = $items;
        }

        //echo '<pre>';print_r($json_data);die();


        $json_data['total_count'] = $total_count;
        $json_data['more_pages'] = $more_pages;
        $json_data['last_query'] = $last_query;

        echo json_encode($json_data);

    }

    /*custom func starts*/
    public
    function getGroupName($group_id)
    {
        $group_info = $this->ion_auth_model->group($group_id)->row();

        if ($group_info) {
            return $group_info->name;
        }
    }

    /*custom func starts*/

    // edit a user


    // create a new group


    public
    function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    public
    function _valid_csrf_nonce()
    {
        $csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
        if ($csrfkey && $csrfkey == $this->session->flashdata('csrfvalue')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public
    function _render_page($view, $data = null, $returnhtml = false)//I think this makes more sense
    {

        $this->viewdata = (empty($data)) ? $this->data : $data;

        $view_html = $this->load->view($view, $this->viewdata, $returnhtml);

        if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
    }


    /*--------------------------------------------------------------------------------------------------------------------*/
    /*
     * custom functions below
     * author: Mahmudur Rahman
     * Web Dev : RS Soft
    */


    public
    function showNeedPermission()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        $this->lang->load('need_permission');
        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("users/auth/custom_folder/need_permission_page");
        $this->load->view("common_module/footer");

    }

    public
    function showDoesNotExist()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        $this->lang->load('does_not_exist');
        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("users/auth/custom_folder/does_not_exist_page");
        $this->load->view("common_module/footer");
    }

    public
    function showPaymentMethodErrorPage()
    {
        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }
        $this->lang->load('payment_method_error');

        $data = array();

        $data['payment_method'] = $this->lang->line('unknown_text');
        $data['error_type'] = $this->lang->line('unknown_text');

        if (isset($_REQUEST['payment_method'])) {
            if (!($_REQUEST['payment_method'] == null || $_REQUEST['payment_method'] == '')) {
                $data['payment_method'] = $_REQUEST['payment_method'];
            }
        }

        if (isset($_REQUEST['error_type'])) {
            if (!($_REQUEST['error_type'] == null || $_REQUEST['error_type'] == '')) {
                $data['error_type'] = $_REQUEST['error_type'];
            }
        }


        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("users/auth/custom_folder/payment_method_error_page", $data);
        $this->load->view("common_module/footer");
    }

    public
    function showMaintenancePage()
    {
        $data = array();

        $html = 'Site Under Maintenance';

        $site_maintenance_html = $this->custom_settings_library->getASettingsValue('general_settings', 'site_maintenance_html');

        if ($site_maintenance_html != '' && $site_maintenance_html != false) {
            $html = $site_maintenance_html;
        }

        $data['html'] = $html;


        $this->load->view("users/auth/custom_folder/maintenance_page", $data);

    }


    /*--Customly written function for thrifter registration-----------------------------*/


    public function thrifterRegistration()
    {
        $this->lang->load('thrifter_registration');
        $data = array();

        $currency_sign = $this->custom_settings_library->getASettingsValue('çurrency_settings', 'currency_sign');
        $thrifter_registration_terms_html = $this->custom_settings_library->getASettingsValue('general_settings', 'thrifter_registration_terms_html');

        if ($currency_sign) {
            $data['currency_sign'] = $currency_sign;
        } else {
            $data['currency_sign'] = '';
        }

        if ($currency_sign) {
            $data['thrifter_registration_terms_html'] = $thrifter_registration_terms_html;
        } else {
            $data['thrifter_registration_terms_html'] = '';
        }

        $data['external_member_email'] = '';
        $data['cpi_id'] = '';
        $data['lpi_id'] = '';

        if (isset($_GET['external_member_email'])) {
            $data['external_member_email'] = $_GET['external_member_email'];
        }

        if (isset($_GET['cpi_id'])) {
            $data['cpi_id'] = $_GET['cpi_id'];
        }

        if (isset($_GET['lpi_id'])) {
            $data['lpi_id'] = $_GET['lpi_id'];
        }

        $data['banks'] = $this->Custom_auth_model->getBanks($only_undeleted = true);

        $settings_code = 'general_settings';
        $thrift_site_logo = $this->custom_settings_library->getASettingsValue($settings_code, 'thrift_site_logo');

        if ($thrift_site_logo == '' || $thrift_site_logo == null || $thrift_site_logo == false) {
            $data['site_logo'] = $this->custom_settings_library->getASettingsValue($settings_code, 'site_logo');
        } else {
            $data['site_logo'] = $thrift_site_logo;
        }

        $subdomain = $this->Utility_model->getSubdomain();
        $individual_logo = $subdomain != "" ? $this->Utility_model->getIndividualLogo($subdomain, false) : false;
        if ($individual_logo) {
            $data['site_logo'] = $individual_logo;
        }

        $this->load->view('users/auth/custom_folder/thrifter_registration_page', $data);
    }

    public function insertThrifterRegistration()
    {
        $this->lang->load('thrifter_registration');

        $login_url = base_url() . 'users/auth/login';
        $login_anchor = '<a ' . ' href="' . $login_url . '" ' . '> ' . $this->lang->line('only_login_text') . '</a>';
        $need_to_login_text = sprintf($this->lang->line('need_to_login_text'), $login_anchor);

        $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');
        $this->form_validation->set_rules('individual_chk', 'Thrifting Membership Status', 'required');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]|callback_validate_password');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]',
            array('is_unique' => $this->lang->line('email_already_exist_text') . ' ' . $need_to_login_text)
        );

        if ($this->input->post('individual_chk') == 'no') {
            $this->form_validation->set_rules('user_employer_id', 'Organization', 'required');
            $this->form_validation->set_rules('user_ofc_id', 'Employee ID', 'required');
            $this->form_validation->set_rules('user_hire_date', 'Hire Date', 'required');
        }

        $this->form_validation->set_rules('first_name', 'First name', 'required');
        $this->form_validation->set_rules('last_name', 'Last name', 'required');
        $this->form_validation->set_rules('phone', 'Phone Number', 'required');

        //$this->form_validation->set_rules('user_salary', 'Salary', 'required|numeric');
        $this->form_validation->set_rules('user_gender', 'Gender', 'required');
        $this->form_validation->set_rules('user_dob', 'Date of Birth', 'required');
        //$this->form_validation->set_rules('user_bank', 'Bank', 'required');

        //$this->form_validation->set_rules('user_bank_account_no', 'Bank Account Number', 'required');
        //$this->form_validation->set_rules('user_bvn', 'BVN', 'required'); //optional

        $first_name = trim($this->input->post('first_name'));
        $first_name =  ucwords($first_name, " \t\r\n\f\v-");

        $last_name = trim($this->input->post('last_name'));
        $last_name =  ucwords($last_name, " \t\r\n\f\v-");

        $this->session->set_flashdata('flash_first_name', $first_name);
        $this->session->set_flashdata('flash_last_name', $last_name);
        $this->session->set_flashdata('flash_email', trim($this->input->post('email')));
        $this->session->set_flashdata('flash_phone', trim($this->input->post('phone')));
        $this->session->set_flashdata('flash_password', trim($this->input->post('password')));
        $this->session->set_flashdata('flash_passconf', trim($this->input->post('passconf')));
        $this->session->set_flashdata('flash_user_ofc_id', trim($this->input->post('user_ofc_id')));
        $this->session->set_flashdata('flash_user_salary', trim($this->input->post('user_salary')));
        $this->session->set_flashdata('flash_user_gender', $this->input->post('user_gender'));
        $this->session->set_flashdata('flash_user_dob', $this->input->post('user_dob'));
        $this->session->set_flashdata('flash_user_hire_date', $this->input->post('user_hire_date'));
        $this->session->set_flashdata('flash_user_bvn', trim($this->input->post('user_bvn')));
        $this->session->set_flashdata('flash_user_bank', $this->input->post('user_bank'));
        $this->session->set_flashdata('flash_user_bank_account_no', trim($this->input->post('user_bank_account_no')));
        $this->session->set_flashdata('flash_user_employer_id', $this->input->post('user_employer_id'));
        $this->session->set_flashdata('flash_individual_chk', $this->input->post('individual_chk'));

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('validation_errors', validation_errors('<p class="validation_errors">', '</p>'));
            redirect('users/auth/thrifter_registration');
        }

        $user_employer_id = 0;
        if ($this->input->post('individual_chk') == 'no') {
            $user_employer_id = $this->input->post('user_employer_id');
        }
        $org_domain_domain_matches = false;
        $direct_verified = false;
        if ($user_employer_id > 0) {
            $org_domain_domain_matches = $this->matchOrgDomain(trim($this->input->post('email')), $user_employer_id);
            $org = $this->Custom_auth_model->getUserDetails($user_employer_id);
            if ($org) {
                if ($org->org_email_domain_check != 1) {
                    $direct_verified = true;
                }
            }
        }


        $dob_date_timestamp = 0;
        if ($this->input->post('user_dob')) {
            $user_dob = $this->input->post('user_dob');
            $dob_date = DateTime::createFromFormat('Y-m-d', $user_dob);
            $dob_date_timestamp = $dob_date->getTimestamp();
        }

        $hire_date_timestamp = 0;
        if ($this->input->post('user_hire_date')) {
            $user_hire_date = $this->input->post('user_hire_date');
            $hire_date = DateTime::createFromFormat('Y-m-d', $user_hire_date);
            $hire_date_timestamp = $hire_date->getTimestamp();
        }

        //trimmed and title case applied
        $data1['first_name'] = $first_name;
        $data1['last_name'] = $last_name;

        $data1['phone'] = trim($this->input->post('phone'));
        $data1['email'] = trim($this->input->post('email'));
        $data1['username'] = trim($this->input->post('email'));

        $pw = trim($this->input->post('password'));
        $data1['password'] = $this->Ion_auth_model->hash_password($pw);
        $data1['active'] = 0;  // registered thrifter should not be active

        $data1['verification'] = 0;  // registered thrifter initially unverified
        $data1['approve'] = 0;  // registered thrifter initially not approved
        $data1['deletion_status'] = 1;  // registered thrifter initially will be treated as deleted

        //only auto approve individual user
        $auto_admin_approval = false;
        if ($this->input->post('individual_chk') == 'yes') {
            $auto_admin_approval = $this->getAutoAdminApproval();
        }


        if ($org_domain_domain_matches || $direct_verified || $auto_admin_approval) {
            $data1['active'] = 1;
            $data1['approve'] = 1;
            $data1['deletion_status'] = 0;
        }


        $data1['created_on'] = $this->custom_datetime_library->getCurrentTimestamp();

        $data1['mem_id_num'] = $this->getThrifterIdNum($data1['first_name'], $data1['last_name']);

        $data2['user_ofc_id'] = trim($this->input->post('user_ofc_id'));
        //$data2['user_salary'] = trim($this->input->post('user_salary'));
        $data2['user_gender'] = $this->input->post('user_gender');
        $data2['user_dob'] = $dob_date_timestamp;
        $data2['user_hire_date'] = $hire_date_timestamp;
        //$data2['user_bvn'] = $this->input->post('user_bvn');
        //$data2['user_bank'] = $this->input->post('user_bank');
        //$data2['user_bank_account_no'] = trim($this->input->post('user_bank_account_no'));
        $data2['user_employer_id'] = $user_employer_id;


        /*echo '<pre>';
        print_r($data1);
        print_r($data2);
        echo '</pre>';
        die();*/


        $data2['user_id'] = $this->Custom_auth_model->insert_employee_credential($data1);
        $this->Custom_auth_model->insert_employee_optional_credential($data2);

        $grp_mem['user_id'] = $data2['user_id'];
        $grp_mem['group_id'] = '2';
        $this->Custom_auth_model->insert_as_group_member($grp_mem);

        $grp_employee['user_id'] = $data2['user_id'];
        $grp_employee['group_id'] = '7';
        $this->Custom_auth_model->insert_as_group_member($grp_employee);

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $data2['user_id'],                                                      //1.    $created_by
            '',                                                                     //2.    $created_for
            'registration',                                                         //3.    $type
            '',                                                                     //4.    $type_id
            'registered',                                                           //5.    $activity
            'employee',                                                             //6.    $activity_by
            '',                                                                     //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            '',                                                                     //10.   $super_type
            '',                                                                     //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        $this->sendVerificationEmail($data2['user_id']);

        if (trim($this->input->post('cpi_id')) != '' && is_numeric(trim($this->input->post('cpi_id')))) {
            $this->correctExernalUserCustomInvitation($data2['user_id'], $this->input->post('cpi_id'), $data1['email']);
        } else {
            $this->correctExernalUserCustomInvitation($data2['user_id'], false, $data1['email']);
        }

        if (trim($this->input->post('lpi_id')) != '' && is_numeric(trim($this->input->post('lpi_id')))) {
            $this->correctExernalUserLoanInvitation($data2['user_id'], $this->input->post('lpi_id'), $data1['email']);
        } else {
            $this->correctExernalUserLoanInvitation($data2['user_id'], false, $data1['email']);
        }

        $mailchimp_data['email_address'] = $data1['email'];
        $mailchimp_data['FNAME'] = $data1['first_name'] != '' ? $data1['first_name'] : 'FN_' . time() . rand(9999, 99999);
        $mailchimp_data['LNAME'] = $data1['last_name'] != '' ? $data1['last_name'] : 'LN_' . time() . rand(9999, 99999);
        $this->createMailChimpSubscription($mailchimp_data);

        $cpi_id = false;
        if ($this->input->post('cpi_id') != '') {
            $cpi_id = $this->input->post('cpi_id');
        }

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('registration_success_text', 'registration_success_text');
        redirect('users/auth/thrifter_registration');

    }

    private function getAutoAdminApproval()
    {
        $auto_approve_thrifter_account = $this->custom_settings_library->getASettingsValue('prosperisgold_settings', 'auto_approve_thrifter_account');
        $auto_admin_approval = false;
        if ($auto_approve_thrifter_account) {
            if ($auto_approve_thrifter_account) {
                $auto_admin_approval = true;
            }
        }

        return $auto_admin_approval;
    }

    private function correctExernalUserCustomInvitation($user_id, $cpi_id, $email)
    {
        $mem_inv_rows_for_correction = $this->Custom_auth_model->getExternalCustomProductInvitationMemberInMultiRow($email);

        if ($mem_inv_rows_for_correction) {
            foreach ($mem_inv_rows_for_correction as $mem_inc_c_row) {
                $upd_data = array();


                $upd_data['cpi_inv_to'] = $user_id;
                $upd_data['cpi_external_member'] = 0;
                $this->Custom_auth_model->updateInvitationMember($upd_data, $mem_inc_c_row->cpi_inv_mem_serial);

                if ($mem_inc_c_row->cpi_id) {
                    $this->sendCustomProductInvitationMessage($mem_inc_c_row->cpi_id, $user_id);
                }
            }
        }
    }

    private function correctExernalUserLoanInvitation($user_id, $lpi_id, $email)
    {
        $mem_inv_rows_for_correction = $this->Custom_auth_model->getExternalLoanProductInvitationMemberInMultiRow($email);

        if ($mem_inv_rows_for_correction) {
            foreach ($mem_inv_rows_for_correction as $mem_inc_c_row) {
                $upd_data = array();


                $upd_data['lpi_inv_to'] = $user_id;
                $upd_data['lpi_external_member'] = 0;
                $this->Custom_auth_model->updateLoanInvitationMember($upd_data, $mem_inc_c_row->lpi_inv_mem_serial);

                if ($mem_inc_c_row->lpi_id) {
                    $this->sendLoanProductInvitationMessage($mem_inc_c_row->lpi_id, $user_id);
                }
            }
        }
    }

    private function matchOrgDomain($user_email, $user_employer_id)
    {
        $ret = false;
        $user_email_has_domain = false;
        $user_email_has_common_domain = false;
        $common_email_domains = $this->getCommonEmailDomains();

        $employer = $this->Custom_auth_model->getUserDetails($user_employer_id);

        $exploded_user_email = explode('@', $user_email);
        if (array_key_exists(1, $exploded_user_email)) {
            $user_email_has_domain = true;
            if (in_array($exploded_user_email[1], $common_email_domains)) {
                $user_email_has_common_domain = true;
            }
        }

        if ($employer && !$user_email_has_common_domain) {
            $exploded_employer_email = explode('@', $employer->email);

            if (array_key_exists(1, $exploded_employer_email) && $user_email_has_domain) {
                if ($exploded_employer_email[1] == $exploded_user_email[1]) {
                    $ret = true;
                }
            }
        }

        return $ret;
    }

    public function getCommonEmailDomains()
    {
        $domains = [
            /* Default domains included */
            "aol.com", "att.net", "comcast.net", "facebook.com", "gmail.com", "gmx.com", "googlemail.com",
            "google.com", "hotmail.com", "hotmail.co.uk", "mac.com", "me.com", "mail.com", "msn.com",
            "live.com", "sbcglobal.net", "verizon.net", "yahoo.com", "yahoo.co.uk",

            /* Other global domains */
            "email.com", "fastmail.fm", "games.com" /* AOL */, "gmx.net", "hush.com", "hushmail.com", "icloud.com",
            "iname.com", "inbox.com", "lavabit.com", "love.com" /* AOL */, "outlook.com", "pobox.com", "protonmail.com",
            "rocketmail.com" /* Yahoo */, "safe-mail.net", "wow.com" /* AOL */, "ygm.com" /* AOL */,
            "ymail.com" /* Yahoo */, "zoho.com", "yandex.com",

            /* United States ISP domains */
            "bellsouth.net", "charter.net", "cox.net", "earthlink.net", "juno.com",

            /* British ISP domains */
            "btinternet.com", "virginmedia.com", "blueyonder.co.uk", "freeserve.co.uk", "live.co.uk",
            "ntlworld.com", "o2.co.uk", "orange.net", "sky.com", "talktalk.co.uk", "tiscali.co.uk",
            "virgin.net", "wanadoo.co.uk", "bt.com",

            /* Domains used in Asia */
            "sina.com", "qq.com", "naver.com", "hanmail.net", "daum.net", "nate.com", "yahoo.co.jp", "yahoo.co.kr", "yahoo.co.id", "yahoo.co.in", "yahoo.com.sg", "yahoo.com.ph",

            /* French ISP domains */
            "hotmail.fr", "live.fr", "laposte.net", "yahoo.fr", "wanadoo.fr", "orange.fr", "gmx.fr", "sfr.fr", "neuf.fr", "free.fr",

            /* German ISP domains */
            "gmx.de", "hotmail.de", "live.de", "online.de", "t-online.de" /* T-Mobile */, "web.de", "yahoo.de",

            /* Italian ISP domains */
            "libero.it", "virgilio.it", "hotmail.it", "aol.it", "tiscali.it", "alice.it", "live.it", "yahoo.it", "email.it", "tin.it", "poste.it", "teletu.it",

            /* Russian ISP domains */
            "mail.ru", "rambler.ru", "yandex.ru", "ya.ru", "list.ru",

            /* Belgian ISP domains */
            "hotmail.be", "live.be", "skynet.be", "voo.be", "tvcablenet.be", "telenet.be",

            /* Argentinian ISP domains */
            "hotmail.com.ar", "live.com.ar", "yahoo.com.ar", "fibertel.com.ar", "speedy.com.ar", "arnet.com.ar",

            /* Domains used in Mexico */
            "yahoo.com.mx", "live.com.mx", "hotmail.es", "hotmail.com.mx", "prodigy.net.mx",

            /* Domains used in Brazil */
            "yahoo.com.br", "hotmail.com.br", "outlook.com.br", "uol.com.br", "bol.com.br", "terra.com.br", "ig.com.br", "itelefonica.com.br", "r7.com", "zipmail.com.br", "globo.com", "globomail.com", "oi.com.br"
        ];

        return $domains;
    }

    public function doVerifyUser()
    {
        $user_id = $this->uri->segment(4);
        $user = $this->Custom_auth_model->getUserDetails($user_id);

        $auto_admin_approval = $this->getAutoAdminApproval();

        if ($user) {
            if ($user->approve == 0) {
                $nm = $user->first_name . ' ' . $user->first_name;
                if ($user->user_employer_id > 0) {
                    $this->sendApprovalRequestMessage("employer", $user_id, $nm, $user->user_employer_id);
                }
                if (!$auto_admin_approval) {
                    $this->sendApprovalRequestMessage("admin", $user_id, $nm, 1);
                }

            }

        }

        if ($user) {
            if ($user->verification == 1 && $user->deletion_status != 1 && $user->approve == 0) {

                $this->session->set_flashdata('success', 'success');
                $this->session->set_flashdata('already_verified_text', 'already_verified_text');
                if ($user->active == 1) {
                    $this->session->set_flashdata('do_login_text', 'do_login_text');
                } else {
                    $this->session->set_flashdata('wait_for_admin_approval_text', 'wait_for_admin_approval_text');
                }

            } else if ($user->verification == 1 && $user->deletion_status != 1 && $user->approve == 1) {

                $this->Custom_auth_model->activateUser($user_id);

            } else {

                $this->Custom_auth_model->verifyUser($user_id);

                if (!$auto_admin_approval) {
                    $this->sendApprovalRequestEmails($user_id);
                }


                $this->session->set_flashdata('success', 'success');
                $this->session->set_flashdata('made_verified_text', 'made_verified_text');

                if ($user->active == 1) {
                    $this->session->set_flashdata('do_login_text', 'do_login_text');
                } else {
                    $this->session->set_flashdata('wait_for_admin_approval_text', 'wait_for_admin_approval_text');
                }

            }
        }


        redirect('users/auth/login');

    }

    public function sendVerificationEmailAgain()
    {
        $this->lang->load('verification_email_send');
        $data = array();
        $data['site_logo'] = $this->custom_settings_library->getASettingsValue('general_settings', 'site_logo');
        $this->load->view('users/auth/custom_folder/verification_email_send_form_page', $data);
    }

    public function doSendVerificationEmailAgain()
    {
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('users/auth/send_verification_email_again');
        }

        $user = $this->Custom_auth_model->getUserByEmail(trim($this->input->post('email')));

        if (!$user) {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('user_email_not_found_text', 'user_email_not_found_text');
            redirect('users/auth/send_verification_email_again');
        } else {
            $this->sendVerificationEmail($user->id);
        }


        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('verification_email_send_success_text', 'verification_email_send_success_text');


    }

    private function sendVerificationEmail($id)
    {
        $this->lang->load('verification_email_send');

        $user = $this->Custom_auth_model->getUser($id);

        if ($user) {

            $username = $user->first_name . ' ' . $user->last_name;

            $mail_data['to'] = $user->email;

            $template = $this->Custom_auth_model->getEmailTempltateByType('new_thrifter_verification');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;

                /*-------*/
                $actual_link = base_url() . 'users/auth/do_verifiy_user/' . $user->id;

                $find = array("{{username}}", "{{actual_link}}");
                $replace = array($username, $actual_link);
                $message = str_replace($find, $replace, $template_message);
                /*--------*/


                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);
            }

        }
    }

    private function sendApprovalRequestEmails($user_id)
    {
        $user = $this->Custom_auth_model->getUserDetails($user_id);
        $employer_id = $user->user_employer_id;
        if ($employer_id > 0) {
            $this->sendApprovalRequestEmail($employer_id, $user);
        }

        $non_bot_admins = $this->Custom_auth_model->getActiveNonBotAdminsIds();

        if ($non_bot_admins) {
            foreach ($non_bot_admins as $nba) {
                $this->sendApprovalRequestEmail($nba->user_id, $user);
            }
        }
    }

    private function sendApprovalRequestEmail($user_id, $requestor)
    {
        $user = $this->Custom_auth_model->getUser($user_id);

        $requestor_name = "{$requestor->first_name} {$requestor->last_name} ({$requestor->email})";

        if ($user) {

            $username = $user->first_name . ' ' . $user->last_name;
            if ($this->ion_auth->in_group('employer', $user_id)) {
                $username = $user->company;
            }

            $mail_data['to'] = $user->email;

            $template = $this->Custom_auth_model->getEmailTempltateByType('new_thrifter_approval_request');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;

                /*-------*/
                $base_url = $this->getBaseUrl($user_id);


                if ($this->ion_auth->in_group('admin', $user_id)) {
                    $actual_link = $base_url . 'employer_module/all_unapproved_employee_info/';
                } else if ($this->ion_auth->in_group('employer', $user_id)) {
                    $actual_link = $base_url . 'employer_module/my_unapproved_employee_info';
                } else {
                    $actual_link = $base_url;
                }

                $find = array("{{username}}", "{{requestor_name}}", "{{actual_link}}");
                $replace = array($username, $requestor_name, $actual_link);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);
            }

        }
    }

    private function sendEmail($mail_data)
    {

        $site_name = $this->custom_settings_library->getASettingsValue('general_settings', 'site_name');
        $site_email = $this->custom_settings_library->getASettingsValue('general_settings', 'site_email');

        if (!$site_name) {
            $site_name = 'Prosperis';
        }

        if (!$site_email) {
            $site_email = 'prosperis@info.com';
        }

        try {

            //$mail_data['to'] = 'mahmud@sahajjo.com';
            $this->email->from($site_email, $site_name);
            $this->email->to($mail_data['to']);

            $this->email->subject($mail_data['subject']);
            $this->email->message(PROSPERIS_MAIL_TOP . $mail_data['message'] . PROSPERIS_MAIL_BOTTOM);
            $this->email->set_mailtype("html");

            //echo '<br><hr><br>';
            //echo $mail_data['subject'].'<br>';
            //print_r($mail_data['message']) .'<br>';
            //echo '<hr><br>';

            @$this->email->send();

            /*$headers = 'From: '.$site_email.'\r\n';
            mail(mail_data['to'],$mail_data['subject'],$mail_data['message'],$headers);*/

        } catch (Exception $e) {
            // echo $e->getMessage();
        }

    }

    public function mc()
    {
        $data['email_address'] = "jamal@mail.com";
        $data['FNAME'] = "Jamal";
        $data['LNAME'] = "Khan";
        $this->createMailChimpSubscription($data);
    }

    public function createMailChimpSubscription($data)
    {
        $mailchimp_api_key = $this->custom_settings_library->getASettingsValue('prosperisgold_settings', 'mailchimp_api_key');
        $mailchimp_thrifter_list_name = $this->custom_settings_library->getASettingsValue('prosperisgold_settings', 'mailchimp_thrifter_list_name');
        if ($mailchimp_api_key == false || $mailchimp_api_key == null || $mailchimp_api_key == '') {
            $mailchimp_api_key = false;
        }

        if ($mailchimp_thrifter_list_name == false || $mailchimp_thrifter_list_name == null || $mailchimp_thrifter_list_name == '') {
            $mailchimp_thrifter_list_name = false;
        }

        $MailChimp = false;
        if ($mailchimp_api_key) {
            $MailChimp = new MailChimp($mailchimp_api_key);
        }

        $lists = null;
        if ($MailChimp) {
            $lists = $MailChimp->get('lists');
        }

        $list_id = null;
        if ($lists && $mailchimp_thrifter_list_name) {
            if (!empty($lists)) {
                if (!empty($lists['lists'])) {
                    foreach ($lists['lists'] as $list) {
                        if ($list['name'] == $mailchimp_thrifter_list_name) {
                            $list_id = $list['id'];
                        }
                    }

                }
            }
        }

        /*echo "<pre>";
        print_r($list);
        echo "</pre>";
        echo "<hr>";*/

        $subscriber = null;
        if ($list_id != null && $list_id != '' && $data['email_address'] != '') {
            $subscriber = $MailChimp->post("lists/$list_id/members", [
                'email_address' => $data['email_address'],
                'status' => 'subscribed',
            ]);

            /*echo "<pre>";
            print_r($subscriber);
            echo "</pre>";
            echo "<hr>";*/
        }

        $updated_subscriber = null;
        if ($subscriber != null && !empty($subscriber)) {
            $subscriber_hash = $MailChimp->subscriberHash($data['email_address']);

            $updated_subscriber = $MailChimp->patch("lists/$list_id/members/$subscriber_hash", [
                'merge_fields' => ['FNAME' => $data['FNAME'], 'LNAME' => $data['LNAME']],
            ]);

            /* echo "<pre>";
             print_r($updated_subscriber);
             echo "</pre>";
             echo "<hr>";*/
        }
        //die();
    }


    public
    function getMessageNumber()
    {
        $num = 'MSG';
        $num .= $this->alphaNum(10, false, false);

        $exists = $this->Custom_auth_model->doesMessageNumberExist($num);

        if ($exists) {
            $x = $this->getMessageNumber();
        } else {
            return $num;
        }
        return $x;

    }

    private
    function sendCustomProductInvitationMessage($cpi_id, $im)
    {
        $to_whom = 'to_thrifter';
        $message_reciever_id = $im;

        $p_data['message_sender_id'] = 1;

        $url = $this->getBaseUrl($im) . 'thrift_module/custom_product_thrift/view/' . $cpi_id;
        $anchor = "<a href='{$url}'>New Thrifter</a>";
        $p_data['message'] = 'Review Thrift Invitations: ' . $anchor;
        $p_data['message_number'] = $this->getMessageNumber();
        $p_data['message_type'] = 'custom_product_invitation';
        $p_data['message_to_whom'] = $to_whom;
        $p_data['message_created_at'] = $this->custom_datetime_library->getCurrentTimestamp();
        $p_data['message_deleted_by_sender'] = 0;
        $p_data['message_archived_by_sender'] = 0;

        $inserted_message_id = $this->Custom_auth_model->insertMessage($p_data);

        $pr_data['message_id'] = $inserted_message_id;
        $pr_data['message_reciever_id'] = $message_reciever_id;
        $pr_data['reciever_read'] = 0;
        $pr_data['reciever_read_at'] = 0;
        $pr_data['message_deleted_by_reciever'] = 0;
        $pr_data['message_archived_by_reciever'] = 0;

        $this->Custom_auth_model->insertMessageReciever($pr_data);
    }

    private
    function sendLoanProductInvitationMessage($cpi_id, $im)
    {
        $to_whom = 'to_thrifter';
        $message_reciever_id = $im;

        $p_data['message_sender_id'] = 1;

        $url = $this->getBaseUrl($im) . 'thrift_module/loan_product_thrift/view/' . $cpi_id;
        $anchor = "<a href='{$url}'>New Thrifter</a>";
        $p_data['message'] = 'Review Thrift Invitations: ' . $anchor;
        $p_data['message_number'] = $this->getMessageNumber();
        $p_data['message_type'] = 'loan_product_invitation';
        $p_data['message_to_whom'] = $to_whom;
        $p_data['message_created_at'] = $this->custom_datetime_library->getCurrentTimestamp();
        $p_data['message_deleted_by_sender'] = 0;
        $p_data['message_archived_by_sender'] = 0;

        $inserted_message_id = $this->Custom_auth_model->insertMessage($p_data);

        $pr_data['message_id'] = $inserted_message_id;
        $pr_data['message_reciever_id'] = $message_reciever_id;
        $pr_data['reciever_read'] = 0;
        $pr_data['reciever_read_at'] = 0;
        $pr_data['message_deleted_by_reciever'] = 0;
        $pr_data['message_archived_by_reciever'] = 0;

        $this->Custom_auth_model->insertMessageReciever($pr_data);
    }


    private function sendApprovalRequestMessage($to_admin_or_employer, $sender_id, $sender_name, $reciever_id)
    {
        $to_whom = '';
        $message_reciever_id = '';
        $to_whom = '';
        $route = '';

        $p_data['message_sender_id'] = $sender_id;
        if ($to_admin_or_employer == "admin") {
            $message_reciever_id = 1;
            $to_whom = 'to_admin';
            $route = 'employer_module/all_unapproved_employee_info';
        } else if ($to_admin_or_employer == "employer") {
            $message_reciever_id = $reciever_id;
            $to_whom = 'to_employer';
            $route = 'employer_module/my_unapproved_employee_info';
        }

        $url = $this->getBaseUrl($message_reciever_id) . $route;
        $anchor = "<a href='{$url}'>See</a>";
        $p_data['message'] = "New Thrifter {$sender_name} needs approval " . $anchor;
        $p_data['message_number'] = $this->getMessageNumber();
        $p_data['message_type'] = 'thrifter_approval_request';
        $p_data['message_to_whom'] = $to_whom;
        $p_data['message_created_at'] = $this->custom_datetime_library->getCurrentTimestamp();
        $p_data['message_deleted_by_sender'] = 0;
        $p_data['message_archived_by_sender'] = 0;

        $inserted_message_id = $this->Custom_auth_model->insertMessage($p_data);

        $pr_data['message_id'] = $inserted_message_id;
        $pr_data['message_reciever_id'] = $message_reciever_id;
        $pr_data['reciever_read'] = 0;
        $pr_data['reciever_read_at'] = 0;
        $pr_data['message_deleted_by_reciever'] = 0;
        $pr_data['message_archived_by_reciever'] = 0;

        $this->Custom_auth_model->insertMessageReciever($pr_data);
    }

    private function getBaseUrl($id)
    {
        $base_url = '';
        if ($this->ion_auth->in_group('admin', $id)) {
            $base_url = $this->config->item('office_base_url');
        } else if ($this->ion_auth->in_group('employer', $id) || $this->ion_auth->in_group('organization_contact', $id)) {
            $base_url = $this->config->item('partner_base_url');
        } else if ($this->ion_auth->in_group('employee', $id)) {
            $base_url = $this->Utility_model->makeThriftUrlWithEmployerSubdomain($this->config->item('thrift_base_url'), $id);
        } else if ($this->ion_auth->in_group('trustee', $id)) {
            $base_url = $this->config->item('trustee_base_url');
        }

        return $base_url;
    }


}
