<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php echo lang('page_title_text') ?>
                    <small><?php echo lang('page_subtitle_text') ?></small>
                </h4>
                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a
                                href="/"><?php echo lang('breadcrumb_home_text') ?></a>
                    </li>
                    <li class="breadcrumb-item"><a
                                href="<?php echo base_url() . 'users/auth' ?>"><?php echo lang('breadcrumb_section_text') ?></a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_text') ?></li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30"><?php echo lang('product_add_form_header_text') ?></h4>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-6">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="m-t-0 header-title"><?php echo lang('box_title_text') ?></h3>
                                <?php if (isset($message)) { ?>
                                    <div class="alert alert-danger">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                        <strong><?= $message; ?></strong>
                                    </div>
                                <?php } ?>
                                <div class="col-md-2"></div>
                                <?php if ($this->session->flashdata('need_to_select_an_employer_text')) { ?>
                                    <div class=" col-md-offset-2 col-md-8"
                                         style="color: maroon;font-size: larger"><?php echo lang('need_to_select_an_employer_text'); ?></div>
                                    <div class="col-md-2"></div>
                                <?php } ?>
                            </div>
                        </div>

                        <form action="<?php echo base_url() . 'users/auth/create_user' ?>" role="form" id=""
                              method="post" enctype="multipart/form-data">
                            <div class="box-body">

                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="first_name"><?php echo lang('label_firstname_text') ?></label>

                                        <input type="text" name="first_name" class="form-control"
                                               id="first_name"
                                               value="<?php if ($this->session->flashdata('first_name'))
                                                   echo $this->session->flashdata('first_name');
                                               ?>"
                                               placeholder="<?php echo lang('placeholder_firstname_text') ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="last_name"><?php echo lang('label_lastame_text') ?></label>

                                        <input type="text" name="last_name" class="form-control" id="last_name"
                                               value="<?php if ($this->session->flashdata('last_name'))
                                                   echo $this->session->flashdata('last_name');
                                               ?>"
                                               placeholder="<?php echo lang('placeholder_lastame_text') ?>">
                                    </div>
                                    <label for="email"><?php echo lang('label_email_text') ?></label>


                                    <input type="email" name="email" class="form-control" id="email"
                                           value="<?php if ($this->session->flashdata('email'))
                                               echo $this->session->flashdata('email');
                                           ?>"
                                           placeholder="<?php echo lang('placeholder_email_text') ?>">
                                </div>
                                <div class="form-group">
                                    <label for="phone"><?php echo lang('label_phone_text') ?></label>
                                    <input type="text" class="form-control" name="phone" id="user_position"
                                           value="<?php if ($this->session->flashdata('phone'))
                                               echo $this->session->flashdata('phone');
                                           ?>"
                                           placeholder="<?php echo lang('placeholder_phone_text') ?>">
                                </div>

                                <div class="form-group" style="display: none">
                                    <label for="company"><?php echo lang('label_company_name_text') ?></label>
                                    <input type="text" class="form-control" name="company" id="company"
                                           value="<?php if ($this->session->flashdata('company'))
                                               echo $this->session->flashdata('company');
                                           ?>"
                                           placeholder="<?php echo lang('placeholder_company_name_text') ?>">
                                </div>
                                <div class="form-group">
                                    <label for="password"><?php echo lang('label_password_text') ?>
                                        <span id="see_password" title="see"  style="color:#2b2b2b"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></span>
                                    </label>
                                    <input type="password" name="password" class="form-control" id="password"
                                           placeholder="<?php echo lang('placeholder_password_text') ?>"
                                           value="">
                                </div>
                                <div class="form-group">
                                    <label for="password_confirm"><?php echo lang('label_confirm_password_text') ?>
                                        <span id="see_password_confirm" title="see"  style="color:#2b2b2b"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></span>
                                    </label>
                                    <input type="password" name="password_confirm" class="form-control"
                                           id="password_confirm"
                                           placeholder="<?php echo lang('placeholder_confirm_password_text') ?>"
                                           value="">
                                </div>
                                <div class="form-group">
                                    <label for="select_group"><?php echo lang('label_select_group_text') ?></label>
                                    <select class="form-control grp_sel" style="height: auto" name="select_group"
                                            id="select_group">
                                        <!--<option value="0"><?php /*echo lang('option_none_text') */ ?></option>-->
                                        <?php if ($all_groups) { ?>

                                            <?php foreach ($all_groups as $a_group) { ?>

                                                <?php if ($this->ion_auth->in_group('superadmin')) { ?>
                                                    <!--do not show member group-->
                                                    <?php if (!($a_group->id == 1 || $a_group->id == 2 || $a_group->id == 6 || $a_group->id == 7 || $a_group->id == 9)) { ?>
                                                        <option value="<?php echo $a_group->id ?>">
                                                            <?php echo ucfirst($a_group->name) ?>
                                                        </option>
                                                    <?php } ?>

                                                <?php } else { ?>
                                                    <?php if (!($a_group->id == 1 || $a_group->id == 2 || $a_group->id == 3 || $a_group->id == 6 || $a_group->id == 7 || $a_group->id == 9)) { ?>
                                                        <option value="<?php echo $a_group->id ?>">
                                                            <?php echo ucfirst($a_group->name) ?>
                                                        </option>
                                                    <?php } ?>
                                                <?php } ?>

                                            <? } ?>

                                        <? } ?>
                                    </select>
                                </div>

                                <div class="form-group" id="select_employer_wrapper">
                                    <label for="select_employer"><?php echo lang('label_select_employer_text') ?></label>
                                    <select class="form-control select_employer select2" style="width: 100%"
                                            name="select_employer"
                                            id="">
                                    </select>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">


                                <button type="submit" id="btnsubmit"
                                        class="btn btn-primary"><?php echo lang('button_submit_text') ?></button>
                            </div>
                        </form>

                    </div><!-- end col -->
                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>
</div>


<script>

    $(function () {

        $('#select_employer_wrapper').hide();

        $('#select_group').on('change', function () {

            var g_id = $(this).val();

            if (g_id == '7' || g_id == 7) {

                $('#select_employer_wrapper').show();

            } else {

                $('#select_employer_wrapper').hide();

            }

        });


        $('.select_employer').select2({

            placeholder: "<?= lang('select_employer_text')?>",
            allowClear: true,

            "language": {
                "noResults": function () {
                    var btn_html = "<?= lang('no_employer_found_text')?>";
                    var div = document.createElement("div");
                    div.innerHTML = btn_html;
                    return div;
                }
            },

            minimumInputLength: 3,
            ajax: {
                url: '<?= base_url() ?>users/auth/get_employers_by_select2',
                dataType: 'json',
                cache: true,
                delay: 1000,
                allowClear: true,


                data: function (params) {

                    return {
                        keyword: params.term, // search term
                        page: params.page,
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    //console.log(data.items);

                    return {
                        results: data.items,
                        pagination: {
                            more: data.more_pages
                        }
                    };
                },

                /*templateResult: formatRepo,
                 templateSelection: formatRepoSelection,*/

                escapeMarkup: function (markup) {
                    return markup;
                } // let our custom formatter work

            }

        });

    });


</script>

<script>
    $(function () {

        $('#see_password').on('click',function () {
            $('#password').attr('type', 'text');
        });

        $('#see_password_confirm').on('click',function () {
            $('#password_confirm').attr('type', 'text');
        });


    });
</script>