
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">
                            <?php echo lang('page_title_text') ?>
                        </h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="product_module/all_product_info"><?php echo lang('breadcrumb_home_text') ?></a></li>
                            <li class="breadcrumb-item"><?php echo lang('breadcrumb_section_text') ?></li>
                            <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_text') ?></li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <h4 class="header-title m-t-0 m-b-30"></h4>
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">
                                <small><?php echo lang('page_subtitle_text') ?></small>
                            </h4>
                            <ol class="breadcrumb float-right">
                                <a class="btn btn-primary"
                                   href="users/auth/create_group"><?php echo lang('add_button_text') ?>
                                    &nbsp;<span class="icon"><i class="fa fa-plus"></i></span>
                                </a>
                            </ol>
                            <?php if ($this->session->flashdata('group_add_success')) { ?>
                                <br>
                                <div class="col-md-6">
                                    <div class="panel panel-success copyright-wrap" id="add-success-panel">
                                        <div class="panel-heading"><?php echo lang('successfull_text') ?>
                                            <button type="button" class="close" data-target="#add-success-panel" data-dismiss="alert"><span
                                                        aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                                            </button>
                                        </div>
                                        <div class="panel-body"><?php echo lang('add_successfull_text') ?></a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('group_update_success')) { ?>
                                <br>
                                <div class="col-md-6">
                                    <div class="panel panel-success copyright-wrap" id="update-success-panel">
                                        <div class="panel-heading"><?php echo lang('successfull_text') ?>
                                            <button type="button" class="close" data-target="#update-success-panel" data-dismiss="alert"><span
                                                        aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                                            </button>
                                        </div>
                                        <div class="panel-body"><?php echo lang('update_successfull_text') ?></a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <!-- Main content -->
                            <section class="content">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="box box-primary">
                                            <div class="box-header">
                                                <h3 class="box-title"><?php echo lang('table_title_text') ?></h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <table id="group-table" class="table table-bordered table-hover table-responsive">
                                                    <thead>
                                                    <tr>
                                                        <th><?php echo lang('column_group_name_text') ?></th>
                                                        <th><?php echo lang('column_group_description_text') ?></th>
                                                        <th><?php echo lang('column_actions_text') ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if ($all_groups) foreach ($all_groups as $a_group) { ?>
                                                        <tr>
                                                            <td><?php echo $a_group->name ?></td>
                                                            <td><?php echo $a_group->description ?></td>
                                                            <td>
                                                                <!--<a title="<?php /*echo lang('tooltip_view_text') */?>"
                                                                   class="" style="color: #2b2b2b" href="" class=""><i
                                                                            class="fa fa-eye fa-lg "
                                                                            aria-hidden="true"></i>
                                                                </a>-->
                                                                &nbsp;
                                                                <a title="<?php echo lang('tooltip_edit_text') ?>" style="color: #2b2b2b"
                                                                   href="<?php echo base_url() . 'users/auth/edit_group/' . $a_group->id ?>"
                                                                   class=""><i class="fa fa-pencil-square-o fa-lg"
                                                                               aria-hidden="true"></i>
                                                                </a>
                                                                &nbsp;
                                                                <a title="<?php echo lang('tooltip_delete_text') ?>" style="color: #2b2b2b"
                                                                   href="<?php echo base_url() ?>"
                                                                   class=""
                                                                   onclick="return confirm('<?php echo lang('confirm_delete_text') ?>')">
                                                                    <i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                    <tr>
                                                        <th><?php echo lang('column_group_name_text') ?></th>
                                                        <th><?php echo lang('column_group_description_text') ?></th>
                                                        <th><?php echo lang('column_actions_text') ?></th>
                                                    </tr>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </section>
                            <!-- /.content -->
                            <div class="clearfix"></div>
                        </div>
                    </div><!-- end col -->
                </div><!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->



<!-- <script>
    $(function () {
        $(document).tooltip();
    })
</script> -->

<!--this css style is holding datatable inside the box-->
<style>
    #group-table {
        table-layout: fixed;
        width: 100% !important;
    }

    #group-table td,
    #group-table th {
        width: auto !important;
        white-space: normal;
        text-overflow: ellipsis;
        overflow: hidden;
    }
</style>

<script>
    $(function () {
        $('#group-table').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
    });
</script>