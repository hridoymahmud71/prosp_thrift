
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h3 class="page-title float-left">
                            <?php echo lang('error_text')?>
                        </h3>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30"><?php echo lang('error_title_text')?></h4>
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-6">
                                <div class="box-body">
                                    <p>
                                        <?php echo lang('error_description_text')?>
                                    </p>
                                    <!--<p>
                                        <a href="<?php /*echo base_url().'contact_module/show_contact_info' */?>"><?php /*echo lang('contact_link_text')*/?></a>
                                    </p>-->
                                    <?php $url = '?payment_method='.$payment_method.'&error_type='.base64_decode($error_type)?>
                                    <p>
                                        <a href="<?php echo base_url().'message_module/send_message/'.$url ?>"><?php echo lang('contact_support_text')?></a>
                                    </p>
                                    <hr>
                                    <p>
                                        <?php echo lang('payment_method_text')?> :  <strong style="color: #2b2b2b"><?php echo $payment_method ?></strong>
                                    </p>

                                    <p>
                                        <?php echo lang('error_type_text')?> : <strong style="color: #2b2b2b"><?php echo base64_decode($error_type) ?></strong>
                                    </p>

                                </div>
                                <hr>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <?php echo lang('thank_you_text')?>
                                </div>

                            </div><!-- end col -->
                        </div><!-- end row -->
                    </div>
                </div><!-- end col -->
            </div>
        </div>
