<base href="<?php echo base_url(); ?>">
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">
    <!-- App Favicon -->
    <link rel="shortcut icon" href="assets/custom_asset/favicon.ico">
    <!-- App title -->
    <title>Thrifting – Prosperis Gold</title>
    <!-- Bootstrap CSS -->
    <link href="assets/backend_assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <!-- App CSS -->
    <link href="assets/backend_assets/css/style.css" rel="stylesheet" type="text/css">
    <!-- Modernizr js -->
    <script src="assets/backend_assets/js/modernizr.min.js"></script>

    <!--jquery-->
    <script src="assets/backend_assets/js/jquery.min.js"></script>

    <!--bootstrap datepicker-->
    <link href="assets/backend_assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">

    <!--select 2-->
    <script src="assets/backend_assets/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <link href="assets/backend_assets/plugins/multiselect/css/multi-select.css" rel="stylesheet" type="text/css"/>
    <link href="assets/backend_assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="<?php echo base_url() . 'style_module/load_style/thrift' ?>">

</head>
<style>
    @media only screen and (min-width: 768px) {
        .wrapper-page {
            margin: 5% auto;
            position: relative;
            max-width: initial;
        !important;
            width: 50%;
        }
    }

    .modal-body {
        max-height: calc(100vh - 200px);
        overflow-y: auto;
    }


</style>

<style>
    #password_error {
        display: none;
    }

    #acceptance_error {
        color: red;
        display: none;
    }
</style>

<style>
    #organization_wrapper {
        display: none;
    }
</style>

<style>
    .validation_errors {
        margin-bottom: 0 !important;
    }
</style>
<style>
    .datepicker-years .year.disabled{
        display: none;
    }
    .datepicker-months .month.disabled{
        display: none;
    }
    .datepicker-days .day.disabled{
        display: none;
    }
</style>
<body>
<div class="account-pages custom_login_backview"></div>
<div class="clearfix"></div>
<div class="wrapper-page">
    <div class="account-bg">
        <div class="card-box mb-0">
            <div class="text-center m-t-20">
                <a href="<?= base_url() ?>" class="logo">
                    <img style="max-width: 50%;"
                         src="<?php echo $this->config->item('pg_upload_source_path') . 'image/' . $site_logo; ?>">
                </a>
            </div>
            <div class="m-t-10 p-20">
                <div class="row">
                    <div class="col-12 text-center">
                        <h6 class="text-muted text-uppercase m-b-0 m-t-0"><?php echo lang('register_thrift_account_text') ?></h6>
                    </div>
                </div>

                <?php if ($this->session->flashdata('success')) { ?>
                    <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <?= $this->session->flashdata('registration_success_text') ? lang('registration_success_text') : '' ?>
                    </div>
                <? } ?>

                <?php if ($this->session->flashdata('error')) { ?>
                    <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <?= $this->session->flashdata('validation_errors') ? $this->session->flashdata('validation_errors') : '' ?>
                    </div>
                <? } ?>

                <form id="reg_form" class="m-t-20" action="users/auth/insert_thrifter_registration" method="post">
                    <div class="form-group row">
                        <label for="" class="col-sm-12 text-muted m-t-5"><?= lang('label_first_name_text') ?></label>
                        <div class="col-12">
                            <input class="form-control" type="text" name="first_name" required
                                   placeholder="<?= lang('placeholder_first_name_text') ?>"
                                   value="<?= $this->session->flashdata('flash_first_name') ? $this->session->flashdata('flash_first_name') : '' ?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-12 text-muted m-t-5"><?= lang('label_last_name_text') ?></label>
                        <div class="col-12">
                            <input class="form-control" type="text" name="last_name" required
                                   placeholder="<?= lang('placeholder_last_name_text') ?>"
                                   value="<?= $this->session->flashdata('flash_last_name') ? $this->session->flashdata('flash_last_name') : '' ?>">
                        </div>
                    </div>

                    <input type="hidden" name="cpi_id" value="<?= $cpi_id ?>">

                    <div class="form-group row">
                        <label for="" class="col-sm-12 text-muted m-t-5"><?= lang('label_email_text') ?></label>
                        <div class="col-12">
                            <input class="form-control" type="text" name="email" required
                                   placeholder="<?= lang('placeholder_email_text') ?>"
                                <?= $external_member_email != '' ? ' readonly ' : '' ?>

                                   value="<?= !$this->session->flashdata('flash_email') && $external_member_email != '' ? $external_member_email : '' ?><?= $this->session->flashdata('flash_email') ? $this->session->flashdata('flash_email') : '' ?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-12 text-muted m-t-5"><?= lang('label_password_text') ?> <br>
                            <small><?= lang('strong_password_text') ?></small>
                            <span id="password_error"
                                    style="color: red"><?= lang('strong_password_text') ?></span></label>
                        <div class="col-12">
                            <input class="form-control" type="password" name="password" required
                                   placeholder="<?= lang('placeholder_password_text') ?>"
                                   value="<?= $this->session->flashdata('flash_password') ? $this->session->flashdata('flash_password') : '' ?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-12 text-muted m-t-5"><?= lang('label_passconf_text') ?></label>
                        <div class="col-12">
                            <input class="form-control" type="password" name="passconf" required
                                   placeholder="<?= lang('placeholder_passconf_text') ?>"
                                   value="<?= $this->session->flashdata('flash_passconf') ? $this->session->flashdata('flash_passconf') : '' ?>">
                        </div>
                    </div>

                    <!--<div class="form-group row">
                        <label for="" class="col-sm-12 text-muted m-t-5"><?/*= lang('label_user_salary_text') */?></label>
                        <div class="col-12">
                            <div class="input-group">
                                <span class="input-group-addon"><?/*= $currency_sign */?> </span>
                                <input class="form-control" type="text" name="user_salary" required
                                       placeholder="<?/*= lang('placeholder_user_salary_text') */?>"
                                       value="<?/*= $this->session->flashdata('flash_user_salary') ? $this->session->flashdata('flash_user_salary') : '' */?>">
                            </div>
                        </div>
                    </div>-->

                    <div class="form-group row">
                        <label for="" class="col-sm-12 text-muted m-t-5"><?= lang('label_phone_text') ?></label>
                        <div class="col-12">
                            <input class="form-control" type="text" name="phone" required
                                   placeholder="<?= lang('placeholder_phone_text') ?>"
                                   value="<?= $this->session->flashdata('flash_phone') ? $this->session->flashdata('flash_phone') : '' ?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-12 text-muted m-t-5"><?= lang('label_user_gender_text') ?></label>
                        <div class="col-12">
                            <select class="form-control" name="user_gender" id="">
                                <option value=""><?= lang('option_select_gender_text') ?></option>
                                <option value="male" <?php if ($this->session->flashdata('flash_user_gender')) {
                                    if ($this->session->flashdata('flash_user_gender') == 'male') {
                                        echo ' selected ';
                                    }
                                } ?> ><?= lang('option_user_gender_male_text') ?></option>
                                <option value="female" <?php if ($this->session->flashdata('flash_user_gender')) {
                                    if ($this->session->flashdata('flash_user_gender') == 'female') {
                                        echo ' selected ';
                                    }
                                } ?> ><?= lang('option_user_gender_female_text') ?></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-12 text-muted m-t-5"><?= lang('label_user_dob_text') ?></label>
                        <div class="col-12">
                            <input readonly class="form-control date_chk" type="text" name="user_dob" id="user_dob"
                                   value="<?= $this->session->flashdata('flash_user_dob') ? $this->session->flashdata('flash_user_dob') : '' ?>"
                                   placeholder="<?= lang('placeholder_user_dob_text') ?>">
                        </div>
                    </div>

                    <!--<div class="form-group row">
                        <label for="" class="col-sm-12 text-muted m-t-5"><?/*= lang('label_user_bvn_text') */?> <span
                                    style="color: darkblue">(<?/*= lang('label_user_optional_text') */?>)</span> </label>
                        <div class="col-12">
                            <input class="form-control" type="text" name="user_bvn"
                                   placeholder="<?/*= lang('placeholder_user_bvn_text') */?>"
                                   value="<?/*= $this->session->flashdata('flash_user_bvn') ? $this->session->flashdata('flash_user_bvn') : '' */?>">
                        </div>
                    </div>-->

                    <!--<div class="form-group row">
                        <label for="" class="col-sm-12 text-muted m-t-5"><?/*= lang('bank_select_text') */?></label>
                        <div class="col-12">
                            <select style="width: 100%" class="form-control user_bank" name="user_bank" id="" required>
                                <option value=""><?/*= lang('bank_select_text') */?></option>
                                <?php /*if ($banks) { */?>
                                    <?php /*foreach ($banks as $bank) { */?>
                                        <option value="<?/*= $bank->bank_id */?>" <?php /*if ($this->session->flashdata('flash_user_bank')) {
                                            if ($this->session->flashdata('flash_user_bank') == $bank->bank_id) {
                                                echo ' selected="selected" ';
                                            }
                                        } */?> ><?/*= $bank->bank_name */?></option>
                                    <?php /*} */?>
                                <?php /*} */?>
                            </select>
                        </div>
                    </div>-->


                    <!--<div class="form-group row">
                        <label for=""
                               class="col-sm-12 text-muted m-t-5"><?/*= lang('label_user_bank_account_no_text') */?></label>
                        <div class="col-12">
                            <input class="form-control" type="text" name="user_bank_account_no" required
                                   placeholder="<?/*= lang('placeholder_user_bank_account_no_text') */?>"
                                   value="<?/*= $this->session->flashdata('flash_user_bank_account_no') ? $this->session->flashdata('flash_user_bank_account_no') : '' */?>">
                        </div>
                    </div>-->

                    <div class="form-group row">
                        <label for="" class="col-sm-12 text-muted m-t-5"><?= lang('label_is_individual_text') ?></label>
                        <div class="col-12">
                            <select class="form-control" name="individual_chk" id="individual_chk">
                                <option value=""><?= lang('option_select_status_text') ?></option>
                                <option value="no" <?php if ($this->session->flashdata('flash_individual_chk')) {
                                    if ($this->session->flashdata('flash_individual_chk') == 'no') {
                                        echo ' selected ';
                                    }
                                } ?> ><?= lang('option_not_individual_text') ?></option>
                                <option value="yes" <?php if ($this->session->flashdata('flash_individual_chk')) {
                                    if ($this->session->flashdata('flash_individual_chk') == 'yes') {
                                        echo ' selected ';
                                    }
                                } ?> ><?= lang('option_individual_text') ?></option>
                            </select>
                        </div>
                    </div>


                    <?php $selected_org = false; ?>

                    <?php if ($this->session->flashdata('flash_user_employer_id')) {
                        $selected_org = $this->Custom_auth_model->getUser($this->session->flashdata('flash_user_employer_id'));
                    } ?>

                    <div id="organization_wrapper">
                        <div class="form-group row">
                            <label for="" class="col-sm-12 text-muted m-t-5"><?= lang('label_employer_text') ?></label>
                            <div class="col-12">
                                <select style="width: 100%" class="form-control user_employer" name="user_employer_id"
                                        id="user_employer_id"
                                        required>
                                    <option value=""><?= lang('placeholder_employer_text') ?></option>
                                    <?php if ($selected_org) { ?>
                                        <option value="<?= $selected_org->id ?>"
                                                selected="selected"><?= $selected_org->company ?></option>
                                    <?php } ?>

                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for=""
                                   class="col-sm-12 text-muted m-t-5"><?= lang('label_user_ofc_id_text') ?></label>
                            <div class="col-12">
                                <input id="user_ofc_id" class="form-control" type="text" name="user_ofc_id" required
                                       placeholder="<?= lang('placeholder_user_ofc_id_text') ?>"
                                       value="<?= $this->session->flashdata('flash_user_ofc_id') ? $this->session->flashdata('flash_user_ofc_id') : '' ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for=""
                                   class="col-sm-12 text-muted m-t-5"><?= lang('label_user_hire_date_text') ?></label>
                            <div class="col-12">
                                <input readonly class="form-control date_chk" type="text" name="user_hire_date" id="user_hire_date"
                                       value="<?= $this->session->flashdata('flash_user_hire_date') ? $this->session->flashdata('flash_user_hire_date') : '' ?>"
                                       placeholder="<?= lang('placeholder_user_hire_date_text') ?>">
                            </div>
                        </div>
                    </div>


                    <br>
                    <div class="form-check mb-2 mr-sm-2 mb-sm-0">
                        <label for="" class="col-sm-12 text-muted m-t-5">
                            <span id="acceptance_error"><?= lang('acceptance_error_text') ?></span>
                        </label>
                        <label id="condition-label" class="form-check-label" data-toggle="modal"
                               data-target="#termsModal">
                            <input class="form-check-input" id="term_checkbox"
                                   type="checkbox">&nbsp;&nbsp;<?= lang('label_acception_checkbox_text') ?>
                        </label>

                    </div>

                    <div class="modal fade  bs-example-modal-lg" id="termsModal" tabindex="-1" role="dialog"
                         aria-labelledby="termsModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title"
                                        id="termsModalLabel"><?= lang('modal_title_terms_and_conditions_text') ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <?= $thrifter_registration_terms_html ?>

                                    <br>
                                    <div class="text-center">
                                        <button type="button" id="accept_btn"
                                                class="btn btn-success"><?= lang('accept_btn_text') ?></button>
                                        <button type="button" id="decline_btn"
                                                class="btn btn-danger"><?= lang('decline_btn_text') ?></button>
                                    </div>
                                    <br>


                                </div>
                                <div class="modal-footer">
                                    <button style="display: none" type="button" id="modal_close_btn"
                                            class="btn btn-secondary"
                                            data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group text-center row m-t-10">
                        <div class="col-12">
                            <button id="reg_submit_btn" class="btn btn-primary btn-block waves-effect waves-light"
                                    type="submit"><?php echo lang('submit_btn_text') ?></button>
                        </div>
                    </div>

                </form>
            </div>

            <div class="col-12">
                <a href="users/auth/login" class="text-muted"><i
                            class="fa fa-sign-in  m-r-5"></i><?= lang('login_text') ?></a>
            </div>

            <div class="col-12">
                <a href="users/auth/send_verification_email_again" class="text-muted"><i
                            class="fa fa-paper-plane-o  m-r-5"></i><?= lang('thrifter_verification_email_text') ?></a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- end card-box-->
    <!-- <div class="m-t-20">
        <div class="text-center">
            <p class="text-white">Don't have an account? <a href="pages-register.html" class="text-white m-l-5"><b>Sign Up</b></a></p>
        </div>
    </div> -->
</div>


<script>
    (function ($) {
        // You pass-in jQuery and then alias it with the $-sign
        // So your internal code doesn't change

        $.fn.hasAttr = function (name) {
            return this.attr(name) !== undefined;
        };

        $(function () {
            $('#user_dob').datepicker({
                format: 'yyyy-mm-dd',
                endDate: '-18y',
                autoclose: true,
                startView: 2
            });

            $('#user_hire_date').datepicker({
                format: 'yyyy-mm-dd',
                endDate: '-1d',
                autoclose: true,
                startView: 2
            });

            /*--------------------------------------------------------------------------------------------------------*/
            $('.user_bank').select2();

            $('.user_employer').select2({

                allowClear: true,
                placeholder: '<?= lang('placeholder_employer_text')?>',

                "language": {
                    "noResults": function () {
                        var btn_html = "<?= lang('employer_not_found_text')?>";
                        var div = document.createElement("div");
                        div.innerHTML = btn_html;
                        return div;
                    }
                },

                minimumInputLength: 1,
                ajax: {
                    url: '<?= base_url() ?>users/auth/get_employers_by_select2',
                    dataType: 'json',
                    cache: true,
                    delay: 1000,
                    allowClear: true,


                    data: function (params) {

                        return {
                            keyword: params.term, // search term
                            page: params.page,
                        };
                    },
                    processResults: function (data, params) {

                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        //console.log(data.items);

                        return {
                            results: data.items,
                            pagination: {
                                more: data.more_pages
                            }
                        };
                    },

                    /*templateResult: formatRepo,
                     templateSelection: formatRepoSelection,*/

                    escapeMarkup: function (markup) {
                        return markup;
                    } // let our custom formatter work


                }

            });
            /*--------------------------------------------------------------------------------------------------------*/

            $('#term_checkbox').on('click', function () {

                var term_checked = $("#term_checkbox").is(":checked");
                $("#term_checkbox").prop('checked', false);
            });

            $('#accept_btn').on('click', function () {

                var term_checked = $("#term_checkbox").is(":checked");

                if (!term_checked) {
                    $("#term_checkbox").prop('checked', true);
                }

                $('#modal_close_btn').click();

            });

            $('#decline_btn').on('click', function () {

                var term_checked = $("#term_checkbox").is(":checked");

                if (term_checked) {
                    $("#term_checkbox").prop('checked', false);
                }

                $('#modal_close_btn').click();
            });

            /*--------------------------------------------------------------------------------------------------------*/
            show_hide_org_wrap();

            function show_hide_org_wrap() {
                if ($('#individual_chk').val() == 'yes' || $('#individual_chk').val() == '') {
                    $('#organization_wrapper').hide();

                    if ($('#user_employer_id').hasAttr('required')) {
                        $('#user_employer_id').removeAttr('required');
                    }

                    if ($('#user_ofc_id').hasAttr('required')) {
                        $('#user_ofc_id').removeAttr('required');
                    }

                } else {
                    $('#organization_wrapper').show();

                    if (!$('#user_employer_id').hasAttr('required')) {
                        $('#user_employer_id').attr('required');
                    }

                    if (!$('#user_ofc_id').hasAttr('required')) {
                        $('#user_ofc_id').attr('required');
                    }
                }
            }

            $('#individual_chk').on('change', function () {
                show_hide_org_wrap();
            });

            /*--------------------------------------------------------------------------------------------------------*/


            //$('#reg_submit_btn').click( function (e) {
            $('#reg_submit_btn').on('click', function (e) {
                e.preventDefault();
                var term_checked = $("#term_checkbox").is(":checked");

                if (term_checked) {
                    $('#acceptance_error').hide();
                    $('#reg_form').submit();

                } else {
                    $('#acceptance_error').show();
                }

            });


            /*--------------------------------------------------------------------------------------------------------*/

        })

    })(jQuery);


</script>
<!-- end wrapper page -->
<!-- <script>
    var resizefunc = [];
</script> -->
<!-- jQuery  -->

<!--datepicker-->
<script src="assets/backend_assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<!--moment-->
<script src="assets/backend_assets/plugins/moment/moment.js"></script>


<script src="assets/backend_assets/js/jquery.min.js"></script>
<script src="assets/backend_assets/js/popper.min.js"></script>
<script src="assets/backend_assets/js/bootstrap.min.js"></script>
<script src="assets/backend_assets/js/detect.js"></script>
<script src="assets/backend_assets/js/fastclick.js"></script>
<script src="assets/backend_assets/js/jquery.blockUI.js"></script>
<script src="assets/backend_assets/js/waves.js"></script>
<script src="assets/backend_assets/js/jquery.nicescroll.js"></script>
<script src="assets/backend_assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/backend_assets/js/jquery.slimscroll.js"></script>
<script src="assets/backend_assets/plugins/switchery/switchery.min.js"></script>
<!-- App js -->
<script src="assets/backend_assets/js/jquery.core.js"></script>
<script src="assets/backend_assets/js/jquery.app.js"></script>

</body>
</html>