
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">
                            <?php echo lang('page_title_text') ?>
                        </h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a
                                        href="/"><?php echo lang('breadcrumb_home_text') ?></a>
                            </li>
                            <li class="breadcrumb-item active"><?php echo lang('breadcrumb_section_text') ?></li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <?php if ($this->session->flashdata('success') ) { ?>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="alert-heading"><?php echo lang('successfull_text') ?></h4>
                                <p>
                                    <?php
                                    if ($this->session->flashdata('add_success')) {
                                        echo lang('user_add_success_text');
                                    }
                                    if ($this->session->flashdata('update_success')) {
                                        echo lang('update_success_text');
                                    }
                                    if ($this->session->flashdata('activate_success')) {
                                        echo lang('activate_success_text');
                                    }
                                    if ($this->session->flashdata('deactivate_success')) {
                                        echo lang('dectivate_success_text');
                                    }
                                    if ($this->session->flashdata('delete_success')) {
                                        echo lang('delete_success_text');
                                    }
                                    if ($this->session->flashdata('password_send_success')) {
                                        echo lang('password_send_success_text');
                                    }
                                    ?>
                                </p>
                                <?php if (1) { ?>
                                    <p class="mb-0">
                                        <?php if (!$this->session->flashdata('delete_success')) { ?>
                                            <a href="<?php echo base_url()
                                                . 'user_profile_module/user_profile_overview/' . $this->session->flashdata('flash_user_id') ?>">
                                                <?php echo lang('see_user_text'); ?>
                                            </a>
                                        <?php } ?>
                                        &nbsp;
                                        <?php if ($this->session->flashdata('add_success')) { ?>
                                            <a href="<?php echo base_url() . 'users/auth/edit_user/' . $this->session->flashdata('flash_user_id') ?>">
                                                <?php echo lang('edit_user_text'); ?>
                                            </a>
                                        <?php } ?>
                                    </p>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </section>
            <?php } ?>

            <?php if ($this->session->flashdata('error') ) { ?>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="alert-heading"><?php echo lang('unsuccessfull_text') ?></h4>
                                <p>
                                    <?php

                                    if ($this->session->flashdata('password_send_error')) {
                                        echo lang('password_send_error_text');
                                    }
                                    ?>
                                </p>
                                <?php if (1) { ?>
                                    <p class="mb-0">
                                        <?php if (!$this->session->flashdata('delete_success')) { ?>
                                            <a href="<?php echo base_url()
                                                . 'user_profile_module/user_profile_overview/' . $this->session->flashdata('flash_user_id') ?>">
                                                <?php echo lang('see_user_text'); ?>
                                            </a>
                                        <?php } ?>
                                        &nbsp;
                                        <?php if ($this->session->flashdata('add_success')) { ?>
                                            <a href="<?php echo base_url() . 'users/auth/edit_user/' . $this->session->flashdata('flash_user_id') ?>">
                                                <?php echo lang('edit_user_text'); ?>
                                            </a>
                                        <?php } ?>
                                    </p>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </section>
            <?php } ?>

            <div class="row">
                <div class="col-12">
                    <h4 class="header-title m-t-0 m-b-30"></h4>
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left m-b-10">
                                <small><?php echo lang('page_subtitle_text') ?></small>
                            </h4>
                            <?php if ($this->ion_auth->is_admin()) { ?>

                                    <a class="btn btn-primary float-right"
                                       href="users/auth/create_user"><?php echo lang('add_button_text') ?>
                                        &nbsp;<span class="icon"><i class="fa fa-plus"></i></span>
                                    </a>

                            <?php } ?>

                           <!--was here-->

                            <!-- Main content -->

                                    <div class="col-xs-12">
                                        <div class="box box-primary">
                                            <div class="box-header">
                                                <div>
                                                    <table style="width: 67%; margin: 5em auto 2em auto;" cellspacing="1"
                                                           cellpadding="3"
                                                           border="0">
                                                        <tbody>
                                                        <tr id="filter_col0" data-column="0">
                                                            <td align="center">
                                                                <label><?php echo lang('column_firstname_text') ?></label>
                                                            </td>
                                                            <td align="center">
                                                                <input class="column_filter form-control"
                                                                       id="col0_filter" type="text">
                                                            </td>
                                                        </tr>
                                                        <tr id="filter_col1" data-column="1">
                                                            <td align="center"><label
                                                                        for=""><?php echo lang('column_lastname_text') ?></label>
                                                            </td>
                                                            <td align="center">
                                                                <input class="column_filter form-control"
                                                                       id="col1_filter" type="text">
                                                            </td>
                                                        </tr>
                                                        <tr id="filter_col2" data-column="2">
                                                            <td align="center"><label
                                                                        for=""><?php echo lang('column_email_text') ?></label>
                                                            </td>
                                                            <td align="center">
                                                                <input class="column_filter form-control"
                                                                       id="col2_filter" type="text">
                                                            </td>
                                                        </tr>
                                                        <tr id="filter_col3" data-column="3">
                                                            <td align="center"><label
                                                                        for=""><?php echo lang('column_group_text') ?></label>
                                                            </td>
                                                            <td align="center">
                                                                <input class="column_filter form-control"
                                                                       id="col3_filter" type="hidden">
                                                                <select id="custom_group_filter" class="form-control">
                                                                    <option value="all"><?php echo lang('option_all_text') ?></option>
                                                                    <!--<option value="admin"><?php /*echo lang('option_admin_text') */ ?></option>
                                                                    <option value="staff"><?php /*echo lang('option_staff_text') */ ?></option>
                                                                    <option value="client"><?php /*echo lang('option_client_text') */ ?></option>-->

                                                                    <?php if ($group_list) { ?>
                                                                        <?php foreach ($group_list as $a_group) { ?>

                                                                            <!--do not show group members-->
                                                                            <?php if ($a_group->id != 2) { ?>
                                                                                <option value="<?php echo $a_group->id ?>">
                                                                                    <?php if($a_group->name == 'employee'){
                                                                                    echo   'Thrifter';
                                                                                    }else if($a_group->name == 'employer'){
                                                                                    echo   'Organization';
                                                                                    }else{
                                                                                    echo  ucfirst(str_replace('_',' ',$a_group->name));
                                                                                    } ?>
                                                                                </option>
                                                                            <?php } ?>

                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr id="filter_col4" data-column="4">
                                                            <td align="center"><label
                                                                        for=""><?php echo lang('column_status_text') ?></label>
                                                            </td>
                                                            <td align="center">
                                                                <input class="column_filter form-control"
                                                                       id="col4_filter" type="hidden">
                                                                <select id="custom_status_filter" class="form-control">
                                                                    <option value="all"><?php echo lang('option_all_text') ?></option>
                                                                    <option value="yes"><?php echo lang('option_active_text') ?></option>
                                                                    <option value="no"><?php echo lang('option_inactive_text') ?></option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        </tbody>

                                                    </table>
                                                </div>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <table id="user-table"
                                                       class="table table-bordered table-hover table-responsive ">
                                                    <thead>
                                                    <tr>
                                                        <th><?php echo lang('column_firstname_text') ?></th>
                                                        <th><?php echo lang('column_lastname_text') ?></th>
                                                        <th><?php echo lang('column_email_text') ?></th>
                                                        <th><?php echo lang('column_group_text') ?></th>
                                                        <th><?php echo lang('column_status_text') ?></th>
                                                        <th><?php echo lang('column_created_on_text') ?></th>
                                                        <th><?php echo lang('column_last_login_text') ?></th>
                                                        <th><?php echo lang('column_actions_text') ?></th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                    <!-- /.col -->

                            <!-- /.content -->
                            <div class="clearfix"></div>
                        </div>
                    </div><!-- end col -->
                </div><!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->



<!-- <script>
    $(function () {
        $(document).tooltip();
    })
</script> -->

<!--clearing the extra arrow-->
<style>
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: unset;
    }
</style>

<!--this css style is holding datatable inside the box-->
<style>

    #user-table {
        table-layout: fixed;
        width: 100% !important;
    }

    #user-table td,
    #user-table th {
        width: auto !important;
        white-space: normal;
        text-overflow: ellipsis;
        overflow: hidden;
    }
</style>

<script>
    $(document).ready(function () {

        var loading_image_src = '<?php echo base_url() ?>' + 'base_demo_images/loading.gif';
        var loading_image = '<img src="' + loading_image_src + ' ">';
        var loading_span = '<span><i class="fa fa-refresh fa-spin fa-4x" aria-hidden="true"></i></span> ';
        var loading_text = "<div style='font-size:larger' ><?php echo lang('loading_text')?></div>";


        $('#user-table').DataTable({

            processing: true,
            serverSide: true,
            paging: true,
            pagingType: "full_numbers",
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            searchDelay: 3000,
            infoEmpty: '<?php echo lang("no_user_found_text")?>',
            zeroRecords: '<?php echo lang("no_matching_user_found_text")?>',
            language: {
                processing: loading_image + '<br>' + loading_text
            },

            columns: [
                {data: "first_name"},
                {data: "last_name"},
                {data: "email"},
                {data: "groups"},
                {
                    data: {
                        _: "act.html",
                        sort: "act.int"
                    }
                },
                {
                    data: {
                        _: "cr_on.display",
                        sort: "cr_on.timestamp"
                    }
                },
                {
                    data: {
                        _: "lt_lg_in.display",
                        sort: "lt_lg_in.timestamp"
                    }
                },
                {data: "action"}

            ],

            columnDefs: [

                {
                    'targets': 0,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },
                {
                    'targets': 1,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },

                {
                    'targets': 2,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },

                {orderable: false, targets: [3, 7]} /*, { visible: false, targets: [3,5,6] }*/
            ],

            aaSorting: [[5, 'desc']],

            ajax: {
                url: "<?php echo base_url() . 'users/auth/get_users_by_ajax' ?>",                   // json datasource
                type: "post",
                complete: function (res) {
                    getConfirm();
                }

                //open succes only for test purpuses . remember when success is uncommented datble doesn't diplay data
                /*success: function (res) {

                 console.log(res.last_query);
                 console.log(res.common_filter_value);
                 console.log(res.specific_filters);
                 console.log(res.order_column);
                 console.log(res.order_by);
                 console.log(res.limit_start);
                 console.log(res.limit_length);
                 }*/
            }

        });
    });
</script>


<script>
    /*column toggle*/
    $(function () {

        var table = $('#user-table').DataTable();

        $('a.toggle-vis').on('click', function (e) {
            e.preventDefault();

            // Get the column API object
            var column = table.column($(this).attr('data-column'));

            // Toggle the visibility
            column.visible(!column.visible());
        });

    });
</script>

<script>
    /*input searches*/
    $(document).ready(function () {
        //customized delay_func starts
        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();
        //customized delay_func ends

        $('input.column_filter').on('keyup', function () {
            var var_this = $(this);
            delay(function () {
                filterColumn($(var_this).parents('tr').attr('data-column'));
            }, 3000);
        });
    });
</script>

<script>
    function filterColumn(i) {

        $('#user-table').DataTable().column(i).search(
            $('#col' + i + '_filter').val(),
            $('#col' + i + '_regex').prop('checked'),
            $('#col' + i + '_smart').prop('checked')
        ).draw();
    }
</script>

<script>
    /*cutom select searches through input searches*/
    $(function () {

        /*-----------------------------*/
        $('#custom_group_filter').on('change', function () {

            $('#col3_filter').val($('#custom_group_filter').val());
            filterColumn(3);

        });
        /*-----------------------------*/
        /*-----------------------------*/
        $('#custom_status_filter').on('change', function () {

            if ($('#custom_status_filter').val() == 'all') {
                $('#col4_filter').val('');
                filterColumn(4);
            } else {
                $('#col4_filter').val($('#custom_status_filter').val());
                filterColumn(4);
            }

        });
        /*-----------------------------*/
    })
</script>

<script>
    function getConfirm() {
        $('.confirmation').click(function (e) {

            e.preventDefault();

            var href = $(this).attr('href');

            swal({
                    title: "<?= lang('swal_title_text')?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= lang('swal_confirm_button_text')?>",
                    cancelButtonText: "<?= lang('swal_cancel_button_text')?>",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = href;
                    }
                });

            return false;
        });

        $('.password_confirmation').click(function (e) {

            e.preventDefault();

            var href = $(this).attr('href');

            swal({
                    title: "<?= lang('swal_password_title_text')?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= lang('swal_password_confirm_button_text')?>",
                    cancelButtonText: "<?= lang('swal_password_cancel_button_text')?>",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = href;
                    }
                });

            return false;
        });
    }

</script>


