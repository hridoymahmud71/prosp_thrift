<base href="<?php echo base_url();?>">
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        <!-- App Favicon -->
        <link rel="shortcut icon" href="assets/custom_asset/prosperis_favicon.ico">
        <!-- App title -->
        <title><?php echo lang('forgot_password_text');?></title>
        <!-- Bootstrap CSS -->
        <link href="assets/backend_assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- App CSS -->
        <link href="assets/backend_assets/css/style.css" rel="stylesheet" type="text/css" />
        <!-- Modernizr js -->
        <script src="assets/backend_assets/js/modernizr.min.js"></script>

        <link rel="stylesheet" href="<?php echo base_url().'style_module/load_style/thrift' ?>">
    </head>
    <body>
        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class="account-bg">
                <div class="card-box mb-0">
                    <div class="text-center m-t-20">
                        <a href="index.html" class="logo">
                            <!-- <img style="max-width: 10%;" src="<?php echo $this->config->item('pg_upload_source_path').'image/' . $site_logo;?>"> -->
                        </a>
                    </div>
                    <div class="m-t-10 p-20">
                        <div class="row">
                            <div class="col-12 text-center">
                                <h6 class="text-muted text-uppercase mb-0 m-t-0"><?php echo lang('send_verification_heading_text') ?></h6>
                                <p class="text-muted m-b-0 font-13 m-t-20"><?php echo lang('send_verification_subheading_text') ?></p>
                            </div>
                        </div>
                        <?php if ($this->session->flashdata('success')) { ?>

                            <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <?= $this->session->flashdata('verification_email_send_success_text') ? lang('verification_email_send_success_text').'<br>':'' ?>
                            </div>
                        <?php } ?>

                        <?php if ($this->session->flashdata('error')) { ?>

                            <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <?= $this->session->flashdata('validation_errors') ? $this->session->flashdata('validation_errors'):'' ?>
                                <?= $this->session->flashdata('user_email_not_found_text') ? lang('user_email_not_found_text'):'' ?>
                            </div>
                        <?php } ?>
                        <form class="m-t-30" action="<?php echo base_url();?>users/auth/do_send_verification_email_again" method="post">
                            <div class="form-group row">
                                <div class="col-12">
                                    <input class="form-control" type="email" name="email" required placeholder="<?php echo lang('placeholder_email_text') ?>">
                                </div>
                            </div>
                            <div class="form-group row text-center m-t-20 mb-0">
                                <div class="col-12">
                                    <button class="btn btn-success btn-block waves-effect waves-light" type="submit"><?php echo lang('submit_btn_text') ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end card-box-->
            <div class="m-t-20">
                <div class="text-center">
                    <p class="text-white"><a href="users/auth/login" class="text-white m-l-5"><b><?php echo lang('login_text');?></b></p>
                    <p class="text-white"><a href="users/auth/thrifter_registration" class="text-white m-l-5"><b><?php echo lang('register_text');?></b></p>
                </div>
            </div>
        </div>
        <!-- end wrapper page -->
        <script>
            var resizefunc = [];
        </script>
        <!-- jQuery  -->
        <script src="assets/backend_assets/js/jquery.min.js"></script>
        <script src="assets/backend_assets/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="assets/backend_assets/js/bootstrap.min.js"></script>
        <script src="assets/backend_assets/js/detect.js"></script>
        <script src="assets/backend_assets/js/fastclick.js"></script>
        <script src="assets/backend_assets/js/jquery.blockUI.js"></script>
        <script src="assets/backend_assets/js/waves.js"></script>
        <script src="assets/backend_assets/js/jquery.nicescroll.js"></script>
        <script src="assets/backend_assets/js/jquery.scrollTo.min.js"></script>
        <script src="assets/backend_assets/js/jquery.slimscroll.js"></script>
        <script src="assets/backend_assets/plugins/switchery/switchery.min.js"></script>
        <!-- App js -->
        <script src="assets/backend_assets/js/jquery.core.js"></script>
        <script src="assets/backend_assets/js/jquery.app.js"></script>


    </body>
</html>